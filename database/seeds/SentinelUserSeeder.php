<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SentinelUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        Sentinel::registerAndActivate([
            'email'    => 'a@tutanc.org',
            'password' => '123123',
            'first_name' => 'Admin',
            'last_name' => 'TUTA',
            'mobile' => '9999999999'
        ]);
        Sentinel::registerAndActivate([
            'email'    => 'u@tutanc.org',
            'password' => '1231231',
            'first_name' => 'User',
            'last_name' => 'TUTA',
            'mobile' => '9999999999'
        ]);


        $this->command->info('Users seeded!');

    }
}
