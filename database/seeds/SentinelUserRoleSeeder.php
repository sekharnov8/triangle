<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SentinelUserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_users')->truncate();

        $aUser = Sentinel::findByCredentials(['login' => 'a@tutanc.org']);
        $uUser = Sentinel::findByCredentials(['login' => 'u@tutanc.org']);

        $aRole = Sentinel::findRoleByName('Admin');
        $uRole = Sentinel::findRoleByName('Users');

        // Assign the roles to the users
        $aRole->users()->attach($aUser);
        $uRole->users()->attach($uUser);

        $this->command->info('Users assigned to roles seeded!');
    }
}
