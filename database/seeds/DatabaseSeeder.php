<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SentinelRoleSeeder::class);
        $this->call(SentinelUserSeeder::class);
        $this->call(SentinelUserRoleSeeder::class);

        $this->command->info('All tables seeded!');
    }
}
