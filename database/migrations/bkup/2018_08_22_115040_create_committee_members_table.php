<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommitteeMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('committeemembers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('committee_id');
            $table->integer('user_id');
            $table->integer('role_id');
            $table->integer('committeecategory_id');
            $table->integer('display_order')->nullable();
            $table->string('designation')->nullable();
            $table->timestamps();
            $table->foreign('committee_id')->references('id')->on('committees')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committeemembers');
    }
}
