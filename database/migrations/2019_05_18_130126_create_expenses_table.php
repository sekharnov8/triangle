<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('event_id')->nullable();
            $table->double('amount')->nullable();
            $table->string('receipts')->nullable();
            $table->date('spent_date')->nullable();
            $table->date('paid_date')->nullable();
            $table->integer('treasurer_id')->nullable();
            $table->double('paid_amount')->nullable();
            $table->text('sender_comments')->nullable();
            $table->text('treasurer_comments')->nullable();
            $table->string('payment_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
