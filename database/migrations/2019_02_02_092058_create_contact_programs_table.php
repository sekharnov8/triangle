<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('img1')->nullable();
            $table->string('img2')->nullable();
            $table->string('url')->nullable();
            $table->string('target')->nullable();
            $table->string('class')->nullable();
            $table->date('expire_date')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('display_order')->nullable();
            $table->integer('display_type')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_programs');
    }
}
