<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('photocategory_id');
            $table->string('image_url');
            $table->text('description');
            $table->boolean('default_image')->default(false);
            $table->integer('display_order')->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('is_home')->default(false);
            $table->text('album_link')->nullable();
            $table->string('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('photos');
    }
}
