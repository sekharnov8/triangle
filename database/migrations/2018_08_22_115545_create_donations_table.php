<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('paymentmethod_id');
            $table->integer('paymen_status_id');
            $table->text('donation_cause')->nullable();
            $table->text('transaction_id')->nullable();
            $table->double('amount',10,2)->nullable();
            $table->dateTime('orderdate')->nullable();
            $table->string('card_number')->nullable();
            $table->string('csv_expiry')->nullable();
            $table->text('payment_by')->nullable();
            $table->string('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
