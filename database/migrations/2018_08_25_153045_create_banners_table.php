<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bannertype')->nullable();
            $table->string('name')->nullable();
            $table->string('image');
            $table->string('redirect_url')->nullable();
            $table->string('target')->nullable();
            $table->date('expire_date')->nullable();
            $table->boolean('status')->default(true);
            $table->integer('order_no')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
