<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('permissions')->nullable();
            $table->datetime('last_login')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('gothram')->nullable();
            $table->string('maternal_gothram')->nullable();
            $table->string('home_phone')->nullable();
            $table->integer('membershiptype_id')->nullable();
            $table->string('fax')->nullable();
            $table->string('occupation')->nullable();
            $table->string('age')->nullable();
            $table->string('mobile')->nullable();
            $table->string('employer')->nullable();
            $table->string('skills')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('primary_member_id')->nullable();
            $table->string('referred_by')->nullable();
            $table->integer('membershiptype_id');
            $table->text('terms_conditions')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('designation')->nullable();
            $table->tinyInteger('is_approved')->nullable();
            $table->tinyInteger('is_lockedout')->nullable();
            $table->tinyInteger('is_activated')->nullable();
            $table->datetime('date_activated')->nullable();
            $table->datetime('last_password_change')->nullable();
            $table->string('member_id')->nullable();
            $table->integer('failed_pwd_count')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
