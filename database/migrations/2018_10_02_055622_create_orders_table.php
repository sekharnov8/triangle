<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('membershiptype_id');
            $table->integer('paymentstatus');
            $table->integer('paymentmethod_id')->nullable();
            $table->float('amount',10,2);
            $table->string('transaction_id')->nullable();
            $table->string('payment_by')->nullable();
            $table->text('admin_comments')->nullable();
            $table->string('bankname')->nullable();
            $table->string('cheque_no')->nullable();
            $table->string('cheque_date')->nullable();
            $table->text('user_comments')->nullable();
            $table->date('order_date')->nullable();
            $table->date('expiry_date')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
