<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sitename');
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('president_email')->nullable();
            $table->string('president_phone')->nullable();
            $table->string('secretary_email')->nullable();
            $table->string('secretary_phone')->nullable();
            $table->string('support_email')->nullable();
            $table->string('enquiry_email')->nullable();
            $table->string('customercare_number')->nullable();
            $table->string('tollfree_number')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('youtube_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->string('gplus_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appinfo');
    }
}
