<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagecategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('pagelevel')->nullable();
            $table->integer('page_parentid')->nullable();
            $table->string('id_path')->nullable();
            $table->integer('position')->nullable();
            $table->boolean('is_topbar')->default(false);
            $table->boolean('is_menubar')->default(false);
            $table->boolean('is_footerlinks')->default(false);
            $table->boolean('status')->default(true);
            $table->string('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('pagecategories');
    }
}
