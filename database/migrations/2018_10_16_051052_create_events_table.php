<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('user_id');
            $table->string('title');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();
            $table->date('registration_start_date')->nullable();
            $table->date('registration_end_date')->nullable();
            $table->text('banner_url')->nullable();
            $table->text('location');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode')->nullable();
            $table->text('event_details')->nullable();
            $table->text('contact_mail')->nullable();
            $table->boolean('status')->default(true);
            $table->string('page_title')->nullable();
            $table->longText('mata_description')->nullable();
            $table->text('mata_keyword')->nullable();
            $table->integer('is_paid')->nullable();
            $table->integer('is_registration')->nullable();
            $table->integer('is_homedisplay')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
