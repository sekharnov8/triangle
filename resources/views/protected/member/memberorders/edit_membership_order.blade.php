{!! Form::model($morder, array('url' => ['member-orders', $morder->id], 'method' => 'POST',  'files' => true,'id' => 'dataform')) !!}
     <h3>Membership Order Details</h3>
    <div class="row">
 <div class="col-md-6"  >
            Member Name: {{$morder->user->first_name}}  {{$morder->user->last_name}}
        </div>
        <div class="col-md-6"  >
            Payment Method: @if($morder->paymentmethod){{$morder->paymentmethod->name}}@endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"  >
            Transaction Id: {{$morder->transaction_id}}
        </div>
        <div class="col-md-6"  >
            Membershihp Type: {{$morder->membershiptype->name}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"  >
            <label  class="control-label">Amount</label>
            {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
        </div>
        <div class="col-md-6"  >
            <label  class="control-label">Donation Amount</label>
            {!! Form::text('donation_amount', null, ['class' => 'form-control', 'placeholder' => 'Donation Amount']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"  >
            <label  class="control-label">Bank Name </label>
            {!! Form::text('bankname', null, ['class' => 'form-control', 'placeholder' => 'Bank Name']) !!}
        </div>

        <div class="col-md-6"  >
            <label  class="control-label">Check No</label>
            {!! Form::text('cheque_no', null, ['class' => 'form-control', 'placeholder' => 'Check No']) !!}
        </div> 
    </div>
      <div class="row">
        <div class="col-md-6"  >
            <label  class="control-label">Check Date </label>
            {!! Form::text('cheque_date', null, ['class' => 'form-control', 'placeholder' => 'Check Date']) !!}
        </div>

        <div class="col-md-6"  >

            <label for="status" class="control-label">Status</label>
            {!! Form::select('status', [1=>'Active', 0 =>'Pending'],null, ['class' => 'form-control','id' =>'status']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"  >
            Order Created on: {{$morder->created_at}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 clearfix" align="right"><br/>
             {!! Form::submit('Update', ['class' => 'btn btn-warning ']) !!}
</div>
    </div>
<div class="col-md-12 m-t10 t-r">
</div>
  
{!! Form::close() !!}