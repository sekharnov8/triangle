<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>S.no</th>
        <th>Name</th>
        <th>Email</th>
        <th>Membership</th>
        <th>Payment Method</th>
        <th>Payment Status</th>
        <th>Transaction Id</th>
        <th>Amount</th>
        <th>Donation Amount</th>
        <th>Bank Name</th>
        <th>Check Number</th>
        <th>Check Date</th>
        <th>Updated on</th>
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach($membership_orders as $data)
    <tr>
        <td>{{ $data->id }} </td>
        <td>{{ $data->user_name }}</td>
        <td>{{ $data->email }}</td>
        <td>{{ $data->membership }}</td>
        <td>{{ $data->payment_method }}</td>
        <td>
            @if($data->paymentstatus_id ==1)
                Completed
            @else
            @if($data->amount>0)
                Pending
            @endif
            @endif
        </td>
        <td>{{ $data->transaction_id }}</td>
        <td>{{ $data->amount }}</td>
        <td>{{ $data->donation_amount }}</td>
        <td>{{ $data->bankname }}</td>
        <td>{{ $data->cheque_no }}</td>
        <td>{{ $data->cheque_date }}</td>
        <td>{{ dateformat($data->updated_at) }}</td>
        <td>{{ dateformat($data->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>