@extends('layouts.user.user')

@section('title', 'Profile')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.css')}}">
   <link rel="stylesheet" type="text/css"
          href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
@endsection
@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Send Mail</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article   class="clearfix t-b-m35 white-bg p35 l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p15 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0"
                                        style="min-height:555px;" id="negative" >

                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular">Send Mail </article>
                                    @include('layouts.user.includes.notifications')

                                    @if(count($events)==0)
                                    <div class="notifications alert-dangger error alert-block b-m10">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                        No events to send Notifications.
                                    </div>
                                    @endif
                                    
                                    <section id="s" class="clearfix tab_content">
                                        {!! Form::open(array('url' => 'send-event-notification', 'method' => 'POST','id' => 'dataform')) !!}
 
 <div class="row eventlist" >
  <label for="image" class="control-label col-md-2">Event</label>
    <div class="col-md-10"  >
        {!! Form::select('event', [''=>'Select Event']+$events, '' , ['class' => 'form-control', 'id' => 'event']) !!}
     </div>
 </div>   
<div class="row">
    <label for="image" class="control-label col-md-2">Subject *</label>

<div class="col-md-10"  >
    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject', 'required']) !!}
</div>
</div>
  
    <div class="row">

 
  <label for="image" class="control-label col-md-2">To Email *</label>

    <div class="col-md-10"  >
        {!! Form::select('members[]', [], '' , ['class' => 'form-control user_id', 'id' => 'members', 'multiple' => 'multiple', 'required']) !!}
        {!! Form::text('member_ids', null, ['class' => 'form-control user_id2', 'style' => 'display:none']) !!}
     
     </div>
    
 </div>
 <div class="row selectall" ><div class="col-md-12"  > 
<label class="font15 black-t3 checkboxstyle b-p3 is_reg_emails">
                                        <input   name="memberscb" id="memberscb" onClick="check2();" value="1" type="checkbox">
                                        <span for="checkbox" class="dis-in"><span
                                                    class="font14 OpenSans mobile-font14 v-align-m l-h20 l-p5">Select All registered members</span></span></label>                                                    
         </div>
 </div>
 <div class="row eventmail">
 <div class="col-md-12"  > 
<label class="font15 black-t3 checkboxstyle b-p3 is_reg_emails">
        <input   name="event_mail"  value="1" type="checkbox">  <span for="checkbox" class="dis-in"><span
                                                    class="font14 OpenSans mobile-font14 v-align-m l-h20 l-p5">Send Event Webinar info in notification</span></span></label></div>
 </div>
<div class="row ">
<div class="col-md-12"  >
     {!! Form::textarea('description', null, ['class' => 'form-control textarea', 'placeholder' => 'Template']) !!}
</div> 
 
                                                   
<div class="col-md-12 m-t10 t-r">
     {!! Form::submit('Send Email', ['class' => 'btn btn-info',  'name' => 'action']) !!}
     {!! Form::submit('Send Notification', ['class' => 'btn btn-info',  'name' => 'action']) !!}
     {!! Form::submit('Send Email and Notification', ['class' => 'btn btn-info' ,  'name' => 'action']) !!}
</div>
</div>
{!! Form::close() !!}

                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>
@stop
@section('scripts')
 <script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/select2/select2.full.min.js')}}"></script>

<script>
CKEDITOR.replace( 'description', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
        $(document).ready(function () {
          $('.selectall').hide();
          $('.eventmail').hide();

           $(".user_id > option").removeAttr("selected");
             $(".user_id").trigger("change");
             $("#memberscb").prop("checked", false);

              $(".user_id").select2({
                 allowClear: true
            });
 
       $("#dataform").validate({
                rules: { 
                 send_mail: {
                  required: function(element) {
                    if(!$('#login_alert').is(':checked'))
                    return true;
                    else
                      return false;
                  },
                 },
                 login_alert: {
                  required: function(element) {
                    if(!$('#send_mail').is(':checked'))
                    return true;
                   else
                      return false;
                  },
                 },
                 sendtype:'required',
                subject: 'required'
                  },
                messages:
                {
                     membertype:{required:"Select Type"},
                     send_mail:{required:"Need Select atleast one checkbox"},
                     login_alert:{required:"Need Select atleast one checkbox"},
                     sendtype:{required:"Select send type"},
                     subject:{required:"Required"}
                }
            });
 
 

            $(document).on('change', '#event', function(e){
            var $this=$(this);
            var event_id = $this.val();
            $.ajax({
                url:  '{{ url('get-users-list-event') }}',
                data: {"_token": "{{ csrf_token() }}",'type':'event','event_id':event_id },
                success: function(data) {
                    if(data.length==0)
                    {
//                           $('.category').hide();
                        $('.user_id').empty();
                        $('.user_id').append('<option value="" disabled>No Users</option>');
                        $(".selectall").hide();
                        $(".eventmail").hide();

                    }else{
                        $('.user_id').empty();
                        $.each( data, function( key, value ) {
                            $('.user_id').append("<option value='"+key+"'>"+ value + "</option>");
                        });
                        $('.selectall').show();
                        $('.eventmail').show();
                        $(".user_id").show();
                        $(".user_id2").hide();
                        $(".select2").show();
                    }
                },
                type: 'POST'
            });
        });

 });

function check2(){

            if($("#memberscb").prop('checked') ){
                $(".user_id > option").prop("selected","selected");
                $(".user_id").trigger("change"); 
                $(".user_id2").show();
                $(".user_id2").val($('.user_id').children('option').length + ' members');
                $(".user_id").hide();
                $(".select2").hide();

            }else{
                $(".user_id").show();
                $(".user_id2").hide();
                $(".select2").show();
                $(".user_id > option").removeAttr("selected");
                $(".user_id").trigger("change");
            }
 }         
</script>
@stop