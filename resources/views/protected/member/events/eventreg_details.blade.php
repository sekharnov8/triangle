 <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Event Registration Details</h4>
 <article class="clearfix part-row b-p5">

 @if($eventreg->status!=1 && $eventreg->amount_paid>0)
  <p><a href="{{ url('event/'.$eventreg->event->slug.'/'.$eventreg->id.'/registration')}}" class=" l-r-p20 Montserrat-Medium font18 border-radius25 white-t t-t-u" style="background-color:#ff9f10; color:#ffffff; padding-bottom:11px; padding-top:11px; float:right">Pay Amount</a>
  </p>
 @endif
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Event Name</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($eventreg->event){{$eventreg->event->name}}@endif</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                          <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Event Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{date("M d, Y", strtotime($eventreg->event->start_date))}}  @if(date("g:i A", strtotime($eventreg->event->start_date))!='00:00 AM') {{'@ '.date("g:i A", strtotime($eventreg->event->start_date)) }} - {{date("g:i A", strtotime($eventreg->event->end_date))}} EST @endif</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Amount</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> ${{$eventreg->amount_paid}}@if($eventreg->amount_paid=='')0 @endif </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Transaction Id </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventreg->transaction_id}}</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Status </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($eventreg->status==1)Confirmed @else Pending @endif</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Method</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($eventreg->paymentmethod){{$eventreg->paymentmethod->name}}@endif</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Order Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{dateformat($eventreg->created_at)}}</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">First Name </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventreg->first_name}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">LastName</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventreg->last_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Email </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventreg->email}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Mobile</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventreg->mobile}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">City </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventreg->city}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">State</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventreg->state}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Address 1 </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventreg->address1}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Address 2</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventreg->address2}} </article>
                                                                </article>
                                                            </article></article>

                                                    </article>

                                                     <table style="background-color: #fff;" class="table2 Poppins-Regular font14 dkblack-t3 b-m0 tabletext-l" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr class="poppinssemibold" style="background-color: #fdc633; text-align:center!important;">
                            <th style="padding-left: 20px; vertical-align:middle!important;" width="30%">Registration Category</th>
                            <th style="vertical-align:middle!important;" width="20%">Suggested Donation</th>
                            <th style="vertical-align:middle!important;" width="20%">Count </th>
                            <th style="vertical-align:middle!important;" width="13%">Donation Amount</th>
                        </tr>@php $total=0; @endphp
                        @foreach($eventreg->registrants as $regdata)


                            <tr class="">
                                <td style="text-align:center;" class="poppinssemibold">{{getEventRegtype($regdata->eventregistrationtype_id)}} </td>

                                <td class="poppinssemibold" style="text-align:center;"> ${{getEventRegtypeAmount($regdata->eventregistrationtype_id)}}</td>

                                <td  style="text-align:center;">
                                 {{$regdata->count}}
                                 @if(getEventRegtype($regdata->eventregistrationtype_id)=='Family')
                                <br/>Adults: {{$regdata->family_adults}}, Kids: {{$regdata->family_kids}}
                                 
                                @endif
                                 </td>

                                <td style="text-align:center;" class="poppinssemibold">@php $regtotal= $regdata->amount;
                                $total=$total+$regtotal; @endphp
                            ${{numberformat($regtotal)}}        </td>
                            </tr>

                            @endforeach

                            @if($eventreg->sponsorship_id)
                              <tr class="">
                                <td style="text-align:center;" class="poppinssemibold">Sponsorship - {{get_sponsorship_name($eventreg->sponsorship_id)}} </td>

                                <td class="poppinssemibold" style="text-align:center;"></td>
                                <td  class="poppinssemibold" style="text-align:center;"></td>
                                <td style="text-align:center;" class="poppinssemibold">
                                   ${{numberformat($eventreg->sponsorship_amount)}}
                                   @php $total=$total+$eventreg->sponsorship_amount; @endphp
                                </td>
                            </tr>
                            @endif
                             <tr class="">
                                <td style="text-align:center;" class="poppinssemibold">Additional Donation </td>

                                <td class="poppinssemibold" style="text-align:center;"></td>
                                <td  class="poppinssemibold" style="text-align:center;">
                                    
                                </td>
                                <td style="text-align:center;" class="poppinssemibold">
                                   ${{numberformat($eventreg->donation_amount)}}
                                   @php $total=$total+$eventreg->donation_amount; @endphp
                                </td>
                            </tr>

                             <tr>
<td colspan="3" style="text-align:center;">Total: </td>
<td style="text-align:center;" class="poppinssemibold">${{numberformat($total)}}</td>
                            </tr>
                        </tbody></table>