@extends('layouts.user.user')

@section('title', 'Profile')

@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Dashboard</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')


                        <article class="clearfix right-block9" id="rb9">

                            <article class="clearfix tab_container">

                                <article class="user-fullblock clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    @include('layouts.user.includes.notifications')
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular"
                                             rel="e"><a class="black-t">Events</a></article>
                                    <section id="e" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0  l-blue-t b-p15">
                                                Member Events - Committee</h4>

                                            <ul class="clearfix list-pn list-f m0 featured-list2">
                                                @foreach($events as $key=>$event)
                                                    <li class="event_{{$event->id}}">
                                                        <article class="clearfix @if($key%2==0)  r-m35 @else  l-m35  @endif tabhorizontal-r-m0">
                                                            <article class="clearfix eventsli-bg border dkgrayborder1">
                                                                <article class="clearfix t-b-p5 l-p15">
                                                                    <h2 class=" Montserrat-Regular font16 gray-t t-b-m0 mobile-l-h18">
                                                                      <a href="{{url('event/'.$event->slug)}}">  {{$event->name}}</a></h2>
                                                                </article>
                                                            <span class="dis-b"><img
                                                                        src="{{url('images/heading-border1.png')}}" width="172"
                                                                        height="1"/></span>
                                                                <article class="clearfix l-r-p25 t-b-p15">
                                                                    @if($event->banner_url  && File::exists('uploads/events/'.$event->banner_url))
                                                                    <img src="{{url('uploads/events/'.$event->banner_url)}}" width="315" height="230" alt="" class="mobileimgwidth"/>
                                                                    @else<img src="{{url('images/event-no-image.png')}}" width="315" height="230" >
                                                                    @endif
                                                                </article>
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-9">
                                                                        <article
                                                                                class="clearfix r-m10 t-b-p15 l-r-p15 low-bg mobile-t-b-p10">
                                                                            <h4 class="clearfix Montserrat-Regular font16 t-b-m0 dk-t1 l-h30 mobile-font14 mobile-l-h20">
                                                                                {{date("M d, Y", strtotime($event->start_date))}}  @if(date("g:i A", strtotime($event->start_date))!='00:00 AM') {{'@ '.date("g:i A", strtotime($event->start_date)) }} - {{date("g:i A", strtotime($event->end_date))}} EST @endif</h4>
                                                                        </article>
                                                                    </article>
                                                                    <article class="clearfix box-3">
                                                                        <article
                                                                                class="clearfix t-b-p20 l-r-p15 orange-bg dis-b t-c mobile-t-b-p10">
                                                                            <i class="loc-i">&nbsp;</i></article>
                                                                    </article>
                                                                </article>
                                                                <article class="clearfix t-b-p15 t-c l-r-p35">
                                                                    <article class="clearfix part-row">
                                                                        <article class="clearfix part-4">
                                                                            <article
                                                                                    class="clearfix r-m35 mobile-r-m0 mobile-b-m15">
                                                                                <a href="{{ URL::route('memberevents.edit',array('id'=>$event->id))}}" class="edit-btn Montserrat-Regular font11 t-t-u dk-t1">Edit</a>
                                                                            </article>
                                                                        </article>
                                                                        <article class="clearfix part-4">
                                                                            <article
                                                                                    class="clearfix r-m10 mobile-r-m0 mobile-b-m15">
                                                                                <a class="delete-btn Montserrat-Regular font11 t-t-u dk-t1" data-id="{{$event->id}}">Delete</a>
                                                                            </article>
                                                                        </article>
                                                                        <article class="clearfix part-4">
                                                                            <article class="clearfix l-m10 mobile-l-m0">
                                                                                <!--<a class="active-btn Montserrat-Regular font11 t-t-u dk-t1">Active</a>-->
                                                                                <article class="onoffswitch">
                                                                                    <input type="checkbox"
                                                                                           name="onoffswitch"
                                                                                           class="onoffswitch-checkbox"
                                                                                           id="myonoffswitch{{$event->id}}" data-id="{{$event->id}}" data-status="{{$event->status}}" @if($event->status==1) checked @endif >
                                                                                    <label class="onoffswitch-label"
                                                                                           for="myonoffswitch{{$event->id}}">
                                                                                        <span class="onoffswitch-inner"></span>
                                                                                        <span class="onoffswitch-switch"></span>
                                                                                    </label>
                                                                                </article>
                                                                            </article>
                                                                        </article>
                                                                        @if($event->is_registration)
                                                                         <article class="clearfix part-4">
                                                                            <article
                                                                                    class="clearfix r-m35 mobile-r-m0 mobile-b-m15">
                                                                                <a href="{{ URL::route('event_registrations',array('slug'=>$event->slug))}}" class="edit-btn Montserrat-Regular font11 t-t-u dk-t1">Registrations</a>
                                                                            </article>
                                                                        </article>
                                                                        @endif
                                                                    </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </li>
                                                @endforeach



                                            </ul>
                                            <div class=" f-r"> 
                                                        {{ $events->links() }}
                                                </div>
                                                @if(count($events)==0)
                                                <p class="norecords">No Events </p>
                                                @endif
                                                    </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
    <script>
   $(document).ready(function () {
   $(document).on('click','.delete-btn',function(){
        var id = $(this).data('id');

         swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function (isConfirm) {
                             if (isConfirm) {

                          $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('/memberevents/') }}' + '/' + id,
                                type: 'DELETE',
                                success: function(data){
                                    if(data=='success')
                                    {
                                     swal("Deleted!", "Event Deleted!", "success");
                                     $('.event_'+id).remove();
                                     }
                                     else
                                     {
                                     swal("Error!", "You cannot acccess this event to delete!", "error");
                                     }
                                }
       }); }
     });
         });
              $(document).on('click','.onoffswitch-checkbox',function(){
              var id = $(this).data('id');
             var status = $(this).data('status');
        $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('change-userevent-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                     swal("Changed!", data, "success");
                                }
       });
    });
     });
</script>
@stop