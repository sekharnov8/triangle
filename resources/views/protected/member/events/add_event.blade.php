@extends('layouts.user.user')

@section('title', 'Profile')

@section('css')
 <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />
 <link rel="stylesheet" href="{{asset('plugins/timepicker/jquery.ptTimeSelect.css')}}">
@stop

@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Dashboard</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="user-fullblock clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular"
                                             rel="e"><a class="black-t">Add Event</a></article>
                                    <section id="e" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                                Add Event</h4>
                                    @include('layouts.user.includes.notifications')

                                            <ul class="clearfix list-pn list-f m0 featured-list2">

                                                <article class="clearfix mobile-l-m10 mobile-t-m10">
                                                {!! Form::open(array('route' =>'memberevents.store', 'method' => 'POST', 'id'=>'dataform','files' => true)) !!}
                                                <article class="clearfix">
                                                     <article class="clearfix part-row b-p5">

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                {!! Form::text('name', null, ['class' => 'form-control  donate-form name gbl_title', 'placeholder'
                                                                => 'Event name *']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                {!! Form::text('slug', null, ['class' => 'form-control  donate-form slug', 'placeholder'
                                                                => 'Slug']) !!}
                                                            </article>
                                                        </article>
                                                    </article>
                                                     
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                {!! Form::text('contact_mail', null, ['class' => 'form-control donate-form', 'id'=>'email_reg',
                                                                'placeholder' => 'Event Contact Email *']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix dropdown"
                                                                     style="background:none;  border:none; padding:0 15px">
                                                                {!! Form::select('category_id', $category_list, null,['class' => 'form-control donate-form','placeholder' => 'Event Category *']) !!}
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                      <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                {!!Form::file('banner_url',['placeholder' => 'Image','id'=>'image', 'class' =>
                                                                'form-control donate-form image'])!!}

                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                {!! Form::text('start_date', null, ['class' => 'form-control datepicker datepickerstart donate-form','autocomplete'=>'off', 'placeholder'
                                                                => 'Event Date *']) !!}
                                                            </article>
                                                        </article> 
                                                    </article>
                                                     <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                {!! Form::text('start_time', null, ['class' => 'form-control start_time donate-form','autocomplete'=>'off', 'id'=>'timepicker', 'placeholder' => 'Start Time (EST)']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                 {!! Form::text('end_time', null, ['class' => 'form-control end_time donate-form','autocomplete'=>'off', 'id'=>'timepicker2', 'placeholder'  => 'End Time (EST)']) !!} 
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix dropdown  r-m15 "
                                                                     style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:2.2px 0px;">
                                                                {!! Form::select('status', ['1'=>'Active', '0'=>'Inactive'],null, ['class' =>
                                                                'form-control dropdown-select','style' => 'padding:0px 0px', 'id'
                                                                =>'title']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix  l-m15  dropdown" style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:2.2px 0px;">
                                                                 {!! Form::select('location_type', ['Online'=>'Online', 'Location'=>'Location'],null, ['class' =>
                                                                'form-control dropdown-select location_type','style' => 'padding:0px 0px', 'placeholder'=>'Select Location Type']) !!}
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <div class="event_location" style="display: none;">
                                                     <article class="clearfix part-row b-p5">
                                                       
                                                        <article class="clearfix part-12">
                                                            <article class="clearfix mobile-l-m0">
                                                                {!! Form::text('location', null, ['class' => 'form-control donate-form', 'placeholder'
                                                                => 'Location *']) !!}
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                {!! Form::text('city', null, ['class' => 'form-control donate-form', 'placeholder'
                                                                => 'City *']) !!}
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                                 <article class="clearfix l-m15 dropdown"
                                                                         style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:2.2px 0px;">
                                                                  <label class="l-gray-t font12 l-h10  b-m0"></label> 
                                                                     {!! Form::select('state', [''=>'Select state']+$states_data, null, ['class' => 'form-control dropdown-select','style' => 'padding:0px 0px', 'id' =>'state']) !!} 
                                                                </article>
                                                            </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                {!! Form::text('zipcode', null, ['class' => 'form-control donate-form zipcode',
                                                                'placeholder' => 'Postal / Zip Code']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    <article class="clearfix dropdown"
                                             style="background:none;   border:none; border-bottom:2px solid #e0e0e0; padding:2px 0px;">
                                        {!! Form::select('country', [''=>'Select Country']+$countries_data,'United States', ['class' => 'form-control dropdown-select','style' => 'padding:0px 0px', 'id' =>'country']) !!}
                                    </article>
                                </article>
                            </article>
                                                      
                                                    </article>
                            </div>

                                                    <h4 class="t-m0 OpenSans font20 sky-b-t b-p10  orangeborder t-p10 b-m10">Registration Details</h4>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-2">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label   class="l-gray-t font12 OpenSans b-m0">Registration</label>
                                                                <input type="checkbox" value="1" style="opacity: unset;" class="is_registration" name="is_registration" checked >
                                                            </article>
                                                        </article>
                                                        <div class="registration_date">
                                                        <article class="clearfix part-3">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                {!! Form::text('registration_start_date', null, ['class' => 'form-control datepicker datepickerstart2 donate-form','autocomplete'=>'off', 'placeholder'
                                                                => 'Registration Start Date']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-3">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                {!! Form::text('registration_end_date', null, ['class' => 'form-control datepicker datepickerend2 donate-form','autocomplete'=>'off', 'placeholder'
                                                                => 'Registration End Date']) !!}
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-3">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                {!! Form::text('total_registration_count', null, ['class' => 'form-control donate-form', 'autocomplete'=>'off', 'placeholder'=>'Total Registration count']) !!}
                                                            </article>
                                                        </article>
                                                         
                                                            </div>
                                                    </article>

                                                    <div class="registration_date">
                                                    <h4 class="t-m0 OpenSans font20 sky-b-t b-p10  orangeborder t-p10 b-m10">Registration Types  <a href="javascript:void(0)" class="addtype btn btn-sm btn-warning f-r">Add Type</a></h4>
                                                    <p>Note: Registration title - 'Family' will ask Adult/Kids count in event registration.</p>

                                                    
                                                    <div class="event_regtypedata"></div>

                                                     <h4 class="t-m0 OpenSans font20 sky-b-t b-p10  orangeborder t-p10 b-m10">Sponsorship Types <a href="javascript:void(0)" class="addsponsortype btn btn-sm btn-warning f-r">Add Type</a></h4>
 
                                                             

                                                             <div class="childinfo"></div>

                                                            <div class="event_sponsortypedata"></div>

                                                      </div>
   <article class="clearfix part-row t-p15  b-p5">
      <div  >
      
  {!! Form::text('amount_desc', null, ['class' => 'form-control donate-form', 'placeholder'
                                                                => 'Amount Description']) !!}
 

                                                        <article class="clearfix part-12">

<label   class="control-label">Event Description</label>
{!! Form::textarea('event_details', null, ['class' => 'form-control']) !!}

  </article>
 
                                                      </article>
                                                       <article class="clearfix part-row t-p15  b-p5">
                                                        <article class="clearfix part-12">

<label   class="control-label">Registration Response Email Body</label>
{!! Form::textarea('mail_description', null, ['class' => 'form-control']) !!}

  </article></article>

                                                    <article class="clearfix part-row t-p15  b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix t-p10 ">
                                                                <a href="{{url('user-events')}}"> <input value="Cancel" class="btn btn-danger  buttoneditprofile border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color: rgb(255, 176, 58);" type="button"></a>

                                                            </article>
                                                        </article>
                                                        <article class="clearfix  part-6">
                                                            <article class="clearfix pull-right t-p10 ">
                                                                <input value="Submit" class="btn btn-danger btn-block buttoneditprofile submitbtn border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color:#3e5066;width:170px;" type="submit">
                                                                <div class="loading"></div>
                                                            </article>
                                                        </article>
                                                    </article>
                                                </article>
                                                {!! Form::close() !!}
                                        </article>



                                            </ul>
                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
<script type="text/javascript" src="{{asset('plugins/timepicker/jquery.ptTimeSelect.js')}}"></script>

    <script>
        $(document).ready(function () { 
       	 
 
            $('#timepicker').ptTimeSelect({
     onClose: function(){
               var start_time = $(".start_time").val();
var end_time = $(".end_time").val(); 
if (start_time != "" && end_time!="") {
var date1 = $(".datepickerstart").val();
if(date1=='')    
date1='04/23/2020';
var aDate= new Date(Date.parse(date1+ ' '+start_time));
var bDate = new Date(Date.parse(date1+ ' '+end_time));
 if (aDate> bDate ){
    alert("End Time  Should Be Greater than Start time");
    $(".end_time").val('');
    return false;
}
 } }
 });
     $('#timepicker2').ptTimeSelect({
     onClose: function(){
               var start_time = $(".start_time").val();
var end_time = $(".end_time").val(); 
if (start_time != "" && end_time!="") {
var date1 = $(".datepickerstart").val();
if(date1=='')    
date1='04/23/2020';
var aDate= new Date(Date.parse(date1+ ' '+start_time));
var bDate = new Date(Date.parse(date1+ ' '+end_time));
}
 if (aDate>=bDate ){
    alert("End Time  Should Be Greater than Start time");
    $(".end_time").val('');
    return false;
} }
 });
 
$(document).on('change', '.datepickerstart2,.datepickerend2', function(e){
  var datestart = $(".datepickerstart2").val();
  var dateend = $(".datepickerend2").val();
   if (datestart != "" && dateend!="") {
  if (Date.parse(datestart) > Date.parse(dateend)) {
      alert("End Date Should Be Greater than Start Date");
      $(".datepickerend2").val('');
      return false;
  }
}
var eventdate = $(".datepickerstart").val(); 
    if (datestart!='' &&  Date.parse(datestart) > Date.parse(eventdate)) {
      alert("Registration Start Date Should Be less than event Date");
      $(".datepickerstart2").val('');
        return false;
  }
    if (dateend!='' && Date.parse(dateend) > Date.parse(eventdate)) {
      alert("Registration End Date Should Be less than event Date");
      $(".datepickerend2").val('');
      return false;
  }
}); 
            $.validator.addMethod("zipcode",function(t,e){return this.optional(e)||t.match(/(^\d{5,6})$/)},"Invalid zip code");

  var i = 1;

$(document).on('click','.addtype',function(){

            var childdata='<div class="childinfo"><article class="clearfix part-row b-p5"> <article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0">      <input type="hidden" name="regtype[]"" value="'+i+'"/>     <input class="form-control donate-form require" placeholder="Registration Title" name="regname_' + i + '" type="text" aria-invalid="true">     </article> </article> <article class="clearfix part-2">     <article class="clearfix r-m15 mobile-l-m0">         <input class="form-control  donate-form require number" placeholder="Amount" name="amount_' + i + '" type="text">     </article> </article> <article class="clearfix part-2">     <article class="clearfix r-m15 mobile-l-m0">         <input class="form-control donate-form  number require min" placeholder="Reg. Count" name="reg_count_' + i + '" type="text">     </article> </article>   <article class="clearfix part-2">     <article class="clearfix r-m15 mobile-l-m0">         <input class="form-control donate-form require number" placeholder="Display Sequence" name="order_no_' + i + '" type="text">   </article></article><article class="clearfix part-2">     <article class="clearfix r-m15 mobile-l-m0"> <a href="javascript:void(0)" class="btn btn-sm btn-info remove_list2 white-t">Remove</a></article></article> </article>';
            i++;

             $('.event_regtypedata').append(childdata);

        });


    var j=1;
                 $(document).on('click','.addsponsortype',function(){

           var childdata='<div class="childinfo"> <article class="clearfix part-row b-p5"> <article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0">        <input type="hidden" name="sponsortype[]"" value="'+j+'"/>     <input class="form-control donate-form require" placeholder="Sponsorship Title" name="sponsortype_' + j + '" type="text">     </article> </article> <article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0">         <input class="form-control require number donate-form" placeholder="Amount" name="sponsoramount_' + j + '" type="text">     </article> </article><article class="clearfix part-2">     <article class="clearfix r-m15 mobile-l-m0">         <input class="form-control number require donate-form" placeholder="Display Sequence" name="sponsororder_no_' + j + '" type="text">   </article></article><article class="clearfix part-2">  <article class="clearfix r-m15 mobile-l-m0"> <a href="javascript:void(0)" class="btn btn-sm btn-info remove_list2 white-t">Remove</a></article></article></article></div>';
           j++;

          $('.event_sponsortypedata').append(childdata);
 

            });

               	$.validator.addMethod("require", function(value, element) {
	return !this.optional(element);
}, "Required");

$.validator.addMethod("number", function(value, element) {
	return this.optional(element) || /^-?\d+$/.test(value);
}, "Number Only");       

$.validator.addMethod('min', function (value, element) {
    return this.optional(element) || value > 0;
}, "please enter more than 0");


            $(document).on('click', '.remove_list2', function(event) {
        $(this).parents(".childinfo").remove();
    });

              $(".addtype").trigger("click"); 

            $(document).on('change', '.is_registration', function(e){
                if (this.checked == true){
                    $('.registration_date').show();

                } else {
                    $('.registration_date').hide();
                }

            });
             $(document).on('change', '.location_type', function(e){
                if (this.value == 'Online'){
                    $('.event_location').hide();
                } else {
                    $('.event_location').show();
                }

            });

            $("#dataform").validate({
                rules: {
                    name: "required",
                    contact_mail:"email required",
                    start_date:"required",
                    start_time:"required",
                    end_time:"required",
                    category_id: "required",
                    location:  { required: function(element) {   if($("select[name='location_type']").val()=='Location')  return true;  }, alpha:true},
                    zipcode: "zipcode",
                    location_type: "required",
                    city: { required: function(element) {   if($("select[name='location_type']").val()=='Location')  return true;  }, alpha:true},
                    state: { required: function(element) {   if($("select[name='location_type']").val()=='Location')  return true;  }},
                    banner_url: {extension: "jpg|png|gif|jpeg"},
                    
               
                 total_registration_count: {
                  required: function(element) {
                    if($('input[name=is_registration]:checked').length==1)
                    return true;
                  },
                  min: function(element) {
                    if($('input[name=is_registration]:checked').length==1)
                    return 1
                  }
                 },
                  registration_start_date: {
                  required: function(element) {
                    if($('input[name=is_registration]:checked').length==1)
                    return true;
                  }
                 },
                  registration_end_date: {
                  required: function(element) {
                    if($('input[name=is_registration]:checked').length==1)
                    return true;
                  }
                 }
                },
                messages: {
                     contact_mail: {required: "Required..!! ", email:"Enter valid email"},
                    start_date: {required: "Required..!! "},
                    start_time: {required: "Required..!! "},
                    end_time: {required: "Required..!! "},
                    name: {required: "Required..!!"},
                    category_id: {required: "Required..!!"},
                    total_registration_count: {required: "Required..!!",min:"Enter greater than 0"},
                    registration_start_date: {required: "Required..!!"},
                    registration_end_date: {required: "Required..!!"},
                    location_type: {required: "Required..!!"},
                    location: {required: "Required..!!"},
                    city: {required: "Required..!!",alpha:'Numbers/Special characters not allowed'},
                    state: {required: "Required..!!"},
                    banner_url: {extension: 'Not an image!'}
                 },
                 submitHandler: function(form) {
                            if($('input[name=is_registration]:checked').length==1)
                            {
                               if($('input[name="regtype[]"]').length<1)
                               {
                                alert('Registration Type required');
                                return false;
                               }

                            }
                            return true;                       
                 }

            });

            $("#email_reg").change(function() {
                var email = $(this).val();
                var data = "email=" + email;
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('check-email') }}',
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    success: function(res){
                        if (res == 1) {
                            $("#signupbtn").attr("disabled", "disabled");
                            $(".email_status").html('<label class="error" for="email">Email already Exists</label>');
                        } else {
                            $("#signupbtn").removeAttr("disabled");
                            $(".email_status").html('');
                        }

                    }
                });
            });
 
 
        });
    </script>
        <script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
<script>
CKEDITOR.replace( 'event_details', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
CKEDITOR.replace( 'mail_description' , { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );

</script>

@stop