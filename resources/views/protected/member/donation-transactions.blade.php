@extends('layouts.user.user')

@section('title', 'Profile')
@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatable/jquery.dataTables.min.css') }}">
@stop
@section('content')


    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Transactions</h2>
                </article>
                <div class="success_profile"></div>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0" style="min-height:745px!important;">
                    <article class="clearfix block-row">
                       @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" rel="p"><a class="black-t">Transactions</a></article>
                                    @include('layouts.user.includes.notifications')

                                    <section  class="clearfix tab_content">

                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                               Donation Transactions</h4>
                                                <article class="clearfix">

                                                     @if(count($donations))

                                                    <article class="clearfix t-p25 overflow_x-a">
                                                        <table class="table3 t-l" width="100%" cellpadding="0" cellspacing="0"
                                                               border="0" id="datalist">
                                                               <thead>
                                                            <tr>
                                                                <th width="20.9%">Date</th>
                                                                <th width="15.9%">Cause</th>
                                                                <th width="15.7%">Status</th>
                                                                <th width="10.5%">Amount</th>
                                                                <th width="10.1%">View</th>
                                                            </tr>
                                                            </thead>

                                                            @foreach($donations as $key=>$donation)
                                                                <tr>
                                                                    <td >{{dateformat($donation->created_at)}}</td>
                                                                    
                                                                    <td >@if($donation->eventuser_id)
                                                                    Event Registration
                                                                    @php $donation_status=($donation->eventuser->status==1)?'Confirmed':'Pending'; @endphp
                                                                    @elseif($donation->order_id)
                                                                    Membership Registration
                                                                    @php $donation_status=($donation->order->status==1)?'Confirmed':'Pending'; @endphp
                                                                    @else
                                                                    @php $donation_status=($donation->status==1)?'Confirmed':'Pending'; @endphp
                                                                    @endif 
                                                                    {{ShortenText($donation->donation_cause,12)}}</td>
                                                                    <td >{{$donation_status}}</td>
                                                                    <td ><span class="Montserrat-Regular font15 @if($donation_status=='Confirmed')blue-t @else red-t @endif">@if($donation->amount)${{ $donation->amount }}@endif</span></td>
                                                                    <td >
                                                                        <a href="{{url('donation/details/'.$donation->id)}}" class="r-m10"  data-toggle="ajax-modal"><img src="{{url('images/view-icon.png')}}" width="22" height="12" border="0" alt=""/></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach


                                                            </tbody>
                                                        </table>
                                                    </article>
                                                    @else
                                                    <h4>No Donations </h4><br/><br/>
                                                       @endif

                                                </article>
                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
    <script src="{{ asset('plugins/datatable/jquery.dataTables.min.js') }}"></script>

 <script>
  $(document).ready(function () {

            $('#datalist').DataTable({
                responsive: true,
                 scrollX: true
            });
        });
     </script>
@stop
