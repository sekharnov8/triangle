@extends('layouts.user.user')

@section('title', 'Profile')

@section('content')


    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Notification Status <a href="{{url('notifications')}}" class="f-r btn btn-info">&lt; Back</a></h2>
                </article>
                <div class="success_profile"></div>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0" style="min-height:745px!important;">
                    <article class="clearfix block-row">
                       @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" rel="p"><a class="black-t">Notifications</a></article>
                                    @include('layouts.user.includes.notifications')

                                    <section  class="clearfix tab_content">

                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                               Sent Notification Status</h4>
                                                <article class="clearfix">

                                                     @if(count($notification->sent_members))

                                                    <article class="clearfix t-p25 overflow_x-a">
                                                        <table class="table3 t-l" width="100%" cellpadding="0" cellspacing="0"
                                                               border="0">
                                                            <tbody>
                                                            <tr>
                                                                <th>User</th>
                                                                <th>Status</th>
                                                                <th >Sent on</th>
                                                                <th >Checked on</th>
                                                            </tr> 
                                                            @foreach($notification->sent_members as $data)
                                                                <tr>
                                                                    <td >{{getUserName($data->user_id)}}</td>
                                                                    <td >  @if($data->status)Checked @endif</td>
                                                                    <td >{!! dateformat($data->created_at) !!}</td>
                                                                    <td >@if($data->status){!! dateformat($data->updated_at) !!} @endif</td>
                                                                    
                                                                </tr>
                                                            @endforeach


                                                            </tbody>
                                                        </table>
                                                    </article>
                                                     @else
                                                    <h4>No Notifications sent </h4><br/><br/>
                                                       @endif

                                                </article>
                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')

@stop
