@extends('layouts.user.user')

@section('title', 'Profile')

@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Committees</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="user-fullblock clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular"
                                             rel="c"><a class="black-t">Committees</a></article>
                                    <section id="c" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                                Committees</h4>
                                            <article class="clearfix t-p25 overflow_x-a">
                                                <table class="table3 t-l" width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                    <tbody>
                                                    <tr>
                                                        <th width="6.9%">SNo</th>
                                                        <th width="27.9%">Committee Name</th>
                                                        <th width="25.7%">Member Name</th>
                                                        <th width="11.5%">Role</th>
                                                        <th >Status</th>
                                                        <th width="15.1%">View</th>
                                                    </tr>

                                                    @foreach($committeemembers as $key=>$committeemember)
                                                        <tr>
                                                            <td >{{$key+1}}</td>
                                                            <td  >{{$committeemember->committee->name}}</td>
                                                            <td style="color:#8b8b8b;">{{$committeemember->user['first_name'].' '.$committeemember->user['last_name']}} </td>

                                                            <td ><span
                                                                        class="Montserrat-Regular font15 blue-t">@if($committeemember->role_id){{ $roles[$committeemember->role_id] }}@endif</span></td>
                                                            <td style="padding:0px 5px!important;">
                                                                <article class="clearfix position-r t-m2n">
                                                                    <i class="dropdown-icon1 tabhorizontal-hide">&nbsp;</i>
                                                                    <article class="clearfix"
                                                                             style=" border-radius:30px; background-color:#fff!important; margin-bottom:0px; height:28px!important; border:1px solid #e1e1e1!important;">
                                                                        <select class="dropdown-select"
                                                                                style="padding: 4px 10px!important; margin-top:-5px; color:#2f2f2f!important;">
                                                                            <option class="option" value="">Active</option>

 

                                                                        </select>


                                                                    </article>
                                                                </article>
                                                            </td>
                                                            <td >

                                                                <a
                                                                @if($committeemember->committee->type=='general')
                                                                        href="{{url('leadership/'. $committeemember->committee->slug)}}"
                                                                        @else
                                                                        href="{{url('leadership/standing-committee')}}"
                                                                    @endif

                                                                        class="r-m10"><img src="{{url('images/view-icon.png')}}" width="22" height="12" border="0" alt=""/></a>


                                                            </td>
                                                        </tr>
                                                    @endforeach


                                                    </tbody>
                                                </table>
                                            </article>
                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('script')
@stop