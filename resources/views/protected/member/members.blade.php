@extends('layouts.user.user')

@section('title', 'Members List')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatable/jquery.dataTables.min.css') }}">
@stop
@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Dashboard</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9" id="rb9">
                            <article class="clearfix tab_container">
                                <article
                                        class="clearfix t-b-m35 white-bg p35 l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p15 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >

                                    <section id="s" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                                Members 
                                                 <a href="{{route('members_export')}}" class="btn f-r btn-info btnexport"
                   data-toggle="tooltip" title="Export to Excel"><i class="fa fa-download" aria-hidden="true"></i>
                    Export to Excel</a>
                                            </h4>
                                            <article class="clearfix t-p25">
                                                <div class="row">

                                                    <div class="col-sm-4 form-inline">
                                                        <lable class="control-label">Date Registered</lable>
                                                        <div class="input-daterange input-group" id="datepicker">
                                                            <input type="text" class="input-sm form-control datepicker" name="datefrom" id="datefrom"
                                                                   value="" placeholder="Date from"/>
                                                            <span class="input-group-addon">to</span>
                                                            <input type="text" class="input-sm form-control datepicker" name="dateto" id="dateto" value=""
                                                                   placeholder="Date to"/>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <lable class="control-label">Keyword</lable>
                                                        <input type="text" name="keyword" class="form-control">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <lable class="control-label">Status</lable>
                                                        {!! Form::select('is_approved', [''=>'All', '-1'=>'Partial/Pending']+$statustypes, -1,['class' => 'form-control','placeholder' =>'Select Status']) !!}
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <lable class="control-label">Membership Type</lable>
                                                        {!! Form::select('mtype', $mtypes, null,['class' => 'form-control','placeholder' =>
                                                        'Membership Type']) !!}
                                                    </div>
                                                    
                                                    <div class="col-sm-3"><br>
                                                        <button type="button" id="dateSearch" class="btn  btn-info">Search</button>
                                                        <span class="filter2" style="display:none">
                              <button type="button" id="dateClear" class="btn  btn-warning">Clear</button>
                             </span>
                                                    </div>
                                                </div>
                                                <br>
                                                <table class="table datatable table-striped table-bordered table-hover" id="datalist">
                                                    <thead>
                                                    <tr >
                                                        <th>S.No.</th>
                                                         <th width="80">Member ID</th>
                        <th  width="120">Membership Type</th>
                        <th width="100">Status</th>
                        <th > First Name</th>
                        <th > Last Name</th>
                        <th width="200">Email</th>
                        <th width="120">Mobile</th>
                        <th width="90">Last Login</th>
                        <th width="90">Created At</th>
                                                    </tr>
                                                    </thead>
                                                    </tbody>
                                                </table>

                                            </article>

                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
    <script src="{{ asset('plugins/datatable/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function () {

            $("#dataform").validate({
                rules: {
                    name: "required"
                },
                messages:
                    {
                        name:{required:"Enter Membership Type]"}
                    }
            });

            $(document).on('change','.status',function(e){
                var thisVal = $(this);
                var id = $(this).data('id');
                var status = this.value;
                var prevStatus = $(this).data('status');
                e.stopImmediatePropagation();
                swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('change-member-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    //console.log(data);
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                        }
                    });

            });

            var oTable = $('#datalist').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                order: [[10, "desc"]],
                lengthMenu: [[5,10, 20, 30, 50], [5,10, 20, 30, 50]],
                ajax: {
                    url: '{{url('/members/data')}}',
                    data: function (d) {
                        d.keyword = $('input[name=keyword]').val();
                        d.datefrom = $('input[name=datefrom]').val();
                        d.dateto = $('input[name=dateto]').val();
                        d.is_approved = $('select[name=is_approved]').val();
                        d.mtype = $('select[name=mtype]').val();
                    }
                },
                columns: [
                    {data: 'DT_Row_Index', name: 'DT_Row_Index', searchable: false, orderable: false},
                    {data: 'member_id', name: 'member_id'},
                    {data: 'membershiptype', name: 'membershiptype',searchable: false, orderable: false},
                    {data: 'status', name: 'status', searchable: false, orderable: false},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'last_login', name: 'last_login'},
                    {data: 'created_at', name: 'created_at'}
                ]
            });
            $('.input-daterange').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: ''
            });
            $('#dateSearch').on('click', function () {
                $('.filter2').show();
                datefrom = $('input[name=datefrom]').val();
                dateto = $('input[name=dateto]').val();
                 keyword= $('input[name=keyword]').val();
                mtype = $('select[name=mtype]').val();
                is_approved=$('select[name=is_approved]').val();
                 @if(is_treasurer())
                 $('.btnexport').attr('href', 'members-export?datefrom=' + datefrom + '&dateto=' + dateto+ '&keyword=' + keyword+ '&is_approved=' + is_approved+ '&mtype=' + mtype);
                 @endif
                oTable.draw();
            });
            $('#dateClear').on('click', function () {
                $('.filter2').hide();
                $('input[name=keyword]').val('');
                $('select[name=is_approved]').val('');
                $('select[name=mtype]').val('');
 @if(is_treasurer())
                $('.btnexport').attr('href', 'members-export');                  
                 @endif
                oTable.draw();
            });
            $('.dataTables_filter').hide();
            $("input[name=keyword]").keypress(function (event) {
                if (event.which == 13) {
                    $("#dateSearch").trigger("click");
                }
            });

            var $modal = $('#ajax-modal');
            $(document).on('click','[data-toggle="edit-ajax-modal"]', function(e) {
                e.preventDefault();
                var url = $(this).attr('href');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: url,
                    type: 'GET',
                    success: function(data){
                        $modal.modal();
                        $('#ajax-modal .modal-body').html(data);

                    }
                });
            });

        });

    </script>
@stop