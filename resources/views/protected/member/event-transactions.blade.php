@extends('layouts.user.user')

@section('title', 'Profile')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatable/jquery.dataTables.min.css') }}">
@stop
@section('content')


    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Transactions</h2>
                </article>
                <div class="success_profile"></div>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0" style="min-height:745px!important;">
                    <article class="clearfix block-row">
                       @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" rel="p"><a class="black-t">Transactions</a></article>
                                    @include('layouts.user.includes.notifications')

                                    <section  class="clearfix tab_content">

                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                                Event Registrations</h4>
                                                         @if(count($eventusers))
                                                    <article class="clearfix t-p25 overflow_x-a">
                                                        <table class="table3 t-l" width="100%" cellpadding="0" cellspacing="0"
                                                               border="0"  id="datalist">
                                                           <thead>
                                                            <tr>
                                                                <th width="12%">Date</th>
                                                                <th width="25.9%">Event</th>
                                                                <th width="15.9%">Tickets</th>
                                                                <th width="10.5%">Amount</th>
                                                                <th width="10.5%">Payment</th>
                                                                <th width="8%">View</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($eventusers as $key=>$eventuser)
                                                                <tr>
                                                                    <td >{{dateformat($eventuser->created_at)}}</td>
                                                                    <td >@if($eventuser->event){{$eventuser->event->name}}@endif</td>
                                                                    <td >@foreach($eventuser->registrants as $rtype)

                                                                           @if(isset($rtype->eventregistrationtype_id))
                                                                            {{ getEventRegtype($rtype->eventregistrationtype_id) }} - {{$rtype->count}}<br/>
                                                                           @endif
                                                                    @endforeach</td>
                                                                     <td ><span class="Montserrat-Regular font15 blue-t">@if($eventuser->amount_paid>0) ${{ $eventuser->amount_paid }}@endif</span></td>
                                                                      <td >@if($eventuser->amount_paid)@if($eventuser->status==1)Confirmed @else Pending @endif @else Registered @endif</td>
                                                                    <td >
                                                                        <a href="{{url('eventreg/details/'.$eventuser->id)}}" class="r-m10" data-toggle="ajax-modal" data-target="#event_modal" ><img src="{{url('images/view-icon.png')}}" width="22" height="12" border="0" alt=""/></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </article>
                                                      @else
                                                              <h2 class="Arizonia-Regular  font30 font-w-b orange-t   t-c"><br/>No Event Registrations</h2> 

                                                        @endif
                                         </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
    <script src="{{ asset('plugins/datatable/jquery.dataTables.min.js') }}"></script>

 <script>
  $(document).ready(function () {

            $('#datalist').DataTable({
                responsive: true,
                 scrollX: true
            });
        });
     </script>
@stop
