@extends('layouts.user.user')
@section('title', 'Profile')
@section('content')

    <section class="clearfix wrapper innerpage-bg  position-r">
    
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Profile</h2>
                </article>
                <div class="success_profile"></div>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0" style="min-height:745px!important;">
                    <article class="clearfix block-row">
                                                @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="user-fullblock clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" rel="p"><a class="black-t">My Profile</a></article>
                                    @include('layouts.user.includes.notifications')
                                  
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" rel="mi"><a class="black-t">Membership Info</a></article>

                                    <section id="mi" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10 membership_info" >
                                                <article class="clearfix">
                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Membership Info</h4>

                                                      @if($user->membership)
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Membership</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->membership->name }} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Expire Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($user->membership->validity=='Lifetime')Lifetime @else {{$user->membership->expiredate }} @endif</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article> 

                                                    @if($order)
                                                        <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Transaction Info</h4>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix r-m15 mobile-r-m0">
                                                                    <article class="clearfix box-row">
                                                                        <article class="clearfix box-4">
                                                                            <label class="l-gray-t font15 OpenSans b-m0">Transaction Id</label>
                                                                        </article>
                                                                        <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($order->transaction_id){{$order->transaction_id }} @else N/A @endif</article>
                                                                    </article>
                                                                </article>
                                                            </article>
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix l-m15 mobile-l-m0 position-r">
                                                                    <article class="clearfix box-row">
                                                                        <article class="clearfix box-4">
                                                                            <label class="l-gray-t font15 OpenSans b-m0">Payment Method </label>
                                                                        </article>
                                                                        <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($order->membershiptype && $order->amount>0) {{$order->paymentmethod->name}} @else Free @endif</article>
                                                                    </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix r-m15 mobile-r-m0">
                                                                    <article class="clearfix box-row">
                                                                        <article class="clearfix box-4">
                                                                            <label class="l-gray-t font15 OpenSans b-m0">Transaction Date</label>
                                                                        </article>
                                                                        <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{dateformat($order->created_at) }} </article>
                                                                    </article>
                                                                </article>
                                                            </article>
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix l-m15 mobile-l-m0">

                                                                    <article class="clearfix box-row">
                                                                        <article class="clearfix box-4">
                                                                            <label class="l-gray-t font15 OpenSans b-m0">Price</label>
                                                                        </article>
                                                                        <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> ${{numberformat($user->membership->price) }} </article>
                                                                    </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                            @if($order->paymentmethod_id && $order->paymentmethod->name=='Check')
                                                                    <article class="clearfix part-6">
                                                                        <article class="clearfix r-m15 mobile-r-m0">
                                                                            <article class="clearfix box-row">
                                                                                <article class="clearfix box-4">
                                                                                    <label class="l-gray-t font15 OpenSans b-m0">Bank Name</label>
                                                                                </article>
                                                                                <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$order->bankname }} </article>
                                                                            </article>
                                                                        </article>
                                                                    </article>
                                                                    <article class="clearfix part-6">
                                                                        <article class="clearfix l-m15 mobile-l-m0">

                                                                            <article class="clearfix box-row">
                                                                                <article class="clearfix box-4">
                                                                                    <label class="l-gray-t font15 OpenSans b-m0">Check No</label>
                                                                                </article>
                                                                                <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$order->cheque_no }} </article>
                                                                            </article>
                                                                        </article>
                                                                    </article>
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                                <article class="clearfix part-6">
                                                                    <article class="clearfix r-m15 mobile-r-m0">
                                                                        <article class="clearfix box-row">
                                                                            <article class="clearfix box-4">
                                                                                <label class="l-gray-t font15 OpenSans b-m0">Check Date</label>
                                                                            </article>
                                                                            <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$order->cheque_date }} </article>
                                                                        </article>
                                                                    </article>
                                                                </article>
                                                                 <article class="clearfix part-6">
                                                                    <article class="clearfix l-m15 mobile-r-m0">
                                                                        <article class="clearfix box-row">
                                                                            <article class="clearfix box-4">
                                                                                <label class="l-gray-t font15 OpenSans b-m0"> Handled to</label>
                                                                            </article>
                                                                            <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$order->handedto }} </article>
                                                                        </article>
                                                                    </article>
                                                                </article>
                                                                @endif

                                                                @if($order->membershiptype->price>0) 
                                                                <article class="clearfix part-6">
                                                                    <article class="clearfix r-m15 mobile-l-m0">

                                                                        <article class="clearfix box-row">
                                                                            <article class="clearfix box-4">
                                                                                <label class="l-gray-t font15 OpenSans b-m0">Payment Status</label>
                                                                            </article>
                                                                            <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{getstatusByTypeAndNo(1,$order->status)}}  </article>
                                                                        </article>
                                                                    </article>
                                                                </article>
                                                                @endif
                                                                 <article class="clearfix part-6">
                                                                    <article class="clearfix l-m15 mobile-r-m0">
                                                                        <article class="clearfix box-row">
                                                                            <article class="clearfix box-4">
                                                                                <label class="l-gray-t font15 OpenSans b-m0">  Donation</label>
                                                                            </article>
                                                                            <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$order->donation_amount }} </article>
                                                                        </article>
                                                                    </article>
                                                                </article>
                                                        </article>
                                                        
                                                        <br/>
                                                       


                                                    @endif
                                                     @if($is_renewal)
                                                             <a href="#" class="btn btn-success renew_button" >Upgrade Membership</a>
                                                        @endif
                                                         <a href="#" class="btn btn-warning orders_button" >Membership Order Transactions</a>
                                                        <!-- 
                                                          <a href="#" class="btn btn-warning orders_button" >Check Membership Orders</a> -->
                                                    @else
                                                    <h3>Please click below link to change or upgrade your membership type</h3>
                                                     <a href="#" class="btn btn-success renew_button" >Become Member</a>
                                                    @endif



                                                </article>
                                        </article>

                                        <article class="clearfix mobile-l-m10 mobile-t-m10  membership_orders" style="display: none;">
                                            <article class="clearfix">
                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Membership  Order Transactions <a href="#" class="f-r close_membership_orders">Close (x)</a></h4>

                                                    @if($orders)
                                                    <article class="clearfix t-p25 overflow_x-a">
                                                        <table class="table2 t-l" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <th width="12%">Date & Time</th>
                                                                <th width="18.2%">Membership</th>
                                                                <th width="8.2%">Payment Type</th>
                                                                <th width="23.7%">Transaction Details</th>
                                                                <th width="12%">Status</th>
                                                                <th width="10%">Total</th>
                                                            </tr>
                                                            @foreach($orders as $data)
                                                            <tr>
                                                                <td >{{dateformat($data->created_at)}}</td>
                                                                <td >{{$data->membershiptype->name}}</td>
                                                                <td >@if($data->paymentmethod_id){{$data->paymentmethod->name}}@endif</td>
                                                                <td >{{$data->transaction_id}}
                                                                    @if($data->paymentmethod_id==2)
                                                                    Bank Name: {{$data->bankname}}<br/>Check No:  {{$data->cheque_no}}<br/>Check Date: {{$data->cheque_date}}
                                                                    <br/>Payment by: {{$data->payment_by}}
                                                                    @if($data->handedto)<br/>Check Handed to: {{$data->handedto}}@endif
                                                                    @endif
                                                                </td>
                                                                <td >@if($data->amount){{getstatusByTypeAndNo(1,$data->status)}} @else Confirmed @endif</td>
                                                                <td >${{numberformat($data->amount+$data->donation_amount)}}</td>
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </article>
                                                    @endif
 
                                                    </article>
                                        </article>

                                         <article class="clearfix mobile-l-m10 mobile-t-m10 renewal_membership"  style="display: none;">
                                            {!! Form::open(['route' => 'renewal-membership', 'method'=>'POST', 'id'=>'dataform']) !!}
                                                <article class="clearfix">
                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Upgrade Membership <a href="#" class="f-r close_renewal_membership">Close (x)</a></h4>

                                                    <h4 class="clearfix t-b-m0 t-p5 b-p10 OpenSans font16 blue-t">Membership Types  *</h4>
                                    <article class="clearfix white-bg shadow border-t3  border-radius5">
                                        <article class="clearfix l-r-p25 t-b-p15">
                                            <article class="clearfix part-row">
                                            @foreach($mtypes as $mtype)
                                                <article class="clearfix part-6">
                                                    <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                        <input  value="{{$mtype->id}}" name="membershiptype_id" data-value="{{$mtype->price}}" data-name="{{$mtype->name}}" type="radio"  class="membershiptype">
                                                        <span for="radio1" class="dis-in lowgray-t1">{{$mtype->name}} ( <span class="orange-t">${{$mtype->price}}</span> )</span>
                                                    </label>
                                                </article>
                                                @endforeach

                                            </article>
                                        </article>
                                    </article>

                                    <div class="is_payment" style="display: none">

                                        <h4  class="clearfix t-b-m10 t-p20 b-p10 OpenSans font16 blue-t">Payment Info</h4>
                                        <article class="clearfix white-bg shadow border-t3  border-radius5">
                                    <article class="clearfix l-r-p25 t-b-p15">
                                     <article class="clearfix part-row b-p5">
                                        <article class="clearfix part-6">
                                            <article class="clearfix r-m15 mobile-r-m0">
                                             <article class="clearfix part-row">
                                                    @foreach($payment_methods as $key=>$data)
                                                        <article class="clearfix part-4">
                                                            <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                                <input @if($key==1) checked @endif  value="{{$key}}" name="paymentmethod_id" type="radio" onClick="check2(this.value);" >
                                                                <span for="radio1" class="dis-in lowgray-t1">{{$data}}</span>
                                                            </label>
                                                        </article>
                                                        @endforeach
                                              </article>
                                            </article>
                                        </article>
                                    </article>
                                    <div id="alreadypayment" style="display: none;">
                                         <div class="row">
                                        <article class="clearfix part-3 r-m10">
                                        <label class="l-gray-t font12 b-m0">Bank Name</label>
                                        <input type="text" name="bankname" maxlength="50" class="form-control donate-form" /></article>
                                        <article class="clearfix part-3 r-m10">
                                        <label class="l-gray-t font12 b-m0">Check No</label>
                                         <input type="text" name="cheque_no" maxlength="20" class="form-control donate-form" /></article>
                                         <article class="clearfix part-3">
                                         <label class="l-gray-t font12 b-m0">Check Date</label>
                                          <input type="text" readonly='true' name="cheque_date" class="form-control donate-form chequedate" placeholder=""  autocomplete="off" /></article>
                                          </div>

                                    </div>
                                  </article>
                                    </article>
                                     </div>
                                        <div class="shadow t-b-p10 t-m20">
                                             <article class="clearfix white-bg  border-t3 l-r-p20 t-b-p5 border-radius5 mtypeinfo">
                                                <article class="clearfix pull-left OpenSans font16 d-gray-t">Membership Type - <span class="mtypename"></span></article>
                                                <article class="clearfix pull-right OpenSans font14 l-gt1">Amount : <span class="l-blue-t Anton font26 v-align-m">$<b class="mtypeprice">0</b></span></article>
                                            </article>
                                      </div>

                            <article class="clearfix t-p20">   <label class="font15 black-t3 checkboxstyle b-p3">
                            <input   name="terms" value="1" type="checkbox" checked>
                            <span for="checkbox" class="dis-in"><span class="font14 OpenSans mobile-font14 v-align-m l-h20 l-p5">I accept the Terms and Conditions.</span></label></article>
                        <article class="clearfix dashborder-t border-t t-m40 mobile-t-m10">
                            <article class="clearfix part-row t-p15">
                                     <article class="clearfix pull-right l-m15 mobile-l-m0">
                                        <article class="clearfix t-p15">
                                        <input type="submit"  class="paymentbutton t-c btn-block Montserrat-Medium font18 border-radius25 l-r-p20 white-t t-t-u"  style="background-color:#ff9f10; padding-bottom:11px; padding-top:11px;" value="Proceed To Pay"> </article>
                                    </article>
                             </article>
                        </article>          </article>
                        {!! Form::close() !!}


                                        </article>


                                    </section>
  
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" ><a   href="{{url('logout')}}" class="black-t">Logout</a></article>
                                   
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section> 

 
@stop
@section('scripts')
    <link media="all" type="text/css" rel="stylesheet" href="{{url('plugins/pikaday/pikaday.css')}}">
    <script src="{{url('plugins/pikaday/moment.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.jquery.js')}}"></script>

<script type="text/javascript" src="{{url('js/res-tab.js')}}"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker();
        var picker = new Pikaday(
                {
                    field: document.getElementById('dob'),
                    firstDay: 1,
                    format: 'MM-DD-YYYY',
                    minDate: new Date(1930, 0, 1),
                    maxDate: new Date(2016, 12, 31),
                    yearRange: [1930,2016]
                });
        picker.gotoYear('2000');
    } );
</script>
<script type="text/javascript">
        $(document).ready(function () {

            $.validator.setDefaults({
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if (element.attr("type") == "radio") {
                    error.insertAfter($(element).parents('.part-row'));
                }
                 else if (element.attr("type") == "checkbox") {
                    error.insertAfter($(element).parents('.checkboxstyle'));
                }
                else
                {
                  error.insertAfter($(element));

                }
            }
        });
              $("#dataform").validate({
                rules: {
                    membershiptype_id:  {required: true},
                     bankname: {
                  required: function(element) {
                    if($("input[name='paymentmethod_id']:checked").val()==2)
                    return true;
                  }
                 },
                 cheque_no: {
                  required: function(element) {
                    if($("input[name='paymentmethod_id']:checked").val()==2)
                    return true;
                  }
                 },
                 cheque_date: {
                  required: function(element) {
                    if($("input[name='paymentmethod_id']:checked").val()==2)
                    return true;
                  }
                 },
                    terms: {required: true}
                },
                messages:
                {
                    membershiptype_id: {"required":"Select Membership Type"},
                     bankname:{required:"Bank Name Required"},
                cheque_no:{required:"Check No. Required"},
                cheque_date:{required:"Check Date Required"},
                    terms: {"required":"Check Terms checkbox"}
                }

        });

            

            $('#changemail').dialog
            ({
                autoOpen: false,
                width: 420,
                modal: true
            });
             $('#changepic').dialog
            ({
                autoOpen: false,
                width: 420,
                modal: true
            });

            $('#changepassword').dialog
            ({
                autoOpen: false,
                width: 400,
                modal: true
            });

            $(".buttonprofileimage").click(function(e) {
               e.preventDefault();
            var formData  = new FormData($('.profile_picture')[0]);

            var s=$('input[type=file]')[0].files[0];

            if(!s)
            {
                alert('Attach picture');
                return false;
            }
            $.ajax({
                url: '{{url('uploadProfilePicture')}}',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data.success)
                    {
                        $('.success_profile').html('<div class="alert alert-success">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                        img='{{url('uploads/users/')}}';
                        img=img+'/'+data.image;
                        $(".userprofileimage").attr("src", img);
                    }
                    else
                    {
                        $('.success_profile').html('<div class="alert alert-warning">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                    }
                    $('#changepic').dialog('close');
                }
            });
        });

        $.validator.addMethod("childemail", function(value, element) {

            if (value=='' || /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Please enter a valid Email.");

          $.validator.addMethod("alpha", function(value, element) {
                return this.optional(element)||value==value.match(/^[a-zA-Z\s]+$/);}, "Numbers/Special Charactesr not allowed");
 
             $(document).on('click', '.close_renewal_membership', function(event) {
                $('.membership_info').show();
                $('.renewal_membership').hide();
            });
             $(document).on('click', '.close_membership_orders', function(event) {
                $('.membership_info').show();
                $('.membership_orders').hide();
            });
             $(document).on('click', '.renew_button', function(event) {

                $('.membership_info').hide();
                $('.renewal_membership').show();
            });
              $(document).on('click', '.orders_button', function(event) {

                $('.membership_info').hide();
                $('.membership_orders').show();
            });

             $(document).on('click', '.membershiptype', function(event) {

                $('.membershipprice').val($(this).data('value'));
                $('.mtypename').html($(this).data('name'));
                $('.mtypeprice').html($(this).data('value'));
                $('.mtypeinfo').show();
                sumtotal();
            });

         $('.chequedate').datepicker({format: 'mm-dd-yyyy', todayHighlight: true, autoclose: true,startDate: 'd'});
 });

function check2(str)
{
    var paymenttype=$("input[name='paymentmethod_id']:checked").val();
     if(str==1)
    {
      document.getElementById("alreadypayment").style.display="none";
    }
    else
    {
      document.getElementById("alreadypayment").style.display="block";
    }
}
function sumtotal()
{
    var mprice=$('.mtypeprice').html();

    if(mprice>0)
    {
        $('.paymentbutton').val('Proceed to Pay');
        $('.is_payment').show();
    }
    else
    {
        $('.paymentbutton').val('Proceed to Confirm');
        $('.is_payment').hide();
    }

}
</script>
@stop