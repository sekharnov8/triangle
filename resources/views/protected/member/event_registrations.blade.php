@extends('layouts.user.user')

@section('title', 'Profile')

@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Dashboard</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article   class="clearfix t-b-m35 white-bg p35 l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p15 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0"
                                        style="min-height:555px;" id="negative" >

                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular"> Event - {{$event->name}} </article>
                                     <article class="clearfix  desktop-visible  mobile-hide "><h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15"> Event - {{$event->name}} </h4></article>
                                    <section id="s" class="clearfix tab_content">
 <p>Event Date: {{dateformat($event->start_date)}} @if($event->start_time!='') {{'@ '.$event->start_time }} @endif
                                    @if($event->end_time!='') {{'-'.$event->end_time }} @endif @if($event->start_time!='' || $event->end_time!='') EST @endif
 <br/>Registration Start Date: {{dateformat($event->registration_start_date)}}, Registration End Date: {{dateformat($event->registration_end_date)}}<br/>
 <strong>Location:</strong> {{$event->location}} @if($event->city){{', '.$event->city}} @endif @if($event->state){{', '.$event->state}} @endif @if($event->zipcode){{' Pincode: '.$event->zipcode}} @endif</p>
<p><strong>Total No. of people register for this event limit: {{$event->total_registration_count}}</strong></p>
@if(count($event->registrations)>0)

 <p>
 <strong> Member Count by Registration Type (Payment  Completed Status)</strong><br/>
 Donation Amount Received: ${{ getSumOfEventDonationById($event->id,1) }}<br/>
 @foreach($event->regtypes as $key=>$regtype)
@if($regtype)
{{$regtype->name}} - {{getSumByEventRegType($regtype->id,1)}}<br/>
@endif
 @endforeach
 <strong>Total Member Count:</strong> {{getMemberCountByEventId($event->id, 1)}}
 </p>
 <p>
<strong> Member Count by Registration Type (Payment  Pending Status)</strong><br/>
Donation Amount Pending Status:  ${{ getSumOfEventDonationById($event->id,0) }}<br/>
 @foreach($event->regtypes as $key=>$regtype)
@if($regtype)
{{$regtype->name}} - {{getSumByEventRegType($regtype->id,0)}}<br/>
@endif
 @endforeach
 <strong>Total Member Count:</strong> {{getMemberCountByEventId($event->id, 0)}}
</p>
@endif


                                        <article class="clearfix mobile-l-m10 mobile-t-m10">

                                            <article class="clearfix t-p25 overflow_x-a">
                                            <table class="table3 t-l" width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                <thead>
                <tr >
                    <th>S.No.</th>
                    <th width="120">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th  width="100">Mobile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Email</th>
                    <th>Amount</th>
                    <th>Transaction Id</th>
                    <th>Created&nbsp;on&nbsp;</th>
                    <th>Payment</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($event->registrations)==0)
                <tr><td colspan="9" class="norecords">No Registrations</td></tr>
                @endif
                @foreach ($event->registrations as $key=>$data)
                    <tr class="tr_{{ $data->id }}">
                        <td>{{ $key+1 }}</td>
                        <td>{{ $data->first_name }} &nbsp; {{ $data->last_name }}</td>
                        <td>{{ $data->mobile }}</td>
                        <td>{{ $data->email }}</td>
                        <td>${{ $data->amount_paid }}</td>
                        <td>{{ $data->transaction_id }}</td>
                        <td>{{ dateformat($data->created_at) }}</td>
                      <td >@if($data->status==1)Confirmed @else Pending @endif</td>
                        <td>
                           <a href="{{url('eventreg/details/'.$data->id)}}" class="r-m10" data-toggle="ajax-modal" data-target="#event_modal" ><img src="{{url('images/view-icon.png')}}" width="22" height="12" border="0" alt=""/></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


                                            </article>

                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>
@stop
@section('scripts')
<script type="text/javascript">

</script>
@stop