@extends('layouts.user.user')

@section('title', 'Profile')
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.css')}}">
@endsection
@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Send Mail</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article   class="clearfix t-b-m35 white-bg p35 l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p15 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0"
                                        style="min-height:555px;" id="negative" >

                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular">Send Mail </article>
                                    @include('layouts.user.includes.notifications')
                                    
                                    <section id="s" class="clearfix tab_content">
                                        {!! Form::open(array('url' => 'send-mail', 'method' => 'POST','id' => 'dataform')) !!}

<div class="row">
    <label for="image" class="control-label col-md-2">Subject *</label>

<div class="col-md-10"  >
    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject', 'required']) !!}
</div>
</div>
  
    <div class="row">

 
  <label for="image" class="control-label col-md-2">To Email *</label>

    <div class="col-md-10"  >
        {!! Form::select('members[]', $members, '' , ['class' => 'form-control', 'id' => 'members', 'multiple' => 'multiple', 'required']) !!}
     
     </div>
    
 </div>
 <div class="row"><div class="col-md-12"  >
<label class="font15 black-t3 checkboxstyle b-p3">
                                        <input   name="memberscb" id="memberscb" onClick="check2();" value="1" type="checkbox">
                                        <span for="checkbox" class="dis-in"><span
                                                    class="font14 OpenSans mobile-font14 v-align-m l-h20 l-p5">Select All</span></span></label>
         </div>
 </div>
<div class="row">
<div class="col-md-12"  >
     {!! Form::textarea('description', null, ['class' => 'form-control textarea', 'placeholder' => 'Template']) !!}
</div> 
 
<div class="col-md-12"  >
<label class="font15 black-t3 checkboxstyle b-p3">
                                        <input   name="send_mail" id="send_mail" value="1" type="checkbox">
                                        <span for="checkbox" class="dis-in"><span
                                                    class="font14 OpenSans mobile-font14 v-align-m l-h20 l-p5">Send Mail along with Notification</span></span></label>
</div>                                                    
<div class="col-md-12 m-t10 t-r">
     {!! Form::submit('Send Mail', ['class' => 'btn btn-info ']) !!}
</div>
</div>
{!! Form::close() !!}

                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>
@stop
@section('scripts')
 <script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/select2/select2.full.min.js')}}"></script>

<script>
CKEDITOR.replace( 'description', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
        $(document).ready(function () {

     $("#members").select2();

 });

function check2(){

            if($("#memberscb").prop('checked') ){
                 $("#members > option").prop("selected","selected");
                $("#members").trigger("change");
            }else{
                $("#members > option").removeAttr("selected");
                $("#members").trigger("change");
            }
 }        
</script>
@stop