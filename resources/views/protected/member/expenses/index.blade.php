@extends('layouts.user.user')

@section('title', 'Expenses')
@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatable/jquery.dataTables.min.css') }}">
@stop
@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Dashboard</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="user-fullblock clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                        @include('layouts.user.includes.notifications')

                                           <section id="s" class="clearfix tab_content">

                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                            Expenses<span
                                                        class="border-radius20 liblueborder t-p8 b-p5 border2 t-c l-r-p25 pull-right l-h14 mobilev-pull-n mobilev-t-c mobilev-dis-b mobilev-t-m10"><a
                                                            href="{{url('expenses/create')}}" class="Montserrat-Regular font13 t-t-u blue-t">Add Expenses</a></span></h4>
                                            <article class="clearfix t-p25 overflow_x-a">
                                                <table class="table3 t-l" id="datalist" width="100%" cellpadding="0" cellspacing="0"
                                                       border="0">
                                                       <thead>
                                                     <tr >
                                                    <th>S.No</th>
                                                     <th>Amount</th>
                                                    <th width="200">Event</th>
                                                    <th>Category</th>
                                                    <th>Spent&nbsp;Date</th>
                                                    <th>Paid&nbsp;Date</th>
                                                    <th>Payment&nbsp;Status</th>
                                                    <th width="200">Comments</th>
                                                     <th>Receipt</th>
                                                    <th>Updated&nbsp;at</th>
                                                    <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                   <tbody> 
                                                    @foreach ($expenses as $key=>$data)
                    <tr class="tr_{{ $data->id }}">
                        <td>{{ $key+1 }}</td>
                         <td>{{ $data->amount }} </td>
                        <td>@if($data->event){{ $data->event->name }}@endif </td>
                        <td>@if($data->category){{ $data->category->name }}@endif </td>
                        <td>{{ dateformat($data->spent_date) }} </td>
                        <td>{{ dateformat($data->paid_date) }} </td>
                        <td>{{ $data->payment_status }} </td>
                        <td>{{ $data->comments }} </td>
                         <td>@if($data->receipt)<a href="/uploads/expenses/{{$data->receipt}}" target="_blank">View</a>@endif</td>
                        <td>{{ dateformat($data->created_at,1) }} </td>
                        <td> @if(!$data->paid_date)<a href="javascript:void(0);"  class="   delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>@endif</td>
                    </tr>
                @endforeach



                                                    </tbody>
                                                </table>
                                            </article>
                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
    <script src="{{ asset('plugins/datatable/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () { 
            $('#datalist').DataTable({
                responsive: true,
                 scrollX: true

            }); 

            $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.tr_'+id).remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/expenses') }}' + '/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            } else {
                                // swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });
            });
        });

</script>
@stop