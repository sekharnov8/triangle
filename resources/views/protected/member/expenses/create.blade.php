@extends('layouts.user.user')

@section('title', 'Profile')

@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Dashboard</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="user-fullblock clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular"
                                             rel="e"><a class="black-t">Add Expenses</a></article>
                                    <section id="e" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                                Add Expenses</h4>

                                            <ul class="clearfix list-pn list-f m0 featured-list2">

                                                <article class="clearfix mobile-l-m10 mobile-t-m10">
                                                {!! Form::open(array('url' =>'expenses', 'method' => 'POST', 'id'=>'dataform','files' => true)) !!}
                                                <article class="clearfix">
                                                    
                                                    <article class="clearfix part-row b-p5">

                                                       
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix dropdown"
                                                                     style="background:none;  border:none;">
                                                              {!! Form::select('event_id', $events_list, null,['class' => 'form-control  donate-form','placeholder' => 'Select Event']) !!}
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                {!! Form::text('amount', null, ['class' => 'form-control donate-form',  
                                                                'placeholder' => 'Amount *']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix dropdown"
                                                                     style="background:none;  border:none;">
                                                                {!! Form::select('category_id', $categories, null,['class' => 'form-control donate-form','placeholder' => ' Category']) !!}
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                {!! Form::text('spent_date', null, ['class' => 'form-control datepicker  donate-form','autocomplete'=>'off', 'placeholder'
                                                                => 'Amount Spent Date *']) !!}
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                 {!!Form::file('attachment',['placeholder' => 'Receipts',  'class' =>
                                                                'form-control donate-form image'])!!} 
                                                            </article>
                                                        </article>
                                                    </article>
                                                    
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-12">
                                                                 {!! Form::textarea('comments', null, ['class' => 'form-control donate-form', 'placeholder'
                                                                => 'Comments *']) !!}
                                                         </article>
                                                        
                                                    </article> 

                                                    <article class="clearfix part-row t-p15  b-p5">
                                                        <article class="clearfix  part-4 f-l">
                                                            <article class="clearfix t-p10 ">
                                                                <a href="{{url('expenses')}}"> <input value="Cancel" class="btn btn-danger    border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color: rgb(255, 176, 58);" type="button"></a> 
                                                            </article></article>
                                                        <article class="clearfix  part-8 f-r">
                                                       
                                                            <article class="clearfix pull-right t-p10 ">
                                                                <input value="Submit" class="btn btn-danger btn-block   border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color:#3e5066;width:170px;" type="submit">
                                                                <div class="loading"></div>
                                                            </article>
                                                        </article>
                                                    </article>
                                                </article>
                                                {!! Form::close() !!}
                                                </article> 
                                            </ul>
                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
    <script>
        $(document).ready(function () {
         
            $("#dataform").validate({
                rules: {
                    amount: {required:true, number:true, notEqual:'0', positiveNumber:true},
                    spent_date:"required",
                    comments: "required"
                },
                messages: {
                    amount:{required:"Required..!", number: "Enter valid amount", notEqual: "Enter amount greater than 0", positiveNumber:"Enter valid amount"},
                    spent_date: {required: "Required..!!"},
                    comments: {required: "Required..!!"} 
                }
            });
 
        });
    </script>
@stop