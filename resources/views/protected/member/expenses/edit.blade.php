@extends('layouts.user.user')

@section('title', 'Profile')

@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"><h2
                            class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                        Dashboard</h2>
                </article>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;">
                    <article class="clearfix block-row">

                        @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9"  id="rb9">
                            <article class="clearfix tab_container">
                                <article class="user-fullblock clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular"
                                             rel="e"><a class="black-t">Edit Event</a></article>
                                    <section id="e" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <h4 class="clearfix SuiGenerisRg-Regular font24 t-b-m0 t-t-c l-blue-t b-p15">
                                                Edit Event</h4>

                                            <ul class="clearfix list-pn list-f m0 featured-list2">

                                                <article class="clearfix mobile-l-m10 mobile-t-m10">
                                                    {!! Form::model($event, array('route' => ['memberevents.update', $event->id], 'method' => 'PUT','id'=>'dataform' , 'files' => true)) !!}
                                                    <article class="clearfix">
                                                        <h4 class="t-m0 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Event Details</h4>
                                                        <article class="clearfix part-row b-p5">

                                                            <article class="clearfix part-6">
                                                                <article class="clearfix r-m15 mobile-l-m0">
                                                                    {!! Form::text('name', null, ['class' => 'form-control capitalize donate-form', 'placeholder'
                                                                    => 'Event name *']) !!}
                                                                </article>
                                                            </article>
                                                             
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix r-m15 mobile-r-m0">
                                                                    {!! Form::text('contact_mail', null, ['class' => 'form-control donate-form', 'id'=>'email_reg',
                                                                    'placeholder' => 'Contact Email *']) !!}
                                                                </article>
                                                            </article>
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix dropdown"
                                                                         style="background:none;  border:none; padding:0 15px">
                                                                    {!! Form::select('category_id', $category_list, null,['class' => 'form-control donate-form','placeholder' => 'Event Category *']) !!}
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix r-m15 mobile-l-m0">
                                                                    {!! Form::text('start_date', null, ['class' => 'form-control datepickerstart donate-form','autocomplete'=>'off', 'placeholder'
                                                                    => 'Start Date']) !!}
                                                                </article>
                                                            </article>
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix l-m15 mobile-l-m0">
                                                                    {!! Form::text('end_date', null, ['class' => 'form-control datepickerend donate-form','autocomplete'=>'off', 'placeholder'
                                                                    => 'End Date']) !!}
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix dropdown"
                                                                         style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:2.2px 0px;">
                                                                    {!! Form::select('status', ['1'=>'Active', '0'=>'Inactive'],null, ['class' =>
                                                                    'form-control dropdown-select','style' => 'padding:0px 0px', 'id'
                                                                    =>'title']) !!}
                                                                </article>
                                                            </article>
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix l-m15 mobile-l-m0">
                                                                    {!! Form::text('location', null, ['class' => 'form-control donate-form', 'placeholder'
                                                                    => 'Location']) !!}
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix r-m15 mobile-l-m0">
                                                                    {!! Form::text('city', null, ['class' => 'form-control donate-form', 'placeholder'
                                                                    => 'City']) !!}
                                                                </article>
                                                            </article>
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix l-m15 mobile-l-m0">
                                                                    {!! Form::text('state', null, ['class' => 'form-control donate-form', 'placeholder'
                                                                    => 'State / Province / Region']) !!}
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix r-m15 mobile-r-m0">
                                                                    {!! Form::text('zipcode', null, ['class' => 'form-control donate-form',
                                                                    'placeholder' => 'Postal / Zip Code']) !!}
                                                                </article>
                                                            </article>
                                                            <article class="clearfix part-6">
                                                                <article class="clearfix l-m15 mobile-l-m0">
                                                                    {!!Form::file('banner_url',['placeholder' => 'Image','id'=>'image', 'class' =>
                                                                    'form-control donate-form image'])!!}
                                                                    @if(isset($event)&&$event->banner_url!='')
                                                                        <div class="productimage_view">
                                                                            <img class="preview_image"  id="image_preview" src="{{url('/uploads/events/'.$event->banner_url)}}"  style="max-height:200px; max-width:200px;"/>
                                                                            <br> <a href="javascript:void(0);"  class="deleteimage">Delete Image</a>
                                                                        </div>
                                                                    @endif
                                                                </article>
                                                            </article>
                                                        </article>


                                                        <h4 class="t-m0 OpenSans font20 sky-b-t b-p10 border-b orangeborder t-p10 b-m10">Registration Details</h4>
                                                        <article class="clearfix part-row b-p5">
                                                            <article class="clearfix part-2">
                                                                <article class="clearfix r-m15 mobile-r-m0">
                                                                    <label   class="l-gray-t font12 OpenSans b-m0">Is Registration</label>
                                                                    <input type="checkbox" value="1" style="opacity: unset;" class="is_registration" name="is_registration" @if($event->is_registration)checked @endif>

                                                                </article>
                                                            </article>
                                                            <div class="registration_date">
                                                                <article class="clearfix part-5">
                                                                    <article class="clearfix r-m15 mobile-l-m0">
                                                                        {!! Form::text('registration_start_date', null, ['class' => 'form-control datepickerstart donate-form','autocomplete'=>'off', 'placeholder'
                                                                        => 'Registration Start Date']) !!}
                                                                    </article>
                                                                </article>
                                                                <article class="clearfix part-5">
                                                                    <article class="clearfix l-m15 mobile-l-m0">
                                                                        {!! Form::text('registration_end_date', null, ['class' => 'form-control datepickerend donate-form','autocomplete'=>'off', 'placeholder'
                                                                        => 'Registration End Date']) !!}
                                                                    </article>
                                                                </article>
                                                            </div>
                                                        </article>

                                                        <div class="registration_date">
                                                            <h4 class="t-m0 OpenSans font20 sky-b-t b-p10 border-b orangeborder t-p10 b-m10">Registration Types <a href="javascript:void()" class="addtype btn btn-sm btn-warning f-r">Add Type</a></h4>
                                                            @foreach($event->regtypes as $registration_type)
                                                                <input type="hidden" name="regid[]" value="{{$registration_type->id}}">

                                                             <article class="clearfix part-row b-p5">
                                                                <article class="clearfix part-4">
                                                                    <article class="clearfix r-m15 mobile-l-m0">
                                                                        {!! Form::text('name[]', $registration_type->name, ['class' => 'form-control donate-form', 'placeholder'=> 'Registration Title']) !!}
                                                                    </article>
                                                                </article>
                                                                <article class="clearfix part-4">
                                                                    <article class="clearfix r-m15 mobile-l-m0">
                                                                        {!! Form::text('amount[]', $registration_type->amount, ['class' => 'form-control donate-form', 'placeholder' => 'Amount']) !!}
                                                                    </article>
                                                                </article>
                                                                <article class="clearfix part-4">
                                                                    <article class="clearfix r-m15 mobile-l-m0">
                                                                        {!! Form::text('reg_count[]',  $registration_type->reg_count, ['class' => 'form-control donate-form', 'placeholder' => 'Reg. Count']) !!}
                                                                    </article>
                                                                </article>

                                                            </article>
                                                            <article class="clearfix part-row b-p5">

                                                                <article class="clearfix part-4">
                                                                    <article class="clearfix r-m15 mobile-l-m0">
                                                                        {!! Form::text('order_no[]',  $registration_type->order_no, ['class' => 'form-control donate-form', 'placeholder'=> 'Order.no']) !!}
                                                                    </article>
                                                                </article>
                                                                <div class="col-md-1"><article class="clearfix part-4">
                                                                        <article class="clearfix r-m15 mobile-l-m0">
                                                                            <a href="#" data-id="{{$registration_type->id}}" class="btn btn-sm btn-info remove_list3 white-t ">Remove</a>
                                                                        </article></article></div></article>

                                                                @endforeach
                                                            <div class="childinfo"></div>

                                                            </article>

                                                            <div class="event_regtypedata"></div>

                                                        </div>

                                                        <article class="clearfix t-p10 t-c">
                                                            <input value="Submit" class="btn btn-danger btn-block buttoneditprofile border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color:#3e5066;margin-top: 30px;" type="submit">
                                                            <div class="loading"></div>
                                                        </article>
                                                    </article>
                                                    {!! Form::close() !!}
                                                </article>



                                            </ul>
                                        </article>
                                    </section>
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

@stop
@section('scripts')
    <script>
        $(document).ready(function () {

            if(!{{$event->is_registration}}) {
                $('.registration_date').hide();
            }

            $.validator.addMethod("zipcode",function(t,e){return this.optional(e)||t.match(/(^\d{5})$/)},"Invalid zip code");

            $(document).on('click','.addtype',function(){

           var childdata='<div class="childinfo"> <article class="clearfix part-row b-p5"> <article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0">         {!! Form::text('name[]', null, ['class' => 'form-control donate-form', 'placeholder' => 'Registration Title']) !!}     </article> </article> <article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0">         {!! Form::text('amount[]', null, ['class' => 'form-control datepicker donate-form', 'placeholder'         => 'Amount']) !!}     </article> </article> <article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0">         {!! Form::text('reg_count[]', null, ['class' => 'form-control donate-form', 'placeholder'         => 'Reg. Count']) !!}     </article> </article>  </article> </article><article class="clearfix part-row b-p5"> <article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0">         {!! Form::text('order_no[]', null, ['class' => 'form-control donate-form', 'placeholder'         => 'Order.no']) !!}   </article></article><div class="col-md-1"  xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"><article class="clearfix part-4">     <article class="clearfix r-m15 mobile-l-m0"> <a href="javascript:void()" class="btn btn-sm btn-info remove_list2 white-t">Remove</a></article></article></article></div></div></div>';
                $('.event_regtypedata').append(childdata);

            });
            $(document).on('click', '.remove_list2', function(event) {
                $(this).parents(".childinfo").remove();
            });
            $(document).on('click', '.remove_list3', function(event) {
        var id=$(this).attr("data-id");
        var data = "id="+id+"&_token={{ csrf_token() }}";
        $(this).parents(".childinfo").remove();
        $.ajax({
        url: '{{url('DeleteEventRegtype')}}',
        type: 'POST',
        data: data,
        success: function (response) {
                swal("Deleted!", "Deleted Registration type Successfully!", "success");
        }
        });
    });

            $(document).on('change', '.is_registration', function(e){
                if (this.checked == true){
                    $('.registration_date').show();
                } else {
                    $('.registration_date').hide();
                }

            });

            $("#dataform").validate({
                rules: {
                    name: "required",
                    email:"required",
                    category_id: "required",
                    location: "required",
                    zipcode: "zipcode",
                    city: "required",
                    state: "required",
                    banner_url: {extension: "jpg|png|gif|jpeg"}
                },
                messages: {
                    email: {required: "Required..!! "},
                    name: {required: "Required..!!"},
                    category_id: {required: "Required..!!"},
                    location: {required: "Required..!!"},
                    city: {required: "Required..!!"},
                    state: {required: "Required..!!"},
                    banner_url: {extension: 'Not an image!'}
                }
            });
            $(document).on('click', '.remove_list3', function(event) {
                var id=$(this).attr("data-id");
                var data = "id="+id+"&_token={{ csrf_token() }}";
                $(this).parents(".childinfo").remove();
                $.ajax({
                    url: '{{url('DeleteEventRegtype')}}',
                    type: 'POST',
                    data: data,
                    success: function (response) {
                        swal("Deleted!", "Deleted Registration type Successfully!", "success");
                    }
                });
            });


        });
    </script>
@stop