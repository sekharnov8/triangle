@extends('layouts.user.user')
@section('title', 'Profile')
@section('content')
  <section id="changepic" style="display: none; overflow: visible;">
        <article class="clearfix dialog-title t-c white-bg border-b b-p5"> <span class="pull-right r-p10 t-p10"><a class="cursor-p OpenSans-Semibold orange-t" onClick=" $('#changepic').dialog('close')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></span>
            <h3 class="font22 OpenSans-Semibold dk-black-t">Change Profile Picture</h3>
        </article>
        <article class="clearfix dialog-content">
            <article class="clearfix p30 mobile-p10 border-radius4">
            {!! Form::open(array('class' => 'profile_picture b-m0', "files" => true)) !!}
                    <label class="Roboto-Regular dk-t2">Upload Image *</label>
                    <article class="clearfix border p5">
                        <input type="file" id="profile-image-upload" name="profile_image" class="useraccount" accept="image/png,image/gif,image/jpeg,image/jpg">
                    </article>
                    <article class="clearfix t-p10 t-c">
                        <input value="Submit" class="btn btn-danger btn-block buttonprofileimage border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color:#3e5066;" type="button">
                    </article>
               {!! Form::close() !!}
            </article>
        </article>
    </section>
    <section class="clearfix wrapper innerpage-bg position-r">
     <article class="sidemenu tabhorizontal-hide" id="side4">
                <article class="clearfix side">
                    <article id="close">
                          <a onClick="leftpane2();"><img src="{{url('images/close1.png')}}" width="50" height="30"/></a>
                    </article>
                     <article id="open" class="hide">
                          <a onClick="leftpane3();"><img src="{{url('images/menu1.png')}}" width="50" height="30"/></a>
                    </article>
                </article></article>
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Profile</h2>
                </article>
                <div class="success_profile"></div>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0" style="min-height:745px!important;">
                    <article class="clearfix block-row">
                        <article class="clearfix left-block9" id="lb9">
                            <article class="clearfix profile-bg p-radius">
                                <article class="clearfix t-p35 b-p15 r-p20 mobile-p15 tabhorizontal-p20 position-r">
                                    <article class="clearfix t-c position-r">
                                        <a onClick="$(&quot;#changepic&quot;).dialog(&quot;open&quot;);"><i class="edit-pic"></i></a>
                 <img src="@if($user->profile_image && File::exists('uploads/users/'.$user->profile_image)) @if(File::exists('uploads/users/thumbs/th_'.$user->profile_image)) {{url('uploads/users/thumbs/th_'.$user->profile_image)}} @else {{url('uploads/users/'.$user->profile_image)}} @endif  @else {{url('images/profile-img.jpg')}} @endif " width="160" height="160" alt="" class="s-blueborder border3 border-radius4 userprofileimage" /></article>

                                      
                                    <h4 class="OpenSans-Semibold white-t font24 t-m0 t-p8 t-c">{{$user->title.' '.$user->first_name}}</h4>
                                    <h5 class="ltblue-t OpenSans font15 t-b-m0 l-h20 t-c">UID: <lable class="white-t OpenSans font15 t-b-m0 ">{{$user->member_id}}</lable></h5>
                                    <h5 class="ltblue-t OpenSans font15 t-b-m0 l-h20 t-c">Role: <lable class="white-t OpenSans font15 t-b-m0 ">{{getUserRoles($user->id)}}</lable></h5>
                                     @if(is_committee_member())
                                                <h5 class="ltblue-t OpenSans font15 t-b-m0 l-h20 t-c">Committee: <br/><lable class="white-t OpenSans font15 t-b-m0 ">{!! is_committee_member()!!}</lable></h5>
                                                @endif

                                </article>
                                <article class="clearfix border-t4 b-blueborder mobile-bor-t0">
                                    <article class="clearfix t-p35 b-p15 r-p20 mobile-p15 tabhorizontal-p20 position-r">
                                        <article class="clearfix tabhorizontal-p10">
                                            <ul class="clearfix list-pn m0 dashboard-list dis-b OpenSans font13 tabs">
                                               
                                                @include('layouts.user.includes.user_leftmenu_common')
                                                  <li  class="profileborder"><a href="{{url('logout')}}"><i class="logout-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Logout</span></a></li>
                                            </ul>
                                        </article>
                                    </article>
                                </article>
                            </article>
                        </article>
                        <article class="clearfix right-block9" id="rb9">
                            <article class="clearfix tab_container">
                                <article class="clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" rel="p"><a class="black-t">My Profile</a></article>
                                    @include('layouts.user.includes.notifications')
                                    <section id="p" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            <form>
                                                <article class="clearfix">
                                                    <h4 class="t-m0 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Personal Details</h4>

                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">First Name</label>
                                                                    </article>
                                                                    <article class="box-8 capitalize black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->first_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Middle Name</label>
                                                                    </article>
                                                                    <article class="box-8 capitalize black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->middle_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Last Name</label>
                                                                    </article>
                                                                    <article class="box-8 black-t capitalize t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->last_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Occupation</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->designation}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>

                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Email Id</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->email}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Mobile</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->mobile}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                    </article>

                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Gender</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->gender}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0 position-r">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">DOB</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->dob}}</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Member Skills</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->skills}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                           
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Referred by</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->referred_by}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                          
                                                    </article>
                                                     

                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Spouse Details</h4>
                                                    @if($spouse)
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">First Name</label>
                                                                    </article>
                                                                    <article class="box-8 black-t capitalize t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($spouse) {{$spouse->first_name}}@endif</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Last Name</label>
                                                                    </article>
                                                                    <article class="box-8 black-t capitalize t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($spouse){{$spouse->last_name}}@endif </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                       
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Mobile</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($spouse) {{$spouse->phone}} @endif </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Email</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($spouse) {{$spouse->email}} @endif </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                       
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Occupation</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($spouse) {{$spouse->occupation}} @endif </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Skills</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($spouse) {{$spouse->skills}} @endif </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    @else
                                                        <h4 style="font-size: 20px;">N/A</h4>
                                                        @endif
                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Children Details</h4>
                                                    @if(!count($childrens))
                                                        <h4 style="font-size: 20px;">N/A</h4>
                                                    @endif
                                                    @foreach ($childrens as $key=>$children)
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Name</label>
                                                                    </article>
                                                                    <article class="box-8 capitalize black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$children->first_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">DOB</label>
                                                                    </article>
                                                                    <article class="box-8 capitalize black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$children->dob}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Skills</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$children->skills}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Relation</label>
                                                                    </article>
                                                                    <article class="box-8 capitalize black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$children->relationship}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    @endforeach

                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 border-b orangeborder t-p10 b-m10">Contact Details</h4>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Address Line1</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->address}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Address Line2</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->address2}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">City</label>
                                                                    </article>
                                                                    <article class="box-8 capitalize capitalize black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->city}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">State</label>
                                                                    </article>
                                                                    <article class="box-8 black-t capitalize t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->state}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Country</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->country}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Zip Code</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->zipcode}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                              
                                                 </article>
                                            </form>
                                        </article>
                                    </section>
                                   

                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" rel="ep"><a class="black-t">Edit Profile</a></article>
                                    <section id="ep" class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10">
                                            {!! Form::open(array('route'=>'updateProfile','class' => 'edit_profile edit-form b-m0',  'id'=>'edit_profile', 'post' => true, 'files'=>true)) !!}
                                                <article class="clearfix">
                                                    <h4 class="t-m0 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Personal Details</h4>
                                                    <article class="clearfix part-row b-p5">

                                                        <article class="clearfix part-2">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix" style="background:none;  border:none; padding:0 15px 0px 0px">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Title</label>
                                                                    {!! Form::select('title', ['Mr.'=>'Mr.', 'Ms.'=>'Ms.', 'Mrs.'=>'Mrs.',
                                                                    'Dr.'=>'Dr.', 'Prof.'=>'Prof.', 'Others'=>'Others', ],$user->title, ['class' =>
                                                                    'form-control donate-form','placeholder' => 'Title', 'id'=>'title']) !!}
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-4">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">First Name</label>
                                                                <input type="text" name="first_name"  class="form-control capitalize donate-form"  value="{{$user->first_name}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-3">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Middle Name</label>
                                                                <input type="text" value="{{$user->middle_name}}"  name="middle_name"  class="form-control capitalize donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-3">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Last Name</label>
                                                                <input type="text" value="{{$user->last_name}}"  name="last_name"  class="form-control capitalize donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>

                                                    </article>

                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Email Id</label>
                                                                <input type="text" name="email" disabled class="form-control donate-form"  value="{{$user->email}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Mobile </label>
                                                                <input type="text" value="{{$user->mobile}}"  name="mobile"  class="form-control phone donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">

                                                       
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix  r-m15 dropdown mobile-r-m0"
                                                                     style="background:none;  border:none; padding-right: 15px"> <label class="l-gray-t font12 OpenSans b-m0">Gender</label>
                                                                {!! Form::select('gender', ['Male'=>'Male', 'Female'=>'Female','Transgender'=>'Transgender'], $user->gender,['class' => 'form-control
                                                                donate-form','placeholder' => 'Gender']) !!}
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Occupation</label>
                                                                <input type="text" value="{{$user->designation}}"  name="designation" value=""  class="form-control donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                       
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Member Skills</label>
                                                                <input type="text" name="skills"  class="form-control donate-form"  value="{{$user->skills}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Date Of Birth</label>
                                                                <input type="text" value="{{$user->dob}}"  name="dob" id="dob" autocomplete="off" class="form-control donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                     <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Referred by</label>
                                                                <input type="text" name="referred_by"  class="form-control  donate-form"  value="{{$user->referred_by}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                      
                                                       
                                                         
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix "
                                                                     style="background:none;  border:none; padding:0"> <label class="l-gray-t font12 OpenSans b-m0">Profile Image</label>
                                                               <input type="file" id="profileimage" name="profileimage" class="useraccount" accept="image/png,image/gif,image/jpeg,image/jpg">
                                                            </article>
                                                            @if($loggedin_user->profile_image && File::exists('uploads/users/'.$loggedin_user->profile_image))  
                                                            <img src="
                                        {{url('uploads/users/'.$loggedin_user->profile_image)}}" width="160" height="160" alt="" class="s-blueborder border3 border-radius4 userprofileimage" /> @endif
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Are you above 18 years?</label>
                                                                 <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="Yes" name="memberage" type="radio" @if($loggedin_user->memberage=='Yes')checked @endif>
                                                    <span for="radio1" class="dis-in lowgray-t1">Yes</span>
                                                </label>
                                                  <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="No" name="memberage" type="radio" @if($loggedin_user->memberage=='No')checked @endif >
                                                    <span for="radio1" class="dis-in lowgray-t1">No</span>
                                                </label>
                                                            </article>
                                                        </article>
 
                                                    </article>
                                                   
                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 border-b orangeborder t-p10 b-m10">Spouse Details</h4>
                                                    @if($spouse) <input type="hidden" name="spouse_id" value="{{$spouse->id}}"/> @endif
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">First Name</label>
                                                                <input type="text" name="spouse_first_name"  class="form-control capitalize donate-form"  value="@if($spouse){{$spouse->first_name}}@endif"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                               <label class="l-gray-t font12 OpenSans b-m0">Last Name</label>
                                                                <input type="text" name="spouse_last_name"  class="form-control capitalize donate-form"  value="@if($spouse){{$spouse->last_name}}@endif"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                       
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Occupation</label>
                                                                <input type="text" value="@if($spouse){{$spouse->occupation}}@endif"  name="spouse_occupation" class="form-control capitalize donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Skills</label>
                                                                <input type="text" name="spouse_skills"  class="form-control  donate-form"  value="@if($spouse){{$spouse->skills}}@endif"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Email Id</label>
                                                                <input type="text" name="spouse_email"  class="form-control childemail donate-form"  value="@if($spouse){{$spouse->email}}@endif"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Mobile</label>
                                                                <input type="text" value="@if($spouse){{$spouse->phone}}@endif"  name="spouse_phone"  class="form-control phone donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>

                                                    </article>
                                                     <article class="clearfix part-row b-p5">
                                                     <article class="clearfix part-6">
                                                            <article class="clearfix "
                                                                     style="background:none;  border:none; padding:0"> <label class="l-gray-t font12 OpenSans b-m0">Spouse Image</label>
                                                               <input type="file" id="spouseimage" name="spouseimage" class="useraccount" accept="image/png,image/gif,image/jpeg,image/jpg">
                                                            </article>
                                                            @if($spouse && $spouse->image && File::exists('uploads/users/'.$spouse->image))  
                                                            <img src="{{url('uploads/users/'.$spouse->image)}}" width="160" height="160" alt="" class="s-blueborder border3 border-radius4 userprofileimage" /> 
                                                            @endif
                                                        </article></article>

                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 border-b orangeborder t-p10 b-m10">Children Details
                                                        <article class="clearfix pull-right"><span class="OpenSans font13 check-t r-p7">Add Child</span><a class="add_child"><i class="plusicon">&nbsp;</i></a></article></h4>

                                                    <input type="hidden" id="childcount" name="childcount" value="{{count($childrens)}}">
                                                    @foreach ($childrens as $key=>$children)
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">First Name</label>
                                                                <input type="hidden" name="child_id[]"  class="form-control donate-form "  value="{{$children->id}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                                <input type="text"  name="child_firstname[]"   class="form-control capitalize donate-form alpha"  value="{{$children->first_name}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">DOB</label>
                                                                <input type="text" value="{{$children->dob}}"   name="child_dob[]"   class="form-control   datepicker donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Skills</label>
                                                                <input type="text"  name="child_skills[]"   class="form-control   donate-form"  value="{{$children->skills}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Relationship</label>
                                                                <input type="text" value="{{$children->relationship}}"   name="child_relation[]"  value=""  class="form-control donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    @endforeach
                                                    <div class="childdata"></div>
                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 border-b orangeborder t-p10 b-m10">Contact Details</h4>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Address Line1</label>
                                                                <input type="text" name="address" class="form-control donate-form"  value="{{$user->address}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Address Line2</label>
                                                                <input type="text" value="{{$user->address2}}"  name="address2" class="form-control donate-form" style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">City</label>
                                                                <input type="text" name="city"  class="form-control donate-form"  value="{{$user->city}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                       
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">
                                                                <article class="clearfix dropdown"
                                                                         style="background:none;   border:none; border-bottom:2px solid #e0e0e0; padding:2px 0px;">
                                                                    {!! Form::select('state', [''=>'Select State']+$states_data, $user->state,['class' => 'form-control dropdown-select','id' => 'state', 'style'=>'padding:0px 0px']) !!}
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
                                                    <article class="clearfix part-row b-p5">
                                                        
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <article class="clearfix dropdown"
                                                                         style="background:none;   border:none; border-bottom:2px solid #e0e0e0; padding:2px 0px;">
                                                                    {!! Form::select('country',  [''=>'Select Country']+$countries_data,$user->country, ['class' => 'form-control dropdown-select','style' => 'padding:0px 0px',
                                                                    'id' =>'country']) !!}
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <label class="l-gray-t font12 OpenSans b-m0">Zipcode</label>
                                                                <input type="text" name="zipcode" class="form-control donate-form zipcode"  value="{{$user->zipcode}}"  style="padding:3px 0!important; color:#3a3a3a!important;"/>
                                                            </article>
                                                        </article>
                                                    </article>
                                                 
                                                    <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                        <article class="clearfix t-p10 t-c">
                                                           <a href="{{url('account')}}"> <input value="Cancel" class="btn btn-danger btn-block buttoneditprofile border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color: rgb(255, 176, 58);width: 50%" type="button"></a>
                                                            <div class="loading"></div>
                                                        </article>
                                                        </article>
                                                            <article class="clearfix part-6">
                                                        <article class="clearfix pull-right t-p10 t-c">
                                                            <input value="Submit" class="btn btn-danger btn-block buttoneditprofile border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color:#3e5066;width:170px;" type="submit">
                                                            <div class="loading"></div>
                                                        </article>
                                                        </article>
                                                    </article>

                                                </article>
                                            {!! Form::close() !!}
                                        </article>
                                    </section> 
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section>

  

 <!-- START: Donation modal -->
 <div class="modal fade modal-size-large" id="donation_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h4>Edit Committee Member</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

             </div>

         </div>
     </div>
 </div>
 <!-- END: Donation modal -->

 <!-- START: Event modal -->
 <div class="modal fade modal-size-large" id="event_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h4>Add Committee Member</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">

                 <div class="clearfix"></div>
             </div>

         </div>
     </div>
 </div>
 <!-- END: Event modal -->
@stop
@section('scripts')
    <link media="all" type="text/css" rel="stylesheet" href="{{url('plugins/pikaday/pikaday.css')}}">
    <script src="{{url('plugins/pikaday/moment.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.jquery.js')}}"></script>

<script type="text/javascript" src="{{url('js/res-tab.js')}}"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker();
        var picker = new Pikaday(
                {
                    field: document.getElementById('dob'),
                    firstDay: 1,
                    format: 'MM-DD-YYYY',
                    minDate: new Date(1930, 0, 1),
                    maxDate: new Date(2016, 12, 31),
                    yearRange: [1930,2016]
                });
        picker.gotoYear('2000');
    } );
</script>
<script type="text/javascript">
        $(document).ready(function () {

 
            $('#changemail').dialog
            ({
                autoOpen: false,
                width: 420,
                modal: true
            });
             $('#changepic').dialog
            ({
                autoOpen: false,
                width: 420,
                modal: true
            });

            $('#changepassword').dialog
            ({
                autoOpen: false,
                width: 400,
                modal: true
            });

            $(".buttonprofileimage").click(function(e) {
               e.preventDefault();
            var formData  = new FormData($('.profile_picture')[0]);
 
            // var s=$('#profile-image-upload.files');
            // alert(JSON.stringify(s));

            // if(!s)
            // {
            //     alert('Attach picture');
            //     return false;
            // }
            $.ajax({
                url: '{{url('uploadProfilePicture')}}',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data.success)
                    {
                        $('.success_profile').html('<div class="alert alert-success">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                        img='{{url('uploads/users/')}}';
                        img=img+'/'+data.image;
                        $(".userprofileimage").attr("src", img);
                    }
                    else
                    {
                        $('.success_profile').html('<div class="alert alert-warning">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                    }
                    $('#changepic').dialog('close');
                }
            });
        });

        $.validator.addMethod("childemail", function(value, element) {

            if (value=='' || /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Please enter a valid Email.");

      

          $.validator.addMethod("alpha", function(value, element) {
                return this.optional(element)||value==value.match(/^[a-zA-Z\s]+$/);}, "Numbers/Special Charactesr not allowed");

   $.validator.addMethod("hasUppercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[A-Z]/.test(value);
}, "Must contain uppercase");
      $.validator.addMethod("hasLowercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[a-z]/.test(value);
}, "Must contain lowercase");

   $.validator.addMethod("hasSymbol", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+\-\d]/.test(value);
}, "Must contain symbol or digit");
            $("#edit_profile").validate({
                rules: {
                    first_name:  {required: true,alpha: true,maxlength:40},
                    last_name: {required: true,alpha: true,maxlength:40},
                    spouse_first_name: {alpha: true,maxlength:40},
                    spouse_middle_name: {alpha: true,maxlength:40},
                    spouse_last_name: {alpha: true,maxlength:40},
                    mobile: "required phone",
                    email:  "required email"
                },
                messages:
                {
                    first_name: {"required":"Enter your Name", "alpha":"Special characters/digits not allowed", "maxlength":"Maximum 40 characters only"},
                    last_name: {"required":"Enter your Name", "alpha":"Special characters/digits not allowed", "maxlength":"Maximum 40 characters only"},spouse_first_name: {"alpha":"Special characters/digits not allowed", "maxlength":"Maximum 40 characters only"},
                    spouse_first_name: {"alpha":"Special characters/digits not allowed", "maxlength":"Maximum 40 characters only"},
                    spouse_middle_name: {"alpha":"Special characters/digits not allowed", "maxlength":"Maximum 40 characters only"},
                    spouse_last_name: {"alpha":"Special characters/digits not allowed", "maxlength":"Maximum 40 characters only"},
                    mobile: {required:"Enter Mobile Number", phone:"Enter Valid mobile number"},
                    email:  {required:"Enter your email address",email:"Please Enter valid email"}
                }

        });
            $("#change_password").validate({
                rules: {
                    oldpwd: {required: true,maxlength:20},
                    pwd:  {required: true,maxlength:20,minlength: 8,hasSymbol:true,  hasUppercase: true, hasLowercase: true},
                    confirmpwd: {required: true,maxlength:20,minlength: 8,equalTo:'#pwd'}

                },
                messages:
                {
                    oldpwd: {"required":"Required..!",  "maxlength":"Maximum 20 characters only"},
                    pwd: {"required":"Required..!", "maxlength":"Maximum 20 characters only",minlength:"Minimum 8 characters", "hasSymbol":"Need Contain Digit or Symbol","hasUppercase":"Need atleast one capital letter","hasLowercase":"Need atleast one lower letter"},
                    confirmpwd: {"required":"Required..!", "equalTo":"Confirm password is not matched ", "maxlength":"Maximum 20 characters only",minlength:"Minimum 8 characters"}
                }

            });

            $(document).on('click','.add_child',function(){

                var childno = $('#childcount').val();

                var childdata='<div class="childinfo"><article class="clearfix part-row b-p5"><article class="clearfix part-6"><article class="clearfix r-m15 mobile-r-m0"><label class="l-gray-t font12 OpenSans b-m0">Name</label><input   name="child_firstname[]"  class="form-control capitalize alpha donate-form" type="text"></article></article><article class="clearfix part-6"><article class="clearfix l-m15 mobile-l-m0"> <label class="l-gray-t font12 OpenSans b-m0">DOB</label><input   name="child_dob[]"  class="form-control datepicker donate-form" type="text"></article></article><article class="clearfix part-row b-p5"><article class="clearfix part-6"><article class="clearfix r-m15 mobile-r-m0"><label class="l-gray-t font12 OpenSans b-m0">Skills</label><input   name="child_skills[]" class="form-control  donate-form" type="text"></article></article><article class="clearfix part-6"><article class="clearfix l-m15 mobile-l-m0"><article class="clearfix dropdown" style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:7px 0px;"><select class="dropdown-select" name="child_relation[]" style="padding:0px 0px;"><option value="">Select Relationship</option><option value="Son">Son</option><option value="Daughter">Daughter</option></select></article></article></article></article><div data-toggle="tooltip" title="Back" class="remove_list"></div></div>';

                 $('.childdata').append(childdata);
                 $('.datepicker').datepicker();

            });
            $(document).on('click', '.remove_list', function(event) {
                $(this).parents(".childinfo").remove();
            });
             $(document).on('click', '.renew_button', function(event) {

                $('.membership_info').hide();
                $('.renewal_membership').show();
            });

             $(document).on('click', '.membershiptype', function(event) {

                $('.membershipprice').val($(this).data('value'));
                $('.mtypename').html($(this).data('name'));
                $('.mtypeprice').html($(this).data('value'));
                $('.mtypeinfo').show();
                sumtotal();
            });

         $('.chequedate').datepicker({format: 'mm-dd-yyyy', autoclose: true,startDate: 'd'});
 });

function check2(str)
{
    var paymenttype=$("input[name='paymentmethod_id']:checked").val();
     if(str==1)
    {
      document.getElementById("alreadypayment").style.display="none";
    }
    else
    {
      document.getElementById("alreadypayment").style.display="block";
    }
}
function sumtotal()
{
    var mprice=$('.mtypeprice').html();

    if(mprice>0)
    {
        $('.paymentbutton').val('Proceed to Pay');
        $('.is_payment').show();
    }
    else
    {
        $('.paymentbutton').val('Proceed to Confirm');
        $('.is_payment').hide();
    }

}
</script>
@stop