@extends('layouts.user.inner')

@section('title')

@section('content')


    <section class="clearfix wrapper innerpage-bg position-r">

                    <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i2">
                        <article class="l-r-p35 t-p45 b-p25 mobile-p15 tabhorizontal-t-p15 tabhorizontal-minheight0" style="min-height:850px!important;">
                            <article class="l-r-m-auto t-c Roboto-Regular font26" style="max-width:610px;">
                                <article class="clearfix low-bg2" style="border-radius:20px;">
                                    <article class="clearfix green-bg t-b-p30" style="border-radius:20px;">
                                        <img src="../images/thankyou.png" width="114" height="113" border="0" alt="" />
                                        <h3 class="Montserrat-Regular t-t-u font34 white-t mobile-font24">Thanks for Upgrade Membership</h3>
                                    </article>
                                    <article class="clearfix t-b-p30 t-c l-r-m-auto lowgray-t3 font26 mobile-font18 mobile-l-r-p10 mobile-t-b-p15" style="max-width:405px;">
                                        Your details has been submitted to administrator for approval</article>
                                </article>
                            </article>
                            <article class="clearfix t-p20">
                                <article class="clearfix border-t3 greenborder border-radius5 shadow white-bg">
                                    <article class="clearfix part-row">
                                        <article class="clearfix part-6 border-r grayborder5">
                                            <article class="clearfix t-p15 b-p15">
                                                <h4 class="clearfix t-m0 Montserrat-Regular font15 dk-t4"><span class="border-b grayborder5 rp35 tabhorizontal-r-p10 b-p10"><span class="l-p25">Your Details</span></span></h4>
                                            </article>
                                            <article class="clearfix b-p25 l-p25">
                                                <h5 class="OpenSans dk-t4 capitalize t-m0 font15">{{$user->first_name}} {{$user->last_name}}</h5>
                                                <h5 class="OpenSans dk-t4 t-m0 font15">UID: {{$user->member_id}}</h5>
                                                <label class="OpenSans font14 lowgray-t3 l-h20">{{$user->address}}<br/>
                                                    {{$user->state}} {{$user->zipcode}}</label>
                                                <a href="mailto:{{$user->email}}" class="OpenSans font14 blue-t1 l-h20">{{$user->email}}</a></article>
                                        </article>
                                        <article class="clearfix part-6">
                                            <article class="clearfix t-p15 b-p15 mobile-t-p0">
                                                <h4 class="clearfix t-m0 Montserrat-Regular font15 dk-t4"><span class="border-b grayborder5 r-p45 b-p10"><span class="l-p25">Payment Received By</span></span></h4>
                                            </article>
                                            <article class="clearfix b-p25 l-p25">
                                                <h5 class="OpenSans dk-t4 t-m0 font15">{{$app_info->sitename}}</h5>
                                                <label class="OpenSans font14 lowgray-t3 l-h20">{{$app_info->address}}</label>
                                                <a href="mailto:{{$app_info->email}}" class="OpenSans font14 blue-t1 l-h20">{{$app_info->email}}</a></article>
                                        </article>
                                    </article>
                                </article>
                            </article>
                            @if($order && $order->amount>0)
                            <article class="clearfix t-p25 overflow_x-a">
                                <table class="table2 t-l" width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                    <tr>
                                        <th width="16.6%">Date & Time</th>
                                        <th width="23.7%">Transaction Details</th>
                                        <th width="28.2%">Summary</th>
                                        <th width="16.9%">Status</th>
                                        <th width="14.6%">Total</th>
                                    </tr>
                                    <tr>
                                        <td  width="16.6%">{{dateformat($order->created_at)}}</td>
                                        <td width="23.7%">@if($order){{$order->transaction_id}}
                                            @if($order->paymentmethod_id==2)
                                            Bank Name: {{$order->bankname}}<br/>Check No:  {{$order->cheque_no}}<br/>Check Date: {{$order->cheque_date}}
                                            @endif
                                            @endif
                                        </td>
                                        <td width="28.2%">{{$order->membershiptype->name}}</td>
                                        <td width="16.9%">{{getstatusByTypeAndNo(1,$order->status)}}</td>
                                        <td width="14.6%">@if($order)${{$order->amount}}  @endif</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </article>
                            @endif
                        </article>
                        <p style="padding: 0px 31px 50px 0px;">                                        <a href="{{url('/')}}"><input type="button" value="Go to Home" class="t-c Montserrat-Medium font18 pull-right border-radius25 white-t t-t-u" style="background-color:#ff9f10; padding:11px 15px;color: #ffffff;"></a></p>
                    </article>
    </section>
@stop
 