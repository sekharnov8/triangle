 <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Donation Details</h4>
 @if($donation->eventuser_id)
 <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Event </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventuser->event->name}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Amount</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> ${{$donation->amount}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Transaction Id </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventuser->transaction_id}}</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Status </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($eventuser->status==1)Confirmed @else Pending @endif</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Method</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($donation->paymentmethod){{$donation->paymentmethod->name}}@endif</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Order Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->created_at}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">First Name </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventuser->first_name}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">LastName</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventuser->last_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Email </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventuser->email}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Mobile</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventuser->mobile}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">City </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventuser->city}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">State</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventuser->state}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Zip Code </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$eventuser->zipcode}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Address</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$eventuser->address}} </article>
                                                                </article>
                                                            </article></article>
                                                            
                                                         <article class="clearfix part-12">
                                                            <article class="clearfix mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-2">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Donation Cause</label>
                                                                    </article>
                                                                    <article class="box-10 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->donation_cause}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
 @elseif($donation->order_id)
 <article class="clearfix part-row b-p5">
                                                     
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Amount</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> ${{$donation->amount}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Transaction Id </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$order->transaction_id}}</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Status </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($order->status==1)Confirmed @else Pending @endif</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Method</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($donation->paymentmethod){{$donation->paymentmethod->name}}@endif</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Order Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->created_at}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        @php $user=getUser($donation->user_id); 
                                                        @endphp
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">First Name </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->first_name}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">LastName</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->last_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Email </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->email}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Mobile</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->mobile}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">City </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->city}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">State</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->state}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Zip Code </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$user->zipcode}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Address</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$user->address}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Check Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$order->cheque_date}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Check No.</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$order->cheque_number}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Bank Name</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$order->bankname}} </article>
                                                                </article>
                                                            </article></article>
                                                         <article class="clearfix part-12">
                                                            <article class="clearfix mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-2">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Donation Cause</label>
                                                                    </article>
                                                                    <article class="box-10 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->donation_cause}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
 @else
 <article class="clearfix part-row b-p5">
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Service </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if(isset($donation->service)){{$donation->service->name}}@endif</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Amount</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> ${{$donation->amount}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Transaction Id </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$donation->transaction_id}}</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Status </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($donation->status==1)Confirmed @else Pending @endif</article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                         <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Payment Method</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>@if($donation->paymentmethod){{$donation->paymentmethod->name}}@endif</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Order Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->orderdate}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">First Name </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$donation->first_name}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">LastName</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->last_name}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Email </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$donation->email}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Mobile</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->mobile}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">City </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$donation->city}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">State</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->state}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                        <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Zip Code </label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$donation->zipcode}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Address</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->address}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-r-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Check Date</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span>{{$donation->cheque_date}}</article>
                                                                </article>
                                                            </article>
                                                        </article>

                                                        <article class="clearfix part-6">
                                                            <article class="clearfix l-m15 mobile-l-m0">

                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Check No.</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->cheque_number}} </article>
                                                                </article>
                                                            </article></article>
                                                            <article class="clearfix part-6">
                                                            <article class="clearfix r-m15 mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-4">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Bank Name</label>
                                                                    </article>
                                                                    <article class="box-8 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->bankname}} </article>
                                                                </article>
                                                            </article></article>
                                                         <article class="clearfix part-12">
                                                            <article class="clearfix mobile-l-m0">
                                                                <article class="clearfix box-row">
                                                                    <article class="clearfix box-2">
                                                                        <label class="l-gray-t font15 OpenSans b-m0">Donation Cause</label>
                                                                    </article>
                                                                    <article class="box-10 black-t t-p2 mobilev-t-p0 font14"> <span class="r-p5 OpenSans-Semibold mobilev-r-p0 tablet-r-p0">: </span> {{$donation->donation_cause}} </article>
                                                                </article>
                                                            </article>
                                                        </article>
                                                    </article>
@endif                                                    