 
            <div class="row"  >
               
                <div class="col-md-6"  >
                    Membership Type: 
                  @if($user->membership)   {{$user->membership->name}} @endif
                </div>
                <div class="col-md-6"  >
                    Is Approved:  {{getstatusByTypeAndNo(2,$user->is_approved)}}

                </div>
                <div class="col-md-6"  >
                    Referred By:   {{$user->referred_by}}
                </div>
                
            </div>

            <fieldset>
                <legend>Personal Details</legend>
                <div class="row"  >
                    <div class="col-md-6"  >
                       Title: 
                                             {{$user->title}}

                    </div>
                    <div class="col-md-6"  >
                       First Name: 
                       {{$user->first_name}}
                    </div>
                    <div class="col-md-6"  >
                       Middle Name: 
                        {{$user->middle_name}}
                    </div>
                    <div class="col-md-6"  >

                       Last Name: 
                         {{$user->last_name}}
                    </div>
                    <div class="col-md-6"  >

                       Email: 
                       {{$user->email}}
                    </div>
                    <div class="col-md-6"  >

                       Mobile Number: 
                        {{$user->mobile}}
                    </div>
                    <div class="col-md-6"  >

                       Alternative Number: 
                        {{$user->home_phone}}
                    </div>
                    
                    <div class="col-md-6"  >

                       DOB: 
                        {{$user->dob}}
                    </div>
                    <div class="col-md-6"  >
                       Profile Image
                       @if($user->profile_image && File::exists('uploads/users/'.$user->profile_image)) <img src="{{url('uploads/users/'.$user->profile_image)}}" width="160" height="160" alt="" class="s-blueborder border3 border-radius4 userprofileimage" />@endif 
                    </div> </div>
            </fieldset>

            <fieldset>
                <legend>Contact Details</legend>
                <div class="row"  >

                    <div class="col-md-6"  >

                       Occupation: 
                         {{$user->occupation}}
                    </div>
                    <div class="col-md-6"  >

                       Address: 
                       {{$user->address}}
                    </div>
                    <div class="col-md-6"  >

                       City: 
                       {{$user->city}}
                    </div>
                    <div class="col-md-6"  >

                       State: 
                         {{$user->state}}
                    </div>
                    <div class="col-md-6"  >

                       Country: 
 {{$user->country}}                    </div>
                    <div class="col-md-6"  >

                       Zipcode: 
 {{$user->zipcode}}                    </div>


                </div>

            </fieldset>
            @if($order)
               <fieldset>
                <legend>Last  Payment Order Details</legend>
                <div class="row"  >

                    <div class="col-md-6"  >

                        Membership Type : 
                   {{$order->membershiptype->name}}

                    </div>
                    <div class="col-md-6"  >
                       Price: $
                         {{$order->amount}}
                    </div>
                    <div class="col-md-6"  >
                       Transaction Id: 
                         {{$order->transaction_id}}
                    </div>
                     <div class="col-md-6"  >
                      Bank Name: 
                         {{$order->bank_name}}
                    </div>
                     <div class="col-md-6"  >
                         Check No.: 
                         {{$order->cheque_no}}
                    </div>
                     <div class="col-md-6"  >
                         Check Date: 
                         {{$order->cheque_date}}
                    </div>
                     <div class="col-md-6"  >
                         Donation Amount: 
                         {{numberformat($order->donation_amount)}}
                    </div>
                   
                    <div class="col-md-6"  > 
                       Payment Status:
                       @if(is_membership_chair()||is_technology_chair())
                       <select name="status" class=" form-control orderstatus" data-style="btn-outline-primary" data-id="{{$order->id}}" data-status="{{$order->status}}" style="width: 200px;">
                    @foreach($statustypes as $key=>$data)
                      <option value="{{$key}}"  @if($order->status == $key)  selected @endif >{{$data}}</option>
                  @endforeach
                </select>@else
                    {{getstatusByTypeAndNo(1,$order->status)}}
                @endif
                    </div>
                    <div class="col-md-6"  >
                       Payment Method: 
                     @if($order->paymentmethod) {{$order->paymentmethod->name}}@endif
                    </div>
                     <div class="col-md-6"  >
                       Created  at: 
                      {{dateformat($order->created_at)}}
                    </div>
                </div>
            </fieldset>
                     @endif

   <script>
        $(document).ready(function () { 

            $(document).on('change','.orderstatus',function(e){
                var thisVal = $(this);
                var id = $(this).data('id');
                var status = this.value;
                var prevStatus = $(this).data('status');
                e.stopImmediatePropagation();
                swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('change-member-order-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    //console.log(data);
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                            $(thisVal).selectpicker("refresh");
                        }
                    });

            });
 
        });
</script>             
