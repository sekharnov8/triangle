@extends('layouts.user.user')
@section('title', 'Profile')
@section('content')

    <section class="clearfix wrapper innerpage-bg position-r">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 tabhorizontal-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Profile</h2>
                </article>
                <div class="success_profile"></div>
                <article class="t-b-p30 tabhorizontal-p20 mobile-p15 tabhorizontal-t-p0 tabhorizontal-minheight0" style="min-height:745px!important;">
                    <article class="clearfix block-row">
                                 @include('layouts.user.includes.user_leftmenu')

                        <article class="clearfix right-block9" id="rb9">
                            <article class="clearfix tab_container">
                                <article class="clearfix t-b-m35 white-bg t-b-p30 l-r-p60  l-m60n tabhorizontal-l-m0 mobile-p15 mobile-t-b-m10 tabhorizontal-p30 shadow-p r-m35 tabhorizontal-r-m0 tabhorizontal-minheight0" style="min-height:555px;" id="negative" >
                                     @include('layouts.user.includes.notifications')
                                  
 
                                    <section    class="clearfix tab_content">
                                        <article class="clearfix mobile-l-m10 mobile-t-m10 membership_info" >
                                                <article class="clearfix">
                                                    <h4 class="t-m20 OpenSans font20 sky-b-t b-p10 b-m10 border-b orangeborder">Change Password</h4>
                                                           <article class="clearfix l-r-m-auto  t-p30 mobile-t-b-p15 mobile-maxwidth100 mobile-l-r-p10" style="max-width:450px;">
                                         
                                            <article class="clearfix white-bg login-shadow border-radius5 bm30 mobile-b-m0 l-r-p30 t-b-p20">
                                                {!! Form::open(array('route'=>'changePassword','class' => 'change_password b-m0',  'id'=>'change_password', 'post' => true)) !!}
                                                    <article class="clearfix position-r">
                                                        <input type="password" name="oldpwd" placeholder="Current Password *" id="oldpwd" class="donate-form form-control" style="" />
                                                    </article>
                                                    <article class="clearfix position-r">
                                                        <input type="password" name="pwd" placeholder="New Password *" id="pwd" class="form-control donate-form" style="" />
                                                        <lable class="notes">Need atleast one symbol or digit, one lower character and one upper character</lable>
                                                    </article>

                                                    <article class="clearfix">
                                                        <input type="password" name="confirmpwd" placeholder="Confirm New Password *" id="confirmpwd" class="form-control donate-form" />
                                                    </article>
                                                    <article class="clearfix t-p10 t-c">
                                                        <input type="submit" name="submit" class="btn-block border-radius5 Montserrat-Medium font22 white-t t-t-u t-b-p15" style="background-color:#3a4a5e;" value="Submit">
                                                    </article>
                                                    {!! Form::close() !!}
                                            </article>

                                        </article>
                                       

                                    </section>
  
                                    <article class="clearfix d_active tab_drawer_heading mobile-visible Roboto-Regular" ><a   href="{{url('logout')}}" class="black-t">Logout</a></article>
                                   
                                </article>
                            </article>
                        </article>
                    </article>
                </article>
            </article>
        </article>
    </section> 


 
@stop
@section('scripts')
    <link media="all" type="text/css" rel="stylesheet" href="{{url('plugins/pikaday/pikaday.css')}}">
    <script src="{{url('plugins/pikaday/moment.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.jquery.js')}}"></script>

<script type="text/javascript" src="{{url('js/res-tab.js')}}"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker();
        var picker = new Pikaday(
                {
                    field: document.getElementById('dob'),
                    firstDay: 1,
                    format: 'MM-DD-YYYY',
                    minDate: new Date(1930, 0, 1),
                    maxDate: new Date(2016, 12, 31),
                    yearRange: [1930,2016]
                });
        picker.gotoYear('2000');
    } );
</script>
<script type="text/javascript">
        $(document).ready(function () {
            $('#changemail').dialog
            ({
                autoOpen: false,
                width: 420,
                modal: true
            });
             $('#changepic').dialog
            ({
                autoOpen: false,
                width: 420,
                modal: true
            });
 
            $(".buttonprofileimage").click(function(e) {
               e.preventDefault();
            var formData  = new FormData($('.profile_picture')[0]);

            var s=$('input[type=file]')[0].files[0];

            if(!s)
            {
                alert('Attach picture');
                return false;
            }
            $.ajax({
                url: '{{url('uploadProfilePicture')}}',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data.success)
                    {
                        $('.success_profile').html('<div class="alert alert-success">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                        img='{{url('uploads/users/')}}';
                        img=img+'/'+data.image;
                        $(".userprofileimage").attr("src", img);
                    }
                    else
                    {
                        $('.success_profile').html('<div class="alert alert-warning">'+data.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
                    }
                    $('#changepic').dialog('close');
                }
            });
        });

      
          $.validator.addMethod("alpha", function(value, element) {
                return this.optional(element)||value==value.match(/^[a-zA-Z\s]+$/);}, "Numbers/Special Charactesr not allowed");
 $.validator.addMethod("hasUppercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[A-Z]/.test(value);
}, "Must contain uppercase");
      $.validator.addMethod("hasLowercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[a-z]/.test(value);
}, "Must contain lowercase");

   $.validator.addMethod("hasSymbol", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+\-\d]/.test(value);
}, "Must contain symbol or digit");

          $("#change_password").validate({
                rules: {
                    oldpwd: {required: true,maxlength:20},
                    pwd:  {required: true,maxlength:20,minlength: 8,hasSymbol:true,  hasUppercase: true, hasLowercase: true},
                    confirmpwd: {required: true,maxlength:20,minlength: 8,equalTo:'#pwd'}

                },
                messages:
                {
                    oldpwd: {"required":"Required..!",  "maxlength":"Maximum 20 characters only"},
                    pwd: {"required":"Required..!", "maxlength":"Maximum 20 characters only",minlength:"Minimum 8 characters", "hasSymbol":"Need Contain Digit or Symbol","hasUppercase":"Need atleast one capital letter","hasLowercase":"Need atleast one lower letter"},
                    confirmpwd: {"required":"Required..!", "equalTo":"Confirm password is not matched ", "maxlength":"Maximum 20 characters only",minlength:"Minimum 8 characters"}
                }

            });

   
  });
  
</script>
@stop