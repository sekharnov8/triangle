                             <h4 class="clearfix t-b-m0 t-b-p10 OpenSans font16 blue-t">Send Mail to Member</h4>
                              <article class="clearfix white-bg shadow border-t3 l-r-p20 t-b-p15 lowskyblueborder border-radius5">
                                <artsicle class="clearfix part-row b-p5">

                                   {!! Form::open(array('url' => 'member-sendmail', 'method' => 'POST')) !!}
<input type="hidden" name="user_id" value="{{$user->id}}"/>
<div class="row">
    <label for="image" class="control-label col-md-2">Subject *</label>
<div class="col-md-10"  >
    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject', 'required']) !!}
</div>
</div>
  
    <div class="row">

 
  <label for="image" class="control-label col-md-2">To Email *</label>

    <div class="col-md-10"  >
       {{$user->email}} 
     </div> 
 </div>
 
<div class="row">
<div class="col-md-12"  >
     {!! Form::textarea('description', null, ['class' => 'form-control textarea', 'placeholder' => 'Mail Body',  'required']) !!}
</div> 
                                                     
<div class="col-md-12 m-t10 t-r">
     {!! Form::submit('Send Mail', ['class' => 'btn btn-info ']) !!}
</div>
</div>
{!! Form::close() !!}</artsicle></article>
       