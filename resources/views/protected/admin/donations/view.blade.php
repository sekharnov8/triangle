<h4>Personal Details</h4>
<div class="row">
<div class="col-md-12">Name : {{$donation->title}} {{$donation->first_name}}  {{$donation->last_name}}  </div>
<div class="col-md-6">Email : {{$donation->email}} </div>
<div class="col-md-6">Mobile : {{$donation->mobile}} </div>
<div class="col-md-12">Address : {{$donation->address}} </div>
<div class="col-md-12">Address 2 : {{$donation->address2}} </div>
<div class="col-md-6">City : {{$donation->city}} </div>
<div class="col-md-6">State : {{$donation->state}} </div>
<div class="col-md-6">Zipcode : {{$donation->zipcode}} </div>
<div class="col-md-6">Country : {{$donation->country}} </div>
<div class="col-md-6">Occupation : {{$donation->occupation}} </div>
</div>
<h4>Payment Details</h4>
<div class="row">
<div class="col-md-6">Amount : {{$donation->amount}} </div>
<div class="col-md-6">Payment Method : {{$donation->paymentmethod->name}} </div>
<div class="col-md-6">Transaction ID : {{$donation->transaction_id}} </div>
<div class="col-md-6">Payment Date : {{dateformat($donation->created_at)}} </div>
<div class="col-md-6">Cheque Number : {{$donation->cheque_number}} </div>
<div class="col-md-6">Bank Name : {{$donation->bankname}} </div>
<div class="col-md-6">Cheque Date : {{$donation->cheque_date}} </div>
</div>
<h4>Other Details</h4>
<div class="row">
<div class="col-md-6">Is Anonymous : {{$donation->is_anonymous}} </div>
<div class="col-md-6">Status : @if($donation->status==1)Completed  @else Pending @endif </div>
<div class="col-md-6">IP Address : {{$donation->ip_address}} </div>
<div class="col-md-12">Donation for : {{$donation->donation_for}} </div>
<div class="col-md-12">Donation Cause : {{$donation->donation_cause}} </div>
<div class="col-md-12">Admin Notes : {{$donation->admin_notes}} </div>
<div class="col-md-12">Checked Services : {{$donation->checked_services}} </div>
</div>