<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>S.no</th>
        <th>Name</th>
        <th>Category</th>
        <th>Amount</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>Address 1</th>
        <th>Address 2</th>
        <th>City</th>
        <th>State</th>
        <th>Country</th>
        <th>Zipcode</th>
        <th>Payment Method</th>
        <th>Transaction Id</th>
        <th>Bank Name</th>
        <th>Check Number</th>
        <th>Check Date</th>
        <th>Check Payment as</th>
        <th>Check Handed to</th>
        <th>Comments</th>
        <th>Status</th>
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($service_donations as $data)
    <tr>
        <td>{{ $data->id }} </td>
        <td>{{ $data->title.' '.$data->first_name.' '.$data->last_name }}</td>
         <td>{{ $data->categoryname }}</td>
         <td>{{ $data->amount }}</td>
        <td>{{ $data->email }}</td>
        <td>{{ $data->mobile }}</td>
        <td>{{ $data->address }}</td>
        <td>{{ $data->address2 }}</td>
        <td>{{ $data->city }}</td>
        <td>{{ $data->state }}</td>
        <td>{{ $data->country }}</td>
        <td>{{ $data->zipcode }}</td>
        <td>@if($data->paymentmethod){{ $data->paymentmethod->name }}@endif</td>
        <td>{{ $data->transaction_id }}</td>
        <td>{{ $data->bankname }}</td>
        <td>{{ $data->cheque_number }}</td>
        <td>{{ $data->cheque_date }}</td>
        <td>{{ $data->payment_by }}</td>
        <td>{{ $data->handedto }}</td>
        <td>{{ $data->comments }}</td> 
        <td>
            @if($data->status ==1)
               Active
            @else
                Pending
            @endif
        </td>

        <td>{{ dateformat($data->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>