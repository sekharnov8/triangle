@extends('layouts.default')
@section('content_header_title','Donations')

@section('content')
    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title">
               
 <a href="{{route('admin_donations_export')}}" class="btn  btn-info btnexport"
                   data-toggle="tooltip" title="Export to Excel"><i class="fa fa-download" aria-hidden="true"></i>
                    Export to Excel</a>
            </h3>

        </div>

        <div class="box-body">
         <div class="row" style="margin:5px 0px 14px 14px;">


                <div class="col-sm-3 form-inline">
                    <lable class="control-label">Received Date</lable>
                    <div class="input-daterange input-group" id="datepicker">
                        <input type="text" class="input-sm form-control datepicker" name="datefrom" id="datefrom" value="" autocomplete="off" placeholder="Date from" />
                        <span class="input-group-addon">to</span>
                        <input type="text" class="input-sm form-control datepicker" name="dateto" id="dateto" value="" autocomplete="off" placeholder="Date to" />
                    </div>
                </div>
                <div class="col-sm-2">
                    <lable class="control-label">Keyword</lable>
                    <input type="text" name="keyword" class="form-control">
                </div> 
                 <div class="col-sm-2">
                    <lable class="control-label">Type</lable>
                    @if(isset($type))
                     {!! Form::select('donationtype', ['donation'=>'Donation','event'=>'Event', 'donationpage'=>'Donation Page','membership'=>'Membership'], $type,['class' => 'form-control','placeholder' => 'Donation Type']) !!}
                     @else
                     {!! Form::select('donationtype', ['donation'=>'Donation','event'=>'Event', 'donationpage'=>'Donation Page','membership'=>'Membership'], null,['class' => 'form-control','placeholder' => 'Donation Type']) !!}
                     @endif
                      </div>
                <div class="col-sm-2">
                    <lable class="control-label">Status</lable>
                    {!! Form::select('status', [1=>'Active',0=>'Pending'], null,['class' => 'form-control','placeholder' =>'Select Status']) !!}
                </div>
                    
                <div class="col-sm-3"><br>
                    <button type="button" id="dateSearch" class="btn btn-sm btn-primary">Search</button><span class="filter2" >
                                    <button type="button" id="dateClear" class="btn btn-sm btn-info">Clear</button>
                                          </span>
                </div>
            </div>
              {!! Form::open(['route' => 'admin_donations_actions', 'class' => 'form-horizontal','id' => 'products-actions-form']) !!}

                                  {!! Form::submit('Delete Checked Donations', ['name' => 'Trash',  'class' => 'btn btn-sm  btn-danger']) !!}
            <div class="table-responsive">


            <table class="table table-striped table-bordered table-hover" id="datalist">
                <thead>
                <tr >
                    <th><input type="checkbox" name="selectall" value="all" id="checkall" /></th>
                    <th>S.no</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Payment Method</th>
                    <th>Transaction Id</th>
                    <th width="100">Status</th>
                    <th width="200">Donation Cause</th>
                    <th>Created at</th>
                    <th width="115">Action</th>
                </tr>
                </thead>
            </table></div>
             {!! Form::close() !!}
        </div>
    </div>

    <!-- START: view modal -->
    <div class="modal fade modal-size-large" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>View Donation Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: view modal -->
@stop
@section('js')
    <script>
    $(function () {
            var oTable = $('#datalist').DataTable({
                scrollX: true,
                processing: true,
                serverSide: true,
                order: [[8, "desc"]],
                lengthMenu: [[10, 20, 30, 50], [10, 20, 30, 50]],
                ajax: {
                    url: '{{url('/admin/donations/data')}}',
                    data: function (d) {
                        d.keyword = $('input[name=keyword]').val();
                        d.datefrom = $('input[name=datefrom]').val();
                        d.dateto = $('input[name=dateto]').val();
                        d.status = $('select[name=status]').val();
                        d.donationtype = $('select[name=donationtype]').val();
                    }
                },
                columns: [
                    {data: 'cb', name: 'cb', searchable: false, orderable: false},
                    {data: 'DT_Row_Index', name: 'DT_Row_Index', searchable: false, orderable: false},
                    {data: 'user_name', name: 'user_name'},
                    {data: 'amount', name: 'amount'},
                    {data: 'paymentmethod', name: 'paymentmethod'},
                    {data: 'transaction_id', name: 'transaction_id'},
                    {data: 'status', name: 'status', searchable: false, orderable: false},
                    {data: 'donation_cause', name: 'donation_cause'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false}
                ]
            });
            $('.input-daterange').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: ''
            });
            $('#dateSearch').on('click', function () {
                $('.filter2').show();
                keyword= $('input[name=keyword]').val();
                datefrom = $('input[name=datefrom]').val();
                dateto = $('input[name=dateto]').val();
                donationtype=$('select[name=donationtype]').val();
                status=$('select[name=status]').val();
                $('.btnexport').attr('href', '/admin/donations-export?datefrom=' + datefrom + '&dateto=' + dateto+ '&keyword=' + keyword+ '&donationtype=' + donationtype+ '&status=' + status);
                 oTable.draw();
            });
            $("input[name=keyword]").keypress(function (event) {
                if (event.which == 13) {
                    $("#dateSearch").trigger("click");
                }
            });
            $('#dateClear').on('click', function () {
                $('.filter2').hide();
                $('input[name=keyword]').val('');
                 $('select[name=donationtype]').val('');
                $('select[name=status]').val('');
                $('.datepicker').datepicker('setDate', null);
                oTable.draw();
                $('.btnexport').attr('href', '/admin/donations-export'); 
                 
                $('.datepicker').datepicker('setDate', null);
                oTable.draw(); 
            });
            $('.dataTables_filter').hide();
            $("input[name=keyword]").keypress(function (event) {
                if (event.which == 13) {
                    $("#dateSearch").trigger("click");
                }
            });
            });

        $(document).on('change','.status',function(e){
            var thisVal = $(this);
            var id = $(this).data('id');
            var status = this.value;
            var prevStatus = $(this).data('status');
            e.stopImmediatePropagation();
            swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('admin/change-donation-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                        }
                    });

        });

        $('body').on('change', '#checkall', function() {
           var rows, checked;
           rows = $('#datalist').find('tbody tr');
           checked = $(this).prop('checked');
           $.each(rows, function() {
              var checkbox = $($(this).find('td').eq(0)).find('input').prop('checked', checked);
           });
         });

          $(document).on('click','.view',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/donations/')}}/'+id,
                    type: 'GET',
                    success: function(data){
                        $('#view_modal .modal-body').html(data);
                        $('#view_modal').modal('show'); 
                    }
                });

            });

        $(document).on('click','.delete',function(){

            var id = $(this).data('id');

            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Deleted!", "Your record has been deleted!", "success");
                                $('.id_' + id).parent().parent().remove();

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('/admin/donations') }}' + '/' + id,
                                type: 'DELETE',
                                dataType: 'json',
                                success: function(data){

                                }
                            });

                        } else {
                          //  swal("Cancelled", "Your record is safe :)", "error");
                        }
                    });

        });


    </script>
@stop
