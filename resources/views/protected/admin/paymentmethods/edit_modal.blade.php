

                    {!! Form::model($paymentmethod, array('route' => ['payment-methods.update', $paymentmethod->id], 'id' =>'dataform', 'method' => 'PUT',  'files' => true)) !!}

                    @include('protected.admin.forms.paymentmethods', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>

