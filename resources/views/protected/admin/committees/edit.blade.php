{!! Form::model($committee, array('route' => ['committees.update', $committee->id], 'method' => 'PUT','id' => 'dataform')) !!}

@include('protected.admin.forms.committees', ['submitButtonText' => 'Update'])

{!! Form::close() !!}
<div class="clearfix"></div>