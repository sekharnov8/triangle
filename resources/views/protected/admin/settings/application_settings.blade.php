@extends('layouts.default')
@section('content')
<div class="box">
            <!-- /.box-header -->
            <div class="box-header">
                <h3 class="box-title dis-in">Application Settings </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Application Name : </label>
                        <input type="text" placeholder="Application Name *" class="form-control" value="@objAppInfo.SiteName" name="SiteName" id="SiteName" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Company Address : </label>
                        <input type="text" placeholder="Company Address *" class="form-control" value="@objAppInfo.CompanyAddress"  name="CompanyAddress" id="CompanyAddress" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Company WebSite : </label>
                        <input type="text" placeholder="Company WebSite *" class="form-control" value="@objAppInfo.CompanyWebSite" name="CompanyWebSite" id="CompanyWebSite" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Company Email : </label>
                        <input type="text" placeholder="Company Email *" class="form-control" value="@objAppInfo.CompanyEmail" name="CompanyEmail" id="CompanyEmail" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Company Phone : </label>
                        <input type="text" placeholder="Company Phone *" class="form-control" value="@objAppInfo.CompanyPhone" maxlength="12" name="CompanyPhone" onkeypress="return Common.isNumberKey(event);" id="CompanyPhone" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>President Email : </label>
                        <input type="text" placeholder="President Email" class="form-control" value="@objAppInfo.PresidentEmail" name="PresidentEmail" id="PresidentEmail" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>President Phone : </label>

                        <input type="text" placeholder="President Phone" class="form-control" value="@objAppInfo.PresidentPhone" maxlength="12"  name="PresidentPhone" onkeypress="return Common.isNumberKey(event);" id="PresidentPhone"  />

                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Secretary Email :</label>
                        <input type="text" placeholder="Secretary Email" class="form-control" value="@objAppInfo.SecretaryEmail"  name="SecretaryEmail" id="SecretaryEmail" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Secretary Phone : </label>
                        <input type="text" placeholder="Secretary Phone" class="form-control" value="@objAppInfo.SecretaryPhone" maxlength="12"  name="SecretaryPhone" onkeypress="return Common.isNumberKey(event);" id="SecretaryPhone" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Customer Care Number : </label>
                        <input type="text" placeholder="Customer Care Number" class="form-control" value="@objAppInfo.CustomerCareNumber" maxlength="12" name="CustomerCareNumber" onkeypress="return Common.isNumberKey(event);" id="CustomerCareNumber" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Toll Free Number :  </label>
                        <input type="text" placeholder="Toll Free Number" class="form-control" value="@objAppInfo.TollFreeNumber" maxlength="12" name="TollFreeNumber" id="TollFreeNumber" onkeypress="return Common.isNumberKey(event);" >
                    </div>

                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Support Email : </label>
                        <input type="text" placeholder="Support Email" class="form-control" value="@objAppInfo.SupportEmail" name="SupportEmail" id="SupportEmail" >
                    </div>
                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Enquery Email : </label>
                        <input type="text" placeholder="Enquery Email" class="form-control" value="@objAppInfo.EnqueryEmail" name="EnqueryEmail" id="EnqueryEmail" >
                    </div>

                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <label>Page Items : </label>
                        <input type="text" placeholder="Page Items" class="form-control" value="@objAppInfo.PageItems" name="PageItems" id="PageItems" >

                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
                <h3 class="box-title">Social Site Details </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-lg-3 col-sm-6">
                        <label>Facebook Url : </label>
                        <input type="text"  placeholder="Facebook Url" class="form-control" value="@objAppInfo.FacebookUrl"   name="FacebookUrl" id="FacebookUrl" size="30">
                    </div>
                    <div class="col-xs-12 col-lg-3 col-sm-6">
                        <label>Twitter Url : </label>
                        <input type="text"  placeholder="Twitter Url" class="form-control" value="@objAppInfo.TwitterUrl" name="TwitterUrl" id="TwitterUrl" size="30">
                    </div>
                    <div class="col-xs-12 col-lg-3 col-sm-6">
                        <label>Youtube Url : </label>
                        <input type="text"  placeholder="Youtube Url" class="form-control" value="@objAppInfo.YoutubeUrl" name="YoutubeUrl" id="YoutubeUrl" size="30">
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body text-right">
                <input type="submit" value="Update" name="button2" class="btn btn-primary">
             </div>
        </div>
@stop