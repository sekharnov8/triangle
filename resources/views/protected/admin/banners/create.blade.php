@extends('layouts.default')
@section('content_header_title','Add Banner')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/banners') }}">Theme Banners</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('banners.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>

            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'banners.store', 'method' => 'POST', 'files' => true,'id'=>'dataform')) !!}
                        @include('protected.admin.forms.banners', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $("#dataform").validate({
                rules: {
                    name: "required alpha",
                    image: {required: true, extension: "jpg|png|gif|jpeg" }
                },
                messages:
                {
                    name:{required:"Required..!!",alpha:'Numbers/Special characters not allowed'},
                    image: {required:"Required..!!",extension: "Not an image!"}
                }
            });
        });
    </script>
@stop
