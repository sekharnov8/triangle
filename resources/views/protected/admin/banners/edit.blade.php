@extends('layouts.default')
@section('content_header_title','Edit Banner')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/banners') }}">Theme Banners</a></li>
@stop
@section('content')
    <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title pull-right">
                    <a href="{{ route('banners.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>

            </div>
                <div class="box-body">
                    {!! Form::model($banner, array('route' => ['banners.update', $banner->id], 'method' => 'PUT',  'files' => true,'id'=>'dataform')) !!}
                    @include('protected.admin.forms.banners', ['submitButtonText' => 'Update'])
                    {!! Form::close() !!}

                </div>
                </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {


    $('.deleteimage').on('click', function () {

        var data = "id={{$banner->id}}&_token={{ csrf_token() }}";
        $.ajax({
        url: '{{url('admin/DeleteBannerImage')}}',
        type: 'POST',
        data: data,
        success: function (response) {
             $('.bannerimage_view').html('');
                swal("Deleted!", "Deleted Image Successfully!", "success");
        }
        });
    });

            $("#dataform").validate({
                rules: {
                    name: "required alpha"
                },
                messages:
                {
                    name:{required:"Required..!!",alpha:'Numbers/Special characters not allowed'}
                }
            });
        });
    </script>
@stop