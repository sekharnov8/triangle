@extends('layouts.default')
@section('content_header_title','Edit Mail Template')
@section('breadcrumb_inner_content')
    <li><a href="{{ url('/admin/mail-templates') }}">Mail Template</a></li>
@stop

@section('content')

    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title pull-right">
                <a href="{{ route('mail-templates.index')}}" class="btn btn-success">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </h3>

        </div>

        <div class="box-body">

            {!! Form::model($template, array('route' => ['mail-templates.update', $template->id], 'method' => 'PUT',  'files' => true,'id' => 'dataform')) !!}

            @include('protected.admin.forms.mailtemplates', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}

        </div>
    </div>
@stop
@section('js')
    <script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
<script>
CKEDITOR.replace( 'template', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
</script>
    <script>

        $(function () {
            $("#dataform").validate({
                rules: {
                    template:  "required",
                    name: "required",
                    subject: "required"

                },
                messages:
                {
                    template:{required:"Required..!!"},
                    name:{required:"Required..!!"},
                    subject:{required:"Required..!!"}
                }
            });

        });
    </script>
@stop