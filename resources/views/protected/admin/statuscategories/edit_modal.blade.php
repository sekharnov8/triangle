

                    {!! Form::model($category, array('route' => ['status-categories.update', $category->id], 'id' =>'dataform', 'method' => 'PUT')) !!}

                    @include('protected.admin.forms.statuscategories', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>

