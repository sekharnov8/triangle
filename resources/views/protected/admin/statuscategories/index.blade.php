@extends('layouts.default')
@section('content_header_title','Status Categories')

@section('content')
<div class="box">

            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="javascript: void(0);" class="btn btn-success pull-right mr-1" data-toggle="modal" data-target="#add_cat">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp; Add Status Category
                    </a>

                </h3>

            </div>

                    <div class="box-body">
                    <table class="table table-striped table-bordered table-hover" id="datalist">
                        <thead>
                        <tr >
                            <th>S.No</th>
                            <th>Status Category</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $key=>$data)
                            <tr class="tr_{{ $data->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $data->category }}</td>
                                <td>
                                    <a href="javascript: void(0);" data-id="{{ $data->id }}" class='btn edit btn-sm btn-success' data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                                    <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>

                </div>

<!-- START: edit modal -->
<div class="modal fade modal-size-large" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit Status Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>
<!-- END: edit modal -->

<!-- START: Add modal -->
<div class="modal fade modal-size-large" id="add_cat" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Status Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'status-categories.store',  'id' =>'dataform','method' => 'POST')) !!}
                @include('protected.admin.forms.statuscategories', ['submitButtonText' => 'Submit'])
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
<!-- END: Add modal -->
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $("#dataform").validate({
                rules: {
                    category: "required"
                },
                messages:
                {
                    category:{required:"Enter Status Category"}
                }
            });

            $('#datalist').DataTable({
                responsive: true
            });
        }); 

        $(document).on('click','.edit',function(){

            var id = $(this).data('id');
            $.ajax({
                headers: {
                    'X-CSRF-Token' : '{{ csrf_token() }}'
                },
                url: '{{ url('admin/status-categories') }}'+'/'+id+'/edit',
                type: 'GET',
                success: function(data){
                    $('#edit_modal .modal-body').html(data);
                    $('#edit_modal').modal('show');
                    $("#dataform").validate({
                        rules: {
                            category: "required"
                        },
                        messages:
                        {
                            category:{required:"Enter Status Category"}
                        }
                    });

                    $('#edit_prod_form .remove-error').on('click', function(){
                        $('#form-validation-simple').removeError();
                    });
                }
            });

        });


        $(document).on('click','.delete',function(){

            var id = $(this).data('id');

            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Deleted!", "Your record has been deleted!", "success");
                            $('.tr_'+id).remove();

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('admin/status-categories') }}' + '/' +id,
                                type: 'DELETE',
                                dataType: 'json',
                                success: function(data){

                                }
                            });

                        } else {
                          //  swal("Cancelled", "Your record is safe :)", "error");
                        }
                    });

        });

    </script>
@stop
