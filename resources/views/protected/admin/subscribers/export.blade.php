<table class="table table-bordered table-hover">
    <thead>
    <tr>        
        <th>Email</th> 
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($subscribers as $data)
    <tr>
        <td>{{ $data->email }}</td>
        <td>{{ dateformat($data->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>