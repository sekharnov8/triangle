@extends('layouts.default')
@section('content_header_title','Subscribers')

@section('content')

<div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                  <a href="{{route('admin_subscribers_export')}}" class="btn  btn-info btnexport"
                   data-toggle="tooltip" title="Export to Excel"><i class="fa fa-download" aria-hidden="true"></i>
                    Export to Excel</a>
                </h3>
            </div>

                    <div class="box-body">
                    <table class="table table-striped table-bordered table-hover" id="datalist">
                        <thead>
                        <tr >
                            <th>S.No</th>
                            <th>Email</th>
                            <th>Created at</th>

							<th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
						@foreach ($subscribers as $key=>$data)
							<tr class="tr_{{ $data->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $data->email }} </td>
								<td>{{ dateformat($data->created_at,1) }} </td>

								<td>
                                @if ($data->slug != 'admin')
                               
                                     <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                                     @endif

								</td>
							</tr>
						@endforeach
						</tbody>
                    </table>
                    </div>

                </div>

@stop
@section('js')
    <script>
	$(function(){

		$('#datalist').DataTable({
			responsive: true
		});


         $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.tr_'+id).remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/subscribers') }}' + '/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            } else {
                                // swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });

            });

	});


</script>
@stop