@extends('layouts.default')
@section('content_header_title','Events Registers')
@section('content')
    <div class="box">
        <div class="box-header clearfix">
        {{$event->name}} event Registrations  
        </div>

        <div class="box-body">

            <table class="table table-striped table-bordered table-hover" id="datalist">
                <thead>
                <tr >
                    <th>S.No.</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Amount</th>
                    <th>Transaction Id</th>
                    <th>Created on</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $key=>$data)
                    <tr class="tr_{{ $data->id }}">
                        <td>{{ $key+1 }}</td>
                        <td>{{ $data->first_name }} &nbsp; {{ $data->last_name }}</td>
                        <td>{{ $data->mobile }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->amount_paid }}</td>
                        <td>{{ $data->transaction_id }}</td>
                        <td>{{ dateformat($data->created_at) }}</td>
                        <td>
                            <select name="status" class=" form-control status"  data-style="btn-outline-primary pt-0 pb-0" data-id="{{ $data->id}}"  data-status="{{$data->status}}">
                                <option value="1" @if($data->status == 1) selected @endif>Active</option>
                                <option value="0" @if($data->status == 0) selected @endif >Pending</option>
                            </select>
                        </td>
                        <td>
                            <a class='btn  btn-sm btn-success' href="{{ url('admin/events/'.$data->event_id.'/registrations/'.$data->id.'/edit')}}" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                            <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $('#datalist').DataTable({
                responsive: true
            });
        });

        $(document).on('change','.status',function(e){
            var thisVal = $(this);
            var id = $(this).data('id');
            var status = this.value;
            var prevStatus = $(this).data('status');
            e.stopImmediatePropagation();
            swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('admin/change-eventuser-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    //console.log(data);
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                            //$(thisVal).selectpicker("refresh");
                        }
                    });

        });

        $(document).on('click','.delete',function(){

            var id = $(this).data('id');

            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Deleted!", "Your record has been deleted!", "success");
                            $('.tr_'+id).remove();

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('/admin/events/'.$event->id.'/registrations/') }}' + '/' + id,
                                type: 'DELETE',
                                dataType: 'json',
                                success: function(data){

                                }
                            });

                        } else {
                          //  swal("Cancelled", "Your record is safe :)", "error");
                        }
                    });

        });


    </script>
@stop
