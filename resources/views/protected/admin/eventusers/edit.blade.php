@extends('layouts.default')
@section('content_header_title','Edit Event')
@section('breadcrumb_inner_content')
    <li><a href="{{ url('/admin/events') }}">Events Registrants Edit Section</a></li>
@stop
@section('content')

    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title pull-right">
                <a href="{{url('admin/events/'.$event->id.'/registrations')}}" class="btn btn-success"  >
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </h3>
        </div>
        <div class="box-body">
            {!! Form::model($reg, array('route' => ['events.registrations.update', $event->id, $reg->id], 'method' => 'PUT',  'files' => true)) !!}

            <fieldset>
                <legend>Registrant Details</legend>
                <div class="row"  >
                    <div class="col-md-4"  >
                        <label   class="control-label">First Name *</label>
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                    </div>
                    <div class="col-md-4"  >
                        <label   class="control-label">Last Name *</label>
                        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                    </div>
                    <div class="col-md-4"  >
                        <label   class="control-label">Mobile *</label>
                        {!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                    </div>

                </div>
                <div class="row"  >
                    <div class="col-md-4"  >
                        <label   class="control-label">Email *</label>
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                    </div>
                    <div class="col-md-4"  >
                        <label for="status" class="control-label">Status</label>
                        {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}
                    </div>
                    <div class="col-md-4"  >

                        <label   class="control-label">Address Line 1 </label>
                        {!! Form::text('address1', null, ['class' => 'form-control', 'placeholder' => 'Location']) !!}
                    </div>
                </div>
                <div class="row"  >

                    <div class="col-md-4"  >

                        <label   class="control-label">Address Line 2 </label>
                        {!! Form::text('address2', null, ['class' => 'form-control', 'placeholder' => 'Location']) !!}
                    </div>
                    <div class="col-md-4"  >

                        <label   class="control-label">City </label>
                        {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
                    </div>
                    <div class="col-md-4"  >

                        <label   class="control-label">State </label>
                        {!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'State']) !!}
                    </div>

                </div>
            </fieldset>
@if($reg->amount_paid>0)
            <fieldset>
                <legend>Payment Details</legend>
                <div class="row"  >
                    <div class="col-md-4"  >
                        <label   class="control-label">Transaction Amount</label>
                        {!! Form::text('amount_paid', null, ['class' => 'form-control', 'placeholder' => 'Transaction Amount']) !!}
                    </div>

                    <div class="col-md-4"  >
                        <label   class="control-label">Transaction Id</label>
                        {!! Form::text('transaction_id', null, ['class' => 'form-control', 'placeholder' => 'Transaction Id']) !!}
                    </div>
                    <div class="col-md-4"  >
                        <label   class="control-label">Payment Date</label>
                        {!! Form::text('payment_date', null, ['class' => 'form-control', 'placeholder' => 'Payment Date']) !!}
                    </div>

                </div>
                <div class="row"  >

                    <div class="col-md-4"  >
                        <label for="status" class="control-label">Payment Status</label>
                        {!! Form::select('status', [1=>'Completed', 0 =>'Pending'],null, ['class' => 'form-control','id' =>'status']) !!}
                    </div>
                    <div class="col-md-4"  >
                        <label for="status" class="control-label">Payment Method</label>
                        {!! Form::select('paymentmethod_id', [''=>'Select']+$paymentmethods,null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-4"  >
                        <label   class="control-label">Updated By</label>
                        {!! Form::text('updated_by', null, ['class' => 'form-control', 'placeholder' => 'Updated By']) !!}
                    </div>

                </div>


            </fieldset>
             @endif
             <fieldset>
                <legend>Comments</legend>
                <div class="row"  >
                    <div class="col-md-12"  >
                        {!! Form::textarea('comments', null, ['class' => 'form-control', 'placeholder' => 'Comments']) !!}
                    </div>

                </div>
            </fieldset>

             <fieldset>
                <legend>Registration Category Details</legend>
                <div class="row"  >
                   <table style="background-color: #fff;" class="table2 Poppins-Regular font14 dkblack-t3 b-m0 tabletext-l" width="100%" cellspacing="0" cellpadding="8" border="1">
                        <tbody>
                        <tr class="poppinssemibold" style="background-color: #cccccc;">
                            <th style="padding-left: 20px; vertical-align:middle!important;  text-align:center!important;" width="20%"  >Registration Category</th>
                            <th style="vertical-align:middle!important;  text-align:center!important;" width="24%"  >Price</th>
                             <th style="vertical-align:middle!important;  text-align:center!important;" width="9%"  >Count </th>
                            <th style="vertical-align:middle!important;  text-align:center!important;" width="13%" >Amount</th>
                        </tr>
                         @php $registrants=$reg->registrants->toArray() @endphp
                         @foreach($registrants as $rdata)
                            <tr class="">
                                <td style="text-align:center;" class="poppinssemibold">{{getEventRegtype($rdata['eventregistrationtype_id'])}} </td>

                                <td class="poppinssemibold" style="text-align:center;">${{$rdata['amount']}}</td>
                                <td  class="poppinssemibold" style="text-align:center;">
                                     {{$rdata['count']}}
                                </td>
                                <td style="text-align:center;" class="poppinssemibold">
                                    ${{number_format($rdata['count'] * $rdata['amount'])}}
                                </td>
                            </tr>
                             @endforeach
                            <tr>
                                <td colspan="3" align="right" style="padding-right: 25px;"><strong>Total</strong></td>
                                <td style="text-align:center;" class="poppinssemibold"> ${{number_format($reg->amount_paid)}}</td>
                            </tr>
                        </tbody></table>

                </div>

            </fieldset>

            <div class="col-md-12 m-t10 t-r">
                <a href="{{url('admin/events/'.$event->id.'/registrations')}}" class="btn btn-success" >Cancel</a>
                {!! Form::submit('Update', ['class' => 'btn btn-warning ']) !!}
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@stop
@section('js')
<script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
<script>
CKEDITOR.replace( 'description', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
</script>

    <script>
        $(document).ready(function () {


            $("#dataform").validate({
                rules: {
                    pagecategory_id:  "required",
                    title: "required",
                    display_order: "number",
                    description: "required"
                },
                messages:
                {
                    pagecategory_id:  {required:"Select Category"},
                    title:{required:"Enter Page Title"},
                    display_order:{number:"Enter Number only"},
                    description:{required:"Enter Description"}
                }
            });
        });
    </script>

@stop



