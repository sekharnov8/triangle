@extends('layouts.default')
@section('content_header_title','Paypal/Sendgrid Settings')
@section('css')
  <link rel="stylesheet" href="{{url('plugins/colorpicker/bootstrap-colorpicker.min.css')}}">
@stop
@section('content')
<div class="box">
            

                    <div class="box-body">
                    {!! Form::model($appinfo, array('url' => 'admin/payment-settings', 'method' => 'POST',  'files' => true,'id' => 'dataform')) !!}

  
    <label  class="control-label"><strong>Paypal Payment  Configuration</strong></label>
    <div class="row">
 <div class="col-md-6"  >
    <label  class="control-label">Paypal Mode </label>
          {!! Form::select('paypal_mode', ['live'=>'Live', 'sandbox' =>'Sandbox'],null, ['class' => 'form-control']) !!}
 </div></div>    <div class="row">

 <div class="col-md-6"  >
    <label  class="control-label">Paypal Sandbox Client Id</label>
 {!! Form::text('paypal_sandbox_client_id', null, ['class' => 'form-control ']) !!}
         </div>
          <div class="col-md-6"  >
    <label  class="control-label">Paypal Sandbox Secret</label>
 {!! Form::text('paypal_sandbox_secret', null, ['class' => 'form-control ']) !!}
         </div></div>
         <div class="row">

          <div class="col-md-6"  >
    <label  class="control-label">Paypal Live Client Id</label>
 {!! Form::text('paypal_client_id', null, ['class' => 'form-control ']) !!}
         </div>
          <div class="col-md-6"  >
    <label  class="control-label">Paypal Live Secret</label>
 {!! Form::text('paypal_secret', null, ['class' => 'form-control ']) !!}
         </div>
           
     </div>
    <label  class="control-label"><strong>Sendgrid Email Configuration</strong></label>
    <div class="row">
 <div class="col-md-6"  >
    <label  class="control-label">Sendgrid Email </label>
 {!! Form::text('sendgrid_email', null, ['class' => 'form-control ']) !!}
 </div>
 <div class="col-md-6"  >
    <label  class="control-label">Sendgrid API Key</label>
 {!! Form::text('sendgrid_api_key', null, ['class' => 'form-control ']) !!}
         </div>
           
     </div>
 
      <div class="row">
<div class="col-md-12 m-t10 t-r">
     {!! Form::submit('Update', ['class' => 'btn btn-info ']) !!}
</div>
</div>
{!! Form::close() !!}
</div></div>
@stop
@section('js') 
<script src="{{url('plugins/colorpicker/bootstrap-colorpicker.min.js')}}"></script>

<script>
 
 $(function () {

       
    $(".colorpicker2").colorpicker();

        });
</script>
@stop
