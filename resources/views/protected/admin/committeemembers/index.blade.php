@extends('layouts.default')
@section('content_header_title','Committee Members')
@section('css')
<style type="text/css">
    .select2-container{
        display: block !important;
        width: 100% !important;
    }
</style>
@endsection
@section('content')
    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title">
                <a href="javascript: void(0);" class="btn btn-success pull-right mr-1" data-toggle="modal" data-target="#add_cat">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp; Add Committee Member
                </a>

            </h3>

        </div>

        <div class="box-body table-responsive">
            {!! Form::open(['url' => 'admin/searchCommitteemembers','id' => 'search_form', 'method'=>'post']) !!}
            <div class="row">
                <div class="col-sm-1">Committees:  </div>
                <div class="col-sm-4">
                    <select name="committees" class="form-control">
                        <option value="0">All</option>
                        @foreach ($committees as $key=>$item)
                            <option value="{{$key}}" @if(isset($committee) && $committee == $key) selected @endif >{{$item}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-4"><button type="submit" id="dateSearch" class="btn btn-sm btn-primary">Search</button>
                                    <span class="filter2" @if(isset($committee)) @else style="display:none" @endif>
                                   <a href="{{url('/admin/committee-members')}}"><button type="button" id="dateClear" class="btn btn-sm btn-info">Clear</button></a>
                                          </span>
                </div>
            </div>
            {!! Form::close() !!}
            <br/>
            <table class="table table-striped table-bordered table-hover" id="datalist">
                <thead>
                <tr >
                    <th>S.No.</th>
                    <th>Committee</th>
                    <th>Member</th>
                    <th>Role</th>
                    <th width="90">Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Display Order</th>
                    <th width="80">Updated on</th>
                    <th width="85">Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($committeemembers as $key=>$data)
                    <tr class="tr_{{ $data->id }}">
                        <td>{{ $key+1 }}</td>
                        <td>@if($data->committee){{ $data->committee->name}}@endif</td>
                        <td>@if($data->user['first_name']){{ $data->user['first_name'].' '.$data->user['last_name'].' ('.$data->user['email'].')' }}@endif</td>
                        <td>@if($data->role_id){{ $roles[$data->role_id] }}@endif</td>
                        <td>
                            <select name="status" class="form-control status"  data-style="btn-outline-primary pt-0 pb-0" data-id="{{ $data->id}}"  data-status="{{$data->status}}">
                                <option value="0" @if($data->status == 0) selected @endif >Inactive</option>
                                <option value="1" @if($data->status == 1) selected @endif>Active</option>
                            </select>
                        </td>
                        <td>{{$data->display_order}}</td>
                        <td>{{ dateformat($data->updated_at) }}</td>
                        <td>
                            <a href="javascript: void(0);" data-id="{{ $data->id }}" class='btn edit btn-sm btn-success' data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                            <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

    <!-- START: edit modal -->
    <div class="modal fade modal-size-large" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Edit Committee Member</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: edit modal -->

    <!-- START: Add modal -->
    <div class="modal fade modal-size-large" id="add_cat" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Add Committee Member</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('route' => 'committee-members.store', 'method' => 'POST', 'id' => 'dataform')) !!}
                    @include('protected.admin.forms.committeemembers', ['submitButtonText' => 'Submit'])
                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Add modal -->

@stop
@section('js')
    <script>
        $(document).ready(function () {

            $(".user_id").select2({
                 dropdownParent: $("#add_cat"),
                allowClear: true
            });

            $("#dataform").validate({
                rules: {
                    user_id:  "required",
                    committee_id: "required",
                    role_id: "required"
                },
                messages:
                {
                    user_id:  {required:"Required..!!"},
                    committee_id:  {required:"Required..!!"},
                    role_id:  {required:"Required..!!"}
                }
            });
            $("#dataform2").validate({
                rules: {
                    committee_id: "required",
                    role_id: "required"
                },
                messages:
                {
                    committee_id:  {required:"Required..!!"},
                    role_id:  {required:"Required..!!"}
                }
            });

             $(document).on('change', '.committee_id', function(e){
            var $this=$(this);
            var committee_id = $this.val();
            $.ajax({
                url:  '{{ url('admin/get-user-filter-by-committee') }}',
                data: {"_token": "{{ csrf_token() }}",'committee_id':committee_id },
                success: function(data) {
                    if(data.length==0)
                    {
//                           $('.category').hide();
                        $('.selectpicker.user_id').empty();
                        $('.selectpicker.user_id').append('<option value="0">No User</option>');

                    }else{
                        $('.selectpicker.user_id').empty();
                        $('.selectpicker.user_id').append('<option value="">Select User</option>');
                        $.each( data, function( key, value ) {
                            $('.selectpicker.user_id').append("<option value='"+key+"'>"+ value + "</option>");
                        });
                        $(".selectpicker.user_id").selectpicker('refresh');
                    }
                },
                type: 'POST'
            });
        });


            $(document).on('change','.validity',function(e){
                var validity= $(this).val();
                if(validity==1)
                    $('.validitydate').hide();
                else
                    $('.validitydate').show();
            });

            $(document).on('change','.status',function(e){
                var thisVal = $(this);
                var id = $(this).data('id');
                var status = this.value;
                var prevStatus = $(this).data('status');
                e.stopImmediatePropagation();
                swal({
                            title: "Are you sure?",
                            text: "want to change this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, change it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Changed!", "Your record has been updated!", "success");
                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('admin/change-committeemember-status') }}',
                                    data: { 'id':id ,'status':status},
                                    type: 'POST',
                                    success: function(data){
                                        //console.log(data);
                                        $(thisVal).attr('data-status',status)
                                    }
                                });

                            } else {
                                swal("Cancelled", "Your record is safe :)", "error");
                                $(thisVal).val(prevStatus);
                                $(thisVal).selectpicker("refresh");
                            }
                        });

            });

            $('#datalist').DataTable({
                responsive: true
            });

            $(document).on('click','.edit',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/committee-members') }}'+'/'+id+'/edit',
                    type: 'GET',
                    success: function(data){
                        $('#edit_modal .modal-body').html(data);
                        $('#edit_modal').modal('show');


                        $(".user_id").select2({
                                         dropdownParent: $("#edit_modal"),
                                        allowClear: true
                                    });
                        $(".selectpicker.user_id").selectpicker('refresh');

                        $("#dataform2").validate({
                            rules: {
                                user_id:  "required",
                                committee_id: "required",
                                role_id: "required"
                            },
                            messages:
                            {
                                user_id:  {required:"Required..!!"},
                                committee_id:  {required:"Required..!!"},
                                role_id:  {required:"Required..!!"}
                            }
                        });

                        $('#edit_prod_form .remove-error').on('click', function(){
                            $('#form-validation-simple').removeError();
                        });
                    }
                });

            });

            $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.tr_'+id).remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/committee-members') }}'+'/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            } else {
                                // swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });

            });
        });

    </script>
@stop
