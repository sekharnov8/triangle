{!! Form::model($committeemember, array('route' => ['committee-members.update', $committeemember->id], 'method' => 'PUT','id' => 'dataform2')) !!}

@include('protected.admin.forms.committeemembers', ['submitButtonText' => 'Update'])

{!! Form::close() !!}
<div class="clearfix"></div>