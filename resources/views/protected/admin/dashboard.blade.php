@extends('layouts.default')
@section('content_header_title','Dashboard')
@section('content')
    <section class="content">
        <div class="row">
            
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$statistics['members']}}</h3>
                        <div><b>Members</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div><br>
                    <a href="{{url('/admin/members')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$statistics['banners']}}</h3>
                        <div><b>Banners</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file-image-o"></i>
                    </div><br>
                    <a href="{{url('/admin/banners')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>  
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>{{$statistics['enquiries']}}</h3>
                        <div><b>Enquiries</b></div>
                    </div>
                    <div class="icon">
                        <i class="ion ion-document"></i>
                    </div><br>
                    <a href="{{url('/admin/contacts')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

             <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{{$statistics['subscribers']}}</h3>
                        <div><b>Subscribers</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div><br>
                    <a href="{{url('/admin/subscribers')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{$statistics['events']}}</h3>
                        <div><b>Events</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-calendar"></i>
                    </div><br>
                    <a href="{{url('/admin/events')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            

             <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-olive">
                    <div class="inner">
                        <h3>{{$statistics['sponsors']}}</h3>
                        <div><b>Sponsors</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div><br>
                    <a href="{{url('/admin/sponsors')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

             <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$statistics['pages']}}</h3>
                        <div><b>Pages</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-files-o"></i>
                    </div><br>
                    <a href="{{url('/admin/pages')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-maroon">
                    <div class="inner">
                        <h3>{{$statistics['articles']}}</h3>
                        <div><b>Articles</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-files-o"></i>
                    </div><br>
                    <a href="{{url('/admin/articles')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
             <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-fuchsia">
                    <div class="inner">
                        <h3>{{$statistics['news']}}</h3>
                        <div><b>News</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-files-o"></i>
                    </div><br>
                    <a href="{{url('/admin/news')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
             <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3>{{$statistics['roles']}}</h3>
                        <div><b>User Roles</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div><br>
                    <a href="{{url('/admin/roles')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
             <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$statistics['donations']}}</h3>
                        <div><b>Received Donations</b></div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-dollar"></i>
                    </div><br>
                    <a href="{{url('/admin/donations')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>
@stop