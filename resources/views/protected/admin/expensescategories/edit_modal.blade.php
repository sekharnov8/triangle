{!! Form::model($category, array('route' => ['expenses-categories.update', $category->id], 'method' => 'PUT','id' => 'dataform')) !!}

@include('protected.admin.forms.expensescategories', ['submitButtonText' => 'Update'])

{!! Form::close() !!}
<div class="clearfix"></div>




