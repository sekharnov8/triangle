@extends('layouts.default')
@section('content_header_title','Edit News')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/news') }}">News</a></li>
@stop

@section('content')

    <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title pull-right">
                    <a href="{{ route('news.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>

            </div>

                <div class="box-body">

                    {!! Form::model($news, array('route' => ['news.update', $news->id], 'method' => 'PUT',  'files' => true,'id' => 'dataform')) !!}

                    @include('protected.admin.forms.news', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
                </div>
@stop
@section('js')
<script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
<script>
CKEDITOR.replace( 'description', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
</script>
<script>

$(function () {

    $("#dataform").validate({
        rules: {
            description: {
                required: true,
                minlength: 5,
                maxlength: 30,
                lettersonly: true
            },
            name: "required"

        },
        messages:
        {
            description:{required:"Description Required"},
            name:{required:"Title Required"}

        }
    });

    $('.deleteimage').on('click', function () {

        var data = "id={{$news->id}}&_token={{ csrf_token() }}";
        $.ajax({
        url: '{{url('admin/DeleteNewsImage')}}',
        type: 'POST',
        data: data,
        success: function (response) {
             $('.newsimage_view').html('');
                swal("Deleted!", "Deleted Image Successfully!", "success");
        }
        });
    });

 });
</script>
@stop