@extends('layouts.default')
@section('content_header_title','Add News')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/news') }}">News</a></li>
@stop


@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('news.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'news.store', 'method' => 'POST','id' => 'dataform', 'files' => true)) !!}
                        @include('protected.admin.forms.news', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop

@section('js')

<script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
<script>
CKEDITOR.replace( 'description', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
</script>
<script>
$("#dataform").validate({
    rules: {
        description:  "required",
        name: "required",
        image: {extension: "jpg|png|gif|jpeg" }

    },
    messages:
    {
        description:{required:"Required..!!"},
        name:{required:"Required..!!"},
        image: {extension: "Not an image!"}

    }
});
</script>
@stop
