

                    {!! Form::model($category, array('route' => ['video-categories.update', $category->id], 'id' =>'dataform', 'method' => 'PUT',  'files' => true)) !!}

                    @include('protected.admin.forms.videocategories', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>

