@extends('layouts.default')
@section('content_header_title','Add Category')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/video-categories') }}">Video Categories</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('video-categories.index')}}" class="btn btn-xs btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'video-categories.store', 'method' => 'POST', 'files' => true)) !!}
                        @include('protected.admin.forms.videocategories', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
