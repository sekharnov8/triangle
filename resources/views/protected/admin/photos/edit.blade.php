@extends('layouts.default')
@section('content_header_title','Edit Photo')
@section('breadcrumb_inner_content')
<li><a href="{{ url('/admin/photos') }}">Photo</a></li>
@stop
@section('content')

<div class="box">
    <div class="box-header clearfix">
        <h3 class="box-title pull-right">
            <a href="{{ route('photos.index')}}" class="btn btn-success">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
        </h3>
    </div>
    <div class="box-body">
        {!! Form::model($photo, array('route' => ['photos.update', $photo->id], 'id' =>'dataform', 'method' => 'PUT', 'files' => true)) !!}

        @include('protected.admin.forms.photos', ['submitButtonText' => 'Update'])

        {!! Form::close() !!}

    </div>
</div>
@stop

@section('js')

<script>
    $(document).ready(function() {
 
        $("#dataform").validate({
            rules: {

                image: {
                    extension: "jpg|png|gif|jpeg"
                },
                photocategory_id: "required"
            },
            messages: {

                image: {
                    extension: "Not an image!"
                },
                photocategory_id: {
                    required: "Required..!"
                }
            }
        });

        $('.deleteimage').on('click', function() {

            var data = "id={{$photo->id}}&_token={{ csrf_token() }}";
            $.ajax({
                url: '{{url('
                admin / DeletePhotoImage ')}}',
                type: 'POST',
                data: data,
                success: function(response) {
                    $('.photoimage_view').html('');
                    swal("Deleted!", "Deleted Image Successfully!", "success");
                }
            });
        });

        $('#datalist').DataTable({
            responsive: true
        });
    });
</script>
@stop