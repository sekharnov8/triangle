@extends('layouts.default')
@section('content_header_title','Add Photo')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/photos') }}">Photos</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('photos.index')}}" class="btn  btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'photos.store','id' =>'dataform', 'method' => 'POST', 'files' => true)) !!}
                        @include('protected.admin.forms.photos', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
@section('js')
 
<script>
  

     $(document).ready(function () {

         $("#dataform").validate({
             rules: {

                 images: {required: true, extension: "jpg|png|gif|jpeg" },
                 photocategory_id: "required"
             },
             messages:
             {

                 images: {required:"Required..!",extension: "Not an image!"},
                 photocategory_id:{required:"Required..!"}
             }
         });

         $('#datalist').DataTable({
             responsive: true
         });
     });
</script>
@stop
