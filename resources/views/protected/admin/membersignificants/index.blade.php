@extends('layouts.default')
@section('content_header_title','Member Significants')

@section('content')
<div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                  <a href="javascript: void(0);" class="btn btn-warning pull-right mr-1" data-toggle="modal" data-target="#add_cat">
                <small><i class="icmn-plus mr-1"></i></small>
                Add Significant
            </a>

                </h3>

            </div>

                    <div class="box-body">
                    <table class="table table-striped table-bordered table-hover" id="datalist">
                        <thead>
                        <tr >
                            <th>S.No.</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>DOB</th>
                            <th>Skills</th>
                            <th>Relationship </th>
                            <th >Occupation</th>
                             <th>Updated on</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($msignificants as $key=>$data)
                            <tr class="tr_{{ $data->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $data->first_name }}</td>
                                <td>{{ $data->last_name }}</td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->dob }}</td>
                                <td>{{ $data->skills }}</td>
                                <td>{{ $data->relationship }}</td>
                                <td>{{ $data->occupation }}</td>

                                 <td>{{ dateformat($data->updated_at) }}</td>
                                <td width="80">
                                     <a href="javascript: void(0);" data-id="{{ $data->id }}" class='btn edit btn-sm btn-success' data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                                     <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>

                </div>

<!-- START: edit modal -->
    <div class="modal fade modal-size-large" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Edit Significant</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: edit modal -->

 <!-- START: Add modal -->
    <div class="modal fade modal-size-large" id="add_cat" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Add Significant</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                {!! Form::open(['route' => ['members.significants.store',$user->id], 'method' => 'POST', 'id' => 'dataform']) !!}
                        @include('protected.admin.forms.membersignificants', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}

                        <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Add modal -->

@stop
@section('js')
    <script>
        $(document).ready(function () {

$("#dataform").validate({
            rules: {
                first_name:  "required",
                relationship: "required",
                last_name: "required"
            },
            messages:
            {
                first_name:  {required:"Enter First Name"},
                last_name:{required:"Enter Last Name"},
                relationship:{required:"Required"}
            }
        });

            $(document).on('change','.status',function(e){
                var thisVal = $(this);
                var id = $(this).data('id');
                var status = this.value;
                var prevStatus = $(this).data('status');
                e.stopImmediatePropagation();
                swal({
                            title: "Are you sure?",
                            text: "want to change this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, change it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Changed!", "Your record has been updated!", "success");
                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('admin/members/'.$user->id.'/significants-status') }}',
                                    data: { 'id':id ,'status':status},
                                    type: 'POST',
                                    success: function(data){
                                        //console.log(data);
                                        $(thisVal).attr('data-status',status)
                                    }
                                });

                            } else {
                                swal("Cancelled", "Your record is safe :)", "error");
                                $(thisVal).val(prevStatus);
                               // $(thisVal).selectpicker("refresh");
                            }
                        });

            });

            $('#datalist').DataTable({
                responsive: true
            });

            $(document).on('click','.edit',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/members/'.$user->id.'/significants') }}'+'/'+id+'/edit',
                    type: 'GET',
                    success: function(data){
                        $('#edit_modal .modal-body').html(data);
                        $('#edit_modal').modal('show');
                        $('.datepicker').datepicker();
                        $("#dataform").validate({
                            rules: {
                                first_name:  "required",
                                relationship: "required",
                                last_name: "required"
                            },
                            messages:
                            {
                                first_name:  {required:"Enter First Name"},
                                relationship:  {required:"Required"},
                                last_name:{required:"Enter Last Name"}
                            }
                        });

                        $('#edit_prod_form .remove-error').on('click', function(){
                            $('#form-validation-simple').removeError();
                        });
                    }
                });

            });

         $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.tr_'+id).remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/members/'.$user->id.'/.significants') }}'+'/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            } else {
                                // swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });

            });
       });

    </script>
@stop
