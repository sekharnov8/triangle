@extends('layouts.default')
@section('content_header_title','Member Orders')

@section('content')
<div class="box">
<div class="box-header clearfix">
            <h3 class="box-title">
                <a href="{{route('admin_orders_export')}}" class="btn  btn-info btnexport"
                   data-toggle="tooltip" title="Export to Excel"><i class="fa fa-download" aria-hidden="true"></i>
                    Export to Excel</a>
                    </h3>
        </div>
         <div class="box-body">
            <div class="row">

                                                    <div class="col-sm-4 form-inline">
                                                        <lable class="control-label">Date Registered</lable>
                                                        <div class="input-daterange input-group" id="datepicker">
                                                            <input type="text" class="input-sm form-control datepicker" name="datefrom" id="datefrom"
                                                                   value="" placeholder="Date from"/>
                                                            <span class="input-group-addon">to</span>
                                                            <input type="text" class="input-sm form-control datepicker" name="dateto" id="dateto" value=""
                                                                   placeholder="Date to"/>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <lable class="control-label">Keyword</lable>
                                                        <input type="text" name="keyword" class="form-control">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <lable class="control-label">Status</lable>
                                                        {!! Form::select('status', [''=>'All']+$statustypes, null,['class' => 'form-control','placeholder' =>'Select Status']) !!}
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <lable class="control-label">Membership Type</lable>
                                                        {!! Form::select('mtype', $mtypes, null,['class' => 'form-control','placeholder' =>
                                                        'Membership Type']) !!}
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <lable class="control-label">Payment Method</lable>
                                                        {!! Form::select('paymentmethod', $paymentmethods+['0'=>'Free'], null,['class' => 'form-control','placeholder' =>
                                                        'Payment Method']) !!}
                                                    </div>
                                                    <div class="col-sm-3"><br>
                                                        <button type="button" id="dateSearch" class="btn  btn-info">Search</button>
                                                        <span class="filter2" style="display:none">
                              <button type="button" id="dateClear" class="btn  btn-warning">Clear</button>
                             </span>
                                                    </div>
                                                </div> 
                                                  {!! Form::open(['route' => 'admin_orders_actions', 'class' => 'form-horizontal','id' => 'products-actions-form']) !!}

 
                                  {!! Form::submit('Delete Checked Orders', ['name' => 'Trash',  'class' => 'btn btn-sm  btn-danger']) !!}

  <table class="table table-striped table-bordered table-hover" id="datalist2">
                <thead>
                <tr >
                    <th><input type="checkbox" name="selectall" value="all" id="checkall" /></th>
                    <th>S.No.</th>
                            <th>Member Id</th>
                            <th>Member Name</th>
                            <th>Membership</th>
                            <th>Payment Method</th>
                            <th>Payment Status</th>
                            <th>Transaction Id</th>
                            <th>Amount</th>
                               <th>Updated on</th>
                            <th width="90">Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th></tr>
                </thead>
              <tbody>
                </tbody>
            </table>
                   {!! Form::close() !!}
                    </div>

                </div>
 
     <!-- START: view modal -->
    <div class="modal fade modal-size-large" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>View Member Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: view modal -->
<!-- START: edit modal -->
    <div class="modal fade modal-size-large" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Edit Member Order</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: edit modal -->
@stop
@section('js')
    <script>
        $(function () {
             var oTable = $('#datalist2').DataTable({
                processing: true,
                serverSide: true,
                order: [[9, "desc"]],
                lengthMenu: [[10, 20, 30, 50], [10, 20, 30, 50]],
                ajax: {
                    url: '{{url('admin/memberorders/data')}}',
                    data: function (d) {
                        d.keyword = $('input[name=keyword]').val();
                        d.datefrom = $('input[name=datefrom]').val();
                        d.dateto = $('input[name=dateto]').val();
                        d.status = $('select[name=status]').val();
                        d.mtype = $('select[name=mtype]').val();
                        d.paymentmethod = $('select[name=paymentmethod]').val();
                    }
                },
                columns: [
                    {data: 'cb', name: 'cb', searchable: false, orderable: false},
                    {data: 'id', name: 'id'},
                    {data: 'member_id', name: 'member_id'},
                    {data: 'user_name', name: 'user_name'},
                   {data: 'membership', name: 'membership'},
                    {data: 'paymentmethod', name: 'paymentmethod'},
                    {data: 'paymentstatus', name: 'paymentstatus', searchable: false, orderable: false},
                    {data: 'transaction_id', name: 'transaction_id'},
                    {data: 'amount', name: 'amount'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false}
                ]
            });
            
            $('.input-daterange').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: ''
            });
           $('#dateSearch').on('click', function () {
                $('.filter2').show();
                datefrom = $('input[name=datefrom]').val();
                dateto = $('input[name=dateto]').val();
                keyword = $('input[name=keyword]').val();
                paymentmethod = $('select[name=paymentmethod]').val();
                mtype = $('select[name=mtype]').val();
                status = $('select[name=status]').val();
                exporturl='/admin/orders-export?s=';
                if (datefrom && dateto)
                    exporturl=exporturl+'&datefrom=' + datefrom + '&dateto=' + dateto;
                if (paymentmethod!='')
                    exporturl=exporturl+'&paymentmethod=' + paymentmethod;
                if (keyword!='')
                    exporturl=exporturl+'&keyword=' + keyword;
                if (mtype!='')
                    exporturl=exporturl+'&mtype=' + mtype;
                 if (status!='')
                    exporturl=exporturl+'&status=' + status;
                    $('.btnexport').attr('href', exporturl);
                oTable.draw();
            });
            $('#dateClear').on('click', function () {
                $('.filter2').hide();
                $('input[name=keyword]').val('');
                $('select[name=status]').val('');
                $('select[name=mtype]').val('');
                $('select[name=paymentmethod]').val('');
                $('.btnexport').attr("href", "/admin/orders-export");                
                $('input[name=datefrom]').val('');
                $('input[name=dateto]').val('');
                oTable.draw();
            });
            $('.dataTables_filter').hide();
            $("input[name=keyword]").keypress(function (event) {
                if (event.which == 13) {
                    $("#dateSearch").trigger("click");
                }
            }); 

             $('body').on('change', '#checkall', function() {
           var rows, checked;
           rows = $('#datalist2').find('tbody tr');
           checked = $(this).prop('checked');
           $.each(rows, function() {
              var checkbox = $($(this).find('td').eq(0)).find('input').prop('checked', checked);
           });
         });

  $(document).on('click','.view',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/members/')}}/'+id,
                    type: 'GET',
                    success: function(data){
                        $('#view_modal .modal-body').html(data);
                        $('#view_modal').modal('show'); 
                    }
                });

            });
            $(document).on('change','.status',function(e){
                var thisVal = $(this);
                var id = $(this).data('id');
                var status = this.value;
                var prevStatus = $(this).data('status');
                e.stopImmediatePropagation();
                swal({
                            title: "Are you sure?",
                            text: "want to change this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, change it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Changed!", "Your record has been updated!", "success");
                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('admin/change-memberorder-status') }}',
                                    data: { 'id':id ,'status':status},
                                    type: 'POST',
                                    success: function(data){
                                        //console.log(data);
                                        $(thisVal).attr('data-status',status)
                                    }
                                });

                            } else {
                                swal("Cancelled", "Your record is safe :)", "error");
                                $(thisVal).val(prevStatus);
                                $(thisVal).selectpicker("refresh");
                            }
                        });

            });
 
            $(document).on('click','.edit',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/memberorders') }}'+'/'+id+'/edit',
                    type: 'GET',
                    success: function(data){
                         $('#edit_modal').modal('show'); 
                         $('#edit_modal .modal-body').html(data);
                        $('#edit_prod_form .remove-error').on('click', function(){
                            $('#form-validation-simple').removeError();
                        });
                    }
                });

            });

         $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.id_' + id).parent().parent().remove();
                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/memberorders') }}'+'/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            }
                        });

            });
       });

    </script>
@stop
