@extends('layouts.default')
@section('content_header_title','Member Orders')

@section('content')
<div class="box">
            <div class="box-header clearfix">
               

            </div>

                    <div class="box-body">
                    <table class="table table-striped table-bordered table-hover" id="datalist">
                        <thead>
                        <tr >
                            <th>S.No.</th>
                             <th>Member Name</th>
                            <th>Membership</th>
                            <th>Payment Method</th>
                            <th>Transaction Id</th>
                            <th>Amount</th>
                            <th>Payment Status</th>
                              <th>Updated on</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($morders as $key=>$data)
                            <tr class="tr_{{ $data->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $data->user_name }}</td>
                                <td>{{ $data->membership }}</td>
                                <td>{{ $data->paymentmethod }}</td>
                                <td>{{ $data->transaction_id }}</td>
                                <td>{{ $data->amount }}</td>
                                <td>
                            @if($data->status == 0) Pending @else Completed @endif 
                                </td>
                                 <td>{{ dateformat($data->updated_at) }}</td>
                                <td>
                                     <a href="javascript: void(0);" data-id="{{ $data->id }}" class='btn edit btn-sm btn-success' data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                                     <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>

                </div>  

                <!-- START: edit modal -->
    <div class="modal fade modal-size-large" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Edit Member Order</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: edit modal -->

@stop
@section('js')
    <script>
            $(function () {

          $(document).on('click','.edit',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/memberorders') }}'+'/'+id+'/edit',
                    type: 'GET',
                    success: function(data){
                         $('#edit_modal').modal('show'); 
                         $('#edit_modal .modal-body').html(data);
                        $('#edit_prod_form .remove-error').on('click', function(){
                            $('#form-validation-simple').removeError();
                        });
                    }
                });

            });
  $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.id_' + id).parent().parent().remove();
                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/memberorders') }}'+'/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            }
                        });

            });
       });


    </script>
@stop
