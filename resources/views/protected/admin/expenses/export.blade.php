<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Category</th>
        <th>Event</th>
        <th>User</th>
        <th>Receipt</th>
        <th>Payment Status</th>
        <th>Spent Date</th>
        <th>Comments</th>
        <th>Paid Date</th>
        <th>Paid Amount</th>
        <th>Treausrer Comments</th>
        <th>Committee</th>
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($expenses as $data)
    <tr>
        <td>@if($data->category){{ $data->category->name }}@endif</td>
        <td>@if($data->event){{ $data->event->name }}@endif</td>
        <td>@if($data->user){{ $data->user->first_name.' '.$data->user->last_name }}@endif</td>
        <td>{{ $data->receipt }}</td>
        <td>{{ $data->payment_status }}</td>
        <td>{{ $data->spent_date }}</td>
        <td>{{ $data->comments }}</td>
        <td>{{ $data->paid_date }}</td>
        <td>{{ $data->paid_amount }}</td>
        <td>{{ $data->treasurer_comments }}</td>
        <td>@if($data->committee){{ $data->committee->name }}@endif</td>
        <td>{{ dateformat($data->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>