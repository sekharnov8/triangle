 
        <div class="row">
         <div class="col-md-6"  >
    <label for="image" class="control-label">Name *</label>
{!! Form::text('name', null, ['class' => 'form-control capitalize', 'placeholder' => 'Name']) !!}
        </div>

                            <div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
                      {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

        </div>
</div>
<div class="row">
  <div class="col-md-12"  >

    <label for="status" class="control-label">Image *</label>

        {!!Form::file('image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
        @if(isset($greeting)&&$greeting->image!='')
            <div class="bannerimage_view">
            <img class="preview_image"  id="image_preview" src="{{url('/uploads/greetings/'.$greeting->image)}}"  style="max-height:200px; max-width:200px;"/>
                 </div>
        @else
            <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
        @endif
        {!! errors_for('image', $errors) !!}
      <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Banner size min be minimum 600px width</label>

 </div>

 </div>
 <div class="row">
    <div class="col-md-6"  >

    <label for="image" class="control-label">Redirect URL</label>
{!! Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'URL']) !!}

        </div>
           <div class="col-md-6"  >

    <label for="status" class="control-label">Target</label>
                      {!! Form::select('target', ['_blank'=>'Blank', '_parent' =>'Parent'],null, ['class' => 'form-control']) !!}
        </div>
        </div>
        <div class="row">
         <div class="col-md-6"  >

    <label for="image" class="control-label">Width</label>
                 {!! Form::text('width', null, ['class' => 'form-control']) !!}

        </div>
        <div class="col-md-6"  >

    <label for="image" class="control-label">Top Padding</label>
                 {!! Form::text('toppadding', null, ['class' => 'form-control']) !!}

        </div>
           <div class="col-md-6"  >

    <label for="image" class="control-label">Expire Date (Blank if no need)</label>
{!! Form::text('expiredate', null, ['class' => 'form-control datepickerstart', 'placeholder' => 'Expire Date']) !!}

        </div>
        </div>
                   <div class="col-md-12 m-t10 t-r">
                       <a href="{{ route('greetings.index')}}" class="btn btn-success" >Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-warning ']) !!}
                  </div>

