<div class="row">
<div class="col-md-6"  >

    <label for="name" class="control-label">Title *</label>
{!! Form::text('name', null, ['class' => 'form-control gbl_title', 'placeholder' => 'Title']) !!}

        </div>

 <div class="col-md-6"  >
    <label class="control-label">Category *</label>
    <select name="pagecategory_id" class="form-control">
<option value="">Select Category</option>
  @foreach ($category_list as $item)
    <option value="{{$item['id']}}" @if(isset($page) && $item['id'] == $page->pagecategory_id) selected @endif >{{$item['name']}}</option>
    @foreach(pagecategories_sublist($item['id']) as $subitem)
    <option value="{{$subitem->id}}" @if(isset($page) && $subitem->id == $page->pagecategory_id) selected @endif>| {{$subitem->name}}</option>
    @foreach(pagecategories_sublist($subitem->id) as $subitem2)
    <option value="{{$subitem2->id}}" @if(isset($page) && $page->pagecategory_id == $subitem2->id) selected @endif>| | {{$subitem2->name}}</option>
@endforeach
@endforeach
    @endforeach
</select>
         </div>
         </div>
<div class="row">
        <div class="col-md-6"  >

    <label for="title" class="control-label">Slug</label>
    @if(isset($page))
        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug']) !!}
    @else
        {!! Form::text('slug', null, ['class' => 'form-control slug', 'placeholder' => 'Slug']) !!}
     @endif
        </div>
 <div class="col-md-6"  >

    <label for="image" class="control-label">Display Order</label>
             @if(isset($maxno))
                 {!! Form::number('display_order', $maxno, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
             @else
                 {!! Form::number('display_order', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
             @endif
        </div>
        </div>
        <div class="row">
        <div class="col-md-12"  >
    <label class="control-label">Description</label>
    {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
        </div></div>
        <div class="row">
 <div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
                      {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

        </div>
    <div class="col-md-6"  >

    <label for="image" class="control-label">Is Login</label>
                      {!! Form::select('is_login', [ '0' =>'No', '1'=>'Yes'],null, ['class' => 'form-control']) !!}

        </div>
</div>
<div class="row">
   <div class="col-md-6"  >

        <label for="title" class="control-label">URL</label>
        {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'URL']) !!}
    </div>
         
         <div class="col-md-6"  >

    <label for="image" class="control-label">Target</label>
                      {!! Form::select('target', ['_parent'=>'Parent', '_blank' =>'Blank'],null, ['class' => 'form-control', 'placeholder'=>'Target']) !!}

        </div>


        </div>
        <div class="row">
          <div class="col-md-6"  >

    <label  class="control-label">Meta Description</label>
{!! Form::text('meta_description', null, ['class' => 'form-control', 'placeholder' => 'Meta Description']) !!}

        </div>
          <div class="col-md-6"  >

    <label  class="control-label">Meta Keywords</label>
{!! Form::text('meta_keywords', null, ['class' => 'form-control', 'placeholder' => 'Meta Keywords']) !!}

        </div>
          </div>
                   <div class="col-md-12 m-t10 t-r">
                       <a href="{{ route('pages.index')}}" class="btn btn-success"  >Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-warning ']) !!}
                  </div>
