<fieldset>
    <legend>Donation Details</legend>
    <div class="row"  >
        <div class="col-md-4"  >
            <label   class="control-label">First Name *</label>
            {!! Form::text('first_name', null, ['class' => 'form-control capitalize', 'placeholder' => 'First name']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Last Name *</label>
            {!! Form::text('last_name', null, ['class' => 'form-control capitalize', 'placeholder' => 'Last Name']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Mobile *</label>
            {!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile']) !!}
        </div>

    </div>
    <div class="row"  >
        <div class="col-md-4"  >
            <label   class="control-label">Email *</label>
            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
        </div>
        <div class="col-md-4"  >
            <label for="status" class="control-label">Status</label>
            {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}
        </div>
        <div class="col-md-4"  >

            <label   class="control-label">Address</label>
            {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Location']) !!}
        </div>
    </div>
    <div class="row"  >
    <div class="col-md-4"  >

            <label   class="control-label">Address Line 2</label>
            {!! Form::text('address2', null, ['class' => 'form-control', 'placeholder' => 'Address Line 2']) !!}
        </div>

        <div class="col-md-4"  >

            <label   class="control-label">City </label>
            {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
        </div>
        <div class="col-md-4"  >

            <label   class="control-label">State </label>
            {!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'State']) !!}
        </div>

    </div>
     <div class="row"  >
    <div class="col-md-4"  >

            <label   class="control-label">Zipcode</label>
            {!! Form::text('zipcode', null, ['class' => 'form-control', 'placeholder' => 'Zipcode']) !!}
        </div>

        <div class="col-md-4"  >

            <label   class="control-label">Country </label>
            {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'Country']) !!}
        </div>
        
    </div>
</fieldset>

<fieldset>
    <legend>Payment Details</legend>
    <div class="row"  >
        <div class="col-md-4"  >
            <label   class="control-label">Transaction Amount</label>
            {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Transaction Amount']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Transaction Id</label>
            {!! Form::text('transaction_id', null, ['class' => 'form-control', 'placeholder' => 'Transaction Id']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Payment Date</label>
            {!! Form::text('payment_date', null, ['class' => 'form-control', 'placeholder' => 'Payment Date']) !!}
        </div>

    </div>
    <div class="row"  >
        <div class="col-md-4"  >
            <label   class="control-label">Cheque Number</label>
            {!! Form::text('cheque_number', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Bank Name</label>
            {!! Form::text('bankname', null, ['class' => 'form-control', 'placeholder' => 'Bank Name']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Cheque  Date</label>
            {!! Form::text('cheque_date', null, ['class' => 'form-control', 'placeholder' => 'Cheque Date']) !!}
        </div>

    </div>
    <div class="row"  >
        <div class="col-md-4"  >
            <label   class="control-label">Payment Status</label>
            {!! Form::text('payment_status', null, ['class' => 'form-control', 'placeholder' => 'Payment Status']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Payment Method</label>
            {!! Form::text('payment_method', null, ['class' => 'form-control', 'placeholder' => 'Payment Method']) !!}
        </div>
        <div class="col-md-4"  >
            <label   class="control-label">Updated By</label>
            {!! Form::text('updated_by', null, ['class' => 'form-control', 'placeholder' => 'Updated By']) !!}
        </div>
    </div>


</fieldset>

<div class="col-md-12 m-t10 t-r">
    <a href="{{ route('donations.index')}}" class="btn btn-success" >Cancel</a>
    {!! Form::submit('Update', ['class' => 'btn btn-warning ']) !!}
</div>