<div class="row">

                            <div class="col-md-6"  >

    <label for="cssclass" class="control-label">Role *</label>
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter the role name']) !!}
        </div>
<div class="col-md-6"  >

    <label for="title" class="control-label">Slug *</label>
{!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter the role slug']) !!}
        </div>
        </div>

                   <div class="col-md-12 m-t10 t-r">
                       <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
                  </div>
