<div class="row">
<div class="col-md-6"  >

    <label for="title" class="control-label">Name *</label>
{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}

        </div>
             <div class="col-md-6"  >

    <label for="image" class="control-label">Sponsor Site URL</label>
{!! Form::text('site_url', null, ['class' => 'form-control', 'placeholder' => 'URL']) !!}
        </div>
        </div>
        <div class="row">
        <div class="col-md-6 clearfix"  >
    <label  lass="control-label">Category *</label>
             {!! Form::select('category_id',$categories, null,['class' => 'form-control selectpicker category','placeholder' => 'Select Category']) !!}
 

        </div>
          <div class="col-md-6"  >

    <label for="image" class="control-label">Display Order</label>
              @if(isset($maxno))
                  {!! Form::number('order_no', $maxno, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
              @else
                  {!! Form::number('order_no', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
              @endif

       </div>
</div>
<div class="row">



  <div class="col-md-12"  >

    <label for="status" class="control-label">Image</label>

        {!!Form::file('image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
        @if(isset($sponsor)&&$sponsor->logo_url!='')
            <div class="sponsorimage_view">
            <img class="preview_image"  id="image_preview" src="/uploads/sponsors/{{$sponsor->logo_url}}"  style="max-height:200px; max-width:200px;"/>
            <br> <a href="javascript:void(0);"  class="deleteimage">Delete Logo</a>
                </div>
        @else
            <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
        @endif
        {!! errors_for('image', $errors) !!}
      <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Image size must be 240*100 pixels</label>
 </div>
 </div>
 <div class="row">
 <div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
                      {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

        </div>
 
    
           <div class="col-md-6"  >
    <label for="status" class="control-label">Target</label>
                      {!! Form::select('target', ['_blank'=>'Blank', '_parent' =>'Parent'],null, ['class' => 'form-control']) !!}

        </div></div>

                   <div class="col-md-12 m-t10 t-r">
                       <a href="{{ route('sponsors.index')}}" class="btn btn-success" >Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-warning ']) !!}
                  </div>
