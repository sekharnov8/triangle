    <label for="title" class="control-label col-md-4">Status Category *</label>
<div class="col-md-6"  >

    {!! Form::text('category', null, ['class' => 'form-control', 'placeholder' => 'Status Category']) !!}

</div>
<div class="col-md-12 m-t10 t-r">
    <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
