<div class="row">
    <div class="col-md-6"  >

        <label class="control-label">Name *</label>
        {!! Form::text('name', null, ['class' => 'form-control capitalize', 'placeholder' => 'Name']) !!}
    </div>
    <div class="col-md-6"  >

        <label class="control-label">Email *</label>
        {!! Form::text('email', null, ['class' => 'form-control capitalize', 'placeholder' => 'Email']) !!}
    </div>

</div>
<div class="row">
    <div class="col-md-6"  >

        <label for="status" class="control-label">Status</label>
        {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

    </div>
    <div class="col-md-6"  >

        <label  class="control-label">Display Order</label>
        @if(isset($maxno))
            {!! Form::number('display_order', $maxno, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
        @else
            {!! Form::number('display_order', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
        @endif
    </div>
</div>

<div class="col-md-12 m-t10 t-r">
    <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
