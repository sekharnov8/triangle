<fieldset>
    <legend>Event Details</legend>
<div class="row"  >
    <div class="col-md-4"  >
        <label   class="control-label">Name *</label>
        {!! Form::text('name', null, ['class' => 'form-control gbl_title', 'placeholder' => 'Name']) !!}
    </div>
    <div class="col-md-4"  >
        <label   class="control-label">Slug *</label>
        {!! Form::text('slug', null, ['class' => 'form-control slug', 'placeholder' => 'Link']) !!}
    </div> 
     <div class="col-md-4"  >
        <label  class="control-label">Event Category *</label>

        {!! Form::select('category_id', $category_list, null,['class' => 'form-control','placeholder' => 'Event Category']) !!}
    </div>
    </div>
    <div class="row"  >
    
   
      <div class="col-md-4"  >
        <label for="event_for" class="control-label">Event for</label>
        {!! Form::select('event_for', [''=>'Select', 'General'=>'General',  'Member'=>'Member', 'Life Member'=>'Life Member'],null, ['class' => 'form-control']) !!}
    </div> 
    <div class="col-md-4"  >
        <label for="status" class="control-label">Status</label>
        {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}
    </div>

    <div class="col-md-4"  >
        <label   class="control-label">Event Date *</label>
        @if(isset($event))
        {!! Form::text('start_date', date("Y-m-d", strtotime($event->start_date)), ['class' => 'form-control datepicker datepickerstart',   'autocomplete'=>'off']) !!}
        @else
        {!! Form::text('start_date', null, ['class' => 'form-control datepicker datepickerstart', 'autocomplete'=>'off', 'placeholder' => 'Event Date']) !!}
        @endif    
    </div>
      
  
</div>
<div class="row"  >
  <div class="col-md-4"  >
        <label   class="control-label">Start Time</label>
        @if(isset($event))
        {!! Form::text('start_time', date("g:i A", strtotime($event->start_date)), ['class' => 'form-control start_time', 'id'=>'timepicker', 'autocomplete'=>'off']) !!}
        @else
        {!! Form::text('start_time', null, ['class' => 'form-control start_time', 'id'=>'timepicker', 'autocomplete'=>'off']) !!}
        @endif        
    </div>
       <div class="col-md-4"  >
        <label   class="control-label">End Time</label>
         @if(isset($event))
        {!! Form::text('end_time', date("g:i A", strtotime($event->end_date)), ['class' => 'form-control end_time', 'id'=>'timepicker2', 'autocomplete'=>'off']) !!}
        @else
        {!! Form::text('end_time', null, ['class' => 'form-control end_time', 'id'=>'timepicker2', 'autocomplete'=>'off']) !!}
        @endif   
    </div>
    <div class="col-md-4"  >

        <label   class="control-label">Location Type </label>
       {!! Form::select('location_type', ['Online'=>'Online', 'Location'=>'Location'],null, ['class' =>
                                                                'form-control location_type']) !!}
    </div></div>
    <div class="row"  >

      <div class="col-md-4"  >

        <label   class="control-label">Location </label>
        {!! Form::text('location', null, ['class' => 'form-control', 'placeholder' => 'Location']) !!}
    </div>
    <div class="col-md-4"  >

        <label   class="control-label">City   </label>
        {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
    </div>
    <div class="col-md-4"  >

        <label   class="control-label">State </label>
                {!! Form::select('state', $states_data, null,['class' => 'form-control','placeholder' => 'State']) !!}

    </div>
</div>
<div class="row"  >

    <div class="col-md-4"  >

        <label   class="control-label">Zipcode</label>
        {!! Form::text('zipcode', null, ['class' => 'form-control', 'placeholder' => 'Zip']) !!}
    </div>
    <div class="col-md-4"  >

        <label   class="control-label">Contact Email *</label>
        {!! Form::text('contact_mail', null, ['class' => 'form-control', 'placeholder' => 'Contact Mail']) !!}
    </div>
</div>
<div class="row"  >
    <div class="col-md-4"  >
        <label   class="control-label">Banner Image</label>
        {!!Form::file('banner_url',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
        @if(isset($event) && $event->banner_url!='')
            <div class="eventimage_view">
                <img class="preview_image"  id="image_preview" src="{{url('/uploads/events/'.$event->banner_url)}}"  style="max-height:200px; max-width:200px;"/>
                <br> <a href="javascript:void(0);"  class="deleteimage">Delete Image</a>
            </div>
        @endif
        <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Image size must be 334*232 pixels</label>
    </div>

</div>
    </fieldset>

<fieldset>
    <legend>Registration Details </legend>
    <div class="row"  >
        <div class="col-md-2"  >
            <label   class="control-label">IS Registration</label>
            {{ Form::checkbox('is_registration', 1, null,['class'=>'is_registration']) }}
        </div>
      <div class="registration_date">
        <div class="col-md-3"  >
            <label   class="control-label">Registration Start Date *</label>
            {!! Form::text('registration_start_date', null, ['class' => 'form-control datepicker datepickerstart2', 'autocomplete'=>'off', 'placeholder' => 'Registration Start Date']) !!}
        </div>

        <div class="col-md-3"  >
            <label   class="control-label">Registration End Date *</label>
            {!! Form::text('registration_end_date', null, ['class' => 'form-control datepicker datepickerend2', 'autocomplete'=>'off', 'placeholder' => 'Registration End Date']) !!}
        </div>
         <div class="col-md-3"  >
            <label   class="control-label">Total Registration count *</label>
            {!! Form::text('total_registration_count', null, ['class' => 'form-control', 'autocomplete'=>'off']) !!}
        </div>
     </div>
      

    </div>
</fieldset>
<fieldset class="registration_date">
    <legend>Registration Types </legend>
    <p><strong>Note:</strong> Registration title - 'Family' will ask Adult/Kids count in event registration. </p>
    <div class="row"  >
        <div class="col-md-3"  >
            <label   class="control-label">Registration Title </label>
        </div>
        <div class="col-md-2"  >

            <label   class="control-label">Amount </label>
        </div>

        <div class="col-md-2"  >

            <label   class="control-label"> Max Count for Reg Type</label>
        </div>
        <div class="col-md-2"  >

            <label   class="control-label"> Display Sequence </label>
        </div>
        <div  class="col-md-1">
            <a href="javascript:void(0)" class="btn btn-sm btn-warning add_regtype">Add</a>
        </div>
    </div>
     @if(isset($event))
    @foreach($event->regtypes as $registration_type)
      <div class="row"  >
        <div class="col-md-3"  >
         <input type="hidden" name="regtype[]" value="{{$registration_type->id}}"> 
        <input type="hidden" name="regid[]" value="{{$registration_type->id}}">
         {!! Form::text('regname2_'.$registration_type->id, $registration_type->name, ['class' => 'form-control require', 'placeholder'=> 'Registration Title']) !!}

         </div>
        <div class="col-md-2"  >

             {!! Form::text('amount2_'.$registration_type->id, $registration_type->amount, ['class' => 'form-control', 'placeholder' => 'Amount', 'required']) !!}
        </div>

        <div class="col-md-2"  >

             {!! Form::text('reg_count2_'.$registration_type->id,  $registration_type->reg_count, ['class' => 'form-control   require number', 'placeholder' => 'Max Count for Reg Type']) !!}        </div>
        <div class="col-md-2"  >
                     {!! Form::text('order_no2_'.$registration_type->id,  $registration_type->order_no, ['class' => 'form-control  require number', 'placeholder'=> 'Display Sequence']) !!}
        </div>
        <div class="col-md-1"  ><a href="javascript:void(0)" data-id="{{$registration_type->id}}" class="btn btn-sm btn-info remove_list3 ">Remove</a></div>
    </div>
    @endforeach
        <div class="childinfo"></div>

    @endif
    <div class="event_regtypedata"></div>
</fieldset>

<fieldset class="registration_date">
    <legend>Sponsorship Types </legend>
     <div class="row"  >
        <div class="col-md-3"  >
            <label   class="control-label">Sponsorship Title </label>
        </div>
        <div class="col-md-2"  >

            <label   class="control-label">Amount </label>
        </div>
 
        <div class="col-md-2"  >

            <label   class="control-label"> Display Sequence </label>
        </div>
        <div  class="col-md-1">
            <a href="javascript:void(0)" class="btn btn-sm btn-warning add_sponsortype">Add</a>
        </div>
    </div>
     @if(isset($event))
    @foreach($event->sponsortypes as $sponsor_type)
      <div class="row"  >
        <div class="col-md-3"  >
              <input type="hidden" name="sponsortype[]" value="{{$sponsor_type->id}}"> 

        <input type="hidden" name="sponsorid[]" value="{{$sponsor_type->id}}">
           {!! Form::text('sponsortype2_'.$sponsor_type->id, $sponsor_type->name, ['class' => 'form-control  require', 'placeholder'=> 'Sponsorship Title']) !!}
        </div>
        <div class="col-md-2"  >

             {!! Form::text('sponsoramount2_'.$sponsor_type->id, $sponsor_type->amount, ['class' => 'form-control   require number', 'placeholder' => 'Amount']) !!}
        </div>
 
        <div class="col-md-2"  >
               {!! Form::text('sponsororder_no2_'.$sponsor_type->id,  $sponsor_type->order_no, ['class' => 'form-control require number', 'placeholder'=> 'Display Sequence']) !!}
        </div>
        <div class="col-md-1"  ><a href="javascript:void(0)" data-id="{{$sponsor_type->id}}" class="btn btn-sm btn-info remove_list5 ">Remove</a></div>
    </div>
    @endforeach
        <div class="childinfo"></div>

    @endif
    <div class="event_sponsortypedata"></div>
 </fieldset>

   <div class="col-md-12"  >

    <label class="control-label">Amount Description </label>
    {!! Form::text('amount_desc', null, ['class' => 'form-control']) !!}
        </div>

   <div class="col-md-12"  >

<label   class="control-label">Event Description</label>
{!! Form::textarea('event_details', null, ['class' => 'form-control', 'placeholder' => 'Event Description']) !!}


<label   class="control-label">Registration Response Email Body</label>
{!! Form::textarea('mail_description', null, ['class' => 'form-control', 'placeholder' => 'Mail Description, Webinar info']) !!}
</div>
<fieldset>
    <legend>SEO Details</legend>
    <div class="row"  >
        <div class="col-md-4"  >

            <label   class="control-label">Page Title</label>
            {!! Form::textarea('page_title', null, ['class' => 'form-control ','style' => 'height:70px' , 'placeholder' => 'Meta Title']) !!}

        </div>
        <div class="col-md-4"  >

            <label   class="control-label">Meta Keyword</label>
            {!! Form::textarea('mata_keyword', null, ['class' => 'form-control ','style' => 'height:70px' , 'placeholder' => 'Meta Keyword']) !!}
        </div><div class="col-md-4"  >

            <label   class="control-label">Meta Description</label>
            {!! Form::textarea('mata_description', null, ['class' => 'form-control ','style' => 'height:70px' , 'placeholder' => 'Meta Description']) !!}
        </div>
    </div>
</fieldset>

<div class="col-md-12 m-t10 t-r">
    <a href="{{ URL::route('events.index')}}" class="btn btn-success" >Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-warning ']) !!}
</div>




