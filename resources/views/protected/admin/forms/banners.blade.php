<div class="row">
 
        
         <div class="col-md-6"  >
    <label for="image" class="control-label">Name *</label>
{!! Form::text('name', null, ['class' => 'form-control capitalize', 'placeholder' => 'Name']) !!}
        </div>

                            <div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
                      {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

        </div>
</div>
<div class="row">
  <div class="col-md-12"  >

    <label for="status" class="control-label">Image *</label>

        {!!Form::file('image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
        @if(isset($banner)&&$banner->image!='')
            <div class="bannerimage_view">
            <img class="preview_image"  id="image_preview" src="{{url('/uploads/banners/'.$banner->image)}}"  style="max-height:200px; max-width:200px;"/>
            <br> <a href="javascript:void(0);"  class="deleteimage">Delete Image</a>
                </div>
        @else
            <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
        @endif
        {!! errors_for('image', $errors) !!}
      <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Banner size must be 1583*503 pixels</label>

 </div>

 </div>
 <div class="row">
    <div class="col-md-6"  >

    <label for="image" class="control-label">Redirect URL</label>
{!! Form::text('redirect_url', null, ['class' => 'form-control', 'placeholder' => 'URL']) !!}

        </div>
           <div class="col-md-6"  >

    <label for="status" class="control-label">Target</label>
                      {!! Form::select('target', ['_blank'=>'Blank', '_parent' =>'Parent'],null, ['class' => 'form-control']) !!}
        </div>
        </div>
        <div class="row">
         <div class="col-md-6"  >

    <label for="image" class="control-label">Display Order</label>
             @if(isset($maxno))
                 {!! Form::number('order_no', $maxno, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
             @else
                 {!! Form::number('order_no', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
             @endif

        </div>
           <div class="col-md-6"  >

    <label for="image" class="control-label">Expire Date (Blank if no expire)</label>
{!! Form::text('expire_date', null, ['class' => 'form-control datepickerstart', 'placeholder' => 'Expire Date']) !!}

        </div>
        </div>
                   <div class="col-md-12 m-t10 t-r">
                       <a href="{{ route('banners.index')}}" class="btn btn-success" >Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-warning ']) !!}
                  </div>

