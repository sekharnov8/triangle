
<div class="row">
<div class="col-md-6"  >
    <label for="image" class="control-label">Title *</label>
    {!! Form::text('name', null, ['class' => 'form-control capitalize', 'placeholder' => 'Title']) !!}
</div>

    <div class="col-md-6"  >
        <label for="image" class="control-label">Subject *</label>
        {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject']) !!}
    </div>

    </div>

<div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
    {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

</div>

<div class="col-md-12"  >

    <label for="status" class="control-label">Template *</label>
    {!! Form::textarea('template', null, ['class' => 'form-control textarea', 'placeholder' => 'Template']) !!}

</div>



<div class="col-md-12 m-t10 t-r">
    <a href="{{ route('mail-templates.index')}}" class="btn btn-success" >Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
