<div class="row">

                  
<div class="col-md-6"  >

    <label for="title" class="control-label">Name *</label>
{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
        </div>
        </div>
        <div class="row">
 <div class="col-md-6"  >
    <label class="control-label">Page Parent</label>
    <select name="page_parentid" class="form-control">
<option value="0">No Sub Menu</option>
  @foreach ($category_list as $item)
    <option value="{{$item['id']}}" @if(isset($category) && $category->page_parentid == $item['id']) selected @endif >{{$item['name']}}</option>
    @foreach(pagecategories_sublist($item['id']) as $subitem)
    <option value="{{$subitem->id}}" @if(isset($category) && $category->page_parentid == $subitem->id) selected @endif>| {{$subitem->name}}</option>
    @foreach(pagecategories_sublist($subitem->id) as $subitem2)
    <option value="{{$subitem2->id}}" @if(isset($category) && $category->page_parentid == $subitem2->id) selected @endif>| | {{$subitem2->name}}</option>
@endforeach
@endforeach
    @endforeach
</select>

        </div>

          <div class="col-md-6"  >

    <label  class="control-label">Display Order</label>
       @if(isset($maxno))
           {!! Form::number('position', $maxno, ['class' => 'form-control', 'placeholder' => 'Position']) !!}
       @else
            {!! Form::number('position', null, ['class' => 'form-control', 'placeholder' => 'Position']) !!}
       @endif
        </div>

  </div>
  <div class="row">                       <div class="col-md-6"  >

    <label for="status" class="control-label">Is Display in Top Topbar</label>
           {{ Form::checkbox('is_topbar', 1, null) }}

        </div>
         <div class="col-md-6"  >

    <label for="status" class="control-label">Is Display in Main Menu</label>
                 <!--  <input type="checkbox" name="is_menubar" value="1" @if(isset($category) && $category->is_menubar==1)checked @endif>-->
             {{ Form::checkbox('is_menubar', 1, null) }}
        </div>

</div>
          <div class="row"> <div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
              {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}
        </div>
          <div class="col-md-6"  >

    <label for="status" class="control-label"> Linking Page Id</label>
        {!! Form::text('id_path', null, ['class' => 'form-control', 'placeholder' => 'Linking Page Id']) !!}
</div>

</div>

                   <div class="col-md-12 m-t10 t-r">
                       <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
                  </div>
