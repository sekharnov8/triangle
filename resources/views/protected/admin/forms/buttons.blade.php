<div class="row">
    <div class="col-md-4"  >

        <label for="title" class="control-label">Button Name *</label>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Button Name']) !!}

    </div>
    
    <div class="col-md-4"  >

        <label for="status" class="control-label">Status</label>
        {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

    </div>
</div>

<div class="row">
    <div class="col-md-4"  >

        <label for="image" class="control-label">Redirect URL</label>
        {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'URL']) !!}

    </div>
    <div class="col-md-4"  >

        <label for="status" class="control-label">Target</label>
        {!! Form::select('target', ['_blank'=>'Blank', '_parent' =>'Parent'],null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4"  >

        <label for="image" class="control-label">Display Order</label>
        @if(isset($maxno))
            {!! Form::number('display_order', $maxno, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
        @else
            {!! Form::number('display_order', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
        @endif

    </div>
</div>
<div class="row">
    <div class="col-md-4"  >

        <label for="status" class="control-label">Icon1  *</label>

        {!!Form::file('img1',['placeholder' => 'Icon','id'=>'image', 'class' => 'image'])!!}
        @if(isset($button)&&$button->img1!='')
            <div class="buttonicon1_view">
                <img class="preview_image"  id="image_preview" src="{{url('/uploads/buttons/'.$button->img1)}}"  style="max-height:200px; max-width:200px;"/>
                <br> <a href="javascript:void(0);"  class="deleteimage1">Delete Icon1</a>
            </div>
        @else
            <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
        @endif
        {!! errors_for('image', $errors) !!}
        <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Banner size must be 1583*503 pixels</label>

    </div>
    <div class="col-md-4"  >

        <label for="status" class="control-label"> Mouse hover Icon2 *</label>

        {!!Form::file('img2',['placeholder' => 'Icon','id'=>'image', 'class' => 'image'])!!}
        @if(isset($button)&&$button->img2!='')
            <div class="buttonicon2_view">
                <img class="preview_image"  id="image_preview" src="{{url('/uploads/buttons/'.$button->img2)}}"  style="max-height:200px; max-width:200px;"/>
                <br> <a href="javascript:void(0);"  class="deleteimage2">Delete Icon2</a>
            </div>
        @else
            <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
        @endif
        {!! errors_for('image', $errors) !!}
        <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Banner size must be 1583*503 pixels</label>

    </div>
    <div class="col-md-4"  >

        <label for="image" class="control-label">Expire Date (Blank if no need)</label>
        {!! Form::text('expire_date', null, ['class' => 'form-control datepickerstart', 'placeholder' => 'Expire Date']) !!}

    </div>

</div>
<div class="row">
    <div class="col-md-4"  >

        <label class="control-label">CSS Class </label>
        {!! Form::text('class', null, ['class' => 'form-control', 'placeholder' => 'Class']) !!}

    </div>
     <div class="col-md-4"  >

        <label class="control-label">Display  For</label>
        {!! Form::select('display_type', [0 =>'All',2=>'After Login',1=>'Guest' ],null, ['class' => 'form-control','id' =>'status']) !!}

    </div>
    </div>

<div class="col-md-12 m-t10 t-r">
    <a href="{{ route('buttons.index')}}" class="btn btn-success" >Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-warning ']) !!}
</div>

