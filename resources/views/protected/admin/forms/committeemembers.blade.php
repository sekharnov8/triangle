<div class="row">
<div class="col-md-12"  >
    <label class="control-label">Committee: * </label>
    @if(isset($committeemember))
     <label class="control-label">{{$committeemember->committee->name}}</label>
    @elseif(isset($committee))
    {!! Form::select('committee_id', $committees, $committee,['class' => 'form-control committee_id','placeholder' => 'Select Committee']) !!}
    @else
    {!! Form::select('committee_id', $committees, null,['class' => 'form-control committee_id','placeholder' => 'Select Committee']) !!}
    @endif
</div>
@if(isset($committeemember))

<div class="col-md-6"  >
    <label for="cssclass" class="control-label">Member: {{getUserName($committeemember->user_id)}}</label>
</div>
<div class="col-md-6"  >
    <label for="cssclass" class="control-label">Email: {{getUserEmail($committeemember->user_id)}}</label>
</div>
@endif
<div class="col-md-12"  >
    <label for="cssclass" class="control-label">Member @if(!isset($committeemember))*@endif
</label>
    {!! Form::select('user_id', $user, null,['class' => 'form-control selectpicker user_id','placeholder' => 'Select Member']) !!}
</div>



</div>
<div class="row">
<div class="col-md-6"  >
    <label  class="control-label">Role *</label>
      {!! Form::select('role_id', $roles, null,['class' => 'form-control','placeholder' => 'Select Role']) !!}

</div>
<div class="col-md-6"  >

    <label  class="control-label">Display Order</label>
        {!! Form::number('display_order', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
 </div>
    </div>
@if(isset($committeemember))
<div class="col-md-6"  >
    <label for="status" class="control-label">Status</label>
    {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}
</div>
@endif
<div class="col-md-12 m-t10 t-r">
    <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
