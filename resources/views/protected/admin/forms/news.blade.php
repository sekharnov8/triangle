
<div class="row">
         <div class="col-md-6"  >

    <label for="image" class="control-label">Name *</label>
{!! Form::text('name', null, ['class' => 'form-control gbl_title', 'placeholder' => 'Name']) !!}



        </div>
    <div class="col-md-6"  >

    <label for="title" class="control-label">Slug </label>
    @if(isset($page))
        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug']) !!}
    @else
        {!! Form::text('slug', null, ['class' => 'form-control slug', 'placeholder' => 'Slug']) !!}
     @endif
        </div>
                           </div>
<div class="row">
         <div class="col-md-6"  >

             <label   class="control-label">Display Order</label>

             @if(isset($maxno))
                 {!! Form::number('display_order', $maxno, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
             @else
                 {!! Form::number('display_order', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
             @endif


         </div>
          <div class="col-md-6"  >

             <label for="image" class="control-label">URL</label> 
                 {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'URL to redirect ']) !!} 
         </div></div>
         <div class="row">
         <div class="col-md-6"  >

             <label  class="control-label">Image</label>

             {!!Form::file('image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
             @if(isset($news)&&$news->image!='')
                 <div class="newsimage_view">
                     <img class="preview_image"  id="image_preview" src="/uploads/news/{{$news->image}}"  style="max-height:200px; max-width:200px;"/>
                     <br> <a href="javascript:void(0);"  class="deleteimage">Delete Image</a>
                 </div>
             @else
                 <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
             @endif
             {!! errors_for('image', $errors) !!}
             <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Image size must be 785*450 pixels</label>
         </div>
          <div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
                      {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

        </div>
         
               <div class="col-md-12"  >

    <label for="status" class="control-label">Description   </label>
{!! Form::textarea('description', null, ['class' => 'form-control textarea', 'placeholder' => 'Description']) !!}

        </div>



                   <div class="col-md-12 m-t10 t-r">
                       <a href="{{ route('news.index')}}" class="btn btn-success" >Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
                  </div>
