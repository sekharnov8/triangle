
<div class="row">
    <div class="col-md-6"  >

        <label  class="control-label">First Name *</label>
        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
    </div>
   

    <div class="col-md-6"  >

        <label  class="control-label">Last Name *</label>
        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
    </div>
     </div>
    <div class="row">
    <div class="col-md-6"  >
        <label  class="control-label">Phone</label>
        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
    </div> 
    <div class="col-md-6"  >
        <label  class="control-label">Email</label>
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
    </div>
     </div>
    <div class="row">
    <div class="col-md-6"  >
        <label  class="control-label">Skills</label>
        {!! Form::text('skills', null, ['class' => 'form-control', 'placeholder' => 'Skills']) !!}
    </div> 
       <div class="col-md-6"  >
        <label  class="control-label">DOB</label>
        {!! Form::text('dob', null, ['class' => 'form-control datepicker' ]) !!}
    </div> 

    <div class="col-md-6"  >
        <label  class="control-label">Relationship *</label>
        {!! Form::select('relationship', [''=>'Select','Spouse'=>'Spouse', 'Son' =>'Son', 'Daughter'=>'Daughter'],null, ['class' => 'form-control']) !!}
     </div>
    <div class="col-md-6"  >
        <label  class="control-label">Occupation</label>
        {!! Form::text('occupation', null, ['class' => 'form-control', 'placeholder' => 'Occupation']) !!}
    </div>
</div>

<div class="row">

<div class="col-md-12 m-t10 t-c t-r">
    <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
</div>