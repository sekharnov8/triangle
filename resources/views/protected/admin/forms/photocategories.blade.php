<div class="col-md-6"  >

    <label for="title" class="control-label">Name *</label>
{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}

        </div>
                             
        <div class="col-md-6"  >

    <label for="title" class="control-label">Slug</label>
{!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug']) !!}

        </div>

  <div class="col-md-6"  >

    <label for="title" class="control-label">Photo Category </label>
             {!! Form::select('parent_id',$category_list, null,['class' => 'form-control category','placeholder' => 'Select Category']) !!}

        </div>
  <div class="col-md-6"  >

    <label for="title" class="control-label">Display Order </label>
{!! Form::text('order_no', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}

        </div>
         <div class="col-md-6"  >

     <label for="status" class="control-label">Status</label>
                       {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

         </div>

   <div class="col-md-12"  >

    <label for="status" class="control-label">Image </label>

        {!!Form::file('image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
        @if(isset($category)&&$category->image!='')
            <div class="bannerimage_view">
            <img class="preview_image"  id="image_preview" src="{{url('/uploads/photos/'.$category->image)}}"  style="max-height:200px; max-width:200px;"/>
                 </div>
        @else
            <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
        @endif
        {!! errors_for('image', $errors) !!}
 
 </div>

  <div class="col-md-12"  >

     <label for="status" class="control-label">Description</label>
  {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}

         </div>

                    <div class="col-md-12 m-t10 t-r">
                       <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
                  </div>
