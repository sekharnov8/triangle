<div class="row">
       
        <div class="col-md-6 "  >
    <label  lass="control-label">Category *</label>
      <select name="photocategory_id" class="form-control selectpicker category">
<option value="">Select Category</option>
  @foreach ($categories as $item)
    <option value="{{$item['id']}}" @if(isset($photo) && $item['id'] == $photo->photocategory_id) selected @endif >{{$item['name']}}</option>
    @foreach(photocategories_sublist($item->id)  as $subitem)
    <option value="{{$subitem->id}}" @if(isset($photo) && $subitem->id == $photo->photocategory_id) selected @endif>| &gt; {{$subitem->name}}</option>
@endforeach
    @endforeach
</select>

 
        </div>
        </div>
<div class="row">
        <div class="col-md-6">
            <label for="status" class="control-label">Title</label>
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}

        </div>

  <div class="col-md-6"  >

         @if(isset($photo))
    <label for="status" class="control-label">Picture  </label>
             {!!Form::file('image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
         @else
    <label for="status" class="control-label">Picture(s) * (Using shift select multiple photos to upload) </label>
         {!!Form::file('images[]',['placeholder' => 'Image','id'=>'image[]', 'multiple'=>true, 'class' => 'image'])!!}
         @endif

         @if(isset($photo)&&$photo->image_url!='')
             <div class="photoimage_view">
             <img class="preview_image"  id="image_preview" src="{{url('/uploads/photos/'.$photo->image_url)}}"  style="max-height:200px; max-width:200px;"/>
             <br> <a href="javascript:void(0);"  class="deleteimage">Delete Image</a>
                 </div>
         @else
             <img class="preview_image"  id="image_preview" style="max-height:200px; max-width:200px;"/>
         @endif
         {!! errors_for('image', $errors) !!}
  </div>
</div>

 
  <div class="col-md-6"  >

     <label for="status" class="control-label">Status</label>
                       {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

         </div>
          <div class="col-md-6"  >

     <label for="image" class="control-label">Display Order</label>
 {!! Form::number('display_order', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}



         </div>
             <div class="col-md-6"  >

    <label for="image" class="control-label">Is Home</label>
                      {!! Form::select('is_home', ['1'=>'Yes', '0' =>'No'],null, ['class' => 'form-control']) !!}

        </div>
                    <div class="col-md-12 m-t10 t-r">
                        <a href="{{ route('photos.index')}}" class="btn btn-success" >Cancel</a>
                      {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
                   </div>
