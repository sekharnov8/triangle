<div class="row">
<div class="col-md-12"  >
    <label for="title" class="control-label">Status Type</label>
     {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
 </div> </div>
 <div class="row">
<div class="col-md-12"  >
     <label  lass="control-label">Category *</label>
    
    @if(isset($statustype))
            {!! Form::select('status_category_id',$categories, null,['class' => 'form-control  category','placeholder' => 'Select Category']) !!}
 @else
   {!! Form::select('status_category_id',$categories, null,['class' => 'form-control selectpicker category','placeholder' => 'Select Category']) !!}
 @endif
 </div></div>
 <div class="row">
<div class="col-md-12"  >
     <label for="title" class="control-label">Status Number *</label>
 
    {!! Form::number('order_no', null, ['class' => 'form-control', 'placeholder' => 'Status Number']) !!}
 </div></div>
 <div class="row">
<div class="col-md-12"  >
     <label for="status" class="control-label">Description</label>
  {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
</div></div>
 <div class="row"> 
<div class="col-md-12 m-t10 t-r">
    <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div></div>
