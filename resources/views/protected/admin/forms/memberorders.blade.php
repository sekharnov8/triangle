<div class="row">

    <div class="col-md-6"  >
         Name: {{getUserName($morder->user_id)}}
    </div>
    <div class="col-md-6"  >
       Payment Method: @if($morder->paymentmethod){{$morder->paymentmethod->name}}@endif
    </div>
</div>
<div class="row">
    <div class="col-md-6"  >
Transaction Id: {{$morder->transaction_id}}
    </div>
     <div class="col-md-6"  >
Membershihp Type: @if($morder->membershiptype){{$morder->membershiptype->name}}@endif
    </div> </div>
<div class="row">
    <div class="col-md-6"  >
        <label  class="control-label">Amount</label>
        {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
    </div>

    <div class="col-md-6"  >
        <label  class="control-label">Donation Amount</label>
            {!! Form::text('donation_amount', null, ['class' => 'form-control', 'placeholder' => 'Donation Amount']) !!}
     </div>

</div>
<div class="row">
    <div class="col-md-6"  >
        <label  class="control-label">Bank Name </label>
        {!! Form::text('bankname', null, ['class' => 'form-control', 'placeholder' => 'Bank Name']) !!}
    </div>

    <div class="col-md-6"  >
        <label  class="control-label">Check No</label>
            {!! Form::text('cheque_no', null, ['class' => 'form-control', 'placeholder' => 'Check No']) !!}
     </div>

</div>
<div class="row">

    <div class="col-md-6"  >
        <label  class="control-label">Check Date</label>
            {!! Form::text('cheque_date', null, ['class' => 'form-control', 'placeholder' => 'Check Date']) !!}
     </div>

    <div class="col-md-6"  >
        <label  class="control-label">Check Handed to</label>
            {!! Form::text('handedto', null, ['class' => 'form-control']) !!}
     </div>
</div>
<div class="row">

    <div class="col-md-12"  >
        <label  class="control-label">Admin Comments </label>
            {!! Form::textarea('admin_comments', null, ['class' => 'form-control','rows'=>2, 'placeholder' => 'Comments']) !!}
     </div>

</div>
<div class="row">
  <div class="col-md-6"  >
Order Created on: {{$morder->created_at}}
    </div>
      <div class="col-md-6"  >
Payment By: {{$morder->payment_by}}
    </div>
</div>
<div class="row">

    <div class="col-md-6"  >

        <label for="status" class="control-label">Status</label>
        {!! Form::select('status', $statustypes,null, ['class' => 'form-control','id' =>'status']) !!}

    </div>

</div>

<div class="col-md-12 m-t10 t-r">
    <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
