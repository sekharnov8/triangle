<div class="col-md-6"  >

    <label for="title" class="control-label">Title *</label>
{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}

        </div>
<div class="col-md-6"  >

    <label for="title" class="control-label">Youtube Video URL *</label>
{!! Form::text('video_url', null, ['class' => 'form-control', 'placeholder' => 'Youtube Video URL']) !!}

        </div>
        
        <div class="col-md-6 "  >
    <label  lass="control-label">Category *</label>
             {!! Form::select('videocategory_id',$categories, null,['class' => 'form-control selectpicker category','placeholder' => 'Select Category']) !!}
 

        </div>




  <div class="col-md-12"  >

    <label for="status" class="control-label">Description</label>
 {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}

 </div>
 <div class="col-md-6"  >

    <label for="status" class="control-label">Status</label>
                      {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

        </div>
    <div class="col-md-6"  >

    <label for="image" class="control-label">Is Home</label>
                      {!! Form::select('is_home', ['1'=>'Yes', '0' =>'No'],null, ['class' => 'form-control']) !!}

        </div>

         <div class="col-md-6"  >

    <label for="image" class="control-label">Display Order</label>
{!! Form::number('display_order', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}



        </div>
                   <div class="col-md-12 m-t10 t-r">
                       <a href="{{ route('videos.index')}}" class="btn btn-success" >Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
                  </div>
