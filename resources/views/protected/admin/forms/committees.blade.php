<div class="row">

<div class="col-md-6"  >

    <label  class="control-label">Name *</label>
    {!! Form::text('name', null, ['class' => 'form-control gbl_title', 'placeholder' => 'Name']) !!}

</div>
 
<div class="col-md-6"  >

    <label  class="control-label">Slug *</label>
    {!! Form::text('slug', null, ['class' => 'form-control slug', 'placeholder' => 'Slug']) !!}
</div></div>
<div class="row">

<div class="col-md-6"  >

    <label  class="control-label">Committee Common Email </label>
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
</div>
<div class="col-md-6"  >

    <label class="control-label">Type</label>
    {!! Form::select('type', ['general'=>'General', 'standing' =>'Standing'],null, ['class' => 'form-control']) !!}

</div></div>
<div class="row">

<div class="col-md-6"  >
    <label for="status" class="control-label">Status</label>
    {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}
</div>
<div class="col-md-6"  >

    <label  class="control-label">Display Order</label>
    @if(isset($maxno))
        {!! Form::number('order_no', $maxno, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
    @else
        {!! Form::number('order_no', null, ['class' => 'form-control', 'placeholder' => 'Display Order']) !!}
    @endif
</div></div>
<div class="row">

<div class="col-md-12 m-t10 t-r">
    <a class="btn btn-primary pull-left" data-dismiss="modal">Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
</div>