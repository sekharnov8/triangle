<div class="row"  >
 
<div class="col-md-6"  >
    <label  class="control-label">Role *</label>
    {!! Form::select('role_id', $roles_info, null,['class' => 'form-control','placeholder' => 'Select Role']) !!}
        </div>
  @if(isset($user) && $user->membershiptype_id)
<div class="col-md-6"  >
    <label  class="control-label">Membership Type *</label>
    {!! Form::select('membershiptype_id', $membershiptypes, null,['class' => 'form-control','placeholder' => 'Select Membership Type']) !!}
        </div>
@endif
</div>
<div class="row"  >
<div class="col-md-6"  >
    <label for="status" class="control-label">Status</label>
    {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

</div>
  <div class="col-md-6"  >
     <label  class="control-label">Referred By</label>
      {!! Form::text('referred_by', null, ['class' => 'form-control', 'placeholder' => 'Referred by']) !!}
  </div>
    
</div>

<fieldset>
    <legend>Personal Details</legend>
<div class="row"  >
    <div class="col-md-6"  >
        <label   class="control-label">Title</label>
        {!! Form::select('title', ['Mr.'=>'Mr.', 'Ms.'=>'Ms.', 'Mrs.'=>'Mrs.', 'Dr.'=>'Dr.', 'Prof.'=>'Prof.', 'Others'=>'Others', ],null, ['class' => 'form-control','id' =>'status']) !!}
    </div>
 <div class="col-md-6"  >
    <label   class="control-label">First Name *</label>
{!! Form::text('first_name', null, ['class' => 'form-control capitalize', 'placeholder' => 'First Name']) !!}
        </div>
        </div>
<div class="row"  >
    <div class="col-md-6"  >
        <label   class="control-label">Middle Name</label>
        {!! Form::text('middle_name', null, ['class' => 'form-control capitalize', 'placeholder' => 'Middle Name']) !!}
    </div>
<div class="col-md-6"  >

    <label   class="control-label">Last Name *</label>
    {!! Form::text('last_name', null, ['class' => 'form-control capitalize', 'placeholder' => 'Last Name']) !!}
</div>
</div>
<div class="row"  >

<div class="col-md-6"  >

    <label   class="control-label">Email *</label>
    {!! Form::text('email', null, ['class' => 'form-control', 'id'=>'email_reg', 'placeholder' => 'Email']) !!}
     <div class="email_status"></div>
</div>
<div class="col-md-6"  >

    <label   class="control-label">Mobile Number </label>
    {!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile']) !!}
</div></div>
<div class="row"  >

<div class="col-md-6"  >

    <label   class="control-label">Member Skills</label>
    {!! Form::text('skills', null, ['class' => 'form-control']) !!}
</div>
  <div class="col-md-6"  >

    <label   class="control-label">Occupation</label>
    {!! Form::text('occupation', null, ['class' => 'form-control capitalize', 'placeholder' => 'Occupation']) !!}
</div>
    </div>
<div class="row"  >
<div class="col-md-6"  >
  <label   class="control-label">Profile Image</label>
    {!!Form::file('profile_image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
 </div>
</div>
</fieldset>
<fieldset>
    <legend>Details</legend>
 
        <div class="row"  >

    <div class="col-md-6"  >

        <label   class="control-label">Address</label>
        {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address']) !!}
    </div>
    <div class="col-md-6"  >

        <label   class="control-label">Address Line 2</label>
        {!! Form::text('address2', null, ['class' => 'form-control']) !!}
    </div>
        </div>
        <div class="row"  >
 <div class="col-md-6"  >

    <label   class="control-label">City</label>
                {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
        </div>

    <div class="col-md-6"  >

        <label   class="control-label">State</label>
        {!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'State']) !!}
    </div>
    </div>
    <div class="row"  >
    <div class="col-md-6"  >

        <label   class="control-label">Country</label>
        {!! Form::text('country', null, ['class' => 'form-control', 'placeholder' => 'Country']) !!}
    </div>

    <div class="col-md-6"  >

        <label   class="control-label">Zipcode</label>
        {!! Form::text('zipcode', null, ['class' => 'form-control', 'placeholder' => 'Zip']) !!}
    </div>


</div>

</fieldset>


    <label   class="control-label">Terms & Conditions</label>
                {!! Form::textarea('terms_conditions', null, ['class' => 'form-control', 'placeholder' => 'Terms & Conditions']) !!}

                   <div class="col-md-12 m-t10 t-r">
                       <a href="{{ route('members.index')}}" class="btn btn-success" >Cancel</a>
                     {!! Form::submit($submitButtonText, ['class' => 'btn btn-warning ', 'id'=>'signupbtn']) !!}
                  </div>

