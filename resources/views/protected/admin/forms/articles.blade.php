<div class="row">
    <div class="col-md-4">

        <label for="image" class="control-label">Name *</label>
        {!! Form::text('name', null, ['class' => 'form-control capitalize name', 'placeholder' => 'Name']) !!}


    </div>

    <div class="col-md-4">

        <label for="status" class="control-label">Status</label>
        {!! Form::select('status', [1=>'Active', 0 =>'Inactive'],null, ['class' => 'form-control','id' =>'status']) !!}

    </div>

<div class="col-md-4">

    <label for="image" class="control-label">Slug</label>
    {!! Form::text('slug', null, ['class' => 'form-control slug', 'placeholder' => 'Slug']) !!}
</div>
</div>

<div class="col-md-8">

    <label for="status" class="control-label">Image</label>

    {!!Form::file('image',['placeholder' => 'Image','id'=>'image', 'class' => 'image'])!!}
    @if(isset($articles)&&$articles->image!='')
        <div class="articlesimage_view">
            <img class="preview_image" id="image_preview" src="/uploads/articles/{{$articles->image}}"
                 style="max-height:200px; max-width:200px;"/>
            <br> <a href="javascript:void(0);" class="deleteimage">Delete Image</a>
        </div>
    @else
        <img class="preview_image" id="image_preview" style="max-height:200px; max-width:200px;"/>
    @endif
    {!! errors_for('image', $errors) !!}
    <label for="status" style="font-weight:normal;margin-top: 4px;" class="control-label"><span style="font-weight: bold;" class="red-t">Note:- </span> Image size must be 785*450 pixels</label>
</div>
<div class="col-md-4"  >

    <label for="cssclass" class="control-label">Posted Date</label>
    {!! Form::text('post_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'Post Date']) !!}
        </div>
 
<div class="col-md-12">

    <label for="status" class="control-label">Short Description *</label>
    {!! Form::textarea('shortdescription', null, ['class' => 'form-control textarea', 'placeholder' => 'Short Description']) !!}
</div>

<div class="col-md-12">

    <label for="status" class="control-label">Description</label>
    {!! Form::textarea('description', null, ['class' => 'form-control textarea', 'placeholder' => 'Description']) !!}

</div>


<div class="col-md-12 m-t10 t-r">
    <a href="{{ route('articles.index')}}" class="btn btn-success" >Cancel</a>
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-info ']) !!}
</div>
