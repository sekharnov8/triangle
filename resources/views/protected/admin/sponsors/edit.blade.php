@extends('layouts.default')
@section('content_header_title','Edit Sponsor')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/sponsors') }}">Sponsors</a></li>
@stop
@section('content')

    <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title pull-right">
                    <a href="{{ route('sponsors.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                <div class="box-body">
                    {!! Form::model($sponsor, array('route' => ['sponsors.update', $sponsor->id], 'method' => 'PUT', 'id' => 'dataform', 'files' => true)) !!}

                    @include('protected.admin.forms.sponsors', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
                </div>
@stop
@section('js')
    <script>

        $("#dataform").validate({
            rules: {
                name: "required",
                category_id: "required"


            },
            messages: {
                category_id: {required: "Select Category"},
                name: {required: "Enter Sponsor Title"}

            }
        });

        $('.deleteimage').on('click', function () {

            var data = "id={{$sponsor->id}}&_token={{ csrf_token() }}";
            $.ajax({
                url: '{{url('admin/DeleteSponsorImage')}}',
                type: 'POST',
                data: data,
                success: function (response) {
                    $('.sponsorimage_view').html('');
                    swal("Deleted!", "Deleted Image Successfully!", "success");
                }
            });
        });

          
    </script>
@stop