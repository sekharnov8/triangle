@extends('layouts.default')
@section('content_header_title','Add Sponsor')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/sponsors') }}">Sponsors</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('sponsors.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'sponsors.store', 'method' => 'POST','id' => 'dataform', 'files' => true)) !!}
                        @include('protected.admin.forms.sponsors', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
@section('js')
<script>

    $("#dataform").validate({
        rules: {
            name: "required",
            category_id: "required"


        },
        messages: {
            category_id: {required: "Select Category"},
            name: {required: "Enter Sponsor Title"}

        }
    });
  
</script>
@stop
