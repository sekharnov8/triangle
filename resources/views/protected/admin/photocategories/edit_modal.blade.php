

 {!! Form::model($category, array('route' => ['photo-categories.update', $category->id], 'method' => 'PUT', 'files' => true,'id' => 'dataform')) !!}

 @include('protected.admin.forms.photocategories', ['submitButtonText' => 'Update'])

 {!! Form::close() !!}
 <div class="clearfix"></div>