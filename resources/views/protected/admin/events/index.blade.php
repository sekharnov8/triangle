@extends('layouts.default')
@section('content_header_title','Events')
@section('content')
    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title">
                <a href="{{ URL::route('events.create')}}" class="btn btn-success">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Event</a>
            </h3>

        </div>

        <div class="box-body">
            <div class="row" style="margin:5px 0px;">

            <div class="box box-success collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Search</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
         </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: none;">
        <div class="row">
        <div class="col-md-3 form-group">
                {!! Form::label('keyword','Keyword') !!}
                {!! Form::text('keyword',null,['class' => 'form-control','id'=>'keyword']) !!}
            </div>
            
             <div class="col-md-3 form-group">
                {!! Form::label('status','Status') !!}
                {!! Form::select('status', [''=>'All',1=>'Active',0=>'Pending'], null,['class' => 'form-control','placeholder' =>'Select Status']) !!}
             </div>
             <div class="col-md-3 form-group">
                {!! Form::label('eventtype','Event Type') !!}
                 {!! Form::select('eventtype', ['upcoming'=>'Upcoming','current'=>'Current','past'=>'Past'], null,['class' => 'form-control','placeholder' =>
                    'Event Type']) !!}
             </div>
             <div class="col-md-3 form-group">
                {!! Form::label('event_for','Register For') !!}
                 {!! Form::select('event_for', ['General'=>'General','Member'=>'Member','Life Member'=>'Life Member'], null,['class' => 'form-control','placeholder' =>
                    'Register for']) !!}
             </div>
             <div class="col-md-3 form-group">
                {!! Form::label('datefrom','Event From') !!}
                  <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="datefrom" class="form-control  datepicker" id="datefrom">
                </div>
            </div>

              <div class="col-md-3 form-group">
                {!! Form::label('dateto','Event Till') !!}
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="dateto" class="form-control  datepicker" id="dateto">
                </div>
            </div>
           <div class="col-md-3 form-group"><br/>
             <button name="Search" type="button" id="dateSearch" class="btn btn-primary"><i class="fa fa-search">&nbsp;</i>Search</button>
                      &nbsp;  <span class="filter2" style="display:none">
                      <button name="Reset" type="button" id="dateClear"  class="btn btn-danger"><i class="fa fa-refresh">&nbsp;</i>Reset</button></span>

  </div>
</div>
     </div>
</div>
             </div>
            <table class="table table-striped table-bordered table-hover" id="datalist">
                <thead>
                <tr >
                    <th>S.No.</th>
                    <th>Category</th>
                     <th>Title</th>
                    <th>Member</th>
                    <th>Banner</th>
                    <th>Event Date</th>
                    <th>Registrations</th>
                    <th width="90">Status</th>
                    <th width="110">Action</th>
                </tr>
                </thead>
            </table>
        </div>

    </div>
@stop
@section('js')
    <script>
        $(function () {
            var oTable = $('#datalist').DataTable({
                processing: true,
                serverSide: true,
                order: [[5, "desc"]],
                lengthMenu: [[10, 20, 30, 50], [10, 20, 30, 50]],
                ajax: {
                    url: '{{url('/admin/events/data')}}',
                    data: function (d) {
                        d.keyword = $('input[name=keyword]').val();
                        d.datefrom = $('input[name=datefrom]').val();
                        d.dateto = $('input[name=dateto]').val();
                        d.status = $('select[name=status]').val();
                        d.eventtype = $('select[name=eventtype]').val();
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'category', name: 'category'},
                     {data: 'name', name: 'name'},
                    {data: 'user_name', name: 'user_name', searchable: false, orderable: false},
                    {data: 'banner_url', name: 'banner_url', searchable: false, orderable: false},
                    {data: 'start_date', name: 'start_date'},
                    {data: 'registrations', name: 'registrations', searchable: false, orderable: false},
                    {data: 'status', name: 'status', searchable: false, orderable: false},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false}
                ]
            });
            $('.input-daterange').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: ''
            });

            $('#dateSearch').on('click', function () {
                $('.filter2').show();
                datefrom = $('input[name=datefrom]').val();
                dateto = $('input[name=dateto]').val();
                oTable.draw();
            });
            $('#dateClear').on('click', function () {
                $('.filter2').hide();
                $('input[name=keyword]').val('');
                $('select[name=status]').val('');
                $('select[name=eventtype]').val('');
                $('.datepicker').datepicker('setDate', null);
                $(".selectpicker").selectpicker("refresh");
                oTable.draw();
            });
            $('.dataTables_filter').hide();
            $("input[name=keyword]").keypress(function (event) {
                if (event.which == 13) {
                    $("#dateSearch").trigger("click");
                }
            });

            });

        $(document).on('change','.status',function(e){
            var thisVal = $(this);
            var id = $(this).data('id');
            var status = this.value;
            var prevStatus = $(this).data('status');
            e.stopImmediatePropagation();
            swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('admin/change-event-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    //console.log(data);
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                           //$(thisVal).selectpicker("refresh");
                        }
                    });

        });

        $(document).on('click','.delete',function(){

            var id = $(this).data('id');

            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Deleted!", "Your record has been deleted!", "success");
                            $('.tr_'+id).remove();

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('/admin/events') }}' + '/' + id,
                                type: 'DELETE',
                                dataType: 'json',
                                success: function(data){

                                }
                            });

                        } else {
                          //  swal("Cancelled", "Your record is safe :)", "error");
                        }
                    });

        });


    </script>
@stop
