@extends('layouts.default')
@section('content_header_title','Add Events')
@section('breadcrumb_inner_content')
    <li><a href="{{ url('/admin/events') }}">Events</a></li>
@stop
@section('content')
    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title">
                <a href="{{ URL::route('events.index')}}" class="btn btn-success"  >
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </h3>
        </div>
        <div class="box-body">
            {!! Form::open(array('route' => 'events.store', 'method' => 'POST', 'id'=>'dataform','files' => true)) !!}
            @include('protected.admin.forms.events', ['submitButtonText' => 'Submit'])
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('css')
 <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />

<link rel="stylesheet" href="{{asset('plugins/timepicker/jquery.ptTimeSelect.css')}}">
@stop
@section('js')
<script type="text/javascript" src="{{asset('plugins/timepicker/jquery.ptTimeSelect.js')}}"></script>

    <script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>

  <script>
        $(document).ready(function () { 
         

            $('#timepicker').ptTimeSelect({
     onClose: function(){
               var start_time = $(".start_time").val();
var end_time = $(".end_time").val(); 
if (start_time != "" && end_time!="") {
var date1 = $(".datepickerstart").val();
if(date1=='')    
date1='04/23/2020';
var aDate= new Date(Date.parse(date1+ ' '+start_time));
var bDate = new Date(Date.parse(date1+ ' '+end_time));
 if (aDate> bDate ){
    alert("End Time  Should Be Greater than Start time");
    $(".end_time").val('');
    return false;
}
 } }
 });
     $('#timepicker2').ptTimeSelect({
     onClose: function(){
               var start_time = $(".start_time").val();
var end_time = $(".end_time").val(); 
if (start_time != "" && end_time!="") {
var date1 = $(".datepickerstart").val();
if(date1=='')    
date1='04/23/2020';
var aDate= new Date(Date.parse(date1+ ' '+start_time));
var bDate = new Date(Date.parse(date1+ ' '+end_time));
}
 if (aDate> bDate ){
    alert("End Time  Should Be Greater than Start time");
    $(".end_time").val('');
    return false;
} }
 });
 
$(document).on('change', '.datepickerstart2,.datepickerend2', function(e){
  var datestart = $(".datepickerstart2").val();
  var dateend = $(".datepickerend2").val();
   if (datestart != "" && dateend!="") {
  if (Date.parse(datestart) > Date.parse(dateend)) {
      alert("End Date Should Be Greater than Start Date");
      $(".datepickerend2").val('');
      return false;
  }
}
var eventdate = $(".datepickerstart").val(); 
    if (datestart!='' &&  Date.parse(datestart) > Date.parse(eventdate)) {
      alert("Registration Start Date Should Be less than event Date");
      $(".datepickerstart2").val('');
        return false;
  }
    if (dateend!='' && Date.parse(dateend) > Date.parse(eventdate)) {
      alert("Registration End Date Should Be less than event Date");
      $(".datepickerend2").val('');
      return false;
  }
}); 
            $.validator.addMethod("zipcode",function(t,e){return this.optional(e)||t.match(/(^\d{5,6})$/)},"Invalid zip code");

  var i = 1;

$(document).on('click','.add_regtype',function(){

            var childdata='<div class="childinfo"><div class="row"><div class="col-md-3"><input type="hidden" name="regtype[]"" value="'+i+'"/>   <input class="form-control" placeholder="Registration Title" name="regtypename_'+i+'" type="text"></div><div class="col-md-2"> <input class="form-control" placeholder="Amount" name="amount_'+i+'" type="text"></div><div class="col-md-2"><input class="form-control" placeholder="Reg. Count" name="reg_count_'+i+'" type="text"> </div><div class="col-md-2"><input class="form-control" placeholder="Display Sequence" name="order_no_'+i+'" type="text"> </div><div class="col-md-1"><a href="javascript:void(0)" class="btn btn-sm btn-info remove_list2 ">Remove</a></div></div></div>';
            i++;

             $('.event_regtypedata').append(childdata);

        });


    var j=1;
                 $(document).on('click','.add_sponsortype',function(){

           var childdata='<div class="childinfo"><div class="row"><div class="col-md-3"> <input type="hidden" name="sponsortype[]"" value="'+j+'"/>  <input class="form-control" placeholder="Sponsorship Title" name="sponsorname_'+j+'" type="text"></div><div class="col-md-2"> <input class="form-control" placeholder="Amount" name="sponsoramount'+j+'" type="text"></div><div class="col-md-2"><input class="form-control" placeholder="Display Sequence" name="sponsororder_no_'+j+'" type="text"> </div><div class="col-md-1"><a href="javascript:void(0)" class="btn btn-sm btn-info remove_list2 ">Remove</a></div></div></div>';
            j++;

          $('.event_sponsortypedata').append(childdata); 
            });

                $.validator.addMethod("require", function(value, element) {
    return !this.optional(element);
}, "Required");

$.validator.addMethod("number", function(value, element) {
    return this.optional(element) || /^-?\d+$/.test(value);
}, "Number Only");       

            $(document).on('click', '.remove_list2', function(event) {
        $(this).parents(".childinfo").remove();
    });

              $(".addtype").trigger("click"); 

            $(document).on('change', '.is_registration', function(e){
                if (this.checked == true){
                    $('.registration_date').show();

                } else {
                    $('.registration_date').hide();
                }

            });

            $("#dataform").validate({
                rules: {
                    name: "required",
                    contact_mail:"email required",
                    start_date:"required",
                    start_time:"required",
                    end_time:"required",
                    category_id: "required",
                    location_type: "required",
                    zipcode: "zipcode",
                    banner_url: {extension: "jpg|png|gif|jpeg"},
                      
               
                 total_registration_count: {
                  required: function(element) {
                    if($('input[name=is_registration]:checked').length==1)
                    return true;
                  }
                 },
                  registration_start_date: {
                  required: function(element) {
                    if($('input[name=is_registration]:checked').length==1)
                    return true;
                  }
                 },
                  registration_end_date: {
                  required: function(element) {
                    if($('input[name=is_registration]:checked').length==1)
                    return true;
                  }
                 }
                },
                messages: {
                    contact_mail: {required: "Required..!! ", email:"Enter valid email"},
                    start_date: {required: "Required..!! "},
                    start_time: {required: "Required..!! "},
                    end_time:{required: "Required..!! "},
                    name: {required: "Required..!!"},
                    category_id: {required: "Required..!!"},
                    total_registration_count: {required: "Required..!!"},
                    registration_start_date: {required: "Required..!!"},
                    registration_end_date: {required: "Required..!!"},
                    location: {required: "Required..!!"},
                    city: {required: "Required..!!"},
                    state: {required: "Required..!!"},
                    banner_url: {extension: 'Not an image!'},
                    committee_id: {required: 'Required Hosting Committee!'}                    
                },
                 submitHandler: function(form) {
                            if($('input[name=is_registration]:checked').length==1)
                            {
                               if($('input[name="regtype[]"]').length<1)
                               {
                                alert('Registration Type required');
                                return false;
                               }

                            }
                            return true;                       
                 }

            });

            $("#email_reg").change(function() {
                var email = $(this).val();
                var data = "email=" + email;
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('check-email') }}',
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    success: function(res){
                        if (res == 1) {
                            $("#signupbtn").attr("disabled", "disabled");
                            $(".email_status").html('<label class="error" for="email">Email already Exists</label>');
                        } else {
                            $("#signupbtn").removeAttr("disabled");
                            $(".email_status").html('');
                        }

                    }
                });
            });
 
 
        });
    </script>
       
<script>
CKEDITOR.replace( 'event_details', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
CKEDITOR.replace( 'mail_description' , { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );

</script>

@stop

     