@extends('layouts.default')
@section('content_header_title','Contacts')

@section('content')

    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title"> 
 <a href="{{route('admin_enquiries_export')}}" class="btn  btn-info btnexport"
                   data-toggle="tooltip" title="Export to Excel"><i class="fa fa-download" aria-hidden="true"></i>
                    Export to Excel</a>
            </h3>

        </div>
        <div class="box-body">
         <div class="row" style="margin:5px 0px 14px 14px;">


                <div class="col-sm-3 form-inline">
                    <lable class="control-label">Received Date</lable>
                    <div class="input-daterange input-group" id="datepicker">
                        <input type="text" class="input-sm form-control datepicker" name="datefrom" id="datefrom" value="" placeholder="Date from" />
                        <span class="input-group-addon">to</span>
                        <input type="text" class="input-sm form-control datepicker" name="dateto" id="dateto" value="" placeholder="Date to" />
                    </div>
                </div>
                <div class="col-sm-2">
                    <lable class="control-label">Keyword</lable>
                    <input type="text" name="keyword" class="form-control">
                </div>
                 <div class="col-sm-2">
                    <lable class="control-label">Program</lable> 
                    {!! Form::select('program_id', $programs, null,['class' => 'form-control','placeholder' => 'All Programs']) !!}
                </div> 
                <div class="col-sm-3"><br>
                    <button type="button" id="dateSearch" class="btn btn-sm btn-primary">Search</button><span class="filter2" >
                                    <button type="button" id="dateClear" class="btn btn-sm btn-info">Clear</button>
                                          </span>
                </div>
            </div>
          {!! Form::open(['route' => 'admin_enquiries_actions', 'class' => 'form-horizontal','id' => 'products-actions-form']) !!}

                                  {!! Form::submit('Delete Checked Enquiries', ['name' => 'Trash',  'class' => 'btn btn-sm  btn-danger']) !!}


        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="datalist" width="100%">
                <thead>
                <tr >
                    <th><input type="checkbox" name="selectall" value="all" id="checkall" /></th>
                    <th>S.No</th>
                    <th>Program Name<br/>Program Email</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th width="120">Mobile</th> 
                    <th>City</th>
                    <th>State</th> 
                    <th width="300">Message</th>
                    <th width="92">Created at</th>
                    <th width="90">Actions</th>
                </tr>
                </thead> 
            </table>
        </div>
          {!! Form::close() !!}
      </div>
    </div>

    <!-- START: edit modal -->
    <div class="modal fade modal-size-large" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>View Contact Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: edit modal -->


@stop
@section('js')
    <script>
        $(function(){

             var oTable = $('#datalist').DataTable({
                autoWidth: false,
                processing: true,
                serverSide: true,
                order: [[9, "desc"]],
                lengthMenu: [[10, 20, 30, 50], [10, 20, 30, 50]],
                ajax: {
                    url: '{{url('/admin/contacts/data')}}',
                    data: function (d) {
                        d.keyword = $('input[name=keyword]').val();
                        d.datefrom = $('input[name=datefrom]').val();
                        d.dateto = $('input[name=dateto]').val();
                        d.program_id = $('select[name=program_id]').val();
                    }
                },
                columns: [
                    {data: 'cb', name: 'cb', searchable: false, orderable: false},
                    {data: 'DT_Row_Index', name: 'DT_Row_Index', searchable: false, orderable: false},
                    {data: 'program', name: 'program'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'city', name: 'city'},
                    {data: 'state', name: 'state'},
                    {data: 'message', name: 'message'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false}
                ]
            });
            $('.input-daterange').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: ''
            });
           $('#dateSearch').on('click', function () {
                $('.filter2').show();
                datefrom = $('input[name=datefrom]').val();
                dateto = $('input[name=dateto]').val();
                keyword = $('input[name=keyword]').val();
                program_id = $('select[name=program_id]').val();
                $('.btnexport').attr('href', '/admin/enquiries-export?datefrom=' + datefrom + '&dateto=' + dateto+ '&keyword=' + keyword+ '&program_id=' + program_id);
                oTable.draw();
            });
            $('#dateClear').on('click', function () {
                $('.filter2').hide();
                $('input[name=keyword]').val('');
                $('select[name=program_id]').val('');
                $('.datepicker').datepicker('setDate', null);
                $('.btnexport').attr('href', '/admin/enquiries-export');

                oTable.draw(); 
            });
            $('.dataTables_filter').hide();
            $("input[name=keyword]").keypress(function (event) {
                if (event.which == 13) {
                    $("#dateSearch").trigger("click");
                }
            });

               $(document).on('click','.view',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/contacts/')}}/'+id,
                    type: 'GET',
                    success: function(data){
                        $('#view_modal .modal-body').html(data);
                        $('#view_modal').modal('show'); 
                    }
                });

            });

           $('body').on('change', '#checkall', function() {
           var rows, checked;
           rows = $('#datalist').find('tbody tr');
           checked = $(this).prop('checked');
           $.each(rows, function() {
              var checkbox = $($(this).find('td').eq(0)).find('input').prop('checked', checked);
           });
         });
  

            $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.tr_'+id).remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/contacts') }}' + '/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            } else {
                                // swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });

            });

        });

    </script>
@stop