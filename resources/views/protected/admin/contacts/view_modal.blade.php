<div class="row" style="line-height: 25px;">
<div class="col-md-12">Program : {{$contact->programdata->name.', Email:  '.$contact->programdata->email}}  </div>
<div class="col-md-6">Name : {{$contact->name}}  </div>
<div class="col-md-6">Email : {{$contact->email}} </div>
<div class="col-md-6">Mobile : {{$contact->mobile}} </div>
<div class="col-md-6">Created at : {{dateformat($contact->created_at)}} </div>
<div class="col-md-12">Address : {{$contact->address}} </div>
<div class="col-md-12">Address 2 : {{$contact->address2}} </div>
<div class="col-md-6">City : {{$contact->city}} </div>
<div class="col-md-6">State : {{$contact->state}} </div>
<div class="col-md-6">Zipcode : {{$contact->zipcode}} </div>
<div class="col-md-6">Country : {{$contact->country}} </div>
<div class="col-md-12">Message : {{$contact->message}} </div>
</div>
