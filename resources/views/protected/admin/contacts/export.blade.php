<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Program Name</th>
        <th>Program Email</th>
        <th>Name</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>Address 1</th>
        <th>Address 2</th>
        <th>City</th>
        <th>State</th>
        <th>Country</th>
        <th>Zipcode</th>
        <th>Message</th>
        <th>Created at</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($contacts as $data)
    <tr>
        <td>@if($data->programdata){{ $data->programdata->name }}@endif</td>
        <td>@if($data->programdata){{ $data->programdata->email }}@endif</td>
        <td>{{ $data->name }}</td>
        <td>{{ $data->email }}</td>
        <td>{{ $data->mobile }}</td>
        <td>{{ $data->address }}</td>
        <td>{{ $data->address2 }}</td>
        <td>{{ $data->city }}</td>
        <td>{{ $data->state }}</td>
        <td>{{ $data->country }}</td>
        <td>{{ $data->zipcode }}</td>
        <td>{{ $data->message }}</td>
        <td>{{ dateformat($data->created_at) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>