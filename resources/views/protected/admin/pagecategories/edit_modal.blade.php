   {!! Form::model($category, array('route' => ['page-categories.update', $category->id], 'method' => 'PUT','id' => 'dataform')) !!}

                    @include('protected.admin.forms.pagecategories', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>