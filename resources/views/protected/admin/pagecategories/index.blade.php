@extends('layouts.default')
@section('content_header_title','Menu Items')

@section('content')
<div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                  <a href="javascript:void(0);" class="btn btn-sm btn-success pull-right mr-1" data-toggle="modal" data-target="#add_cat">
                      <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp; Add Menu
            </a>

                </h3>

            </div>

                    <div class="box-body">
                    <table class="table table-striped table-bordered table-hover" id="datalist">
                        <thead>
                        <tr >
                            <th>S.No.</th>
                            <th>Menu Items</th>
                            <th>Parent Category</th>
                            <th>Display Order</th>
                            <th width="120">#</th>
                            <th width="95">Status</th>
                             <th width="90">Updated on</th>
                            <th width="85">Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $key=>$data)
                            <tr class="tr_{{ $data->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $data->name }}</td>
                                <td>@if($data->page_parentid){{ $data->parent->name }}@endif</td>
                                <td>{{ $data->position }}</td>
                                <td>@if($data->id_path)
                                <a href="{{url('/admin/pages/'.$data->id_path.'/edit')}}">Edit Page Details</a>
                                @else
                                <a data-toggle="modal" data-target="#page_modal" data-id="{{$data->id}}" class="page_link">Add Page to Item</a>
                                @endif
                                </td>
                                 <td>
                            <select name="stock_status" class="form-control status"  data-style="btn-outline-primary pt-0 pb-0" data-id="{{ $data->id}}"  data-status="{{$data->status}}">
                            <option value="0" @if($data->status == 0) selected @endif >Inactive</option>
                            <option value="1" @if($data->status == 1) selected @endif>Active</option>
                        </select>
                                </td>
                                 <td>{{ dateformat($data->updated_at) }}</td>
                                <td>
                                     <a href="javascript:void(0);" data-id="{{ $data->id }}" class='btn edit btn-sm btn-success' data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                                     <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>

                </div>

<!-- START: edit modal -->
    <div class="modal fade modal-size-large" id="page_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Add Page to Menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
  {!! Form::open(array('route' => 'add_link_to_pagecategories', 'method' => 'POST', 'id' => 'dataform2')) !!}
                <div class="row">
                <div class="col-md-12">
                <input type="hidden" name="category_id" value="" id="category_id" />
                Page Linking to Category
                 {!! Form::select('id_path', $pages_list, null,['class' => 'form-control','required']) !!}
                 </div>
 <div class="col-md-12 m-t10 t-r">
        <a class="btn btn-success" data-dismiss="modal">Cancel</a>

     {!! Form::submit('Add', ['class' => 'btn btn-info ']) !!}
</div>

               </div>
          {!! Form::close() !!}

                </div>

            </div>
        </div>
         </div>
     <!-- END: edit modal -->

<!-- START: edit modal -->
    <div class="modal fade modal-size-large" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Edit Menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: edit modal -->

 <!-- START: Add modal -->
    <div class="modal fade modal-size-large" id="add_cat" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Add Menu</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('route' => 'page-categories.store', 'method' => 'POST', 'id' => 'dataform')) !!}
                        @include('protected.admin.forms.pagecategories', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                        <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>
    <!-- END: Add modal -->

@stop
@section('js')
    <script>
        $(document).ready(function () {

$("#dataform").validate({
            rules: {
                name:"required",
                position: "number"
            },
            messages:
            {
                name:{required:"Enter Category Name" },
                position:{number:"Enter Number only"}
            }
        });


            $(document).on('change','.status',function(e){
                var thisVal = $(this);
                var id = $(this).data('id');
                var status = this.value;
                var prevStatus = $(this).data('status');
                e.stopImmediatePropagation();
                swal({
                            title: "Are you sure?",
                            text: "want to change this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, change it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Changed!", "Your record has been updated!", "success");
                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('admin/change-pagecategory-status') }}',
                                    data: { 'id':id ,'status':status},
                                    type: 'POST',
                                    success: function(data){
                                        //console.log(data);
                                        $(thisVal).attr('data-status',status)
                                    }
                                });

                            } else {
                                swal("Cancelled", "Your record is safe :)", "error");
                                $(thisVal).val(prevStatus);
                            }
                        });

            });

            $('#datalist').DataTable({
                responsive: true
            });

            $(document).on('click','.edit',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/page-categories') }}'+'/'+id+'/edit',
                    type: 'GET',
                    success: function(data){
                        $('#edit_modal .modal-body').html(data);
                        $('#edit_modal').modal('show');
                        $("#dataform").validate({
                            rules: {
                                name:"required",
                                position: "number"
                            },
                            messages:
                            {
                                name:{required:"Enter Category Name"},
                                position:{number:"Enter Number only"}
                            }
                        });
                        $('input[type="text"]').change(function(){
                            s = $.trim(this.value);
                            s= $( $.parseHTML(s)).text();
                            this.value=s;
                        });
                    }
                });

            });

        $(document).on('click','.page_link',function(){
                var id = $(this).data('id');
                $('#category_id').val(id);
                });

         $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.tr_'+id).remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/page-categories') }}'+'/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            } else {
                                // swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });

            });
       });

    </script>
@stop
