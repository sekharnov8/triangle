@extends('layouts.default')
@section('content_header_title','Send Mail')

@section('content')
<div class="box">
            

                    <div class="box-body">
            {!! Form::open(array('url' => 'admin/send_mail', 'method' => 'POST','id' => 'dataform')) !!}

<div class="row">
    <label for="image" class="control-label col-md-2">Subject *</label>

<div class="col-md-10"  >
    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject']) !!}
</div>
</div>
   <div class="row">
     <label for="image" class="control-label col-md-2">From Email *</label>

    <div class="col-md-10"  >
        {!! Form::text('from_email', 'info@tutanc.org', ['class' => 'form-control', 'placeholder' => 'From Email']) !!}
    </div>
    </div>
    <div class="row">

 
  <label for="image" class="control-label col-md-2">To Email *</label>

    <div class="col-md-10"  >
        {!! Form::textarea('to_email', null, ['class' => 'form-control', 'placeholder' => 'To Email', 'rows'=>4]) !!}
    </div>
     <div class="col-md-2"  ></div>  <div class="col-md-10"  >
    (If multiple email ids, separate mail ids by comma)</div>
 </div>
<div class="row">
<div class="col-md-12"  >
     {!! Form::textarea('description', null, ['class' => 'form-control textarea', 'placeholder' => 'Template']) !!}
</div> 
<div class="col-md-12 m-t10 t-r">
     {!! Form::submit('Send Mail', ['class' => 'btn btn-info ']) !!}
</div>
</div>
{!! Form::close() !!}
</div></div>
@stop
@section('js')
 <script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
<script>
CKEDITOR.replace( 'description', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );

 $(function () {

        $("#dataform").validate({
            rules: {
                subject:  "required",
                from_email: "required email",
                to_email: "required"

            },
            messages:
            {
                subject:{required:"Required..!!"},
                from_email:{required:"Required..!!", email:"Invalid Email"},
                to_email:{required:"Required..!!"}
            }
        });
        });
</script>
@stop
