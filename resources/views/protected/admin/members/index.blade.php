@extends('layouts.default')
@section('content_header_title','Members')
@section('content')
    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title">
                <a href="{{route('admin_members_export')}}" class="btn  btn-info btnexport"
                   data-toggle="tooltip" title="Export to Excel"><i class="fa fa-download" aria-hidden="true"></i>
                    Export to Excel</a>
                     <a href="{{route('admin_members_export')}}?info=full" class="btn  btn-info btnexport2"
                   data-toggle="tooltip" title="Export Member Info, Significants, Order Info to Excel"><i class="fa fa-download" aria-hidden="true"></i>
                    Export Full Info </a>
                    <a href="#" class="btn  btn-info"
               data-toggle="modal"  title="Import from Excel" data-target="#members-import">
               <i class="fa fa-upload" aria-hidden="true"></i>
                    Import Members</a>
                <a href="{{ route('members.create')}}" class="btn btn-success">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp; Add Member</a>
            </h3>
        </div>
        <div class="box-body">
            <div class="row">

  <div class="box box-success collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Search</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
         </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="display: none;">
        <div class="row">
        <div class="col-md-3 form-group">
                {!! Form::label('keyword','Keyword') !!}
                {!! Form::text('keyword',null,['class' => 'form-control','id'=>'keyword']) !!}
            </div>
            
          <div class="col-md-3 form-group">
                {!! Form::label('role','Role') !!}
                {!! Form::select('role', $roles, null,['class' => 'form-control selectpicker',  'data-live-search'=>"true",'data-live-search-placeholder'=>"Search", 'data-dropup-auto'=>"false", 'placeholder' =>  'Select Role']) !!}
            </div>
             <div class="col-md-3 form-group">
                {!! Form::label('is_approved','Status') !!}
                {!! Form::select('is_approved', [''=>'All']+$statustypes, null,['class' => 'form-control','placeholder' =>'Select Status']) !!}
             </div>
             <div class="col-md-3 form-group">
                {!! Form::label('mtype','Membership Type') !!}
                 {!! Form::select('mtype', $mtypes, null,['class' => 'form-control','placeholder' =>
                    'Membership Type']) !!}
             </div>
             <div class="col-md-3 form-group">
                {!! Form::label('reg_from','Registered From') !!}
                  <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="datefrom" class="form-control reg_from datepicker" id="datefrom">
                </div>
            </div>

              <div class="col-md-3 form-group">
                {!! Form::label('from','Registered Till') !!}
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="dateto" class="form-control reg_till datepicker" id="dateto">
                </div>
            </div>
           <div class="col-md-3 form-group"><br/>
             <button name="Search" type="button" id="dateSearch" class="btn btn-primary"><i class="fa fa-search">&nbsp;</i>Search</button>
                      &nbsp;  <span class="filter2" style="display:none">
                      <button name="Reset" type="button" id="dateClear"  class="btn btn-danger"><i class="fa fa-refresh">&nbsp;</i>Reset</button></span>

  </div>
</div>
     </div>
</div>
</div>
 

    <div class="box-header">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="userslist">
                    <thead>
                    <tr>

                        <th width="80">Member ID</th>
                        <th > First Name</th>
                        <th > Last Name</th>
                        <th width="200">Email</th>
                        <th width="120">Mobile</th>
                        <th>Role</th>
                        <th  width="120">Membership Type</th>
                        <th width="100">Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th width="85">Last Login</th>
                        <th width="85">Created At</th>
                        <th width="150">Actions&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="members-import" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Import Members</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => 'admin_members_import', 'class' => 'form-horizontal','id' => 'cards-import-form', 'files' => true]) !!}

                    <div class="form-group">
                        {!! Form::label('Import Excel', 'Import Excel File', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="col-sm-8">
                            {!! Form::file('file', ['required' => 'required']) !!}
                            {{ errors_for('file', $errors) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            {!! Form::submit('Import Members', ['class' => 'btn btn-lg btn-info btn-block']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <p align="center">  <a href="{{ route('admin_members_sample') }}" class="btn btn-xs btn-warning"
                            data-toggle="tooltip"  title="Sample Excel"><i class="fa fa-download" aria-hidden="true"></i>
                            Sample Excel</a></p>
                 </div>


            </div><!-- /.modal-content -->

        </div><!-- /.modal-dialog -->

    </div>

         <!-- START: view modal -->
    <div class="modal fade modal-size-large" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>View Member Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>

            </div>
        </div>
    </div>
    <!-- END: view modal -->

@stop
@section('js')
    <script>
        $(function () {
            var oTable = $('#userslist').DataTable({
                scrollX: true,
                processing: true,
                serverSide: true,
                order: [[0, "desc"]],
                lengthMenu: [[10, 20, 30, 50], [10, 20, 30, 50]],
                ajax: {
                    url: '{{url('/admin/members/data')}}',
                    data: function (d) {
                        d.keyword = $('input[name=keyword]').val();
                        d.datefrom = $('input[name=datefrom]').val();
                        d.dateto = $('input[name=dateto]').val();
                        d.is_approved = $('select[name=is_approved]').val();
                        d.role = $('select[name=role]').val();
                        d.mtype = $('select[name=mtype]').val();
                    }
                },
                columns: [
                    {data: 'member_id', name: 'member_id'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'role', name: 'role',searchable: false, orderable: false},
                    {data: 'membershiptype', name: 'membershiptype',searchable: false, orderable: false},
                    {data: 'status', name: 'status', searchable: false, orderable: false},
                    {data: 'last_login', name: 'last_login'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'actions', name: 'actions', searchable: false, orderable: false}
                ]
            });
            $('.input-daterange').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: ''
            });

            $('#dateSearch').on('click', function () {
                $('.filter2').show();
                datefrom = $('input[name=datefrom]').val();
                dateto = $('input[name=dateto]').val();
                keyword = $('input[name=keyword]').val();
                mtype = $('select[name=mtype]').val();
                status = $('select[name=is_approved]').val();
                role=$('select[name=role]').val();
                exporturl='/admin/members-export?s=';
                exporturl2='/admin/members-export?info=full';
                export_q='';
                if (datefrom && dateto)
                    export_q=export_q+'&datefrom=' + datefrom + '&dateto=' + dateto;
                if (mtype!='')
                    export_q=export_q+'&mtype=' + mtype;
                if (keyword!='')
                    export_q=export_q+'&keyword=' + keyword;
                if (role!='')
                    export_q=export_q+'&role=' + role;
                 if (status!='')
                    export_q=export_q+'&status=' + status;
                    $('.btnexport').attr('href', exporturl+export_q);
                    $('.btnexport2').attr('href', exporturl2+export_q);
                oTable.draw();
            });
            $('#dateClear').on('click', function () {
                $('.filter2').hide();
                $('.btnexport').attr("href", "/admin/members-export");
                $('.btnexport2').attr("href", "/admin/members-export?info=full");
                $('input[name=keyword]').val('');
                $('select[name=is_approved]').val('');
                $('select[name=role]').val('');
                $('select[name=mtype]').val('');
                $(".selectpicker").selectpicker("refresh");

                oTable.draw();
            });
            $('.dataTables_filter').hide();
            $("input[name=keyword]").keypress(function (event) {
                if (event.which == 13) {
                    $("#dateSearch").trigger("click");
                }
            });

              $(document).on('change','.status',function(e){
            var thisVal = $(this);
            var id = $(this).data('id');
            var status = this.value;
            var prevStatus = $(this).data('status');
            e.stopImmediatePropagation();
            swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('admin/change-user-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    //console.log(data);
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                           //$(thisVal).selectpicker("refresh");
                        }
                    });

        });

              $(document).on('click','.view',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/members/')}}/'+id,
                    type: 'GET',
                    success: function(data){
                        $('#view_modal .modal-body').html(data);
                        $('#view_modal').modal('show'); 
                    }
                });

            });


            $(document).on('click', '.delete', function () {

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.id_' + id).parent().parent().remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token': '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/members') }}' + '/' + id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function (data) {
                                    }
                                });

                            } else {
                                // swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });

            });

        });
    </script>
@stop
