@extends('layouts.default')
@section('content_header_title','Add Member')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/members') }}">Members</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('members.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'members.store', 'method' => 'POST', 'files' => true,'id'=>'dataform')) !!}
                        @include('protected.admin.forms.members', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $("#dataform").validate({
                rules: {
                    role_id:  "required",
                    membershiptype_id: "required",
                    first_name: "required alpha",
                    last_name: "required alpha",
                    email: "required email",
                    description: "required",
                    profile_image: {extension: "jpg|png|gif|jpeg"},
                    zipcode:"number"
                },
                messages:
                {
                    role_id:  {required:"Select Role"},
                    membershiptype_id:{required:"Select Membership type"},
                    first_name:{required:"Enter First Name",alpha:"Special characters not allowed"},
                    last_name:{required:"Enter Last Name",alpha:"Special characters not allowed"},
                    email:{required:"Enter Email Id", email: "Enter valid email id"},
                    profile_image: {extension: 'Not an image!'},
                    zipcode: {number: 'number only'}
                }
            });
              $("#email_reg").change(function() {
                var email = $(this).val();
                var data = "email=" + email;
                $.ajax({
                headers: {
                    'X-CSRF-Token' : '{{ csrf_token() }}'
                },
                url: '{{ url('check-email') }}',
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(res){
                    if (res == 1) {
                        $("#signupbtn").attr("disabled", "disabled");
                        $(".email_status").html('<label class="error" for="email">Already registered with this email id.</label>');
                    } else {
                        $("#signupbtn").removeAttr("disabled");
                        $(".email_status").html('');
                    }

                }
            });
        });
        });
    </script>

@stop

