<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Mobile</th>
        <th>Email</th>
        <th>Membership Type</th>
        <th>Last Login</th>
        <th>Role</th>
        <th>Member Since</th>
        <th>Status</th>
        <th>Created At</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user->id}}</td>
            <td>{{ $user->first_name }} {{ $user->last_name }}</td>
            <td>{{ $user->mobile }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->membershiptype }}</td>
            <td>{{ $user->last_login }}</td>
            <td>{{ getUserRoles($user->id) }}</td>
        <th>{{ $user->member_since }}</th>
        <th> {{getstatusByTypeAndNo(2,$user->is_approved)}}</th>
            <td>{{ $user->created_at }}</td>
        </tr>
    @endforeach
</table>