@extends('layouts.default')
@section('content_header_title','Edit Member')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/members') }}">Members</a></li>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css')}}">

@endsection
@section('content')

    <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title pull-right">
                    <a href="{{ route('members.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
        <div class="box-body">
            {!! Form::model($user, array('route' => ['members.update', $user->id], 'method' => 'PUT',  'files' => true,'id'=>'dataform')) !!}

            <div class="row"  >
               
                 <div class="col-md-6"  >
                    <label  class="control-label">Role</label>
                    {!! Form::select('roles[]', $roles, $member_roles,['class' => 'form-control role',  'multiple' => 'multiple']) !!}
                </div>
                <div class="col-md-6"  >
                    <label  class="control-label">Membership Type</label>
                    {!! Form::select('membershiptype_id', $membershiptypes, $user->membershiptype_id,['class' => 'form-control','placeholder' => 'Select Membership Type']) !!}
                </div>
                </div>
                <div class="row">
                <div class="col-md-6"  >
                    <label for="status" class="control-label">Is Approved</label>
                    {!! Form::select('is_approved', [''=>'Select Approve Status']+$statustypes,null, ['class' => 'form-control']) !!}

                </div>
                <div class="col-md-6"  >
                    <label  class="control-label">Referred By</label>
                    {!! Form::text('referred_by', $user->referred_by, ['class' => 'form-control', 'placeholder' => 'Referred by']) !!}
                </div>
                </div>
                <div class="row">
                 <div class="col-md-6"  >
                    <label  class="control-label">Member Since</label>
                    {!! Form::text('member_since', $user->member_since, ['class' => 'form-control datepicker']) !!}
                </div>
                 <div class="col-md-6"  >
                    <label  class="control-label">Are you above 18 years?</label>
                {!! Form::select('memberage', ['Yes'=>'Yes', 'No'=>'No'],null, ['class' => 'form-control']) !!}

                </div>
                </div>
                <div class="row">
                 <div class="col-md-6"  >
                        <label   class="control-label">Password (Leave blank if no change)</label>
                        <input type="password" name="password" class="form-control" > 
                     </div>
                     <div class="col-md-6"  >
                        <label   class="control-label">Confirm Password </label>
                         <input type="password" name="confirmpassword" class="form-control" > 
                     </div></div> 
                 <h3>Personal Details</h3>
                   <div class="row">
                     <div class="col-md-6"  >
                        <label   class="control-label">Title</label>
                        {!! Form::select('title', ['Mr.'=>'Mr.', 'Ms.'=>'Ms.', 'Mrs.'=>'Mrs.', 'Dr.'=>'Dr.', 'Prof.'=>'Prof.', 'Others'=>'Others' ],null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-6"  >
                        <label   class="control-label">First Name</label>
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                    </div>
                    </div>
                      <div class="row">

                    <div class="col-md-6"  >
                        <label   class="control-label">Middle Name</label>
                        {!! Form::text('middle_name', null, ['class' => 'form-control', 'placeholder' => 'Middle Name']) !!}
                    </div>
                    <div class="col-md-6"  >

                        <label   class="control-label">Last Name</label>
                        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                    </div>
                    </div>
                      <div class="row">
                    <div class="col-md-6"  >

                        <label   class="control-label">Email</label>
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                    </div>
                    <div class="col-md-6"  >

                        <label   class="control-label">Mobile Number</label>
                        {!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile']) !!}
                    </div>
                    </div>
                      <div class="row">
                   
                     <div class="col-md-6"  >

                        <label   class="control-label">Skills</label>
                        {!! Form::text('skills', $user->skills, ['class' => 'form-control']) !!}
                    </div>
                     <div class="col-md-6"  >

                        <label   class="control-label">Occupation</label>
                        {!! Form::text('occupation', $user->occupation, ['class' => 'form-control', 'placeholder' => 'Occupation']) !!}
                    </div>
                    
                    <div class="col-md-6"  >

                        <label   class="control-label">DOB</label>
                        {!! Form::text('dob', $user->dob, ['class' => 'form-control','id'=>'dob', 'placeholder' => 'DOB']) !!}
                    </div>
                    <div class="col-md-6"  >
                        <label   class="control-label">Profile Image</label>
                        {!!Form::file('profile_image',['placeholder' => 'Image','id'=>'image', 'class' => 'image','accept'=>'.jpg,.png,.gif,jpeg'])!!}
                    </div></div>
                    @if($user->profile_image && File::exists('uploads/users/'.$user->profile_image))
                     <img src="{{url('uploads/users/'.$user->profile_image)}}" width="110" height="110" alt="" border="" />
                     @endif
  
            <fieldset>
                <legend>Contact Details</legend>
                <div class="row"  >

                   
                    <div class="col-md-6"  >

                        <label   class="control-label">Address</label>
                        {!! Form::text('address', $user->address, ['class' => 'form-control', 'placeholder' => 'Address']) !!}
                    </div>
                     <div class="col-md-6"  >

                        <label   class="control-label">Address Line 2</label>
                        {!! Form::text('address2', $user->address2, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-6"  >

                        <label   class="control-label">City</label>
                        {!! Form::text('city', $user->city, ['class' => 'form-control', 'placeholder' => 'City']) !!}
                    </div>
                    <div class="col-md-6"  >

                        <label   class="control-label">State</label>
                        {!! Form::text('state', $user->state, ['class' => 'form-control', 'placeholder' => 'State']) !!}
                    </div>
                    <div class="col-md-6"  >

                        <label   class="control-label">Country</label>
                        {!! Form::text('country', $user->country, ['class' => 'form-control', 'placeholder' => 'Country']) !!}
                    </div>
                    <div class="col-md-6"  >

                        <label   class="control-label">Zipcode</label>
                        {!! Form::text('zipcode', $user->zipcode, ['class' => 'form-control', 'placeholder' => 'Zip']) !!}
                    </div>


                </div>

            </fieldset>
            @if($order)
             <fieldset>
                <legend>Payment Details</legend>
                <div class="row"  >

                    <div class="col-md-6"  >

                        <label   class="control-label"> Membership Type </label><br/>
                  {{$order->membershiptype->name}}

                    </div>
                    <div class="col-md-6"  >
                        <label   class="control-label">Price</label>
                        {!! Form::text('amount', $order->amount, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
                    </div>
                    <div class="col-md-2"  >
                        <label   class="control-label">Transaction Id</label>
                    </div>
                    <div class="col-md-4"  ><label   class="control-label">{{  $order->transaction_id }}</label></div>
                    <div class="col-md-2"  >
                        <label   class="control-label ">Donation Amount</label>
                    </div>
                     <div class="col-md-4"  ><label   class="control-label">$ @if(!$order->donation_amount)0 @endif {{  $order->donation_amount }}</label></div>
                    <div class="col-md-6"  >

                        <label   class="control-label">Payment Status</label>
                        {!! Form::select('status',$order_statustypes,$order->status, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-6"  >
                        <label   class="control-label">Payment Method</label>
                        {!! Form::select('paymentmethod_id', [''=>'Select Payment Method']+$paymentmethods,$order->paymentmethod_id, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </fieldset>
            @endif 
            <label   class="control-label">Terms & Conditions</label>
            {!! Form::textarea('terms_conditions', $user->terms_conditions, ['class' => 'form-control', 'placeholder' => 'Terms & Conditions']) !!}

            <div class="col-md-12 m-t10 t-r">

                {!! Form::submit('Update', ['class' => 'btn btn-warning ']) !!}
            </div>
            {!! Form::close() !!}

        </div>
        <div class="clearfix"></div>
              </div>
@stop

@section('js')
    <script type="text/javascript" src="{{asset('plugins//bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
<link media="all" type="text/css" rel="stylesheet" href="{{url('plugins/pikaday/pikaday.css')}}">
    <script src="{{url('plugins/pikaday/moment.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.jquery.js')}}"></script>
<script>
 
        $(document).ready(function () {

     $(".role").select2({multiple: true});

      var picker = new Pikaday(
                  {
                      field: document.getElementById('dob'),
                      firstDay: 1,
                      format: 'MM-DD-YYYY',
                      minDate: new Date({{date("Y")-100}}, 0, 1),
                      maxDate: new Date({{date("Y")-1}}, 12, 31),
                      yearRange: [{{date("Y")-100}},{{date("Y")-1}}]
                  });
    

 });
     
</script>

@stop