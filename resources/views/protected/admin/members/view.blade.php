 
            <div class="row"  >
                
                <div class="col-md-6"  >
                    Membership Type: 
                  @if($user->membership)   {{$user->membership->name}} @endif
                </div>
                <div class="col-md-6"  >
                    Is Approved:  {{getstatusByTypeAndNo(2,$user->is_approved)}}

                </div>
                <div class="col-md-6"  >
                    Referred By:   {{$user->referred_by}}
                </div>
                  <div class="col-md-6"  >
                    Member Since:   {{$user->member_since}}
                </div>
                 <div class="col-md-6"  >
                    Are you above 18 years?:   {{$user->memberage}}
                </div>
                
            </div>

            <h4>
               Personal Details</h4>
                <div class="row"  >
                    <div class="col-md-6"  >
                       Title: 
                                             {{$user->title}}

                    </div>
                    <div class="col-md-6"  >
                       First Name: 
                       {{$user->first_name}}
                    </div>
                    <div class="col-md-6"  >
                       Middle Name: 
                        {{$user->middle_name}}
                    </div>
                    <div class="col-md-6"  >

                       Last Name: 
                         {{$user->last_name}}
                    </div>
                    <div class="col-md-6"  >

                       Email: 
                       {{$user->email}}
                    </div>
                    <div class="col-md-6"  >

                       Mobile Number: 
                        {{$user->mobile}}
                    </div>
                    <div class="col-md-6"  >

                       Alternative Number: 
                        {{$user->home_phone}}
                    </div>
                   
                    <div class="col-md-6"  >

                       DOB: 
                        {{$user->dob}}
                    </div>
                    <div class="col-md-6"  >
                       Profile Image
                       @if($user->profile_image && File::exists('uploads/users/'.$user->profile_image)) <img src="{{url('uploads/users/'.$user->profile_image)}}" width="160" height="160" alt="" class="s-blueborder border3 border-radius4 userprofileimage" />@endif 
                    </div>
                      <div class="col-md-6"  >  Member Skills:   {{$user->skills}}
                    </div> <div class="col-md-6"  > 
                       Occupation:  {{$user->occupation}}
                    </div>
                                    </div>
               <h4>
               Spouse Details</h4>
  @if($spouse)

   <div class="col-md-6"  >First Name: {{$spouse->first_name}}</div>
   <div class="col-md-6"  >Last Name: {{$spouse->last_name}}</div>
   <div class="col-md-6"  >Mobile: {{$spouse->phone}}</div>
   <div class="col-md-6"  >Enail: {{$spouse->email}}</div>
   <div class="col-md-6"  >Occupation: {{$spouse->occupation}}</div>
   <div class="col-md-6"  >Skills: {{$spouse->skills}}</div>

 
                                                    @else
                                                        <h4 style="font-size: 20px;">N/A</h4>
                                                        @endif
                <h4>
           <br/>    Children Details</h4>

               @if(!count($childrens))
                                                        <h4 style="font-size: 20px;">N/A</h4>
                                                    @endif
                                                    @foreach ($childrens as $key=>$children)
   <div class="col-md-6"  >Name: {{$children->first_name}}</div>
    <div class="col-md-6"  >DOB: {{$children->dob}}</div>
   <div class="col-md-6"  >Relation: {{$children->relationship}}</div>
   <div class="col-md-6"  >Skills: {{$children->skills}}</div>
                                                 
                                                    @endforeach
 
            <h4>
               Contact Details</h4>
                <div class="row"  >

                   
                    <div class="col-md-6"  >

                       Address Line 1: 
                       {{$user->address}}
                    </div>
                      <div class="col-md-6"  >

                       Address Line 2: 
                       {{$user->address2}}
                    </div>

                    <div class="col-md-6"  >

                       City: 
                       {{$user->city}}
                    </div>
                    <div class="col-md-6"  >

                       State: 
                         {{$user->state}}
                    </div>
                    <div class="col-md-6"  >

                       Country: 
 {{$user->country}}                    </div>
                    <div class="col-md-6"  >

                       Zipcode: 
 {{$user->zipcode}}                    </div>


                </div>

             

               
