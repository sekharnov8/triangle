<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Mobile</th>
        <th>Email</th>
        <th>Membership Type</th>
        <th>Is Above 18 years</th>
        <th>Gender</th>
        <th>Referred By</th>
        <th>Date of Birth</th>
        <th>Occupation</th>
        <th>Member Skills</th>
        <th>Address Line 1</th>
        <th>Address Line 2</th>
        <th>City</th>
        <th>State</th>
        <th>Zipcide</th>
        <th>Country</th>
        <th>Last Login</th>
        <th>Role</th>
        <th>Created at</th>
        <th>Member Status</th>
        <th>Spouse First Name</th>
        <th>Spouse Last Name</th>
        <th>Spouse Email</th>
        <th>Spouse Phone</th>
        <th>Spouse Occupation</th>
        <th>Spouse Skils</th>
        <th>Child 1 Info</th>
        <th>Child 2 Info</th>
        <th>Child 3 Info</th>
        <th>Child 4 Info</th>
        <th>Order Amount </th>
        <th>Payment Method </th>
        <th>Transaction Id </th>
        <th>Bank Name </th>
        <th>Check No </th>
        <th>Check Date </th>
        <th>Check Handed to </th>
        <th>Donation Amount </th>
        <th>Order Status </th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user->id}}</td>
            <td>{{ $user->title }}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->middle_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->mobile }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->membershiptype }}</td>
            <td>{{ $user->memberage }}</td>
            <td>{{ $user->gender }}</td>
            <td>{{ $user->referred_by }}</td>
            <td>{{ $user->dob }}</td>
            <td>{{ $user->occupation }}</td>
            <td>{{ $user->skills }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->address2 }}</td>
            <td>{{ $user->city }}</td>
            <td>{{ $user->state }}</td>
            <td>{{ $user->zipcode }}</td>
            <td>{{ $user->country }}</td>
            <td>{{ $user->last_login }}</td>
            <td>{{ $user->role }}</td>
            <td>{{ $user->created_at }}</td>
            <td>{{getstatusByTypeAndNo(2,$user->is_approved)}}</td>
            @php $spouse=getSpouseInfo($user->memberid); @endphp
            @if($spouse)
            <td>{{$spouse->first_name}}</td>
            <td>{{$spouse->last_name}}</td>
            <td>{{$spouse->email}}</td>
            <td>{{$spouse->phone}}</td>
            <td>{{$spouse->occupation}}</td>
            <td>{{$spouse->skills}}</td>
            @else
            <td></td><td></td><td></td><td></td><td></td><td></td>
            @endif
            @php $childrens=getChildrenInfo($user->memberid); @endphp
            @foreach($childrens as $data)
            <td>Name: {{$data->first_name}}, DOB: {{$data->dob}}, Relation: {{$data->relationship}}, Skills: {{$data->skills}}</td>
            @endforeach
            @for($i=1;$i<=4-count($childrens);$i++)
            <td></td>
            @endfor
            @php $order=getLatestOrder($user->memberid,$user->membershiptype_id); @endphp
            @if($order)
            <td>{{$order->amount}}</td>
            <td>@if($order->paymentmethod){{$order->paymentmethod->name}}@endif</td>
            <td>{{$order->transaction_id}}</td>
            <td>{{$order->bankname}}</td>
            <td>{{$order->cheque_no}}</td>
            <td>{{$order->cheque_date}}</td>
            <td>{{$order->handedto}}</td>
            <td>{{$order->donation_amount}}</td>
            <td>{{($order->status==1)?'Completed':'Pending'}}</td>
            @else
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
            @endif    
        </tr>
    @endforeach
</table>