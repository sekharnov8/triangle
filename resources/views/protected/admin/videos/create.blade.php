@extends('layouts.default')
@section('content_header_title','Add Video')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/videos') }}">Videos</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('videos.index')}}" class="btn  btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'videos.store', 'id' =>'dataform', 'method' => 'POST', 'files' => true)) !!}
                        @include('protected.admin.forms.videos', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
@section('js')
<script> 

 $(function() {
       

     $(document).ready(function () {

         $("#dataform").validate({
             rules: {
                 name: "required",
                 video_url: "required",
                 videocategory_id: "required"
             },
             messages:
             {
                 name:{required:"Enter Video Title"},
                 video_url:{required:"Enter Video Url"},
                 videocategory_id:{required:"Select Video Category"}
             }
         });

         $('#datalist').DataTable({
             responsive: true
         });
     });


    });
</script>
@stop
