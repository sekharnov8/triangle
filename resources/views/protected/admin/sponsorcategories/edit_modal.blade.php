{!! Form::model($category, array('route' => ['sponsor-categories.update', $category->id], 'method' => 'PUT','id' => 'dataform')) !!}

@include('protected.admin.forms.sponsorcategories', ['submitButtonText' => 'Update'])

{!! Form::close() !!}
<div class="clearfix"></div>




