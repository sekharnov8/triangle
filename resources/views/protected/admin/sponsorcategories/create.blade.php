@extends('layouts.default')
@section('content_header_title','Add Category')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/sponsor-categories') }}">Sponsor Categories</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('sponsor-categories.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'sponsor-categories.store', 'method' => 'POST','id'=>'dataform', 'files' => true)) !!}
                        @include('protected.admin.forms.sponsorcategories', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
