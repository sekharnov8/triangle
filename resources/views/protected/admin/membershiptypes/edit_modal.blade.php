   {!! Form::model($mtype, array('route' => ['membershiptypes.update', $mtype->id], 'method' => 'PUT','id' => 'dataform')) !!}

                    @include('protected.admin.forms.membershiptypes', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>

   <script>
       if({{$mtype->validity}}==0)
       {
           $('.validitydate').show();
       }
   </script>