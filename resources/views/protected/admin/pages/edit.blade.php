@extends('layouts.default')
@section('content_header_title','Edit Page')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/pages') }}">Pages</a></li>
@stop
@section('content')

    <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title pull-right">
                    <a href="{{ route('pages.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                <div class="box-body">
                    {!! Form::model($page, array('route' => ['pages.update', $page->id], 'method' => 'PUT',  'files' => true, 'id'=>'dataform')) !!}

                    @include('protected.admin.forms.pages', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
                </div>
@stop

@section('js')


    <script type="text/javascript" src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>
<script>
CKEDITOR.replace( 'description', { contentsCss : '{{ asset('css/editor.css')}}',
        filebrowserBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('plugins/ckfinder/ckfinder.html?type=Images') }}',
        filebrowserUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
    } );
</script>

    <script>
        $(document).ready(function () {


            $("#dataform").validate({
                rules: {
                    pagecategory_id:  "required",
                    name: "required",
                    display_order: "number",
                    description: "required"
                },
                messages:
                {
                    pagecategory_id:  {required:"Select Category"},
                    name:{required:"Enter Page Title"},
                    display_order:{number:"Enter Number only"},
                    description:{required:"Enter Description"}
                }
            });
        });
    </script>

@stop
