@extends('layouts.default')
@section('content_header_title','Pages')
@section('content')
    <div class="box">
        <div class="box-header clearfix">
            <h3 class="box-title">
                <a href="{{ route('pages.create')}}" class="btn btn-success" >
                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Page</a>
            </h3>

        </div>

        <div class="box-body">
            {!! Form::open(['url' => 'admin/searchPages','id' => 'search_form', 'method'=>'post']) !!}
             <div class="row">
                <div class="col-sm-1">Category:  </div>
              <div class="col-sm-4">
            <select name="category" class="form-control">
                <option value="0">All</option>
                @foreach ($category_list as $item)
                    <option value="{{$item['id']}}" @if(isset($category) && $category == $item['id']) selected @endif >{{$item['name']}}</option>
                    @foreach(pagecategories_sublist($item['id']) as $subitem)
                        <option value="{{$subitem->id}}" @if(isset($category) && $category == $subitem->id) selected @endif>| {{$subitem->name}}</option>
                        @foreach(pagecategories_sublist($subitem->id) as $subitem2)
                            <option value="{{$subitem2->id}}" @if(isset($category) && $category == $subitem2->id) selected @endif>| | {{$subitem2->name}}</option>
                        @endforeach
                    @endforeach
                @endforeach
            </select>
                  </div>
                <div class="col-sm-4"><button type="submit" id="dateSearch" class="btn btn-sm btn-primary">Search</button>
                                    <span class="filter2" @if(isset($category)) @else style="display:none" @endif>
                                   <a href="{{url('/admin/pages')}}"><button type="button" id="dateClear" class="btn btn-sm btn-info">Clear</button></a>
                                          </span>
                </div>
                </div>
            {!! Form::close() !!}
            <br/>
            <table class="table table-striped table-bordered table-hover" id="datalist">
                <thead>
                <tr >
                    <th>S.No</th>
                    <th>Menu</th>
                    <th>Title</th>
                    <th>Order No</th>
                    <th>Is Login</th>
                    <th>Status</th>
                    <th>Updated on</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($pages as $key=>$data)
                    <tr class="tr_{{ $data->id }}">
                        <td>{{ $key+1 }}</td>
                        <td>@if(isset($data->category)){{ $data->category->name }}@endif</td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->display_order }}</td>
                        <td>{{ ($data->is_login == 0)?'No':'Yes' }}</td>
                        <td>
                            <select name="status" class="form-control status"  data-style="btn-outline-primary pt-0 pb-0" data-id="{{ $data->id}}"  data-status="{{$data->status}}">
                                <option value="0" @if($data->status == 0) selected @endif >Inactive</option>
                                <option value="1" @if($data->status == 1) selected @endif>Active</option>
                            </select>
                        </td>
                        <td>{{ dateformat($data->updated_at) }}</td>
                        <td>
                          <a class='btn  btn-sm btn-info' href="{{ url($data->slug)}}" data-toggle="tooltip" title="View" target="_blank"><i class="ion-eye"></i></a>
                            <a class='btn  btn-sm btn-success' href="{{ route('pages.edit',$data->id)}}" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                            <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $('#datalist').DataTable({
                responsive: true
            });
        });


        $(document).on('change','.status',function(e){
            var thisVal = $(this);
            var id = $(this).data('id');
            var status = this.value;
            var prevStatus = $(this).data('status');
            e.stopImmediatePropagation();
            swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('admin/change-page-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    //console.log(data);
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                            //$(thisVal).selectpicker("refresh");
                        }
                    });

        });

        $(document).on('click','.delete',function(){

            var id = $(this).data('id');

            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Deleted!", "Your record has been deleted!", "success");
                            $('.tr_'+id).remove();

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('/admin/pages') }}' + '/' + id,
                                type: 'DELETE',
                                dataType: 'json',
                                success: function(data){

                                }
                            });

                        } else {
                          //  swal("Cancelled", "Your record is safe :)", "error");
                        }
                    });

        });


    </script>
@stop
