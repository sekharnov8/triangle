

                    {!! Form::model($category, array('route' => ['donation-categories.update', $category->id], 'id' =>'dataform', 'method' => 'PUT',  'files' => true)) !!}

                    @include('protected.admin.forms.donationcategories', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>

