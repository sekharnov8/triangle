

                    {!! Form::model($statustype, array('route' => ['status-types.update', $statustype->id], 'id' =>'dataform', 'method' => 'PUT')) !!}

                    @include('protected.admin.forms.statustypes', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}
                    <div class="clearfix"></div>

