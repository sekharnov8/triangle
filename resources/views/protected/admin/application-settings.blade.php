@extends('layouts.default')
@section('content_header_title','Application Settings')
@section('css')
  <link rel="stylesheet" href="{{url('plugins/colorpicker/bootstrap-colorpicker.min.css')}}">
@stop
@section('content')
<div class="box">
            

                    <div class="box-body">
                    {!! Form::model($appinfo, array('url' => 'admin/appinfo', 'method' => 'POST',  'files' => true,'id' => 'dataform')) !!}

 
<div class="row">
<div class="col-md-6"  >
    <label  class="control-label">Site Name *</label>
 {!! Form::text('sitename', null, ['class' => 'form-control ']) !!}
        </div>
        <div class="col-md-6"  >
    <label  class="control-label">Website URL *</label>
 {!! Form::text('website', null, ['class' => 'form-control ']) !!}
        </div>
        <div class="col-md-6"  >
    <label  class="control-label">Email</label>
 {!! Form::text('email', null, ['class' => 'form-control ']) !!}
        </div>
          <div class="col-md-6"  >
    <label  class="control-label">Phone</label>
 {!! Form::text('phone', null, ['class' => 'form-control ']) !!}
        </div>
          <div class="col-md-6"  >
    <label  class="control-label">Mail Limit </label>
 {!! Form::text('mail_limit', null, ['class' => 'form-control ']) !!}
        </div>

<div class="col-md-12"  >
    <label  class="control-label">Address</label>
 {!! Form::text('address', null, ['class' => 'form-control ']) !!}
        </div>

<div class="col-md-12"  >
    <label  class="control-label">Equick Stock Member Services Info</label>
 {!! Form::text('stockmember_info', null, ['class' => 'form-control ']) !!}
        </div>

        </div>
 
  <label  class="control-label"><strong>Social Links</strong></label>
    <div class="row">

 <div class="col-md-6"  >
    <label  class="control-label">Facebook URL</label>
 {!! Form::text('facebook_url', null, ['class' => 'form-control ']) !!}
        </div>
 <div class="col-md-6"  >
    <label  class="control-label">Twitter URL</label>
 {!! Form::text('twitter_url', null, ['class' => 'form-control ']) !!}
        </div>
         <div class="col-md-6"  >
    <label  class="control-label">Linkedin URL</label>
 {!! Form::text('linkedin_url', null, ['class' => 'form-control ']) !!}
        </div>
         <div class="col-md-6"  >
    <label  class="control-label">Youtube URL</label>
 {!! Form::text('youtube_url', null, ['class' => 'form-control ']) !!}
        </div>
  </div>
    <label  class="control-label"><strong>Layout Settings</strong></label>
    <div class="row">
 <div class="col-md-6"  >
    <label  class="control-label">Top Bar Background Color </label>
     <div class="input-group colorpicker2"><input type="text" name="top_bg_color" class="form-control" value="{{$appinfo->top_bg_color}}"><div class="input-group-addon"><i></i></div></div>
 </div>
 <div class="col-md-6"  >
    <label  class="control-label">Menu Background Color</label>
     <div class="input-group colorpicker2"><input type="text" name="menu_bg_color" class="form-control" value="{{$appinfo->menu_bg_color}}"><div class="input-group-addon"><i></i></div></div>
         </div>
          <div class="col-md-6"  >
    <label  class="control-label">Menu Background Active/Hover Color</label>
     <div class="input-group colorpicker2"><input type="text" name="menu_bg_color2" class="form-control" value="{{$appinfo->menu_bg_color2}}"><div class="input-group-addon"><i></i></div></div>
         </div>
         <div class="col-md-6"  >
    <label  class="control-label">Menu Text Color</label>
     <div class="input-group colorpicker2"><input type="text" name="menu_text_color" class="form-control" value="{{$appinfo->menu_text_color}}"><div class="input-group-addon"><i></i></div></div>
         </div>
     </div>
   
      <div class="row">
   
<div class="col-md-12 m-t10 t-r">
     {!! Form::submit('Update', ['class' => 'btn btn-info ']) !!}
</div>
</div>
{!! Form::close() !!}
</div></div>
@stop
@section('js') 
<script src="{{url('plugins/colorpicker/bootstrap-colorpicker.min.js')}}"></script>

<script>
 
 $(function () {

        $("#dataform").validate({
            rules: {
                subject:  "required",
                from_email: "required email",
                to_email: "required"

            },
            messages:
            {
                subject:{required:"Required..!!"},
                from_email:{required:"Required..!!", email:"Invalid Email"},
                to_email:{required:"Required..!!"}
            }
        });
    $(".colorpicker2").colorpicker();

        });
</script>
@stop
