@extends('layouts.default')
@section('content_header_title','Greetings')

@section('content')
<div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                 <a href="{{ route('greetings.create')}}" class="btn btn-success" >
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Greeting</a>

                </h3>

            </div>

                    <div class="box-body">
                    <table class="table table-striped table-bordered table-hover" id="datalist">
                        <thead>
                        <tr >
                            <th>S.No.</th>
                            <th>Title</th>
                            <th>Greeting</th>
                            <th>Expire Date</th>
                            <th>Status</th>
                             <th>Updated on</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($greetings as $key=>$data)
                            <tr class="tr_{{ $data->id }}">
                                <td>{{ $key+1 }}</td>
                                <td>{{ $data->name }}</td>
                                <td>@if($data->image)<img src="{{ url('uploads/greetings/'.$data->image) }}" width="100">
                                    @else<img src="{{ url('images/event-no-image.png')}}" height="60" width="90">
                                    @endif</td>
                                <td>{{ dateformat($data->expiredate) }}</td>

                                 <td>
                                    <select name="status" class="form-control status"  data-style="btn-outline-primary pt-0 pb-0" data-id="{{ $data->id}}"  data-status="{{$data->status}}">
                                        <option value="0" @if($data->status == 0) selected @endif >Inactive</option>
                                        <option value="1" @if($data->status == 1) selected @endif>Active</option>
                                    </select>
                                </td>
                                 <td>{{ dateformat($data->updated_at) }}</td>
                                <td>
                                     <a class='btn  btn-sm btn-success' href="{{ route('greetings.edit',$data->id)}}" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                                    <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $data->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>

                </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $('#datalist').DataTable({
                responsive: true
            });
        });

        $(document).on('change','.status',function(e){
            var thisVal = $(this);
            var id = $(this).data('id');
            var status = this.value;
            var prevStatus = $(this).data('status');
            e.stopImmediatePropagation();
            swal({
                        title: "Are you sure?",
                        text: "want to change this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, change it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Changed!", "Your record has been updated!", "success");
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('admin/change-greeting-status') }}',
                                data: { 'id':id ,'status':status},
                                type: 'POST',
                                success: function(data){
                                    //console.log(data);
                                    $(thisVal).attr('data-status',status)
                                }
                            });

                        } else {
                            swal("Cancelled", "Your record is safe :)", "error");
                            $(thisVal).val(prevStatus);
                        }
                    });

        });

        $(document).on('click','.delete',function(){

            var id = $(this).data('id');

            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this record!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm){

                            swal("Deleted!", "Your record has been deleted!", "success");
                            $('.tr_'+id).remove();

                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url: '{{ url('/admin/greetings') }}' + '/' + id,
                                type: 'DELETE',
                                dataType: 'json',
                                success: function(data){

                                }
                            });

                        } else {
                          //  swal("Cancelled", "Your record is safe :)", "error");
                        }
                    });

        });

        function deleteRecords($id) {
            var closable = alertify.dialog('confirm').setting('closable');
            alertify.dialog('confirm')
                    .set({

                        'labels':{ok:'Yes', cancel:'No'},
                        'message': 'Are you sure want to Delete ?' ,
                        'onok': function(){
                            $(".tr_"+$id).remove();
                            $.ajax({
                                headers: {
                                    'X-CSRF-Token' : '{{ csrf_token() }}'
                                },
                                url:  '{{ url('admin/greetings') }}' + '/' + $id,

                                success: function(data) {
                                    console.log(data);
                                },
                                type: 'DELETE'
                            });
                            alertify.success('Deleted Successfully.');
                        },
                    }).show();
        }
    </script>
@stop
