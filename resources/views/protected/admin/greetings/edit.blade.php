@extends('layouts.default')
@section('content_header_title','Edit Greeting')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/greetings') }}">Theme Greetings</a></li>
@stop
@section('content')
    <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title pull-right">
                    <a href="{{ route('greetings.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>

            </div>
                <div class="box-body">
                    {!! Form::model($greeting, array('route' => ['greetings.update', $greeting->id], 'method' => 'PUT',  'files' => true,'id'=>'dataform')) !!}
                    @include('protected.admin.forms.greetings', ['submitButtonText' => 'Update'])
                    {!! Form::close() !!}

                </div>
                </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $("#dataform").validate({
                rules: {
                    name: "required"
                },
                messages:
                {
                    name:{required:"Required..!!"}
                }
            });
        });
    </script>
@stop