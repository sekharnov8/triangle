@extends('layouts.default')
@section('content_header_title','Add Greeting')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/greetings') }}">Theme Greetings</a></li>
@stop
@section('content')
  <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ route('greetings.index')}}" class="btn btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>

            </div>
                    <div class="box-body">
                        {!! Form::open(array('route' => 'greetings.store', 'method' => 'POST', 'files' => true,'id'=>'dataform')) !!}
                        @include('protected.admin.forms.greetings', ['submitButtonText' => 'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {

            $("#dataform").validate({
                rules: {
                    name: "required",
                    image: {required: true, extension: "jpg|png|gif|jpeg" }
                },
                messages:
                {
                    name:{required:"Required..!!"},
                    image: {required:"Required..!!",extension: "Not an image!"}
                }
            });
        });
    </script>
@stop
