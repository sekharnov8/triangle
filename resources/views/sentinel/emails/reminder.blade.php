


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body style="padding:0; margin:0;">
<table width="700" border="0" bgcolor="#f5f7f6" style="font-family:Arial, Helvetica, sans-serif;" align="center" cellpadding="0"  cellspacing="0" >
    <tr>
        <td bgcolor="#ff9900" height="6"></td>
    </tr>
    <tr>
        <td align="center">
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">
                <tr style="background:url({{env('SITEURL')}}/images/curve.png) no-repeat left center; background-size:100%;">
                    <td align="center">
                        <a href="{{env('SITEURL')}}"><img src="{{env('SITEURL')}}/images/mobile-logo.png" width="550" height="66" border="0" alt="" style="margin-top: 4% !important; margin-bottom: 3% !important;"/></a>
                    </td></tr>
                <tr height="10"><td></td></tr>
            </table>
        </td>
    </tr>


    <tr>
        <td><table width="600" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" align="center" style="border:#e9e9e9 1px solid; padding:28px;">


                <tr>

                    <td width="100%" style="border-bottom:#eaeaea 1px solid; padding-bottom:20px;">
                        <span style="font-size:20px; font-family:Arial, Helvetica, sans-serif; line-height:10px; color:#696969; border-bottom:3px solid #db3233;padding-bottom:12px;">Dear <span style="color:#0065af;font-size:22px;">{{$user->first_name}},</span></span></td>

                </tr>
                <tr>
                    <td width="100%" >&nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" style="text-align:left;line-height:24px;font-size:17px; color:#333333; padding-top:13px; font-family:Arial, Helvetica, sans-serif;">You have requested password reset.</td>
                </tr>
                <tr>
                    <td width="100%" style="text-align:center;line-height:24px;font-size:17px; color:#333333; padding-top:13px; font-family:Arial, Helvetica, sans-serif;"><strong>Please find new Password Details below: </strong></td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" style="font-family:rial, Helvetica, sans-serif; font-size: 15px; color: #666666; padding-top: 10px;" width="100%">
                            <tbody>
                            <tr height="30">
                                <td width="22%">
                                    Email</td>
                                <td width="28%">
                                    : {{$user->email}}</td>
                            </tr>
                            <tr height="30">
                                <td width="20%">
                                    Password</td>
                                <td width="30%">
                                    : Reset your password by clicking <a href="{{ URL::to("reset/{$user->getUserId()}/{$code}") }}">here</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%" >&nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" style="text-align:left;line-height:22px;font-size:14px; color:#666666; padding:22px; font-family:Arial, Helvetica, sans-serif; border:1px solid #d2ecff;">If you have any issues or have any questions or comments, please feel free to email us at <a href="mailto:info@tutanc.org" style="color:#f28900; text-decoration:none;">info@tutanc.org</a></td>
                </tr>

            </table></td>
    </tr>
    <tr>
        <td height="10" width="100%"></td>
    </tr>

    <tr>
        <td><table width="600" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" align="center" style="border:#e9e9e9 1px solid; padding:28px; border-bottom:#dedede 5px solid;">

                <tr>
                    <td width="100%" style="text-align:left;line-height:24px;font-size:16px; color:#0065af; font-family:Arial, Helvetica, sans-serif;">Thank You !</td>
                </tr>

                <tr>
                    <td width="100%" style="text-align:left;line-height:24px;font-size:18px; color:#bf202f; font-family:Arial, Helvetica, sans-serif;">Admin TUTA</td>
                </tr>
                <tr>
                    <td width="100%" style="text-align:left;line-height:24px;font-size:14px; color:#bf202f; font-family:Arial, Helvetica, sans-serif;"><a href="{{env('SITEURL')}}" style="color: #ff6600" target="_blank">{{env('SITEURL')}}</a></td>
                </tr>
                <tr>
                    <td width="100%" style="text-align:left;line-height:20px;font-size:14px; color:#6f6f6f; font-family:Arial, Helvetica, sans-serif;">info@tutanc.org</td>
                </tr>


            </table></td>
    </tr>
    <tr>
        <td height="15"></td>
    </tr>


    <tr>
        <td><table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="background:url({{env('SITEURL')}}/images/curve-b.png) no-repeat left center; background-size:100% 100%; border-bottom:#3a4a5e 8px solid;">

                <tr>
                    <td height="25">&nbsp;</td>
                </tr>

                <tr>
                    <td align="center" style="margin-top:-20px!important;">
                        <a href="#"><img src="{{env('SITEURL')}}/images/fb.png" style="margin-right:5px" width="38" height="38" border="0" align="Facebook" /></a>
                        <a href="#"><img src="{{env('SITEURL')}}/images/tw.png" style="margin-right:5px" width="38" height="38" border="0" align="Twitter" /></a>
                        <a href="#"><img src="{{env('SITEURL')}}/images/lin.png" style="margin-right:5px"  width="38" height="38" border="0" align="linkdin" /></a>
                        <a href="#"><img src="{{env('SITEURL')}}/images/p.png" width="38" height="38" border="0" align="picaso" /></a></td>
                </tr>
                <tr>
                    <td style="line-height:7px;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="font-size:14px; padding-bottom:10px; color:#fff; font-family:Arial, Helvetica, sans-serif; line-height:18px;" align="center">© Copyright - 2021 TUTA. All rights Reserved.</td>
                </tr>
                <tr>
                    <td >&nbsp;</td>
                </tr>
            </table></td>
    </tr>
</table>
</body>
</html>
