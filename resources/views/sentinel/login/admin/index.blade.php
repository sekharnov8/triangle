@extends('backend.layouts.admin_login')

{{-- Page title --}}
@section('title', 'Login')

{{-- Inline styles --}}
@section('styles')

@stop

@section('content')
		<!-- Modal -->
<div class="login_modal admin_login">
	<div class="modal-dialog login_modal-dialog">
		<!-- Modal content-->
		<div class="modal-content login_modal-content">
			<div class="modal-body login_modal-body">
				<div class="login_signup_logo">
					<img src="{{ URL::to('images/GVSR_logo.jpg') }}" />
				</div>
				<div class="login_signup_info">
					<h4>Admin Login</h4>
					<form action="{{ URL::to('login') }}" method="post" autocomplete="off">
						{!! csrf_field() !!}

						<div class="input-field form-group">
							<label for="email">Email or Username</label>
							<input name="email" type="email" class="form-control" id="email" required autofocus >
						</div>
						<div class="input-field form-group">
							<label for="pwd">Password:</label>
							<input name="password" type="password" class="form-control" id="pwd" required >
						</div>
						{{--<div class="checkbox">--}}
							{{--<label>--}}
								{{--<input type="checkbox" name="remember" id="remember"> <span>Remember me</span></label>--}}
							{{--<span class="forgot_txt"><a href="{{ URL::to('reset') }}">Forgotten Password ?</a></span>--}}
						{{--</div>--}}
						<div class="form-group">
							<button type="submit" class="btn btn-default login_btn">Login</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
@stop
