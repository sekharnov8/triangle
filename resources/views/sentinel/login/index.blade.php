@extends('layouts.user.innerfull')

{{-- Page title --}}
@section('title', 'Login')

{{-- Page content --}}
@section('content')
<div class="container">
        <div class="row">
          <div class="col content_area p-3 p-md-4 rounded-4 rounded-xs-4">
             <div class="row">
                
                <div class="my-lg-5 col-md-8 col-lg-4 offset-lg-4 offset-md-2">
                  <div class="col">
                    <h2>Login</h2>
                  </div>
                  @include('layouts.user.includes.notifications')

                  <form method="post" name="loginform" autocomplete="off" id="loginform">
                    {!! csrf_field() !!}
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Email address *</label>
                      <input  name="email" id="loginemail" class="form-control p-3" type="email" value="@if(Session::has('remember')){{ Session::get('user_email')}}@endif"  placeholder="Email id *"    >                       
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Password *</label>
                      <input name="password" id="loginpwd" class="form-control p-3"  type="password" value="@if(Session::has('remember')){{ Session::get('user_pwd')}}@endif"  placeholder="Password *" style="padding-left:25px;" >
                    </div>  
                    <div class="row align-items-center">
                      <div class="col-4">
                        <button type="submit" class="btn px-4 btn-lg btn-danger rounded-pill tuta_btn">Login</button>    
                      </div>
                      <div class="col-8 text-end">
                        <a href="{{url('reset')}}">Forgot Password?</a>
                      </div>
                    </div>
                  </form>

                  <hr>

                  <div class="row text-center mb-4">                      
                      <div class="col">                    
                        <p>Don't have an account?</p>
                        <a href="{{url('membership')}}"><button type="button" class="btn px-4 btn-lg btn-outline-danger rounded-pill">Register Now</button></a>    
                      </div>
                  </div>

                </div>
                
             </div>
          </div>
        </div>
        
        <!-- Content Block End -->
      </div>
 
@stop
@section('scripts')
    <script>
        $(document).ready(function(e) {

$.validator.addMethod("validate_email", function(value, element) {

    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email.");

            $("#loginform").validate({
                rules: {

                    email: "required validate_email",
                    password: "required"
                 },
                messages:
                {
                    email:{  required:"Required..!!", validate_email: "Enter valid email id"},
                    password:{  required:"Required..!!"}
                 }

            });

        });
    </script>
@stop
