@extends('layouts.default')
@section('content_header_title',$role->exists === false ? 'Create Role' : 'Update Role')
@section('breadcrumb_inner_content')
        <li><a href="{{ url('/admin/roles') }}">Roles</a></li>
@stop
@section('content')
 <div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                    <a href="{{ URL::to('admin/roles') }}" class="btn btn-xs btn-success" >
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </h3>
            </div>
                    <div class="box-body">
                    <form method="post" action="" autocomplete="off" class="validate-form" role="form">

  						<div class="box-body">
							<!-- CSRF Field -->
							{!! csrf_field() !!}

									<!-- Role Name -->
							<div class="form-group{{ $errors->first('name', ' has-error') }}">

								<label for="name">Name</label>

								<input type="text" class="form-control" name="name" id="name" value="{{ old('name', $role->name) }}" placeholder="Enter the role name." required autofocus>

								<span class="help-block">{{ $errors->first('name', ':message') }}</span>

							</div>

							<!-- Role Slug -->
							<div class="form-group{{ $errors->first('slug', ' has-error') }}">

								<label for="slug">Slug</label>

								<input type="text" class="form-control" name="slug" id="slug" value="{{ old('slug', $role->slug) }}" placeholder="Enter the role slug." required>

								<span class="help-block">{{ $errors->first('slug', ':message') }}</span>

							</div>

						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							@if ($role->exists === true && $role->users->count() === 0)
								<a href="{{ route('role.delete', $role->id) }}" data-action-delete class="btn btn-danger">Delete</a>
							@endif

							<a class="btn btn-default" href="{{ route('roles.index') }}">Cancel</a>

							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>

                    </div>
                </div>

 @stop
 @section('js')

<!-- START: page scripts -->
<script>
	$(function(){

		$('#form-validation').validate({
			submit: {
				settings: {
					inputContainer: '.form-group',
					errorListClass: 'form-control-error',
					errorClass: 'has-danger'
				}
			}
		});

		$('#form-validation .remove-error').on('click', function(){
			$('#form-validation').removeError();
		});

		$('#form-validation-simple').validate({
			submit: {
				settings: {
					inputContainer: '.form-group',
					errorListClass: 'form-control-error-list',
					errorClass: 'has-danger'
				}
			}
		});

		$('#form-validation-simple .remove-error').on('click', function(){
			$('#form-validation-simple').removeError();
		});


		$('.select2').select2();
		$('.select2-tags').select2({
			tags: true,
			tokenSeparators: [',', ' ']
		});

	});
</script>
<!-- END: page scripts -->
@stop
