@extends('layouts.default')
@section('content_header_title','Roles')

@section('content')

<div class="box">
            <div class="box-header clearfix">
                <h3 class="box-title">
                 <a href="javascript: void(0);" class="btn btn-sm btn-warning pull-right mr-1" data-toggle="modal" data-target="#add_role">
                       <i class="icmn-plus mr-1"></i> Add Role</a>
                </h3>
            </div>

                    <div class="box-body">
                    <table class="table table-striped table-bordered table-hover" id="datalist">
                        <thead>
                        <tr >
                            <th>Name</th>
							<th>Slug</th>
							<th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
						@foreach ($roles as $role)
							<tr class="tr_{{ $role->id }}">
								<td>{{ $role->name }} </td>
								<td>{{ $role->slug }} </td>
								<td>
                                @if ($role->slug != 'admin')
                                 <a href="javascript: void(0);" data-id="{{ $role->id }}" class='btn edit btn-sm btn-success' data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                                     <a href="javascript:void(0);"  class="btn btn-sm btn-danger delete" data-id="{{ $role->id }}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>
                                     @endif

								</td>
							</tr>
						@endforeach
						</tbody>
                    </table>
                    </div>

                </div>

<!-- START: Add modal -->
<div class="modal fade modal-size-large" id="add_role" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Role</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'roles.store', 'method' => 'POST', 'id' => 'dataform')) !!}
                @include('protected.admin.forms.roles', ['submitButtonText' => 'Submit'])
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
<!-- END: Add modal -->


<!-- START: edit modal -->
<div class="modal fade modal-size-large" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit Role</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>
<!-- END: edit modal -->


@stop
@section('js')
    <script>
	$(function(){

		$('#example1').DataTable({
			responsive: true
		});

         $("#dataform").validate({
                rules: {
                    name: "required",
                    slug: "required"
                },
                messages:
                {
                    name:{required:"Enter Role Name"},
                    slug:{required:"Enter Role SLug"}
                }
            });

         $(document).on('click','.edit',function(){

                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('admin/roles') }}'+'/'+id+'/edit',
                    type: 'GET',
                    success: function(data){
                        $('#edit_modal .modal-body').html(data);
                        $('#edit_modal').modal('show');
                        $("#dataform").validate({
                            rules: {
                                title: "required",
                                order_no: "required number"
                            },
                            messages:
                            {
                                title:{required:"Enter Service Type"},
                                order_no:{required:"Enter Price", number:"Number only"}
                            }
                        });

                        $('#edit_prod_form .remove-error').on('click', function(){
                            $('#form-validation-simple').removeError();
                        });
                    }
                });

            });

         $(document).on('click','.delete',function(){

                var id = $(this).data('id');

                swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this record!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "No, cancel pls!",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function(isConfirm){
                            if (isConfirm){

                                swal("Deleted!", "Your record has been deleted!", "success");
                                $('.tr_'+id).remove();

                                $.ajax({
                                    headers: {
                                        'X-CSRF-Token' : '{{ csrf_token() }}'
                                    },
                                    url: '{{ url('/admin/roles') }}' + '/'+id,
                                    type: 'DELETE',
                                    dataType: 'json',
                                    success: function(data){

                                    }
                                });

                            } else {
                                swal("Cancelled", "Your record is safe :)", "error");
                            }
                        });

            });

	});


</script>
@stop