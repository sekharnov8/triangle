@extends('layouts.user.master')

@section('title', 'Reset Password')

@section('content')

<div class="container">
        <div class="row"><br/><br/>
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                     
                    <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 b-p15 mobile-b-p5 t-t-u">Reset Password</h2>
                    <div class="panel-body">
    @include('layouts.user.includes.notifications')

                     <form method="post" action="" id="changepwd" autocomplete="off" class="validate-form">

			<input type="hidden" name="_token" value="{{ csrf_token() }}">


			<div class="form-group{{ $errors->first('password', ' has-error') }}">

				<label for="password">New Password</label>

				<input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}">

				<span class="help-block">{{ $errors->first('password', ':message') }}</span>
    <article class="clearfix part-row b-p5">
                                                    <lable class="notes">Need atleast one symbol or digit, one lower character  and one upper character</lable>
</article>
			</div>

			<div class="form-group{{ $errors->first('password_confirmation', ' has-error') }}">

				<label for="password-confirmation">Confirm New Password</label>

				<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}">

				<span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>

			</div>

			<hr>

			<div class="text-right">

				<button type="submit" class="btn btn-primary">Reset</button>
			</div>

		</form>
                    </div>
                </div>



            </div>
        </div>
    </div>





@stop
@section('scripts')
    <script>
        $(document).ready(function(e) {

   $.validator.addMethod("hasUppercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[A-Z]/.test(value);
}, "Must contain uppercase");
      $.validator.addMethod("hasLowercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[a-z]/.test(value);
}, "Must contain lowercase");

   $.validator.addMethod("hasSymbol", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+\-\d]/.test(value);
}, "Must contain symbol or digit");

            $("#changepwd").validate({
           rules: {
                    password:  {required: true,maxlength:20,minlength: 8,hasSymbol:true,  hasUppercase: true, hasLowercase: true},
                    password_confirmation: {required: true,maxlength:20, minlength: 8, equalTo:'#password'}

                },
                messages:
                {
                    password:{"required":"Required..!", "maxlength":"Maximum 20 characters only",minlength:"Minimum 8 characters", "hasSymbol":"Need Contain Digit or Symbol","hasUppercase":"Need atleast one capital letter","hasLowercase":"Need atleast one lower letter"},
                    password_confirmation: {"required":"Required..!", "equalTo":"Confirm password is not matched ", "maxlength":"Maximum 20 characters only",minlength:"Minimum 8 characters"}

                }

            });

        });
    </script>
@stop
