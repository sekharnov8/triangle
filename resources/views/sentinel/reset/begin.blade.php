@extends('layouts.user.master')

@section('title', 'Reset Password')

@section('content')

 <section class="clearfix wrapper innerpage-bg">
<article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">

<article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
<article class="clearfix l-r-m-auto  t-p55 mobile-t-b-p15 mobile-maxwidth100 mobile-l-r-p10" style="max-width:530px;">
@include('layouts.user.includes.notifications')

<h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 b-p15 mobile-b-p5 t-t-u">Forgot Password?</h2>
<article class="clearfix white-bg login-shadow border-radius5 bm30 mobile-b-m0">

  <form method="post" autocomplete="off" id="loginform">
 {!! csrf_field() !!}
<article class="clearfix l-r-p75 t-p75 border-b dashborder-b mobile-l-r-p20 mobile-t-p20">
<article class="clearfix position-r">
<i class="profile-icon2"></i>
 <input  name="email" id="loginemail" class="form-control donate-form" type="email" value="@if(Session::has('remember')){{ Session::get('user_email')}}@endif"  placeholder="Email Id *" style="padding-left:25px;"  >

</article>

<article class="clearfix t-p30 b-p20 mobile-t-b-p5">
 <button type="submit"  class="btn-block border-radius5 Montserrat-Medium font22 white-t t-t-u t-b-p20" style="background-color:#ff9900; min-height:60px;">Reset</button>
</article>

</article>

</form>
</article>

</article>
</article>
</article>
  </section>
@stop
@section('scripts')
    <script>
        $(document).ready(function(e) {
            $("#loginform").validate({
            rules: {

            email: "required validate_email"
            },
            messages:
            {
            email:{  required:"Required..!!", validate_email: "Enter valid email id"}
            }

            });

        });
    </script>
@stop