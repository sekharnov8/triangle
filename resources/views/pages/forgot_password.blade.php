@extends('layouts.user.inner')

@section('title','Forgot Password')

@section('content')


    <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
        <article class="clearfix l-r-m-auto t-c t-p55 mobile-t-b-p15 mobile-maxwidth100 mobile-l-r-p10" style="max-width:530px;">
            <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 b-p10 mobile-b-p5 t-t-u">Forgot Password</h2>
            <article class="clearfix font13 dk-t l-h18 b-p20">
                <span class="red-t font15 Roboto-Regular">Note :-</span> “ If you have forgotten your password and would like to change it, enter your email address and we'll send you a new password reset request.
            </article>
            <article class="clearfix white-bg login-shadow border-radius5">

                <form>
                    <article class="clearfix l-r-p75 t-p75 mobile-l-r-p20 mobile-t-p20">
                        <article class="clearfix position-r">
                            <i class="profile-icon2"></i>
                            <input class="form-control donate-form" type="text" name="" id="" placeholder="User Name" style="padding-left:25px;">
                        </article>
                        <article class="clearfix">
                            <span class="dis-b clearfix t-p5 t-c"><img src="Content/images/or-img.png" alt="" width="312" height="54"></span>
                        </article>
                        <article class="clearfix position-r">
                            <i class="email-icon2"></i>
                            <input class="form-control donate-form" type="text" name="" id="" placeholder="Email Id" style="padding-left:25px;">
                        </article>
                        <article class="clearfix t-p30 b-p20 mobile-t-b-p5">
                            <a href="" class="btn-block border-radius5 Montserrat-Medium font22 white-t t-t-u t-b-p20" style="background-color:#ff9900; min-height:60px;">Submit</a>
                        </article>
                    </article>

                </form>
            </article>
            <article class="clearfix bm30 mobile-b-m0">
                <a href="login.html" class="pull-right Roboto-Regular font15 orange-t t-p10" style="text-decoration:underline;">Back to Login</a>
            </article>

        </article>
    </article>

@stop
@section('script')

@stop