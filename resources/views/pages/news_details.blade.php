@extends('layouts.user.inner')

@section('title', $news->name)

@section('content')


    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
            {{$news->name}}</h2>
    </article>
    <article class="l-r-p35 t-p20 b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:755px!important;">
        <article class="clearfix white-bg shadow-i border-t3 greenborder border-radius4">

        </article>
        @if($news->image && File::exists('uploads/news/'.$news->banner_url) )
        <article class="clearfix t-m15">
            <img src="{{url('uploads/news/'.$news->image)}}" width="762" height="720" alt="" class="width100 border lowborder p5" />
        </article>
        @endif
        <h4 class="t-m0 OpenSans font20 dk-t1 t-b-p25 mobile-t-b-p15"><span class="border-b orangeborder b-p10 r-p40">Description</span></h4>

        <p><span class="dis-b b-p15">Dear Vasavites</span></p>
        {!! $news->description !!}


        <p class="b-m15">For any further details, please contact us at<a href="mailto:info@tutanc.org" class="blue-t"> info@tutanc.org</a></p>

 

    </article>

@stop
@section('script')

@stop