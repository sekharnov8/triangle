@extends('layouts.user.inner')

@section('title', 'Check Membership Status')

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Check Membership Status</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:700px!important;">
    <p>Enter first name/last name to check your membership status. Else enter email id to check. </p>
    {!! Form::open(array('url' => 'membership-status', 'method' => 'POST', 'id' => 'dataform', 'files'=>true)) !!}
    <div class="row">
    <div class="col-md-2">
        <label for="title" class="control-label">First Name</label>
    </div>
     <div class="col-md-6">
        <input class="form-control"  name="firstname" type="text">

    </div>
    </div>
     <div class="row">
    <div class="col-md-2">

        <label for="title" class="control-label">Last Name</label>
    </div>
     <div class="col-md-6">

        <input class="form-control"  name="lastname" type="text" value="">
    </div>
    </div>
     <div class="row">
<div class="col-md-2">
</div>
<div align="center" class="col-md-6">(or)</div></div>
 <div class="row">
 <div class="col-md-2">

        <label for="title" class="control-label">Email</label>
     </div>
     <div class="col-md-6">

        <input class="form-control"  name="email" type="text" value="">
    </div>
    </div>
     <div class="row">
 <div class="col-md-2"></div>
 <div class="col-md-6">
        {!! Form::submit('Search', ['class' => 'btn btn-info ']) !!}
       
        @if(isset($users))
        <a href="{{url('membership-status')}}">{!! Form::button('Clear', ['class' => 'btn btn-warning ']) !!}</a>
        @endif
</div></div>
 {!! Form::close() !!} <br/>
@if(isset($users))
 @if($type=='single')
 <h3>Member Info:</h3>
 <div class="row">
 <div class="col-md-6">First Name: {{$users->first_name}}</div>
 <div class="col-md-6">Last Name: {{$users->last_name}}</div>
 <div class="col-md-6">Member Id: {{$users->member_id}}</div>
 <div class="col-md-6">Membership Type: {{getMembershipName($users->membershiptype_id)}}</div>
 </div>

@elseif($type=='list' && count($users)>1)
<h3>Members Info:</h3>
<table class="table-striped table-hover table3 Montserrat-Regular t-l" width="100%" cellspacing="0" cellpadding="0">
    <tbody><tr><th>Member ID</th><th>First Name</th><th>Last Name</th><th>Membership Type</th></tr>
@foreach($users as $user)
<tr><td>{{$user->member_id}}</td><td>{{$user->first_name}}</td><td>{{$user->last_name}}</td><td>{{getMembershipName($user->membershiptype_id)}}</td></tr>
@endforeach
    </tbody>
</table> 
@endif
@endif
@if(isset($type) && $type=='norecords')
No Members found with this keyword.

@endif
    </article>
@stop
@section('scripts')
 <script>

        $(document).ready(function(e) {
  $("#dataform").validate({
                rules: {
                     
                 lastname: {
                  required: function(element) {
                    if($("input[name='email']").val()=='')
                    return true;
                  },
                minlength: 3
                 },
                 email: {
                  email:true
                 },
                 },
                messages:
                {
                     lastname:{required:"Required"},
                email:{required:"Required", email:'Enter Valid email'}
                }
            });
    });
    </script>
@stop
