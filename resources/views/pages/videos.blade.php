@extends('layouts.user.inner')

@section('title','Video Gallery')

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Video Gallery</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">
    @foreach($videos as $cat)
    @if(count($cat->videos))
    <h2>{{$cat->name}}</h2>
    @endif
    <ul class="clearfix list-pn list-f m0 photogallery-list">
    @foreach($cat->videos as $data)

<li>
      <article class="clearfix lowgray-bg1 p5 position-r shadow-i"> <a href="https://www.youtube.com/embed/{{$data->video_url}}?autoplay=1" data-fancybox-group="gallery" class="various fancybox.iframe dis-b"><span class="video-icon-hover">&nbsp;</span><img src="https://i1.ytimg.com/vi/{{$data->video_url}}/hqdefault.jpg" alt="" class="width100" width="260" height="180" border=""></a> </article>
      <p>{{$data->name}}</p>
                </li>
    @endforeach
    </ul>
    @endforeach

    </article>
@stop
@section('script')

@stop
