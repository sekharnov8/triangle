@extends('layouts.user.master')

@section('title', 'Home')

@section('content')
<div class="container">
  <div class="row">
    <div class="col content_area py-3 rounded-4 rounded-xs-4">
      <!-- Banner -->
      <div class="row">
        <div class="col slider">
          <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
              @foreach($banners as $key=>$banner)
              <button type="button" data-bs-target="#myCarouse{{$key+1}}" data-bs-slide-to="{{$key}}" @if($key==0) class="active" aria-current="true" @endif aria-label="Slide {{$key+1}}"></button>
              @endforeach
            </div>
            <div class="carousel-inner">
              @foreach($banners as $key=>$banner) 
                <div class="carousel-item @if($key==0)active @endif"><a href="@if($banner->redirect_url){{$banner->redirect_url}}@else # @endif"><img src="{{url('uploads/banners/'.$banner->image)}}" alt="" class="rounded-4"></a></div>
              @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="visually-hidden">Previous</span> </button>
            <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="visually-hidden">Next</span> </button>
          </div>
        </div>
      </div>
      <!-- Banner End -->
      <div class="p-lg-4 mb-4">
        <!-- Content Block -->
        <!-- Block 1 -->
        {!!$page->description!!}
        <!-- Block 1 End -->
        <hr>
        <!-- Block 2 -->
        @if(count($events))

        <div class="row">
          <div class="col">
            <h2 class="mt-md-4 text-center section_title"><cite>TUTA</cite>Upcoming Events</h2>
            <div class="row">
              <!-- Swiper -->
              <div class="swiper pt-4 pb-5 pt-md-4 px-3 mySwiper">
                <div class="swiper-wrapper">
                  @foreach($events as $data)
                  <div class="swiper-slide card p-3 text-center border-0 rounded-4 tutm_card">
                    @if($data->banner_url && File::exists('uploads/events/'.$data->banner_url))
                    <img src="{{url('uploads/events/'.$data->banner_url)}}" alt="{{$data->name}}" class="card-img-top" />
                    @else
                    <img src="assets/images/thumb_01.jpg" class="card-img-top" alt="Event thumb">
                    @endif
                    <div class="card-body px-0 pb-0">
                      <h6 class="card-title mb-0">{{$data->name}}</h6>
                    </div>
                  </div>
                  @endforeach

                </div>
                <div class="swiper-pagination"></div>
              </div>

              <!-- Swiper End -->
            </div>
          </div>
        </div>
        @endif

      </div>
      <!-- Block 2 End -->
      <!-- Block 3 -->
      <div class="row">
        <div class="py-3 py-md-5 mb-4 bg_color4 text-white">
          <h2 class="text-center section_title"><cite>TUTA</cite>Board of Directors</h2>
          <!-- Swiper -->
          <div class="swiper py-3 pb-5 pt-md-5 directors_slider">
            <div class="swiper-wrapper">
              @foreach($members as $member)
              <div class="swiper-slide text-center"> <img src="@if($user->profile_image && File::exists('uploads/users/'.$user->profile_image))
                                        {{url('uploads/users/'.$user->profile_image)}} @else {{url('assets/images/userdefault_img.png')}}@endif" class="border border-white border-5 rounded-circle" alt="Director">

                <h6 class="card-title pt-3">{{$userfirst_name.' '.$user->last_name}}
                  <small class="d-block">{{$roles_info[$user->role_id]}}</small>
                </h6>
              </div>
              @endforeach

            </div>
            <div class="swiper-pagination"></div>
          </div>
          <!-- Swiper End -->
        </div>
      </div>
      <!-- Block 3 End -->

      <!-- Block 4 -->
      {!!$president->description!!}
      <!-- Block 4 End -->
      <hr>
      <!-- Block 5 -->
      <div class="p-lg-4">
        <div class="row">
          <div class="col">
            <h2 class="text-center section_title"><cite>TUTA</cite>Our Gallery</h2>
            <div class="row">
              <!-- Swiper -->
              <div class="swiper pt-4 pb-5 pt-md-4 px-3 GallerySwiper">
                <div class="swiper-wrapper">
                  @foreach($photos as $key=>$image)
                  <div class="swiper-slide card p-3 text-center border-0 rounded-4 tutm_card"> <a href="#"><img src="assets/images/gallery_01.jpg" class="card-img-top rounded-4" alt="Event gallery"></a>
                    <div class="card-body px-0 pb-0">
                      <h6 class="card-title">Bathukamma Sambaralu in 2021</h6>
                    </div>
                  </div>
                  @endforeach
                </div>
                <div class="swiper-pagination"></div>
              </div>

              <div class="p-3 mb-4">

                <div class="text-center">
                  <button type="button" class="btn px-4 btn-lg btn-danger rounded-pill tuta_btn">View all</button>
                </div>

              </div>
              <!-- Swiper End -->
            </div>
          </div>
        </div>
      </div>
      <!-- Block 5 End -->

      <hr>

      <!-- Block 6 -->
      <div class="p-lg-4">
        <div class="row">
          <div class="col">
            <h2 class="text-center section_title"><cite>TUTA</cite>Video Gallery</h2>
            <div class="row">
              <!-- Swiper -->
              <div class="swiper pt-4 pb-5 pt-md-4 px-3 VideoSwiper">
                <div class="swiper-wrapper">
                  @foreach($videos as $key=>$video)
                  <div class="swiper-slide card p-3 text-center border-0 rounded-4 tutm_card"> <a href="https://www.youtube.com/embed/{{$video->video_url}}" data-fancybox-group="gallery" class="various fancybox.iframe dis-b"><span class="video-icon-hover">&nbsp;</span><img src="https://i1.ytimg.com/vi/{{$video->video_url}}/hqdefault.jpg" class="card-img-top rounded-4" alt="Event gallery"></a>
                    <div class="card-body px-0 pb-0">
                      <h6 class="card-title">{{$video->name}}</h6>
                    </div>
                  </div>
                  @endforeach
                </div>
                <div class="swiper-pagination"></div>
              </div>

              <div class="p-3 mb-4">
                <div class="text-center">
                  <button type="button" class="btn px-4 btn-lg btn-danger rounded-pill tuta_btn">View all</button>
                </div>

              </div>
              <!-- Swiper End -->
            </div>
          </div>
        </div>
      </div>
      <!-- Block 6 End -->

      <!-- Block 7 -->
      <div class="p-lg-4">
        <div class="row">
          <div class="col">
            <h2 class="text-center section_title"><cite>TUTA</cite>Sponsors</h2>
            <div class="row mt-md-5">
              <!-- Easy Tabs -->
              <div id="horizontalTab">
                <ul class="resp-tabs-list">
                  @foreach($sponsorcategories as $category)
                  <li class="fs-5 text-uppercase">{{$category->category_name}}</li>
                  @endforeach
                </ul>
                <div class="resp-tabs-container">
                  <!-- Grand -->
                  @foreach($sponsorcategories as $category)
                  <div>
                    <div class="row text-center">
                      @foreach($category->sponsors as $sponsor)
                      <div class="col-6 col-sm-4 col-md-3 col-lg-2 py-2 py-md-4">
                       <a @if($sponsor->site_url) href="{{$sponsor->site_url}}" target="_blank" @endif> <img src="{{url('uploads/sponsors/'.$sponsor->logo_url)}}" alt="{{$sponsor->name}}"></a>
                      </div>
                      @endforeach

                    </div>
                  </div> @endforeach


                </div>
              </div>


              <!-- Easy Tabs End -->
            </div>
          </div>
        </div>
      </div>
      <!-- Block 7 End -->

      <hr>
      <!-- Block 8 -->
      <div class="p-lg-4">
        <div class="row">
          <div class="col-sm-4">
            <img src="assets/images/sm_post01.jpg" alt="">
          </div>
          <div class="col-sm-4">
            <img src="assets/images/sm_post02.jpg" alt="">
          </div>
          <div class="col-sm-4">
            <img src="assets/images/sm_post03.jpg" alt="">
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- Content Block End -->
</div>
 @stop
 @section('scripts')
 <script type="text/javascript" src="{{ URL::to('assets/js/script.min.js')}}"></script>
 @if($greeting)
<link rel="stylesheet" href="{{url('assets/css/lightbox.css')}}" type="text/css" media="screen" />
<div  id="hideshow" style="visibility:hidden;">
  <div id="fade"></div>
   <div class="popup-main-block"  style="top:{{$greeting->toppadding}}px; max-width:{{$greeting->width}}px">
    <div class="popup_block2" style="background:#fff"> <a href="javascript:hideDiv()"><img src="{{url('assets/images/close3.png')}}" alt="Close"   border="0"  title="Close" class="close3" /></a>
  @if($greeting->link!='') <a href="{{$greeting->link}}" target="{{$greeting->target}}">
 <img src="{{url('uploads/greetings/'.$greeting->image)}}" width="{{$greeting->width}}"  border="0"  alt="{{$greeting->name}}" /></a>
 @else
  <img src="{{url('uploads/greetings/'.$greeting->image)}}" width="{{$greeting->width}}"  border="0"  alt="{{$greeting->name}}" />
 @endif
</div>
  </div>

</div>
<script type="text/javascript">
showDiv();
  function hideDiv() {
if (document.getElementById) {
document.getElementById('hideshow').style.visibility = 'hidden';
}
else {
if (document.layers) { // Netscape 4
document.hideshow.visibility = 'hidden';
}
else { // IE 4
document.all.hideshow.style.visibility = 'hidden';
}
}
}

function showDiv() {
if (document.getElementById) { // DOM3 = IE5, NS6
document.getElementById('hideshow').style.visibility = 'visible';
}
else {
if (document.layers) { // Netscape 4
document.hideshow.visibility = 'visible';
}
else { // IE 4
document.all.hideshow.style.visibility = 'visible';
}
}
}
</script>
@endif
 @stop