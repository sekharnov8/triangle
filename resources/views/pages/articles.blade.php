@extends('layouts.user.inner')

@section('title', $page->name)

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">{{$page->name}}</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">

<ul class="clearfix m0 list-pn news-list">
    @foreach($articles as $data)
          <li>           <section class="clearfix">
                              <section class="clearfix p10 tabhorizontal-p0">
                <article class="clearfix r-p15 pull-left tabhorizontal-r-p15 mobile-pull-n mobile-t-c mobile-dis-b mobile-r-p0 mobile-b-p10">@if($data->image && File::exists('uploads/articles/'.$data->image) )  <img width="200" border="0" height="105" src="{{url('uploads/articles/'.$data->image) }}" alt="IMG" />
                    @else<img width="200" border="0" height="105" src="{{url('images/event-no-image.png')}}" alt="IMG" /> @endif </article>
                 <article class="clearfix font13 dkgray-t2 poppins-regular l-h22 tabhorizontal-p10">
                      <h4 class="font16 t-b-m0 l-h18 black-t poppins-medium mobilev-b-p5 b-p5">{{$data->name}}</h4>
                     {!! $data->shortdescription!!}
                      </article>
                </section>
                <span class="brown-t font13   latobold tablet-pull-n tablet-dis-b tablet-t-p10 "><span class="yellow-t2 font14 font-w-b">Posted</span> @if($data->post_date) on  {{date("M d, Y", strtotime($data->post_date))}} @endif By {{getUserName($data->updated_by)}}</span>
                  <a href="{{url('article/'.$data->slug)}}" class="more more-btn ">More</a>
              </section>
              </li>
    @endforeach
    </ul>
    </article>
@stop
@section('script')

@stop
