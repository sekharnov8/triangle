@extends('layouts.user.inner')

@section('title', $page->name)

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">{{$page->name}}</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">

<ul class="clearfix m0 list-pn news-list">
    @foreach($news as $data)
          <li>           <section class="clearfix">
                              <section class="clearfix p10 tabhorizontal-p0">
                <article class="clearfix r-p15 pull-left tabhorizontal-r-p15 mobile-pull-n mobile-t-c mobile-dis-b mobile-r-p0 mobile-b-p10">@if($data->image && File::exists('uploads/news/'.$data->banner_url) )  <img width="200" border="0" height="105" src="{{url('uploads/news/'.$data->image) }}" alt="IMG" />
                    @else<img width="200" border="0" height="105" src="{{url('images/event-no-image.png')}}" alt="IMG" /> @endif </article>
                 <article class="clearfix font13 dkgray-t2 poppins-regular l-h22 tabhorizontal-p10">
                      <h4 class="font16 t-b-m0 l-h20 black-t poppins-medium mobilev-b-p5 b-p10">{{$data->name}}<span class="brown-t font13 pull-right latobold tablet-pull-n tablet-dis-b tablet-t-p10 "><span class="yellow-t2 font14 font-w-b">Posted on : </span>{{date("M d, Y", strtotime($data->created_at))}}</span></h4>
                     {!! ShortenText($data->description,150)!!}
                      </article>
                </section>
                  <a href="@if($data->url) {{url($data->url)}} @else {{url('news/'.$data->slug)}} @endif" class="more more-btn ">More</a>
              </section>
              </li>
    @endforeach
    </ul>
    </article>
@stop
@section('script')

@stop
