@extends('layouts.user.inner')

@section('title', 'Sponsors')

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Sponsors</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">
    @foreach($sponsors as $sponsorcategory)
    @if(count($sponsorcategory->sponsors))
    <h2>{{$sponsorcategory->category_name}}</h2>
    @endif
    <ul class="clearfix list-pn list-f sponsors-list1 m0">
    @foreach($sponsorcategory->sponsors as $data)
     @if($data->status)
        @if($data->logo_url && File::exists('uploads/sponsors/'.$data->logo_url) )
          <li> <a @if($data->redirect_url) href="{{$data->redirect_url}}" target="_blank" @endif>
            <article class="clearfix border dkgrayborder">
              <article class="clearfix t-b-p10 l-r-p10 t-c"> <img src="{{url('uploads/sponsors/'.$data->logo_url)}}" alt="" width="165" height="80" border=""> </article>
            </article>
            </a></li>
        @endif
      @endif
    @endforeach
    </ul>
    @endforeach

    </article>
@stop
@section('script')

@stop
