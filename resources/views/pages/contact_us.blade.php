@extends('layouts.user.inner')

{{-- Page title --}}
@section('title', 'Contact us')

@section('content')

          <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Contact Us</h2>
            </article>
             <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:700px!important;">
                <article class="clearfix left-block12">
                     {!!$page->description!!}
                </article>
                <article class="clearfix right-block12">
                    <form name="contact" id="contact" novalidate="novalidate">
                        <article class="clearfix dkblue-bg1 l-r-p30 t-b-p20 mobile-p15 tabhorizontal-t-m20">
                            <div class="contact-msg"></div>
                            <h2 class="white-t SuiGenerisRg-Regular font24 t-m0 t-c">Get In Touch</h2>
                            <span class="white-t font15 l-h18 OpenSans-Semibold">Simply fill out the details below and we will be contact with you as soon as we can</span>
                            <article class="clearfix org-drop dropdown" style="background:none; min-height:28px; border:none; border-bottom:2px solid #fff; padding:7px 0px;">
                                {!! Form::select('program', $programs, null,['class' => 'org-drop dropdown-select','placeholder' => 'Select Program *','style'=>'color:#ffffff;padding:0']) !!}

                            </article>
                            <article class="clearfix part-row">
                                <article class="clearfix part-6">
                                    <article class="clearfix r-m5 mobile-r-m0">
                                        <input type="text" placeholder="Name *" name="name" id="" class="form-control donate-form1">
                                    </article>
                                </article>
                                <article class="clearfix part-6">
                                    <article class="clearfix l-m5 mobile-l-m0">
                                        <input type="text" placeholder="Email *" name="email" id="" class="form-control donate-form1">
                                    </article>
                                </article>
                            </article>
                            <article class="clearfix part-row">
                                <article class="clearfix part-6">
                                    <article class="clearfix r-m5 mobile-r-m0">
                                        <input type="text" placeholder="Phone No *" name="mobile" id="" class="form-control phone donate-form1" maxlength="14">
                                    </article>
                                </article>
                                <article class="clearfix part-6">
                                    <article class="clearfix l-m5 mobile-l-m0">
                                        <input type="text" placeholder="Address Line 1" name="address" id="" class="form-control donate-form1">
                                    </article>
                                </article>
                            </article>

  <article class="clearfix part-row">
                                <article class="clearfix part-12">
                                    <article class="clearfix l-m5 mobile-l-m0">
                                        <input type="text" placeholder="Address Line 2" name="address2" id="" class="form-control donate-form1">
                                    </article>
                                </article>
                            </article>
                            <article class="clearfix part-row">
                                <article class="clearfix part-6">
                                    <article class="clearfix r-m5 mobile-r-m0">
                                        <input class="form-control donate-form1" placeholder="City" name="city" type="text">
                                    </article>
                                </article>
                                <article class="clearfix part-6">
                                    <article class="clearfix l-m5 mobile-l-m0">
                                        <input class="form-control donate-form1" placeholder="State" name="state" type="text">
                                    </article>
                                </article>
                            </article>
                            <article class="clearfix part-row">
                                <article class="clearfix part-6">
                                    <article class="clearfix r-m5 mobile-r-m0">
                                        <article class="clearfix org-drop dropdown" style="background:none; min-height:28px; border:none; border-bottom:2px solid #fff; padding:7px 0px;">
                                            {!! Form::select('country', $countries_data, 'United States',['class' => 'org-drop dropdown-select','placeholder' => 'Select Country *','style'=>'color:#ffffff;padding:0']) !!}

                                        </article>
                                    </article>
                                </article>
                                <article class="clearfix part-6">
                                    <article class="clearfix l-m5 mobile-l-m0">
                                        <input class="form-control donate-form1 zipcode" placeholder="Zip Code" name="zipcode" type="text">
                                    </article>
                                </article>
                            </article>
                            <textarea class="form-control donate-form1" rows="" placeholder="Message" name="message" id="message" value="Comments *"></textarea>
                                <article class="clearfix box-row">
              <article class="clearfix box-12 white-t">
               @php 
  $min_number = 1;
  $max_number = 15;
  $random_number1 = rand($min_number, $max_number);
  $random_number2 = rand($min_number, $max_number);
     @endphp
     {{$random_number1 . ' + ' . $random_number2 . ' = '}}
                  <input type="text" style="width:120px; display: inline-block;" maxlength="10" placeholder="Enter total" 
 size="10" name="security_code" id="security_code" onChange="checkcaptchamath()" required class="form-control donate-form1"  required /> 

 <input name="firstNumber" type="hidden" id="firstNumber" value="{{ $random_number1}}">
      <input name="secondNumber" type="hidden" id="secondNumber" value="{{$random_number2}}">
 </article>
                </article>
                            <div id="captcha_response"></div>

                            <article class="clearfix t-b-p10 f-r tablet-t-b-p5 mobile-t-b-p0">

                                <input name="signupbtn" id="signupbtn" value="Submit" class="form-control b-m0 poppins-regular font18 white-t t-t-u font-w-b" style="min-height:40px!important; border:0px; border-radius:0px; background-color:#e24242;" type="submit">
                            </article>
                        </article>
                    </form>
                </article>
            </article>

@stop
@section('scripts')
    <script>
        $(document).ready(function(){

            $.validator.addMethod("validate_email", function(value, element) {

                if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                    return true;
                } else {
                    return false;
                }
            }, "Please enter a valid Email.");

                $("#captcha").change(function() {
                var captcha = $(this).val();
                var data = "captcha=" + captcha;
                $.ajax({
                    headers: {
                        'X-CSRF-Token' : '{{ csrf_token() }}'
                    },
                    url: '{{ url('check-captcha') }}',
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    error: function (request, error) {
                        if(error=='parsererror')
                        {
                            $("#signupbtn").removeAttr("disabled");
                            $(".captcha_status").html('<label class="success" for="email">Captca Verified</label>');
                        }
                        else
                        {
                             $("#signupbtn").attr("disabled", "disabled");
                            $(".captcha_status").html('<label class="error" for="email">Invalid Captcha</label>');
                        }
                     },
                    success: function(res){

                    }
                });
            });

            $("#contact").validate({
                rules: {
                    name: "required",
                    mobile: "required",
                    message: "required",
                    email: "required validate_email",
                    captcha: "required"
                },
                messages: {
                    name: {required: "Required..!!"},
                    mobile: {required: "Required..!!"},
                    message: {required: "Required..!!"},
                    email:{required:"Required..!", validate_email: "Enter valid email id"},
                    captcha: {required: "Required..!!"}

                },

                submitHandler: function(form) {
                    $.ajax({
                        headers: {
                            'X-CSRF-Token' : '{{ csrf_token() }}'
                        },
                        url: '{{ url('contact-us') }}',
                        data: $('#contact').serialize(),
                        type: 'POST',
                        success: function(data){
                            if(data =='success'){
                                $('.contact-msg').html('<div class="success">Submitted your details. Will get back you soon.</div>');
                                 $('#contact')[0].reset();
                            }
                            else
                            {
                                $('.contact-msg').html('<div class="error">Error in sending</div>');
                            }
                        }
                    });

                }
            });

        });
    </script>
@stop