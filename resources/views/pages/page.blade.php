@extends('layouts.user.inner')

@section('title', $page->name)

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">{{$page->name}}</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:700px!important;">
        @if(!$page->description)
        <h2 class="Arizonia-Regular  font38 font-w-b orange-t tbp20 t-c">Content Will Update Soon...</h2>
        @else
        {!! $page->description !!}
        @endif
    </article>
@stop
@section('script')

@stop
