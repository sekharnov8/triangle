@extends('layouts.user.inner')

@section('title', $committee->name)

@section('content')



    <article class="l-r-p35 t-p20 mobile-p15">
        <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p0">Leadership</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 tabhorizontal-minheight0" style="min-height:745px!important;">

        <h4 class="clearfix t-b-m0 font20 Montserrat-Regular dk-t1 l-h20  b-p30 mobile-b-p10"><span class="border-b orangeborder1 b-p5">{{$committee->name}}</span></h4>
        @if($chair)
        <article class="clearfix whiteborder border2 shadow-i yellow-bg1 t-p20 mobile-t-p10">
            <article class="clearfix part-row">
                <article class="clearfix part-3">
                    <article class="clearfix l-p15 t-m30n mobile-t-m10 mobile-dis-b mobile-t-c">

                        <article class="clearfix imgcircle-c mobile-dis-in">
                            <img src="@if($chair->user['profile_image'] && File::exists('uploads/users/'.$chair->user['profile_image']))
                                        {{url('uploads/users/'.$chair->user['profile_image'])}} @else {{url('images/pp-img.jpg')}}@endif" width="110" height="110" alt="" border="" />

                        </article>
                    </article>
                </article>
                <article class="clearfix part-5">
                    <article class="clearfix t-b-p15 mobile-t-c mobile-t-b-p5">
                        <h4 class="clearfix t-b-m0 white-t Montserrat-Regular font20">{{$chair->user['title'].' '.$chair->user['first_name'].' '.$chair->user['last_name']}}<span class="OpenSans font14 low-t"> - Chair</span></h4>
                      <label class="clearfix b-m0 l-h10 font16 t-p7"><i class=" mail-i r-m10">&nbsp;</i><a href="mailto:{{$chair->user['email']}}" class="t-m3 l-h10 red-t">{{$chair->user['email']}}</a></label>  

                    </article>
                </article>
                @if($chair->user['mobile'])
                <article class="clearfix part-4">
                    <article class="clearfix t-b-p15 pull-right mobile-pull-n mobile-t-c mobile-l-r-p15">
                        <article class="clearfix call-btn mobile-dis-in">
                            <article class="clearfix Oswald-Light font24 tabhorizontal-r-p0 white-t l-h24 tabhorizontal-font18"><i class="call-ic r-m7"></i>{{$chair->user['mobile']}}</article>
                        </article>
                    </article>
                </article>
                @endif
            </article>

        </article>
        @endif

        <article class="clearfix t-m17">
            <ul class="clearfix list-pn m0 commitees-list">
            @foreach($members as $key=>$member)
            @if($key%2==0)
                <li>
                    <ul class="clearfix list-pn list-f memebers-list m0">
                    @endif
                     @if($member->user['first_name'])
                     <li>
<article class="clearfix dashbor">
  <article class="clearfix l-r-p15 t-b-p10 mobile-v-p5">
      <article class="clearfix box-row">
          <article class="clearfix box-4">
              <article class="clearfix imgcircle-b1">
                  <img src="@if($member->user['profile_image'] && File::exists('uploads/users/'.$member->user['profile_image']))
                                        {{url('uploads/users/'.$member->user['profile_image'])}} @else {{url('images/members-img.jpg')}}@endif" alt="" width="150" height="160" border="">
              </article>
          </article>
          <article class="clearfix box-8">
              <article class="clearfix   mobilev-l-p5 t-m5">
                  <h4 class="clearfix t-b-m0 dk-t1 Montserrat-Regular font18 mobile-font14">{{$member->user['title'].' '.$member->user['first_name'].' '.$member->user['last_name']}}</h4>
                  <span class="OpenSans font16 blue-t">{{$roles_info[$member->role_id]}}</span>
                  <label class="clearfix b-m0 l-h20 font16 t-p7 mobile-v-font12 tablet-font13"><a href="mailto:{{$member->user['email']}}" class="t-m3 l-h10 ltblue-t1 mailicon">{{$member->user['email']}}</a></label>  
                   @if($member->user['mobile'])
                        <article class="clearfix OpenSans font15 ltblue-t1 l-h26 t-p5"><i class="graycall-i r-m10"></i>{{$member->user['mobile']}}</article>
                        @endif
              </article>
          </article>
      </article>
  </article>
</article>
</li>           @endif
                         @if($key%2==1)
                    </ul>
                </li>
                    @endif
            @endforeach



            </ul>
        </article>

    </article>


@stop
@section('script')

@stop
