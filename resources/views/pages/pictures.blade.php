@extends('layouts.user.inner')

@section('title','Photo Gallery')

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Photos <a href="{{url('photos')}}" class="f-r font16">&laquo; Back</a></h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">
  
    
    <h2>{{$photocategory->name}}</h2>
    {!!$photocategory->description!!}

     <ul class="clearfix list-pn list-f m0 photogallery-list">
    @foreach($photocategory->photos as $data)
<li>
                  <article class="clearfix lowgray-bg1 p5 shadow-i">

                  <a href="{{url('uploads/photos/'.$data->image_url)}}" class="fancybox" data-fancybox-group="5"><img src="{{url('uploads/photos/'.$data->image_url)}}" alt="" class="width100 " width="260" height="180" border=""></a>
                  </article>
                </li>
    @endforeach
    </ul>
 
    </article>
@stop
@section('script')

@stop
