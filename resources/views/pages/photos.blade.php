@extends('layouts.user.inner')

@section('title','Photo Gallery')

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Photo Gallery</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">
     @foreach($categories as $photocategory)
     @if($photocategory->subitems)
     <h2>{{$photocategory->name}}</h2>
 
  <ul class="clearfix list-pn list-f m0 photogallery-list">
     @foreach($photocategory->subitems as $photoalbum)
      @if(count($photoalbum->photos))
<li>
                  <article class="clearfix lowgray-bg1 p5 shadow-i">

                  <a href="{{url('photos/'.$photoalbum->slug)}}" >
                  @if($photoalbum->image)  
                  <img src="{{url('uploads/photos/'.$photoalbum->image)}}" alt="" class="width100 " width="260" height="180" border="">
                   @else
                   @if($photoalbum->photos->first())
                   <img src="{{url('uploads/photos/'.$photoalbum->photos->first()->image_url)}}" alt="" class="width100 " width="260" height="180" border="">
                   @else
                   <img src="{{url('images/members-img.jpg')}}" alt="" class="width100 " width="260" height="180" border="">                   
                   @endif 
                   @endif
                  </a>
                  </article>
                   <h4>{{$photoalbum->name}}</h4>

                </li>
                @endif
                @endforeach
    </ul>
    @endif
    @endforeach
 
    </article>
@stop
@section('script')

@stop
