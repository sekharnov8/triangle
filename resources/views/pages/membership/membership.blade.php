@extends('layouts.user.master')

@section('title', 'Membership')

@section('content')

    <section class="clearfix wrapper innerpage-bg">
        <article class="clearfix inner-wrapper b-m55 tabhorizontal-b-m20">
            <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i">
                <article class="l-r-p35 t-p20 mobile-p15">
                    <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p0 mobile-font20">
                        TUTA Membership Form</h2>
                </article>
                                @include('layouts.user.includes.notifications')

                <article class="l-r-m-auto t-b-p20 tabhorizontal-p20 mobile-p15 mobile-t-p0 tabhorizontal-minheight0"
                         style="min-height:745px!important;max-width:810px;"> 
 
                     <article class="clearfix t-p25">
                     {!! Form::open(['action' => 'MembershipController@register', 'id'=>'dataform',  'files'=>true]) !!}
                     
                       
                         
                          <h4 class="clearfix t-b-m0 font18 OpenSans dk-t1 l-h20  b-p30 mobile-b-p10"><span class="border-b orangeborder1 b-p7 r-p20 mobile-r-p0">Login Details</span>
                        </h4>
                         <article class="clearfix part-row b-p5">
                            
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    {!! Form::text('email', null, ['class' => 'form-control donate-form', 'id'=>'email_reg', 'autocomplete'=>'off', 'placeholder' => 'Email *']) !!}
                                    <div class="email_status"></div>
                                </article>
                            </article>
                        </article>
                         <article class="clearfix part-row b-p5">
                                                    <lable class="notes">Need atleast one symbol or digit, one lower character  and one upper character</lable>
                        </article>
                        <article class="clearfix part-row b-p5">
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                <input type="password" name="password" placeholder="Password *" id="password" class="form-control donate-form"  ></article>
                            </article>
                             <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                <input type="password" name="password2" placeholder="Confirm Password *" class="form-control donate-form" ></article>
                            </article>
                        </article>
                           <h4 class="clearfix t-b-m0 font18 OpenSans dk-t1 l-h20  b-p30 mobile-b-p10"><span class="border-b orangeborder1 b-p7 r-p20 mobile-r-p0">Personal  Details</span>
                        </h4>

                         <article class="clearfix part-row b-p5">
                          <article class="clearfix part-4">
                                <label><strong>Are you above 18 years?</strong></label>
                            </article>
                            <article class="clearfix part-6">
                                                 <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="Yes" name="memberage" type="radio" checked>
                                                    <span for="radio1" class="dis-in lowgray-t1">Yes</span>
                                                </label>
                                                  <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="No" name="memberage" type="radio" >
                                                    <span for="radio1" class="dis-in lowgray-t1">No</span>
                                                </label>
                             </article>
                        </article>
                         
                        <article class="clearfix part-row b-p5">
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    <article class="clearfix box-row">
                                        <article class="clearfix box-2">
                                            <article class="clearfix dropdown"
                                                     style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:2.2px 0px;">
                                                {!! Form::select('title', ['Mr.'=>'Mr.', 'Ms.'=>'Ms.', 'Mrs.'=>'Mrs.',
                                                'Dr.'=>'Dr.', 'Prof.'=>'Prof.', 'Others'=>'Others', ],null, ['class' =>
                                                'form-control dropdown-select','style' => 'padding:0px 0px', 'id'
                                                =>'title']) !!}
                                            </article>
                                        </article>
                                        <article class="clearfix box-10">
                                            {!! Form::text('first_name', null, ['class' => 'form-control capitalize donate-form',
                                            'placeholder' => 'First Name *']) !!}
                                        </article>
                                    </article>
                                </article>
                            </article>
                            <article class="clearfix part-3">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    {!! Form::text('middle_name', null, ['class' => 'form-control capitalize donate-form',
                                    'placeholder' => ' Middle Name']) !!}
                                </article>
                            </article>
                             <article class="clearfix part-3">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    {!! Form::text('last_name', null, ['class' => 'form-control capitalize donate-form',
                                    'placeholder' => ' Last Name *']) !!}
                                </article>
                            </article>
                        </article>
                             <article class="clearfix part-row b-p5">
                                <article class="clearfix part-6">
                                    <article class="clearfix r-m15 dropdown"
                                             style="background:none;  border:none; padding:0 15px 0px 0px">
                                        {!! Form::select('gender', ['Male'=>'Male', 'Female'=>'Female'], null,['class' => 'form-control
                                        donate-form','placeholder' => 'Gender *']) !!}
                                    </article>
                                </article>
                                <article class="clearfix part-6">
                                    <article class="clearfix l-m15 mobile-l-m0">
                                        {!! Form::text('mobile', null, ['class' => 'form-control phone donate-form',  'autocomplete'=>'off', 'placeholder'
                                        => 'Mobile Number *']) !!}
                                    </article>
                                </article>
                                
                            </article>
                             <article class="clearfix part-row b-p5">
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    <label class="font15 black-t3 checkboxstyle b-p3">Profile Image</label>
                                    {!!Form::file('profile_image',['placeholder' => 'Image','id'=>'image', 'class' =>
                                    'form-control donate-form image'])!!}
                                </article>
                            </article><article class="clearfix part-6">
                                 <article class="clearfix l-m15 mobile-l-m0">
                                            {!! Form::text('referred_by', null, ['class' => 'form-control donate-form', 'placeholder' => 'Referred by']) !!}
                                        </article>
                            </article>
                        </article> 
                            

                        <article class="clearfix part-row b-p5">
                          <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    {!! Form::text('dob', null, ['class' => 'form-control donate-form', 'id'=>'dob', 'autocomplete'=>'off',
                                    'placeholder' => 'Date of Birth']) !!}
                                </article>
                            </article>
                            <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    {!! Form::text('occupation', null, ['class' => 'form-control donate-form',
                                    'placeholder' => 'Occupation']) !!}
                                </article>
                            </article>
                        </article>
                         <article class="clearfix part-row b-p5">
                          <article class="clearfix part-12">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    {!! Form::text('skills', null, ['class' => 'form-control donate-form',  'autocomplete'=>'off',
                                    'placeholder' => 'Member Skills']) !!}
                                </article>
                            </article>
                          
                        </article>
                         <h4 class="clearfix t-b-m0 font18 OpenSans dk-t1 l-h20  b-p30 mobile-b-p10"><span class="border-b orangeborder1 b-p7 r-p20 mobile-r-p0">Contact  Details</span>
                        </h4>

                        <article class="clearfix part-row b-p5">
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    {!! Form::text('address', null, ['class' => 'form-control donate-form',
                                    'placeholder' => 'Address Line1']) !!}
                                </article>
                            </article>
                            <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    {!! Form::text('address2', null, ['class' => 'form-control donate-form',
                                    'placeholder' => 'Address Line2']) !!}
                                </article>
                            </article>
                        </article>
                        <article class="clearfix part-row b-p5">
                         <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    {!! Form::text('city', null, ['class' => 'form-control donate-form', 'placeholder'
                                    => 'City']) !!}
                                </article>
                            </article>
                            <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    {!! Form::text('state', null, ['class' => 'form-control donate-form', 'placeholder'
                                    => 'State / Province / Region']) !!}
                                </article>
                            </article>
                        </article>

                        <article class="clearfix part-row b-p5">
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    {!! Form::text('zipcode', null, ['class' => 'form-control donate-form zipcode',
                                    'placeholder' => 'Postal / Zip Code']) !!}
                                </article>
                            </article>
                            <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    <article class="clearfix dropdown"
                                             style="background:none;   border:none; border-bottom:2px solid #e0e0e0; padding:2px 0px;">
                                        {!! Form::select('country', [''=>'Select Country']+$countries_data,'United States', ['class' => 'form-control dropdown-select','style' => 'padding:0px 0px', 'id' =>'country']) !!}
                                    </article>
                                </article>
                            </article>
                        </article>
                       

                        <article class="clearfix t-p25">
                        <h4 class="clearfix t-b-m0 font18 OpenSans dk-t1 l-h20  b-p30 mobile-b-p10"><span class="border-b orangeborder1 b-p7 r-p20 mobile-r-p0">Spouse Details</span></h4>
                        <input type="hidden" id="childcount" name="childcount" value="1">
                         <article class="clearfix part-row b-p5">
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    <input placeholder="Spouse First Name" name="spouse_first_name" class="form-control capitalize donate-form" type="text">
                                </article>
                            </article>
                          <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    <input placeholder="Spouse Last Name" name="spouse_last_name"   class="form-control capitalize donate-form" type="text">
                                </article>
                            </article>
                        </article>
                        <article class="clearfix part-row b-p5">
                           
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    <input placeholder="Spouse Email Id" name="spouse_email"  class="form-control donate-form" type="text">
                                </article>
                            </article>
                              <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    <input placeholder="Spouse Phone Number" name="spouse_phone"  class="form-control phone donate-form" type="text">
                                </article>
                            </article>
                        </article>
                        <article class="clearfix part-row b-p5">
                          
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                    <input placeholder="Spouse Occupation" name="spouse_occupation"  class="form-control capitalize donate-form" type="text">
                                </article>
                            </article>
                              <article class="clearfix part-6">
                                <article class="clearfix l-m15 mobile-l-m0">
                                    <input placeholder="Spouse Skills/Interests" name="spouse_skills"  class="form-control donate-form" type="text">
                                </article>
                            </article>
                        </article><br/>
                        <h4 class="clearfix t-b-m0 font18 OpenSans dk-t1 l-h20  b-p30 mobile-b-p10"><span class="border-b orangeborder1 b-p7 r-p20 mobile-r-p0">Children Details</span></h4>

                        <div class="childinfo"><article class="clearfix part-row b-p5"><article class="clearfix part-6"><article class="clearfix r-m15 mobile-r-m0"><input placeholder="Child Name" name="child_name[]"  class="form-control capitalize alpha donate-form" type="text"></article></article><article class="clearfix part-6"><article class="clearfix l-m15 mobile-l-m0"><input placeholder="Child DOB" name="child_dob[]"  class="form-control datepicker donate-form" type="text"></article></article><article class="clearfix part-row b-p5"><article class="clearfix part-6"><article class="clearfix r-m15 mobile-r-m0"><input placeholder="Child Skills" name="child_skills[]" class="form-control  donate-form" type="text"></article></article><article class="clearfix part-6"><article class="clearfix l-m15 mobile-l-m0"><article class="clearfix dropdown" style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:7px 0px;"><select class="dropdown-select" name="child_relation[]" style="padding:0px 0px;"><option value="Son">Son</option><option value="Daughter">Daughter</option></select></article></article></article></article><div class="remove_list"></div></div>
                       
                        <div class="childdata"></div>
                        <article class="clearfix t-m15 b-m20">
                            <article class="clearfix pull-right"><span class="OpenSans font13 check-t r-p7">Add Child</span><a class="add_child"><i class="plusicon">&nbsp;</i></a></article>
                        </article>

                       
                    </article>
                    <h4 class="clearfix t-b-m0 font18 OpenSans dk-t1 l-h20  b-p30 mobile-b-p10"><span class="border-b orangeborder1 b-p7 r-p20 mobile-r-p0">Membership Info</span></h4> 
                                 <article class="clearfix white-bg shadow " style=" border-top: #dddddd 1px solid;"> 
                                    <article class="clearfix l-r-p25 t-b-p15">
                                        <article class="clearfix part-row">
                                        @foreach($mtypes as $mtype)
                                            <article class="clearfix part-6">
                                                <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="{{$mtype->id}}" name="membershiptype_id" type="radio" onClick="check1('{{$mtype->name}}',{{$mtype->price}})">
                                                    <span for="radio1" class="dis-in lowgray-t1">{{$mtype->name}} ( <span class="orange-t">${{$mtype->price}}</span> )</span>
                                                </label>
                                            </article>
                                            @endforeach
                                         <input type="hidden" name="packageamount" class="packageamount" value="">
                                         <input type="hidden" name="donation_amount" class="donation_amount" value="">


                                        </article>
                                    </article>
                                </article>
                                 <h4 class="clearfix t-b-m10 t-p20 b-p10 OpenSans font16 blue-t">Additional Donation Amount:</h4>
                                <article class="clearfix white-bg shadow " style=" border-top: #dddddd 1px solid;" >
                                    <article class="clearfix l-r-p25 t-b-p15">
                                        <article class="clearfix part-row">
                                             <article class="clearfix part-2 l-m4">
                                            {!! Form::text('donation_dollars', null, ['class' => 'form-control donate-form donation_dollars', 'placeholder' => 'Dollars']) !!}$ Dollars
                                            </article>
                                            <article class="clearfix part-2 l-m6">
                                            {!! Form::text('donation_cents', null, ['class' => 'form-control donate-form donation_cents', 'placeholder' => 'Cents']) !!}Cents
                                            </article>

                                              </article>

                                        </article>
                                    </article>
                                </article>
                                 

                                <article class="clearfix t-p25">
                                <div class="payment_section" style="display: none;"> 
                      <h4 class="clearfix t-b-m0 font18 OpenSans dk-t1 l-h20  b-p20 mobile-b-p10"><span class="border-b   b-p7 rp10 mobile-r-p0">Payment Info</span></h4>
                         <article class="clearfix part-row b-p5">
                            <article class="clearfix part-6">
                                <article class="clearfix r-m15 mobile-r-m0">
                                 <article class="clearfix part-row">
                                        @foreach($payment_methods as $key=>$data)
                                            <article class="clearfix part-4">
                                                <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input @if($key==1) checked @endif  value="{{$key}}" name="paymentmethod_id" type="radio" onClick="check2(this.value);" >
                                                    <span for="radio1" class="dis-in lowgray-t1">{{$data}}</span>
                                                </label>
                                            </article>
                                            @endforeach
                                  </article>
                                </article>
                            </article>
                        </article>

                        <div id="alreadypayment" style="display: none;">
                            <p>You will receive a confirmation email once the payment is credited to Triangle United Telugu Association account. Please follow-up with your Triangle United Telugu Association contact, if you did not receive email within seven days.
</p>
                            <p>Check details:
                            <div class="row">
                            <article class="clearfix part-3 r-m10">
                            <input type="text" name="bankname" class="form-control donate-form" placeholder="Bank Name" /></article>
                            <article class="clearfix part-3 r-m10">
                             <input type="text" name="cheque_no" class="form-control donate-form" placeholder="Check No" /></article>
                             <article class="clearfix part-3">
                              <input type="text" name="cheque_date" class="form-control donate-form chequedate" placeholder="Check Date"  autocomplete="off" /></article>
                              </div>
                              </p>
                              <p>
                                <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="Mailed Cheque" name="payment_by" type="radio" onClick="check4(this.value);" >
                                                    <span for="radio1" class="dis-in lowgray-t1">Mailed the cheque already to Triangle United Telugu Association</span>
                                                </label></p>
                                                  <p><label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="Handed Cheque" name="payment_by" type="radio" onClick="check4(this.value);" >
                                                    <span for="radio1" class="lowgray-t1">Handed the cash/cheque to  - 
                                                    <input type="text" name="handedto" class="form-control  handedto" /></span> 
                                                </label></p>
                        </div>
                       </div>
                                
                         <article class="clearfix white-bg shadow l-r-p20 t-b-p15   is_membership" style="display: none; border-top: #dddddd 1px solid;">
                            <article class="clearfix pull-left OpenSans font16 d-gray-t">Membership Type - <span class="membertype"></span></article>
                            <article class="clearfix pull-right OpenSans font14 l-gt1">Amount : <span class="l-blue-t Anton regamount font26 v-align-m"></span></article>
                        </article>
                        <div class="is_donation" style="display: none;">
                        <article class="clearfix white-bg shadow l-r-p20 t-b-p15  ">
                            <article class="clearfix pull-left OpenSans font16 d-gray-t">Other Donation</article>
                            <article class="clearfix pull-right OpenSans font14 l-gt1">Amount : <span class="l-blue-t Anton font26 otherdonationamount v-align-m"></span></article>
                        </article>
                        <article class="clearfix white-bg shadow l-r-p20 t-b-p15  ">
                            <article class="clearfix pull-left OpenSans font16 d-gray-t">Total Amount</article>
                            <article class="clearfix pull-right OpenSans font14 l-gt1">Amount : <span class="l-blue-t Anton font26 totalamount v-align-m"></span></article>
                        </article>
                       </div>

                            <article class="clearfix t-p20">   <label class="font15 black-t3 checkboxstyle b-p3">
                            <input   name="terms" value="1" type="checkbox" checked>
                            <span for="checkbox" class="dis-in"><span class="font14 OpenSans mobile-font14 v-align-m l-h20 l-p5">I accept the Terms and Conditions </span></span></label></article>
                        <article class="clearfix dashborder-t border-t t-m40 mobile-t-m10">
                            <article class="clearfix part-row t-p15"> 
                                     <article class="clearfix pull-right l-m15 mobile-l-m0">
                                        <article class="clearfix t-p15">

                                        <input type="submit"  class="t-c btn-block submitbtn Montserrat-Medium font18 border-radius25 l-r-p20 white-t t-t-u"  style="background-color:#ff9f10; padding-bottom:11px; padding-top:11px;" value="Proceed To  Confirm"> </article>
                                    </article>
                             </article>
                        </article>

                </article>

                <div class="clearfix  border shadow-b2 t-b-p7 l-r-p10 t-l white-bg t-m20">
                    <p>Please make checks payable to "<strong>Triangle United Telugu Association</strong>" &amp; send to the following address with this FORM To : <br><span class="clearfix font14">Triangle United Telugu Association<br>P.O.Box# 191, Morrisville, NC-27560, USA</span> </p>
                </div>


                        {!! Form::close() !!}
                    </article>
                </article>
            </article>
        </article>
    </section>
 <section id="memberpwd" style="display: none; overflow: visible;">
        <article class="clearfix dialog-title t-c white-bg border-b b-p5"> <span class="pull-right r-p10 t-p10"><a class="cursor-p OpenSans-Semibold orange-t" onClick=" $('#memberpwd').dialog('close')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></span>
            <h3 class="font22 OpenSans-Semibold dk-black-t">Member Login</h3>
        </article>
        <article class="clearfix dialog-content">
            <article class="clearfix p30 mobile-p10 border-radius4">
            <div class="login-message"></div>
              <form method="post" autocomplete="off" id="loginform" action="{{url('login')}}">
                    {!! csrf_field() !!}

                    <article class="clearfix p5">
                    <input type="hidden" name="email" id="loginemail" value="">
                    <input name="password" id="loginpwd" class="form-control donate-form"  type="password"   placeholder="Enter Your Password *"  >

                     </article>
                    <article class="clearfix t-p10 t-c">
                        <input value="Submit" class="btn btn-danger btn-block buttonprofileimage border-radius0 t-t-u Montserrat-Regular border0 r-m10 font18" style="background-color:#3e5066;" type="submit">
                    </article>
              </form>
            </article>
            <article class="clearfix b-p20 mobile-b-p5 t-c">
<a class="Roboto-Regular font15" href="{{url('reset')}}">Forgot Password?</a>

</article>
        </article>
    </section>
@stop
@section('scripts')
<link media="all" type="text/css" rel="stylesheet" href="{{url('plugins/pikaday/pikaday.css')}}">
    <script src="{{url('plugins/pikaday/moment.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.js')}}"></script>
    <script src="{{url('plugins/pikaday/pikaday.jquery.js')}}"></script>
    <script>

        $(document).ready(function(e) {

             $.validator.setDefaults({
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if (element.attr("type") == "radio") {
                    error.insertAfter($(element).parents('.part-row'));
                }
                else
                {
                  error.insertAfter($(element));

                }
            }
        });

              $('.donation_dollars').on('change', function () {
                check3();
            });
               $('.donation_cents').on('change', function () {
                check3();
            });

               $.validator.addMethod("alpha", function(value, element) {
                return this.optional(element)||value==value.match(/^[a-zA-Z\s]+$/);}, "Numbers/Special Charactesr not allowed");
 $.validator.addMethod("hasUppercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[A-Z]/.test(value);
}, "Must contain uppercase");
      $.validator.addMethod("hasLowercase", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[a-z]/.test(value);
}, "Must contain lowercase");

   $.validator.addMethod("hasSymbol", function(value, element) {
    if (this.optional(element)) {
        return true;
    }
    return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+\-\d]/.test(value);
}, "Must contain symbol or digit");
        $('.chequedate').datepicker({format: 'mm-dd-yyyy', autoclose: true, todayHighlight: true, startDate: 'd'});

        $("#loginform").validate({
                rules: {
                      password:  {required: true,maxlength:20,minlength: 6},
                },
                messages:
                {
                    password: {"required":"Required..!", "maxlength":"Maximum 20 characters only",minlength:"Minimum 6 characters"},
                },
                submitHandler: function(form) {
                $('.loading').html('<img src="{{url('images/loading.gif')}}" style="width: 50px; padding: 5px 10px;">');
                 $('#signupbtn').attr('disabled','disabled');
                 return true;
                 }
            });

          $("#loginform").submit(function(e) {
                e.preventDefault();
                var data = $(this).serialize();
                if($('#loginpwd').val()!='')
                {
                 $.ajax({
                     headers: {
                       'X-CSRF-Token' : '{{ csrf_token() }}'
                     },
                    method: "POST",
                    url: "ajaxlogin",
                    data: data
                }).success(function(response) {
                    if (response == 'success') {
                        window.location='{{url('account')}}';
                       }
                        else {
                        $('.login-message').html('<div class="notifications alert-dangger error alert-block"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>'+response+'</div>');
                    }
                });
                }
            });

           var picker = new Pikaday(
                  {
                      field: document.getElementById('dob'),
                      firstDay: 1,
                      format: 'MM-DD-YYYY',
                      minDate: new Date({{date("Y")-100}}, 0, 1),
                      maxDate: new Date({{date("Y")-1}}, 12, 31),
                      yearRange: [{{date("Y")-100}},{{date("Y")-1}}]
                  });
            picker.gotoYear('2000');
            $('#memberpwd').dialog
            ({
                autoOpen: false,
                width: 420,
                modal: true
            });


               $(document).on('click','.add_child',function(){ 

            var childno = $('#childcount').val();

            var childdata='<div class="childinfo"><article class="clearfix part-row b-p5"><article class="clearfix part-6"><article class="clearfix r-m15 mobile-r-m0"><input placeholder="Child Name" name="child_name[]"  class="form-control capitalize alpha donate-form" type="text"></article></article><article class="clearfix part-6"><article class="clearfix l-m15 mobile-l-m0"><input placeholder="Child DOB" name="child_dob[]"  class="form-control datepicker donate-form" type="text"></article></article><article class="clearfix part-row b-p5"><article class="clearfix part-6"><article class="clearfix r-m15 mobile-r-m0"><input placeholder="Child Skills" name="child_skills[]" class="form-control  donate-form" type="text"></article></article><article class="clearfix part-6"><article class="clearfix l-m15 mobile-l-m0"><article class="clearfix dropdown" style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:7px 0px;"><select class="dropdown-select" name="child_relation[]" style="padding:0px 0px;"><option value="Son">Son</option><option value="Daughter">Daughter</option></select></article></article></article></article><div class="remove_list"></div></div>';
            $('.childdata').append(childdata);


        });
            $(document).on('click', '.remove_list', function(event) {
        $(this).parents(".childinfo").remove();
    });



            $("#dataform").validate({
                rules: {
                    gender:  "required",
                   // first_name:"required alpha",
                    first_name: {
                        required: true,
                        alpha: true,
                        minlength: 3
                    },
                    last_name:"required alpha",
                    email: "required validate_email",
                    mobile: "required phone",
                    home_phone:"phone",
                    country: "required",
                    zipcode:"zipcode",
                    profile_image: {extension: "jpg|png|gif|jpeg"},
                    password:  {required: true, maxlength:20,minlength: 8,hasSymbol:true,  hasUppercase: true, hasLowercase: true},
                    password2: {
                        required: true,
                        minlength: 8,
                        equalTo: "#password"
                    },
                     membershiptype_id:  "required",
                donation_dollars:  "number",
                donation_cents:  "number",
                terms:  "required",
                bankname: {
                  required: function(element) {
                    if($("input[name='paymentmethod_id']:checked").val()==2)
                    return true;
                  }
                 },
                 cheque_no: {
                  required: function(element) {
                    if($("input[name='paymentmethod_id']:checked").val()==2)
                    return true;
                  }
                 },
                 cheque_date: {
                  required: function(element) {
                    if($("input[name='paymentmethod_id']:checked").val()==2)
                    return true;
                  }
                 }
                },
                messages:
                {
                    gender:  {required:"Required..!"},
                    first_name:{required:"Required..!",minlength:"Minimum 3 characters",alpha:'Numbers/Special characters not allowed'},
                    email:{required:"Required..!", validate_email: "Enter valid email id"},
                    last_name:{required:"Required..!",alpha:'Numbers/Special characters not allowed'},
                    password:{"required":"Required..!", "maxlength":"Maximum 20 characters only",minlength:"Minimum 8 characters", "hasSymbol":"Need Contain Digit or Symbol","hasUppercase":"Need atleast one capital letter","hasLowercase":"Need atleast one lower letter"},
                    password2:{required:"Required..!",minlength:"Minimum 8 characters", equalTo:"Mismatch with password"},
                    mobile:{required:"Required..!", phone: "Enter valid mobile no"},
                    home_phone:{ phone: "Enter valid mobile no"},
                    zipcode:{ zipcode: "Enter valid Zipcode"},
                    country:{required:"Country Required"},
                    profile_image: {extension: 'Not an image!'},
                    membershiptype_id:{required:"Membership Type Required"},
                donation_dollars:{number:"Number only"},
                donation_cents:{number:"Number only"},
                terms:{required:"Terms Required"},
                bankname:{required:"Bank Name Required"},
                cheque_no:{required:"Check No. Required"},
                cheque_date:{required:"Check Date Required"},
                donation_dollars:{number:"Number only"},
                donation_cents:{number:"Number only"}
                },
                submitHandler: function(form) {
                $('.loading').html('<img src="{{url('images/loading.gif')}}" style="width: 50px; padding: 5px 10px;">');
                 $('#signupbtn').attr('disabled','disabled');
                 return true;
                 }
            });

            $("#email_reg").change(function() {
        var email = $(this).val();
        var data = "email=" + email;
        $.ajax({
                headers: {
                    'X-CSRF-Token' : '{{ csrf_token() }}'
                },
                url: '{{ url('check-email') }}',
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(res){
                    if (res == 1) {
                        $("#signupbtn").attr("disabled", "disabled");
                        $(".email_status").html('<label class="error" for="email">Already registered with this email id. Please <a onClick="$(&quot;#memberpwd&quot;).dialog(&quot;open&quot;);">click here</a> to continue.</label>');
                        $('#loginemail').val(email);
                    } else {
                        $("#signupbtn").removeAttr("disabled");
                        $(".email_status").html('');
                    }

                }
            });
        });
        });

  function check2(str)
{
    var paymenttype=$("input[name='paymentmethod_id']:checked").val();
 if(str==1)
{
  document.getElementById("alreadypayment").style.display="none";
}
else
{
  document.getElementById("alreadypayment").style.display="block";

}
}

function check4(str)
{
 if(str=='Mailed Cheque')
{
  $(".handedto").hide();
}
else
{
    $(".handedto").show();
}
}


  function check1(mtype,price)
{
    total=price;
  $(".is_membership").show();
  $(".membertype").html(mtype);
  $(".regamount").html('$'+price);
  $(".packageamount").val(price);
  check3(); 
}

 function check3()
{
  total=Number($(".packageamount").val());
  donation_dollars=Number($(".donation_dollars").val());
  donation_cents=Number($(".donation_cents").val());
  donationamount=donation_dollars+(donation_cents/100)
  total=donationamount+total;
  if(donation_dollars>0 || donation_cents>0)
  {
  $(".is_donation").show();
  $(".otherdonationamount").html('$'+donationamount);
  $(".donation_amount").val(donationamount);
  }
  else
   {
  $(".is_donation").hide();
  }
    $(".totalamount").html('$'+total);
    if(total>0)
    {
        $('.payment_section').show();
    }
    else
    {
     $('.payment_section').hide();   
    }
}

    </script>
@stop
