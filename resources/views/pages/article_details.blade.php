@extends('layouts.user.inner')

@section('title', $article->name)

@section('content')


    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
            {{$article->name}}</h2>
    </article>
    <article class="l-r-p35 t-p20 b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:755px!important;">
    <p>Posted @if($article->post_date)on {{date("M d, Y", strtotime($article->post_date))}}, @endif By {{getUserName($article->updated_by)}}</p>
        <article class="clearfix white-bg shadow-i border-t3 greenborder border-radius4">

        </article>
        <article class="clearfix t-m15">
            <img src="{{url('uploads/articles/'.$article->image)}}" width="762" height="500" alt="" class=" border lowborder p5" />
        </article>
<br/>
         {!! $article->description !!}

        <p class="b-m15">For any further details, please contact us at<a href="mailto:info@tutanc.org" class="blue-t"> info@tutanc.org</a></p>


    </article>

@stop
@section('script')

@stop