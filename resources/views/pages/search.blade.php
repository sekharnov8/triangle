@extends('layouts.user.inner')

@section('title', 'Search Results')

@section('content')

    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Search Results</h2>
    </article>
    <article class="l-r-p35 t-b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">
    <h3>Keyword: {{$keyword}}</h3>
    <h3>Results found: {{count($pages)+count($articles)+count($events)}}</h3>
  <br/>
    @if($pages || $articles || $events)                                
    @foreach($pages as $data)
    <h4><a href="{{url($data->slug)}}">{{$data->name}}</a></h4>
    <p>{{shortenText($data->description,200)}}</p> <br/>
    @endforeach
    @foreach($events as $event)
    <h4><a href="{{url('event/'.$event->slug)}}">{{$event->name}}</a></h4>
    <p>{{shortenText($event->event_details,200)}}</p>  <br/>
    @endforeach
    @foreach($articles as $article)
    <h4><a href="{{url('article/'.$article->slug)}}">{{$article->name}}</a></h4>
    <p>{{shortenText($article->description,200)}}</p>  <br/>
    @endforeach
    @endif
    </article>
@stop
@section('script')

@stop
