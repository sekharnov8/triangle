@extends('layouts.user.inner')

{{-- Page title --}}
@section('title', 'Page Not Found')

{{-- Inline styles --}}
@section('styles')

@stop

@section('content')

   <section>
        <div class="innerpage_body errorpage_info">
	        <div class="container_info">
	        <div class="error_img"><img src="{{ URL::to('images/error_img.png') }}"/></div>
	            <h2 class="text-center"><span>403</span>
	                Page not Found</h2>
	            <p>The page you are looking for was moved removed or <br>
	                renamed or might never existed.</p>
                    <p>&nbsp;<br/><br/></p>
	        </div>
        </div>
    </section>
@stop
