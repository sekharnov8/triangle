@extends('layouts.user.inner')

{{-- Page title --}}
@section('title', 'Internal Error')

{{-- Inline styles --}}
@section('styles')

@stop

@section('content')

   <section>
        <div class="innerpage_body errorpage_info">
	        <div class="container_info">
	        <div class="error_img"><img src="{{ URL::to('images/error_img.png') }}"/></div>
	            <h2 class="text-center"><span>500</span>
	                Internal Error</h2>
	            <p>Internal Error Exception found. We will get back soon.</p>
                    <p>&nbsp;<br/><br/></p>
	        </div>
        </div>
    </section>
@stop
