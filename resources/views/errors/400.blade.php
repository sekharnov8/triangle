@extends('layouts.user.inner')

{{-- Page title --}}
@section('title', 'Error')

{{-- Inline styles --}}
@section('styles')

@stop

@section('content')

   <section>
        <div class="innerpage_body errorpage_info">
	        <div class="container_info">
	        <div class="error_img"><img src="{{ URL::to('images/error_img.png') }}"/></div>
	            <h2 class="text-center"><span>400</span>
	               Error</h2>
	            <p>The page you are looking for was got error. Sorry for the inconvenience.</p>
                    <p>&nbsp;<br/><br/></p>
	        </div>
        </div>
    </section>
@stop
