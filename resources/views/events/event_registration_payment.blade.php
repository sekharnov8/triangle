@extends('layouts.user.inner')

@section('title', $event->name)

@section('content')



        <article class="l-r-p35 t-p20 mobile-p15">
            <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">Event Registration - {{$event->name}}</h2>
        </article>
        {!! Form::open(['route' => 'eventpayment', 'method'=>'POST', 'id'=>'dataform']) !!}
        <article class="l-r-p35 t-p10 b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">

       
       <div class="alert alert-warning b-m10" > You  registration amount is pending. Please pay  using Proceed to Pay below button. </div>
      
          <article class="clearfix white-bg shadow-i border-t3 greenborder border-radius4">
                            <article class="clearfix part-row">
                                <article class="clearfix part-5">
                                    <article class="clearfix border-r dkwhiteborder">
                                        <article class="clearfix  p10 tablet-l-r-p10">
                                               <label class="b-m0"><i class="cla-d-icon">&nbsp;</i> <span class="Montserrat-Regular font13 dk-t1 l-p10 tablet-l-p5"> {{date("M d, Y", strtotime($event->start_date))}}  @if(date("g:i A", strtotime($event->start_date))!='00:00 AM') {{'@ '.date("g:i A", strtotime($event->start_date)) }} - {{date("g:i A", strtotime($event->end_date))}} EST @endif</span></label>

                                        </article>
                                    </article>
                                </article>
                                <article class="clearfix part-7">
                                    <article class="clearfix  tablet-l-m0">

                                        <article class="clearfix p10 tablet-l-r-p5">
                                            <label class="b-m0"><i class="loc-d-icon">&nbsp;</i> <span class="Montserrat-Regular font13 dk-t1 l-p10 tablet-l-p5">@if($event->location_type!='Online'){{$event->location}}@if($event->city){{', '.$event->city}}@endif @if($event->state){{', '.$event->state}}@endif   @if($event->zipcode){{' - '.$event->zipcode}}@endif @else {{$event->location_type}} @endif</span></label>
                                        </article>

                                    </article>
                                </article>
                            </article>
                        </article>
            <article class="clearfix l-r-m-auto t-m10" style="max-width:750px;">
                 <article class="clearfix part-row b-p5">
                    <article class="clearfix part-6">
                        <article class="clearfix r-m15 mobile-r-m0">
                            {!! Form::text('first_name', $eventuser->first_name, ['class' => 'form-control capitalize donate-form',
                            'placeholder' => 'First Name *']) !!}
                            {!! Form::hidden('event_id', $event->id) !!}
                             {!! Form::hidden('eventuser_id', $eventuser->id) !!}
                        </article>
                    </article>
                    <article class="clearfix part-6">
                        <article class="clearfix l-m15 mobile-l-m0">
                            {!! Form::text('last_name', $eventuser->last_name, ['class' => 'form-control capitalize donate-form',
                            'placeholder' => ' Last Name *']) !!}
                        </article>
                    </article>
                </article>
                <article class="clearfix part-row b-p5">
                    <article class="clearfix part-6">
                        <article class="clearfix r-m15 mobile-r-m0">
                            {!! Form::text('email', $eventuser->email, ['class' => 'form-control donate-form',
                            'placeholder' => 'Email *']) !!}
                        </article>
                    </article>
                    <article class="clearfix part-6">
                        <article class="clearfix l-m15 mobile-l-m0">
                            {!! Form::text('mobile', $eventuser->mobile, ['class' => 'form-control phone donate-form', 'placeholder'
                            => 'Mobile Number *']) !!}
                        </article>
                    </article>
                </article>
                <article class="clearfix part-row b-p5">
                    <article class="clearfix part-6">
                        <article class="clearfix r-m15 mobile-r-m0">
                            {!! Form::text('address1', $eventuser->address1, ['class' => 'form-control donate-form',
                            'placeholder' => 'Address Line1']) !!}
                        </article>
                    </article>
                    <article class="clearfix part-6">
                        <article class="clearfix l-m15 mobile-l-m0">
                            {!! Form::text('address2', $eventuser->address2, ['class' => 'form-control donate-form',
                            'placeholder' => 'Address Line2']) !!}
                        </article>
                    </article>
                </article>
                <article class="clearfix part-row b-p5">
                    <article class="clearfix part-6">
                        <article class="clearfix r-m15 mobile-r-m0">

                            {!! Form::text('city', $eventuser->city, ['class' => 'form-control donate-form',
                            'placeholder' => 'City']) !!}

                        </article>
                    </article>
                    <article class="clearfix part-6">
                        <article class="clearfix l-m15 mobile-l-m0">
                            <article class="clearfix dropdown" style="background:none; min-height:28px; border:none; border-bottom:2px solid #e0e0e0; padding:7px 0px;">
                                {!! Form::select('state', $states_data, $eventuser->state,['class' => 'dropdown-select','placeholder' => 'State', 'style'=>'padding-left:0']) !!}
                            </article>
                        </article>
                    </article>
                </article>
               
                @php $is_event_amount=0;@endphp
            @if(count($event->regtypes))
           
                <article class="clearfix overflow_x-a t-m10">
                    <table style="background-color: #fff;" class="table2 Poppins-Regular font14 dkblack-t3 b-m0 tabletext-l" width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr class="poppinssemibold" style="background-color: #fdc633; text-align:center!important;">
                            <th style="padding-left: 20px; vertical-align:middle!important;" width="30%">Registration Category</th>
                            <th style="vertical-align:middle!important;" width="20%">Cost of the   <br/>Event</th>
                            <th style="vertical-align:middle!important;" width="13%">Number of <br/>Attendees </th>
                            <th style="vertical-align:middle!important;" width="13%">Amount</th>
                         </tr>
                         @foreach($eventuser->registrants as $key=>$regdata)
                         @php
                         $regtype=get_event_regtype($regdata->eventregistrationtype_id);
                         @endphp
                             <tr class="">
                                <td style="text-align:center;" class="poppinssemibold">{{$regtype->name}} </td>

                                <td class="poppinssemibold" style="text-align:center;">${{$regtype->amount}}
                                 @if($regtype->amount) @php $is_event_amount=1;@endphp @endif</td> 
                                <td>
                                <input id="hamount{{$key+1}}" value="{{$regtype->amount}}" type="hidden" name="regtypeamount[]">
                               <input type="hidden" name="regtype[]" id="regtype_{{$key+1}}" value="{{$regtype->name}}">
                               <input type="hidden" name="regtype_count{{$key+1}}" id="regtype_count{{$key+1}}" value="{{$regtype->reg_count}}">
                                <input type="hidden" name="regtype_id[]" value="{{$regtype->id}}">
                               
                                     <input type="text" class="form-control number b-m0" name="count[]" id="people{{$key+1}}"  onkeyup="calculate_total({{$key+1}},{{$regtype->id}})" value="{{$regdata->count}}" style="min-height: 32px;" max="{{$regtype->reg_count}}">
  
                                </td>
                                <td style="text-align:center;" class="poppinssemibold">
                                    <article id="amount{{$key+1}}">${{$regdata->count*$regdata->amount}}</article>
                                </td> 
                            </tr>
                              @if($regtype->name=='Family')
                              <tr class="is_family" @if(!$regdata->count) style="display: none;" @endif ><td>  </td><td colspan="3"><article class="part-row mobile-t-c">
                                                <article class="part-2">
                                                Adults
                                                </article>
                                                <article class="part-3">
                                                <input class="form-control number b-m0" name="family_adults" id="family_adults" onkeyup="calculate_total({{$key+1}},{{$regtype->id}},this.value)" style="min-height: 32px;" value="{{$regdata->family_adults}}" type="text">
                                                </article>

                                                 <article class="part-2 t-c">
                                                Kids
                                                </article>
                                                <article class="part-3">
                                                <input class="form-control number b-m0" name="family_kids" id="family_kids" onkeyup="calculate_total({{$key+1}},{{$regtype->id}})" style="min-height: 32px;"  type="text" value="{{$regdata->family_adults}}">
                                                </article>
                                            </article>
                                             
                                            </td>
                                        </tr>
                                        @endif
                           
                            @endforeach
                           
                        </tbody></table></article>
                @endif


                      @if(count($event->sponsortypes))  
                      <h4 class="clearfix t-b-m0 font16 OpenSans dk-t1 l-h20 t-p10"> <label class="font15 black-t3 checkboxstyle b-p3">
                                        <input  name="is_sponsorship" class="is_sponsorship" value="1" type="checkbox" @if($eventuser->sponsorship_id) checked @endif >
                                        <span for="checkbox" class="dis-in"><span
                                                    class="font14 OpenSans mobile-font14 v-align-m l-h20 l-p5">Is Sponsor to this event?</span></span></label></h4>
                                                    @endif
                                                    <input type="hidden" name="sponsorship_amount" id="sponsorship_amount" value="0">

                       

                  <article class="clearfix  t-m10" id="sponsorships" @if(!$eventuser->sponsorship_id)  style="display: none;" @endif>

                <article class="clearfix part-row">
                                        @foreach($event->sponsortypes as $stype)
                                            <article class="clearfix part-4">
                                                <label class="checkboxstyle dis-in b-m0 font15 OpenSans">
                                                    <input  value="{{$stype->id}}" name="sponsorship_id" type="radio" onClick="check_es({{$stype->amount}});" data-value="{{$stype->amount}}" @if($eventuser->sponsorship_id==$stype->id) checked @endif>                                                    
                                                    <span for="radio1" class="dis-in lowgray-t1">{{$stype->name}} ( <span class="orange-t">${{$stype->amount}}</span> )</span>
                                                </label>
                                            </article>
                                            @endforeach

                                        </article>
                    </article>
                    <article class="clearfix part-row">
                    <article class="clearfix part-4">
                   <h4 class="clearfix t-b-m0 font16 OpenSans dk-t1 l-h20 t-p20">   Additional donation amount : </h4></article>
                   <article class="clearfix part-3"> 
                                    {!! Form::text('donation_amount', $eventuser->donation_amount, ['class' => 'form-control donate-form',
                                    'placeholder' => 'Donation Amount','onkeyup'=>'calculate_total_2()', 'id'=>'donation']) !!}
                                </article>
</article>


                    <input type="hidden"  id="AmountPaid" name="payamount" value="{{$eventuser->amount_paid}}"/>
                    <input type="hidden"  id="people_alert" name="people_alert" value=""/>

                     <article class="clearfix border gray-border white-bg shadow-b p10 t-m20 ">
                    <article class="part-row mobile-t-c">

                        <article class="part-3 "><article class="clearfix v-align-m font15 t-m15 mobile-t-m10 ispayment">Payment & Authorized :</article> </article>
                        <article class="part-5 l-m0 t-c ">
                            <img alt="Paypal" src="{{url('images/paypal.jpg')}}" class="ispayment">
                        </article>
                        <article class="part-4">
                            <article class="clearfix v-align-m t-b-p15 t-c">Total Amount : <span class="font16 red-t" id="total">
                                      ${{$eventuser->amount_paid}}</span></article>
                        </article></article>

                </article>
              
                     <article class="clearfix box-row">
              <article class="clearfix box-12">
               @php 
  $min_number = 1;
  $max_number = 15;
  $random_number1 = rand($min_number, $max_number);
  $random_number2 = rand($min_number, $max_number);
     @endphp
     {{$random_number1 . ' + ' . $random_number2 . ' = '}}
                  <input type="text" style="width:120px; display: inline-block;" maxlength="10" placeholder="Enter total" 
 size="10" name="security_code" id="security_code" onChange="checkcaptchamath()" required class="inputstyle"  required /> 

 <input name="firstNumber" type="hidden" id="firstNumber" value="{{ $random_number1}}">
      <input name="secondNumber" type="hidden" id="secondNumber" value="{{$random_number2}}">
 </article>
                </article>
                            <div id="captcha_response"></div> 
 
                <article class="clearfix part-row box-row t-b-m20">

                    <article class="clearfix part-12">
                        <article class="clearfix pull-left">
                            <article class="clearfix ">
                                <a href="{{url('event/'.$event->slug)}}"><input type="button" value="Back" class="t-c l-r-p20 Montserrat-Medium font18 border-radius25 white-t t-t-u" style="background-color:#f7f7f7;
                                            padding-bottom:11px;color: #707070; padding-top:11px;"></a>
                            </article>
                        </article>
                        <article class="clearfix pull-right mobile-t-c tabhorizontal-t-m10 tabhorizontal-dis-in ">
                            {!! Form::submit('Register', ['id'=>'signupbtn', 'class' => 't-c l-r-p20 Montserrat-Medium
                            font18 border-radius25 white-t t-t-u','style'=>'background-color:#ff9f10;
                            padding-bottom:11px; padding-top:11px;']) !!}   
                              <p align="right" class="red-t alreadyregister" style="display: none;">Already registered for this event.</p> 
                             <div class="loading" style="display: none;"><img src="{{url('images/loading.gif')}}" style="width: 50px; padding: 5px 10px;"></div>
                             </article>
                    </article>
                </article>
            </article>
        </article>
        {!! Form::close() !!}

@stop
@section('scripts')
    <script>
        $(document).ready(function () { 

calculate_total_2();
check_es({{$eventuser->sponsorship_amount}});
           @if($is_event_amount==0)
                 $('.ispayment').hide();
           @endif 
        jQuery.validator.addMethod("notEqual", function (value, element, param) {
            return this.optional(element) || value != '0';
        });
       $.validator.addMethod("number", function(value, element) {
    return this.optional(element) || /^-?\d+$/.test(value);
}, "Number Only");
        $.validator.addMethod("number2", function(value, element) {
    return this.optional(element) || /^\+?[0-9]*\.?[0-9]+$/.test(value);
}, "Invalid Amount");

         $.validator.addMethod('positiveNumber',
            function (value) { return Number(value) > 0; }, 'Enter a positive number.');
            $("#dataform").validate({
                rules: {
                    first_name:"required alpha",
                    last_name:"required alpha",
                    email: "required email",
                    mobile: "required phone",
                    @if(count($event->regtypes))
                    payamount: {required:true, number:true, notEqual:'0', positiveNumber:true},
                    @endif
                    security_code: "required"
                },
                messages:
                {
                    first_name:{required:"Required..!!",alpha:'Numbers/Special characters not allowed'},
                    email:{required:"Required..!!", email: "Enter valid email id"},
                    last_name:{required:"Required..!!",alpha:'Numbers/Special characters not allowed'},
                    mobile:{required:"Required..!!", phone: "Enter valid mobile no"},
                    payamount:{required:"Required..!", number: "Enter valid amount", notEqual: "Enter amount greater than 0", positiveNumber:"Enter valid amount"},
                    security_code: {required: "Required..!!"}   
                },
             submitHandler: function(form) {
                var $filledTextboxes = $(":text[name='count[]']").filter(function(){
                    if($(this).val()<=0 || $(this).val()=='-')
                        $(this).val('');
                return $.trim($(this).val()) != "" ;
                });
                if($filledTextboxes.size() == 0)
                {
                   alert("Need fill registration category count");
                   return false;
                }
                 else
                {
                    $('#signupbtn').attr('disabled',true);   
                    $('.loading').show(); 
                    return true;
                }
            }
            });

             $(document).on('change', '.is_sponsorship', function(event) {
 var str=$("input[name='is_sponsorship']:checked").val();
   if(str==1)
{
$('#sponsorships').show();
}
else
{
 $('#sponsorships').hide();
 $("input:radio").attr("checked", false);
 $("#sponsorship_amount").val(0);
}

calculate_total_2();

     });
        });

  function check_es(amount)
{
 var str=$("input[name='is_sponsorship']:checked").val();    
 if(str==1)
{
$("#sponsorship_amount").val(amount);
}
else
{
$("#sponsorship_amount").val(0);
}
calculate_total_2();
}
function calculate_total_2()
{
	 total5 = 0;
                         
                         for (i = 1; i <= {{count($event->regtypes)}}; i++) {
                            p=0;          
                             regtype=$('#regtype_' + i).val();
                                 p = document.getElementById('people' + i).value;
                                   p = parseInt(p);

                              if (p > 0) {  
                                total5 = total5 + parseFloat($('#hamount' + i).val()*p);
                               }  
                          }
                         total5=total5+parseFloat($('#sponsorship_amount').val());
                         if($('#donation').val())
                         total5 = total5 + parseFloat($('#donation').val());


                          document.getElementById("total").innerHTML = "$" + (total5);
                         $('#AmountPaid').val(total5);
                         if(total5>0)
                         {
                            $('#signupbtn').val('Proceed to Pay');
                            $('.ispayment').show();
                         }
                         else
                         {
                            $('#signupbtn').val('Register');
                            $('.ispayment').hide();
                         }
}
 function calculate_total(id,regid)
        {
             regtype=$('#regtype_' + id).val();
                maxcount= document.getElementById('regtype_count' + id).value;
                 peoplecount=$('#people' + id).val();
 

              if(regtype=='Family')
              {
                maxcount=maxcount*peoplecount;
                 if(peoplecount>0)
              {
                $('.is_family').show();
              }
              else
              {
                 $('.is_family_'+ id).hide();
              }

                  people_a=$('#family_adults').val();

                  people_k=$('#family_kids').val();
                  peoplesum=parseInt(people_a)+parseInt(people_k);
                  if(peoplesum==0)
                  {
                     alert('Enter No. of Adult/Kids'); 
                     return false;
                  }
                    if(peoplesum>maxcount)
                    {
                     alert('Need enter less than '+maxcount);   
                     $('#family_adults').val('');
                     $('#family_kids').val('');
                      $('#amount' + id).html('$0');
                        return false;
                  }

              }
              else
              {
                 if(peoplecount>maxcount)
            {
                 $('#people' + id).val('');
                  alert('Need enter less than '+maxcount); 
                    return false;
             }
              }

            if(peoplecount<0 )
            {
                alert('Enter Valid number');
                return false;
             }
            var data = "id=" + regid;  
             
            $.ajax({
                headers: {
                    'X-CSRF-Token' : '{{ csrf_token() }}'
                },
                url: '{{ url('check-regcount') }}',
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(res){
                    // console.log(res)
                     ucount=peoplecount+res.count;
                     if (res.totalcount<ucount) {
                     // if (res.count<peoplecount) {
                        alert('Event Maximum registration is reached.');
                        if(regtype=='Family')
                        {
                            $('#family_adults').val(0);
                            $('#family_kids').val(0);

                        }
                        else
                        {
                        $('#people' + id).val(0);
                        }
                        $('#amount' + id).html('$0');
                        calculate_total(id,regid);
                        return false;
                    }
                    else
                    {
                        var total2=$('#hamount' + id).val() * $('#people' + id).val();
                         

                        if($('#hamount' + id).val()==0 ||peoplecount==0)
                            total2=0;
                         document.getElementById("amount" + id).innerHTML = "$" + total2;
                             
                          total5 = 0;
                         var z = 0;
                         var otherpeople=0;
                         var familypeople=0;
                         for (i = 1; i <= {{count($eventuser->registrants)}}; i++) {
                            p=0;        
                             if($('#regtype_' + i).val()=='Family')
                             {
                                people_a=$('#family_adults').val();
                                  people_k=$('#family_kids').val();
                                  p=parseInt(people_a)+parseInt(people_k);
                             }  
                             else 
                            p = document.getElementById('people' + i).value;
                            p = parseInt(p);
                            if (p > 0) {
                                  if($('#regtype_' + i).val()=='Family')
                                    familypeople++;
                                else
                                {
                                   otherpeople++;  
                                }

                                total5 = total5 + parseFloat($('#hamount' + i).val()*p);
                             }
                            
                          }
                          if(familypeople>0 && otherpeople>0)
                          {
                             $('#people_alert').val(1);
                             
                             if(!$('#people_alert').val())
                             {  
                                $('#people2').focus();
                             }
                          }  
                         calculate_total_2();
                         
                    }
                }
            });
     

        } 

    </script>
@stop