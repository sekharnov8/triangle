@extends('layouts.user.inner')

@section('title')

@section('content')


    <section class="clearfix wrapper innerpage-bg">


        <article class="clearfix white-bg border-t4 about-topborder border-radius4 shadow-i2">
            <article class="l-r-p35 t-p45 b-p25 mobile-p15 tabhorizontal-t-p15 tabhorizontal-minheight0" style="min-height:850px!important;">
                <article class="l-r-m-auto t-c Roboto-Regular font26" style="max-width:610px;">
                    <article class="clearfix low-bg2" style="border-radius:20px;">
                        <article class="clearfix green-bg t-b-p30" style="border-radius:20px;">
                            <img src="{{url('images/thankyou.png')}}" width="114" height="113" border="0" alt="" />
                            <h3 class="Montserrat-Regular t-t-u font34 white-t mobile-font24">Thanks for Registering</h3>
                        </article>
                        <article class="clearfix t-b-p30 t-c l-r-m-auto lowgray-t3 font20 mobile-font18 mobile-l-r-p10 mobile-t-b-p15" style="max-width:510px;">
                            You've successfully registered for our event! You will also receive a confirmation e-mail for your registration.</article>
                    </article>
                </article>
                 @if($eventuser->amount_paid>0)
                <article class="clearfix t-p20">
                    <article class="clearfix border-t3 greenborder border-radius5 shadow white-bg">
                        <article class="clearfix part-row">
                            <article class="clearfix part-6 border-r grayborder5">
                                <article class="clearfix t-p15 b-p15">
                                    <h4 class="clearfix t-m0 Montserrat-Regular font15 dk-t4"><span class="border-b grayborder5 rp35 tabhorizontal-r-p10 b-p10"><span class="l-p25">Your Details</span></span></h4>
                                </article>
                                <article class="clearfix b-p25 l-p25">
                                    <h5 class="OpenSans dk-t4 t-m0 font15">{{$eventuser->first_name}} {{$eventuser->last_name}}</h5>
                                    <label class="OpenSans font14 lowgray-t3 l-h20">{{$eventuser->address1}} {{$eventuser->address2}}<br/>
                                        {{$eventuser->city}} {{$eventuser->state}}</label>
                                    <a href="mailto:info@kksystems.in" class="OpenSans font14 blue-t1 l-h20">{{$eventuser->email}}</a></article>
                            </article>
                             <article class="clearfix part-6">
                                <article class="clearfix t-p15 b-p15 mobile-t-p0">
                                    <h4 class="clearfix t-m0 Montserrat-Regular font15 dk-t4"><span class="border-b grayborder5 r-p45 b-p10"><span class="l-p25">Payment Received By</span></span></h4>
                                </article>
                                <article class="clearfix b-p25 l-p25">
                                    <h5 class="OpenSans dk-t4 t-m0 font15">Triangle United Telugu Association</h5>
                                    <label class="OpenSans font14 lowgray-t3 l-h20">P.O.Box# 191, Morrisville, NC-27560, USA</label>
                                    <a href="mailto: info@tutanc.org" class="OpenSans font14 blue-t1 l-h20"> info@tutanc.org</a></article>
                            </article>
                         </article>
                    </article>
                </article>

                <article class="clearfix t-p25 overflow_x-a">
                    <table class="table2 t-l" width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                        <tr>
                            <th width="16.6%">Date & Time</th>
                            <th width="23.7%">Transaction Details</th>
                            <th width="28.2%">Summery</th>
                            <th width="16.9%">Status</th>
                            <th width="14.6%">Total</th>
                        </tr>
                        <tr>
                            <td  width="16.6%">{{dateformat($eventuser->created_at)}}</td>
                            <td width="23.7%">{{$eventuser->transaction_id}}</td>
                            <td width="28.2%">Thank you for Register in {{$eventuser->event->name}} event </td>
                            <td width="16.9%">Completed</td>
                            <td width="14.6%">${{$eventuser->amount_paid}}</td>
                        </tr>
                        </tbody>
                    </table>
                </article>
                @endif
            </article>
        </article>


    </section>
@stop
@section('script')

@stop