@extends('layouts.user.inner')

@section('title', $event->name)

@section('content')
                    <article class="l-r-p35 t-p20 mobile-p15"> <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0 border-b dashborder-b b-p15 mobile-b-p5">
                            {{$event->name}}<div class="f-r " style="padding-bottom: 10px">@if($event->end_date>date("Y-m-d"))<a href="{{url('events')}}" class="btn btn-success active">Upcoming Events</a>@else<a href="{{url('past-events')}}" class="btn"> Past Events</a>@endif</div></h2>
                    </article>
                    <article class="l-r-p35 t-p20 b-p25 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:755px!important;">
                        <article class="clearfix white-bg shadow-i border-t3 greenborder border-radius4">
                            <article class="clearfix part-row">
                                <article class="clearfix part-4">
                                    <article class="clearfix border-r dkwhiteborder">
                                        <article class="clearfix  p10 tablet-l-r-p10">
                                            <label class="b-m0"><i class="cla-d-icon">&nbsp;</i> <span class="Montserrat-Regular font13 dk-t1 l-p10 tablet-l-p5">{{date("M d, Y", strtotime($event->start_date))}}@if($event->start_date!=$event->end_date) - {{date("M d, Y", strtotime($event->end_date))}}@endif  @if($event->start_time!='') {{'@ '.$event->start_time }} @endif
                                    @if($event->end_time!='') {{'-'.$event->end_time }} @endif</span></label>
                                        </article>
                                    </article>
                                </article>
                                <article class="clearfix part-5">
                                    <article class="clearfix tablet-l-m0">

                                        <article class="clearfix p10 tablet-l-r-p5">

                                            <label class="b-m0"><i class="loc-d-icon">&nbsp;</i> <span class="Montserrat-Regular font13 dk-t1 l-p10 tablet-l-p5">{{$event->location}}@if($event->city){{', '.$event->city}}@endif @if($event->state){{', '.$event->state}}@endif   @if($event->zipcode){{' - '.$event->zipcode}}@endif</span></label>
                                        </article>

                                    </article>
                                </article>
                                <article class="clearfix part-3">
                                    <article class="clearfix t-m10 mobile-b-m15 r-m5 pull-right mobile-pull-n mobile-t-c mobile-t-m0">
                                      @if($event->is_registration==1 && $event->registration_start_date<=date("Y-m-d") && $event->registration_end_date>=date("Y-m-d"))
                                        <a href="{{url('event/'.$event->slug.'/registration')}}" style="background-color:#00a1dc;" class="l-r-p20 t-b-p10 font13 t-t-u Montserrat-Regular border-radius4 white-t">Register Now</a>
                                      @endif
                                    </article>
                                </article>
                            </article>
                        </article>
                        <div class="clearfix part-row t-p10"> <article class="clearfix part-6"><strong>Category:</strong> {{$event->category->name}}</article>  
                            <article class="clearfix part-6"><strong>Contact Email:</strong> {{$event->contact_mail}}</article>
                         <article class="clearfix part-6"><strong>Event For:</strong> {{$event->event_for}}</article></div>
                        @if($event->banner_url && File::exists('uploads/events/'.$event->banner_url))
                        <article class="clearfix t-m15">
                            <img src="{{url('uploads/events/'.$event->banner_url)}}" width="762"  alt="{{$event->name}}" class="width100 border lowborder p5" />
                        </article>
                        @endif
                        @if($event->event_details)
                        <h4 class="t-m0 OpenSans font20 dk-t1 t-b-p25 mobile-t-b-p15"><span class="border-b orangeborder b-p10 r-p40">Description</span></h4>

                             {!!$event->event_details!!}
                         @endif

            </article>

@stop
@section('script')

@stop