@extends('layouts.user.inner')

@section('title', 'Past Events')

@section('content')


        <article class="l-r-p35 t-p20 mobile-p15">
            <article class="clearfix part-row border-b dashborder-b">

                <article class="clearfix part-12">
                    <h2 class="dkblue-t SuiGenerisRg-Regular font28 t-m0">Events <div class="f-r " style="padding-bottom: 10px"><a href="{{url('events')}}" class="btn ">Upcoming Events</a>&nbsp;<a href="{{url('past-events')}}" class="btn btn-success active"> Past Events</a></div>  </h2>
                </article>
            </article>
        </article>
        <article class="l-r-p35 t-p20 b-p10 mobile-p15 mobile-t-p0 tabhorizontal-minheight0" style="min-height:778px!important;">
            <ul class="clearfix list-pn list-f m0 featured-list3">
                @foreach($events as $data)
                <li>
                    <article class="clearfix eventsli-bg border dkgrayborder1">
                        <article class="clearfix t-b-p5 l-p15">
                            <h2 class=" Montserrat-Regular font16 gray-t t-b-m0">{{ShortenText($data->name,35)}}</h2>
                        </article>
                        <span class="dis-b"><img src="{{url('images/heading-border1.png')}}" width="172" height="1"></span>
                        <article class="clearfix l-r-p25 t-b-p15"> @if($data->banner_url && File::exists('uploads/events/'.$data->banner_url)) <img src="{{url('uploads/events/'.$data ->banner_url)}}" style="width: 315px;height: 230px;" alt="" class="mobilemaxwidth1 width100">
                            @else <img src="{{url('images/event-no-image.png')}}" width="315" height="230" alt="" class="mobileimgwidth1" style="width: 315px;height: 230px;" />@endif</article>
                        <article class="clearfix box-row">
                            <article class="clearfix box-9">
                                <article class="clearfix r-m10 t-b-p7 l-r-p15 low-bg">
                                    <h4 class="clearfix Montserrat-Regular font16 t-b-m0 dk-t1 l-h26">                                    @if($data->start_date!=$data->end_date) {{date("M d", strtotime($data->start_date)).'-'.date("M d, Y", strtotime($data->end_date))}} @else 
                                     {{date("M d, Y", strtotime($data->start_date))}} @endif
                                    @if($data->start_time!='') {{'@ '.$data->start_time }} @endif
                                    @if($data->end_time!='') {{'-'.$data->end_time }} @endif
                                     </h4>
                                </article>
                            </article>
                            <a class="masterTooltip" title="{{$data->location.' '.$data->city.' '.$data->state.' '.$data->zipcode}}">
                                <article class="clearfix box-3">
                                    <article class="clearfix t-b-p6 l-r-p6 orange-bg dis-b t-c">  <img src="{{url('images/loc-icon.png')}}" width="26" height="25" alt="" />  </article>
                                </article> </a>
                        </article>
                        <article class="clearfix t-b-p15 t-c">
                            <a class="more-btn Montserrat-Regular font11 t-t-u dk-t1 l-m7" href="{{url('event/'.$data->slug)}}">More</a>
                        </article>
                    </article>
                </li>
             @endforeach

            </ul>
        </article>

@stop
@section('script')

@stop
