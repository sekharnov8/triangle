<!DOCTYPE html>
<html lang="en">

@section('head')
    @include('layouts.includes.head')
@show
<body class="skin-yellow sidebar-mini">
<div class="wrapper">

    @include('layouts.includes.header')

    @include('layouts.includes.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layouts.includes.content_header')

        <!-- Main content -->
        <section class="content">
            @include('layouts.includes.notifications')
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layouts.includes.control_sidebar')

    @include('layouts.includes.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layouts.includes.scripts')
    @yield('js')
@show

</body>
</html>
