@if ($errors->any())
<div class="notifications alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert"><i class="fa fa-minus-square"></i></button>
	@if ($message = $errors->first(0, ':message'))
	{{ $message }}
	@else
	Please check the form below for errors
	@endif
</div>
@endif

@if ($message = Session::get('success'))
	<script type="text/javascript">
		swal("Thanks!", "{{ $message }}", "success");
	</script>

@endif
