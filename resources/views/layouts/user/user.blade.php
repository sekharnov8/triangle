<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en-US"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en-US"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
@include('layouts.user.includes.meta')
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/tam.css')}}"/>
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert/dist/sweetalert.css')}}">
    <!-- Font Awesome Icons -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet"  type="text/css"/>
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    @yield('css')
</head>
<body>
@include('layouts.user.includes.header')
@include('layouts.user.includes.news')
@if(set_active_admin('account')!='active')
 <article class="sidemenu tabhorizontal-hide" id="side4">
                <article class="clearfix side">
                    <article id="close">
                          <a onClick="leftpane2();"><img src="{{url('images/close1.png')}}" width="50" height="30"/></a>
                    </article>
                     <article id="open" class="hide">
                          <a onClick="leftpane3();"><img src="{{url('images/menu1.png')}}" width="50" height="30"/></a>
                    </article>
                </article></article>
@endif                
@yield('content')
@include('layouts.user.includes.footer')
@include('layouts.user.includes.scripts')
@yield('scripts')
</body>
</html>