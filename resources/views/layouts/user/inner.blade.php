@include('layouts.user.includes.meta')
@yield('css')
</head>

<body class="tuta">
    <main>
        @include('layouts.user.includes.header')
        <div class="container">
            <div class="row">
                <div class="col content_area pt-3 p-md-4 rounded-4 rounded-xs-4">
                    <div class="row">
                        <div class="col-md-8 col-lg-9">
                            <!-- Inner Banner -->
                            <div class="row mb-4">
                                <div class="col">
                                    <img src="assets/images/bhavita_banner.jpg" alt="" class="rounded-4">
                                </div>
                            </div>

                            <!-- Inner Banner End -->
                            @yield('content')
                        </div>
                        @include('layouts.user.includes.sidebar')
                    </div>
                </div>
            </div>
            @include('layouts.user.includes.footer')
        </div>
    </main>
    @include('layouts.user.includes.scripts')
    @yield('scripts')
</body>

</html>