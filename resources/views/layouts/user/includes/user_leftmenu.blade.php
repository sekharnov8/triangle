<article class="clearfix left-block9" id="lb9">
    <article class="clearfix profile-bg p-radius tabhorizontal-minheight0"
             style="min-height:950px;">

        <article class="clearfix t-p35 b-p15 r-p20 mobile-p15 tabhorizontal-p20 position-r">
            <article class="clearfix t-c position-r">
                 <img @if($loggedin_user->profile_image && File::exists('uploads/users/'.$loggedin_user->profile_image)) src="
                                        {{url('uploads/users/'.$loggedin_user->profile_image)}}" @else src="{{url('images/profile-img.jpg')}}" @endif width="160" height="160" alt="" class="s-blueborder border3 border-radius4 userprofileimage" /></article>
            <h4 class="OpenSans-Semibold white-t font24 t-m0 t-p8 t-c">{{$loggedin_user->title.' '.$loggedin_user->first_name}}</h4>
            <h5 class="ltblue-t OpenSans font15 t-b-m0 l-h20 t-c">UID: <lable class="white-t OpenSans font15 t-b-m0 ">{{$loggedin_user->member_id}}</lable></h5>
                                                <h5 class="ltblue-t OpenSans font15 t-b-m0 l-h20 t-c">Role: <lable class="white-t OpenSans font15 t-b-m0 ">{{getUserRoles($loggedin_user->id)}}</lable></h5>
                                                @if(is_committee_member())
                                                <h5 class="ltblue-t OpenSans font15 t-b-m0 l-h20 t-c">Committee: <br/><lable class="white-t OpenSans font15 t-b-m0 ">{!! is_committee_member()!!}</lable></h5>
                                                @endif
                                             


        </article>

        <article class="clearfix border-t4 b-blueborder mobile-bor-t0">
            <article class="clearfix tabhorizontal-p10">


               <ul class="clearfix list-pn m0 dashboard-list dis-b OpenSans font13 tabs">
                        
 

                                                @include('layouts.user.includes.user_leftmenu_common')
                                                 <li  class="profileborder"><a href="{{url('logout')}}"><i class="logout-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Logout</span></a></li>
                                            </ul>
            </article>
        </article>
    </article>
</article>
 