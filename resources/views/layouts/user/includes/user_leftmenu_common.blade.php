 <li class="profileborder"  ><a href="{{url('account')}}"><i class="ur-icon r-m13">&nbsp;</i><span class="v-align-m t-p5">My Profile</span></a></li>
 @if(set_active_admin('account')=='active' )
                                                 <li rel="ep" class="profileborder"><a><i class="ed-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Edit Profile</span></a></li>
@endif
                                                <li  class="{{ set_active_admin('membership-info') }}  profileborder">  <a href="{{url('membership-info')}}"><i class="ii-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Membership Info</span></a></li>
                                                  <li  class="{{ set_active_admin('donation-transactions') }} profileborder">  <a href="{{url('donation-transactions')}}"><i class="ii-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">My Donation Transactions</span></a></li>
  <li  class="{{ set_active_admin('event-transactions') }} profileborder">  <a href="{{url('event-transactions')}}"><i class="ii-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Registered Events</span></a></li> 
@if(is_committee_member_any() || $loggedin_user->inRole('treasurer')||$loggedin_user->inRole('joint-treasurer'))
<li class="{{ set_active_admin('user-events') }} profileborder"><a href="{{url('user-events')}}"><i class="ed-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Posted Events</span></a></li>
@if($loggedin_user->inRole('treasurer')||$loggedin_user->inRole('joint-treasurer'))
<li class="{{ set_active_admin('treasurer/expenses') }} profileborder"><a href="{{url('treasurer/expenses')}}"><i class="s-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Expenses</span></a></li>
@else
<li class="{{ set_active_admin('expenses') }} profileborder"><a href="{{url('expenses')}}"><i class="s-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Expenses</span></a></li>
@endif
@endif
@if($loggedin_user->inRole('treasurer')||$loggedin_user->inRole('joint-treasurer'))
 <li class="profileborder {{ set_active_admin('treasurer/donation') }}"  ><a href="{{url('treasurer/donation')}}"><i class="ur-icon r-m13">&nbsp;</i><span class="v-align-m t-p5">Donations</span></a></li>
   <li  class="{{ set_active_admin('members') }} profileborder">  <a href="{{url('members')}}"><i class="ii-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Members List</span></a></li>
<li class="profileborder {{ set_active_admin('member-orders') }}"  ><a href="{{url('member-orders')}}"><i class="ur-icon r-m13">&nbsp;</i><span class="v-align-m t-p5">Membership Orders</span></a></li>
@endif
@if(is_membership_chair())
  <li  class="{{ set_active_admin('members') }} profileborder">  <a href="{{url('members')}}"><i class="ii-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Members List</span></a></li>
 <li class="profileborder {{ set_active_admin('member-orders') }}"  ><a href="{{url('member-orders')}}"><i class="ur-icon r-m13">&nbsp;</i><span class="v-align-m t-p5">Membership Orders</span></a></li>  
  <li  class="{{ set_active_admin('send-mail') }} profileborder">  <a href="{{url('send-mail')}}"><i class="ii-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Send Member Notifications</span></a></li>
  <li  class="{{ set_active_admin('notifications') }} profileborder">  <a href="{{url('notifications')}}"><i class="ii-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Check Member Notifications</span></a></li>
@endif 
    <li   class="{{ set_active_admin('change-password') }} profileborder"><a href="{{url('change-password')}}"><i class="lk-icon r-m13 v-align-m">&nbsp;</i><span class="v-align-m">Change Password</span></a></li>

