<div class="col-md-4 col-lg-3">
                 <!-- Grand Sponsors -->
                 <div class="col p-3 text-center mb-3 rounded-4 bg_color7">
                   <h5 class="py-2">Grand Sponsors</h5>
                   <div class="col rounded-4 p-3 white_bg">
                     <ul class="list-unstyled mb-0">
                       <div class="swiper SposonrSwiper">
                          <div class="swiper-wrapper our_sposors">
                           <li class="swiper-slide"><img src="assets/images/logo1.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo2.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo3.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo4.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo5.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo6.jpg" alt=""></li>
                          </div>
                          
                        </div>
                     </ul>
                   </div>
                 </div>

                 <!-- Buttons -->

                  <div class="p-0 mb-3 text-center">
                    <a href="#" class="btn py-3 btn-lg d-block rounded-4 btn-dark tuta_btn_Nblue">Become a Member</a>
                  </div>

                  <div class="p-0 mb-3 text-center">
                    <a href="#" class="btn py-3 btn-lg d-block rounded-4 btn-dark tuta_btn_Nblue">Our Patasala</a>
                  </div>

                  <div class="p-0 mb-3 text-center">
                    <a href="#" class="btn py-3 btn-lg d-block rounded-4 btn-dark tuta_btn_Nblue">Our Events</a>
                  </div>

                 <!-- Buttons End -->

                 <!-- Platinum Sponsors -->
                 <div class="col p-3 text-center mb-3 rounded-4 bg_color8">
                   <h5 class="py-2">Platinum Sponsors</h5>
                   <div class="col rounded-4 p-3 white_bg">
                     <ul class="list-unstyled mb-0">
                       <div class="swiper PlatinumSponsors">
                          <div class="swiper-wrapper our_sposors">
                           <li class="swiper-slide"><img src="assets/images/logo1.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo2.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo3.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo4.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo5.jpg" alt=""></li>
                           <li class="swiper-slide"><img src="assets/images/logo6.jpg" alt=""></li>
                          </div>
                          
                        </div>
                     </ul>
                   </div>
                 </div>

                 <!-- Gold Sponsors -->
                 <div class="col p-3 text-center mb-3 rounded-4 bg_color9">
                   <h5>Gold Sponsors</h5>
                   <div class="col rounded-4 p-3 white_bg">
                     <ul class="list-unstyled mb-0">
                       <div class="swiper PlatinumSponsors">
                          <div class="swiper-wrapper our_sposors">
                            <li class="swiper-slide"><img src="assets/images/logo2.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo3.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo1.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo4.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo5.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo6.jpg" alt=""></li>
                          </div>
                          
                        </div>
                     </ul>
                   </div>
                 </div class="py-2">

                 <!-- Silver Sponsors -->
                 <div class="col p-3 text-center mb-3 rounded-4 bg_color10">
                   <h5 class="py-2">Silver Sponsors</h5>
                   <div class="col rounded-4 p-3 white_bg">
                     <ul class="list-unstyled mb-0">
                       <div class="swiper PlatinumSponsors">
                          <div class="swiper-wrapper our_sposors">
                            <li class="swiper-slide"><img src="assets/images/logo5.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo6.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo2.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo3.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo1.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo4.jpg" alt=""></li>
                          </div>
                          
                        </div>
                     </ul>
                   </div>
                 </div>

                 <!-- Yearly Sponsors -->
                 <div class="col p-3 text-center mb-3 rounded-4 bg_color11">
                   <h5 class="py-2">Yearly Sponsors</h5>
                   <div class="col rounded-4 p-3 white_bg">
                     <ul class="list-unstyled mb-0">
                       <div class="swiper PlatinumSponsors">
                          <div class="swiper-wrapper our_sposors">
                            <li class="swiper-slide"><img src="assets/images/logo5.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo6.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo2.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo3.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo1.jpg" alt=""></li>
                            <li class="swiper-slide"><img src="assets/images/logo4.jpg" alt=""></li>
                          </div>
                          
                        </div>
                     </ul>
                   </div>
                 </div>


               </div>
             </div>