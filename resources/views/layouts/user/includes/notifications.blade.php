<div class="clearfix inner-wrapper" style="padding: 10px 20px">
    @if (isset($errors) && $errors->any())
    <div class="notifications alert alert-danger alert-dismissible">
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif

    @if (isset($error) && $error->any())
    <div class="notifications alert alert-danger alert-dismissible">
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>

        @if ($message = $errors->first(0, ':message'))
        {{ $message }}
        @else
        Please check the form below for errors
        @endif
    </div>
    @endif

    @if ($message = Session::get('success'))
    <div class="notifications alert alert-success alert-dismissible   ">
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
        {{ $message }}
    </div>
    @endif
    @if ($message = Session::get('error'))
    <div class="notifications alert alert-danger alert-dismissible ">
        <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
        {{ $message }}
    </div>
    @endif
</div>