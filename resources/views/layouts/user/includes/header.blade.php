    <div class="container-fluid tuta_header d-none d-lg-block">
        <div class="container-xxl">
            <div class="row">
                <div class="col-5"><img src="{{url('assets/images/triangle_telugu.png')}}" alt="{{$app_info->sitename}}"></div>
                <div class="col-2 align-middle text-center"><img src="{{url('assets/images/triangle_logo.png')}}" alt="{{$app_info->sitename}}"></div>
                <div class="col-5 text-end"><img src="{{url('assets/images/triangle_eng.png')}}" alt="{{$app_info->sitename}}"></div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark border-top border-bottom border-white bg_color2 mobile_navbar" aria-label="Eighth navbar tuta_nav">
        <div class="container">
            <div class="d-block d-lg-none mob-logo"><a href="index.html"><img src="{{url('assets/images/triangle_logo_mob.png')}}" alt=""></a></div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
            <div class="collapse navbar-collapse" id="navbarsExample07">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    @foreach($menulinks as $item)
                    @php $subitems=$item->subitems()->where('status',1)->orderBy('position')->get();
                    @endphp
                    <li class="nav-item @if(count($subitems)>0)dropdown @endif"><a @if(count($subitems)>0) class="nav-link dropdown-toggle" id="dropdown07" data-bs-toggle="dropdown" aria-expanded="false" @else class="nav-link  @if($item->page){{ set_active_admin($item->page->slug)}}@endif" aria-current="page" @endif @if($item->page) @if($item->page->target) target="{{$item->page->target}}" @endif @if($item->page->url!='') href="{{url($item->page->url)}}" @else href="{{url(($item->id_path=='')?'/':$item->page->slug)}}" @endif @else href="{{url('/')}}" @endif >{{$item->name}}</a>
                        @if(count($subitems)>0)
                        <ul class="dropdown-menu" aria-labelledby="dropdown07">
                            @foreach($subitems as $subitem)
                            @php $subitems2=$subitem->subitems()->where('status',1)->orderBy('position')->get();
                            @endphp

                            <li><a class="dropdown-item"  @if($subitem->page) @if($subitem->page->url!='') href="{{url($subitem->page->url)}}" target="{{$subitem->page->target}}" @else href="{{url(($subitem->id_path=='')?'/':$subitem->page->slug)}}" @endif @else href="#" onclick="return false;" @endif >{{$subitem->name}}</a>
                                @if(count($subitems2)>0)
                                <ul class="dropdown-menu" aria-labelledby="dropdown07">
                                    @if($subitem->name=='Leadership')
                                    @foreach($committee_list as $subitem2)
                                    <li><a class="dropdown-item" href="{{url('leadership/'.$subitem2->slug)}}">{{$subitem2->name}}</a></li>
                                    @endforeach
                                    @else
                                    @foreach($subitems2 as $subitem2)
                                    <li><a class="dropdown-item" @if($subitem2->page) @if($subitem2->page->url!='') href="{{url($subitem2->page->url)}}" target="{{$subitem->page->target}}" @else href="{{url(($subitem2->id_path=='')?'/':$subitem2->page->slug)}}" @endif @else href="{{url('/')}}" @endif >{{$subitem2->name}}</a></li>
                                    @endforeach
                                    @endif
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                         @endif
                    </li>
                    @endforeach
                    @if (!Sentinel::guest())
                    <li class="nav-item dropdown">  <a class="nav-link dropdown-toggle show" href="#" id="dropdown07" data-bs-toggle="dropdown" aria-expanded="true">Profile</a> 
                    <ul class="dropdown-menu show" aria-labelledby="dropdown07" data-bs-popper="none"> 
                    <li  > <a  class="dropdown-item"  href="#">View Profile</a> </li>
                    <li  > <a  class="dropdown-item"  href="#">Edit Profile</a> </li>
                    <li  > <a  class="dropdown-item"  href="#">Membership Info</a> </li>
                    <li  > <a  class="dropdown-item"  href="#">My Registered Events</a> </li>
                    <li  > <a  class="dropdown-item"  href="#">My Donations</a> </li>
                    <li  > <a  class="dropdown-item"  href="{{url('logout')}}">Logout</a> </li>
                    </ul></li>
                    @else
                    <li class="nav-item"> <a class="nav-link" aria-current="page" href="{{url('login')}}">Login</a> </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>