<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<meta name="generator" content="Hugo 0.88.1">
<meta charset="utf-8">
@php
$metadescription='';
$metatitle=$app_info->sitename;
if(isset($page) && $page->meta_description)$metadescription=$page->meta_description;
if(isset($article) && $article->meta_description)$metadescription=$article->meta_description;
if(isset($event) && $event->meta_description)$metadescription=$event->meta_description;
$metakeywords='';
if(isset($page) && $page->meta_keywords)$metakeywords=$page->meta_keywords;
if(isset($article) && $article->meta_keywords)$metakeywords=$article->meta_keywords;
if(isset($event) && $event->meta_keywords)$metakeywords=$event->meta_keywords;
$stype='Page';
$metaimage=url('/').'/images/tam.png';
if(isset($page))
{
$metatitle=$page->name.' - '.$metatitle;
}
if(isset($article))
{
$stype='Article';
$metatitle=$article->name.' - '.$metatitle;
if($article->image)
$metaimage=url('/').'/uploads/articles/'.$article->image;
}
if(isset($event))
{
$stype='Event';
$metatitle=$event->name.' - '.$metatitle;
if($event->banner_url)
$metaimage=url('/').'/uploads/events/'.$event->banner_url;
}
@endphp
<title>{{$metatitle}}</title>
<meta name="description" content="{{$metadescription}}" />
<meta name="keywords" content="{{$metakeywords}}" />
<meta name="author" content="TUTA" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
<link rel="shortcut icon" href="{{ URL::to('/images/favicon.ico')}}"/>

<meta property="og:type" content="{{$stype}}" />
<meta property="og:title" content="{{$metatitle}}" />
<meta property="og:description" content="{{$metadescription}}" />
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:site_name" content="{{$metatitle}}" />
<meta property="og:image" content="{{$metaimage}}" />
<meta name="twitter:card" content="{{$stype}}" />
<meta name="twitter:title" content="{{$metatitle}}" />
<meta name="twitter:description" content="{{$metakeywords}}" />
<meta property="twitter:image" content="{{$metaimage}}" />

<link href="{{url('assets/css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{url('assets/css/swiper-bundle.min.css')}}" />
<link rel="stylesheet" href="{{url('assets/css/easy-responsive-tabs.css')}}" />
<link href="{{url('assets/css/style.css')}}" rel="stylesheet"> 
<link href="{{url('assets/css/features.css')}}" rel="stylesheet"> 
<link href="{{url('assets/css/responsive.css')}}" rel="stylesheet"> 