<div class="container">
    <div class="row text-white py-3">
        <div class="col-xl-2 col-md-3">
            <h4 class="m-0 text-white">Latest News:</h4>
        </div>
        <div class="col-xl-10 col-md-9 marquee_text">
            <marquee width="100%" direction="left" onMouseOver="this.stop()" onMouseOut="this.start()">
                @if($news_data && count($news_data))

                @foreach($news_data as $data)
                {{$data->name}} &nbsp;&nbsp; | &nbsp;&nbsp;
                @endforeach

                @endif

            </marquee>
        </div>
    </div>
</div>