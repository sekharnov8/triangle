
    <!-- Footer -->
    <footer class="container-fluid footer_bg text-center text-lg-start footer-text">
      <div class="pattern position-relative">
        <div class="container footer_btm_bg py-4 py-md-5">
          <div class="row">
            <div class="col-md-8 offset-md-2 col-lg-3 offset-lg-0 order-lg-3 mb-4 mb-lg-0">
              <h4 class="text-white mb-md-3 mb-lg-3 text-center text-lg-start">Newsletter Signup</h4>
              <form>
                <formgroup class="row gx-2">
                  <div class="col-sm-9 col-lg-12">
                    <input type="text" name="Email" placeholder="Enter your email" class="form-control-md rounded-pill border-0 form-control mb-2">
                  </div>
                  <div class="col-sm-3 col-lg-6">
                    <button class="btn btn-md px-4 btn-warning rounded-pill form-control">Signup</button>
                  </div>
                </formgroup>
                 
              </form>
            </div>
            
            <div class="col-md-12 col-lg-6 order-lg-2 mb-4 mb-lg-0">
              <div class="row">
                <div class="col-lg-6 mb-4 mb-sm-0">
                  <h4 class="text-white mb-md-2 mb-lg-4">Useful Links</h4>
                  <div class="row"> 
                    <div class="col-lg-6">
                      <ul class="list-unstyled">
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">About Us</a></li>
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">Membership</a></li>
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">Board</a></li>
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">Bhavita</a></li>
                      </ul>
                    </div>
                    <div class="col-lg-6">
                      <ul class="list-unstyled">
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">Patasala</a></li>
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">Gallery</a></li>
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">Vanita</a></li>
                        <li class="d-inline-block d-lg-block mb-lg-3 mx-2 mx-lg-0"><a href="#" class="text-decoration-none">Events</a></li>
                      </ul>
                    </div>
                  </div>
                 </div>

                <div class="col-lg-6">
                  <h4 class="text-white mb-md-2 mb-sm-4">Contact</h4>
                  TUTA<br class="d-none d-lg-block">
                  P.O.Box# 191, Morrisville,<br class="d-none d-lg-block">
                  NC-27560, USA<br>
                  Email: <a href="mailto:feedback@tutanc.org">feedback@tutanc.org</a>
                </div>
              </div>
            </div>

            <div class="col-md-6 offset-md-3 col-lg-3 offset-lg-0 order-lg-1">
              <div>
                <img src="assets/images/triangle_logo.png" alt="">
                <p>&copy; 2021 Telugu United Telugu Association (TUTA), NC. </p>
              </div>
              <div class="mt-4 social_icons">
                <ul class="p-0 m-0">
                  <li class="mr-2 list-unstyled"><a href="#" class="fa fa-facebook border border-white rounded-circle text-decoration-none text-center"></a></li>
                  <li class="mr-2 list-unstyled"><a href="#" class="fa fa-twitter border border-white rounded-circle text-decoration-none text-center"></a></li>
                  <li class="mr-2 list-unstyled"><a href="#" class="fa fa-google border border-white rounded-circle text-decoration-none text-center"></a></li>
                  <li class="mr-2 list-unstyled"><a href="#" class="fa fa-linkedin border border-white rounded-circle text-decoration-none text-center"></a></li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
    <!-- Footer End -->