@include('layouts.user.includes.meta')
@yield('css')
</head>
<body class="tuta">
<main>
@include('layouts.user.includes.header')
<div class="container-fluid maroon_bg">
@include('layouts.user.includes.news')
@yield('content')
@include('layouts.user.includes.footer')
</div>
</main>
@include('layouts.user.includes.scripts')
@yield('scripts')
</body>
</html>