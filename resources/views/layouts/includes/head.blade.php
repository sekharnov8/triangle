<head>
    <meta charset="UTF-8">
    <title> TUTA Admin - @yield('content_header_title', 'Your title here') </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}"/>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('backend/css/bootstrap.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="{{asset('backend/css/font-awesome.min.css')}}" rel="stylesheet"  type="text/css"/>
    <!-- Ionicons -->
    <link href="{{asset('backend/css/ionicons.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('plugins/datatable/jquery.dataTables.min.css') }}">
    <link href="{{ asset('backend/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('backend/css/custom.css') }}" rel="stylesheet" type="text/css"/>

    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <!-- iCheck -->
    <link href="{{ asset('plugins/iCheck/minimal/orange.css') }}" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css')}}">
    <link href="{{ asset('backend/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/alertifyjs/css/alertify.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/alertifyjs/css/themes/default.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('plugins/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert/dist/sweetalert.css')}}">

    @yield('css')

</head>
