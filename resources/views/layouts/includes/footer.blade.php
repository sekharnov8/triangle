<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Powered By <a href="http://www.medialabwork.com/" target="_blank">Media Lab Work<b></b></a>.
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2021 @if(date("Y")!='2021')-{{date("Y")}}@endif Triangle United Telugu Association.</strong>
</footer>
