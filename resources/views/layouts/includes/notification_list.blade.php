<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>N</b>A</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>TUTA </b>Admin </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Sentinel::guest())
                    <li><a href="{{ url('login') }}">Login</a></li>
                    <li><a href="{{ url('login') }}">Register</a></li>
                @else
                  <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            @if(Sentinel::getUser()->profile_pic == "")

                            <img src="{{ url('images/user.png')}}" class="user-image" alt="User Image"/>
                                @else

                                    <img src="{{ url(Sentinel::getUser()->profile_pic) }}" class="user-image" alt="Profile img">
                                 @endif
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Sentinel::getUser()->first_name .' '. Sentinel::getUser()->last_name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                @if(Sentinel::getUser()->profile_pic == "")
                                <img src="{{ url('images/user.png')}}" class="img-circle" alt="User Image" />
                                @else

                                    <img src="{{ url(Sentinel::getUser()->profile_pic) }}" class="img-circle" alt="Profile img">
                                @endif
                                <p>
                                    {{ Sentinel::getUser()->first_name .' '. Sentinel::getUser()->last_name}}
                                 </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url('account') }}" target="_blank" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                @endif


            </ul>
        </div>
    </nav>
</header>