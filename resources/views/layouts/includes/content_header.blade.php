<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('content_header_title', 'Page Header here')
        <small>@yield('content_header_description')</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        @yield('breadcrumb_inner_content')
        <li class="active">@yield('content_header_title')</li>
    </ol>
</section>