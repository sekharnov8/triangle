<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Sentinel::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    @if(Sentinel::getUser()->profile_pic == "")
                        <img src="{{asset('assets/images/user.png')}}" class="img-circle" alt="User Image"/>
                    @else
                        <img src="{{asset(Sentinel::getUser()->profile_pic)}}" class="img-circle profile-img"
                             alt="Profile img">
                    @endif
                </div>
                <div class="pull-left info">
                    <p>{{ Sentinel::getUser()->first_name .' '. Sentinel::getUser()->last_name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            @endif

                    <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MENU</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="{{ set_active_admin('admin')}}"><a href="{{ url('admin') }}"><i class='fa fa-home'></i>
                        <span>Dashboard</span></a></li>

                
                <li class="treeview {{ set_active_admin('admin/page-categories') .  set_active_admin('admin/pages')  .  set_active_admin('admin/pages/*') . set_active_admin('admin/status-types')  .  set_active_admin('admin/status-types/*') .
             set_active_admin('admin/sponsors')  .  set_active_admin('admin/sponsors/*') . set_active_admin('admin/sponsor-categories') . set_active_admin('admin/sponsor-categories/*') .
             set_active_admin('admin/news')  .  set_active_admin('admin/news/*') .  set_active_admin('admin/banners')  .  set_active_admin('admin/banners/*') . set_active_admin('admin/greetings')  .  set_active_admin('admin/greetings/*') .
             set_active_admin('admin/page-categories/*') . set_active_admin('admin/articles') . set_active_admin('admin/articles/*') . set_active_admin('admin/buttons') . set_active_admin('admin/buttons/*') . set_active_admin('admin/securityquestions') . set_active_admin('admin/securityquestions/*') .
              set_active_admin('admin/contactprogram') . set_active_admin('admin/contactprogram/*') }} {{ set_active_admin('admin/documenttypes') . set_active_admin('admin/documenttypes/*') .  set_active_admin('admin/status-categories') .  set_active_admin('admin/statyus-types')  }}">
                    <a href="#"><i class="fa fa-folder"></i><span>Content Management</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/page-categories') . set_active_admin('admin/page-categories/*') }}">
                            <a href="{{ url('admin/page-categories') }}"><i class="fa fa-circle-o"></i> Menu Items</a>
                        </li>
                        <li class="{{ set_active_admin('admin/pages') . set_active_admin('admin/pages/*') }}"><a
                                    href="{{ url('admin/pages') }}"><i class="fa fa-circle-o"></i> Menu Pages </a></li>
                        <li class="{{ set_active_admin('admin/banners') . set_active_admin('admin/banners/*') }}"><a
                                    href="{{ url('admin/banners') }}"><i class="fa fa-book"></i>Theme Banners</a></li>
                        <li class="{{ set_active_admin('admin/greetings') . set_active_admin('admin/greetings/*') }}"><a
                                    href="{{ url('admin/greetings') }}"><i class="fa fa-book"></i>Greetings</a></li>
                        <li class="{{ set_active_admin('admin/sponsors') . set_active_admin('admin/sponsors/*') }}"><a
                                    href="{{ url('admin/sponsors') }}"><i class="fa fa-book"></i>Sponsors</a></li>
                        <li class="{{ set_active_admin('admin/sponsor-categories') . set_active_admin('admin/sponsor-categories/*') }}">
                            <a href="{{ url('admin/sponsor-categories') }}"><i class="fa fa-book"></i>Sponsor Categories</a>
                        </li> 
                        <li class="{{ set_active_admin('admin/news') . set_active_admin('admin/news/*') }}"><a
                                    href="{{ url('admin/news') }}"><i class="fa fa-book"></i>Latest News</a></li>
                        <li class="{{ set_active_admin('admin/articles') . set_active_admin('admin/articles/*') }}"><a
                                    href="{{ url('admin/articles') }}"><i class="fa fa-book"></i>Articles</a></li>
                           
                        <li class="{{ set_active_admin('admin/contactprogram') . set_active_admin('admin/contactprogram/*') }}"><a href="{{ url('admin/contactprogram') }}"><i class="fa fa-envelope"></i>Contact Program</a></li>
                             <li class="{{ set_active_admin('admin/status-types') . set_active_admin('admin/status-types/*') }}"><a href="{{ url('admin/status-types') }}"><i class="fa fa-question-circle"></i>Status Types</a></li>
                          <li class="{{ set_active_admin('admin/status-categories') . set_active_admin('admin/status-categories/*') }}"><a href="{{ url('admin/status-categories') }}"><i class="fa fa-question-circle"></i>Status Categories</a></li>
                    </ul>
                </li>

                <li class="treeview {{  set_active_admin('admin/committees')  .  set_active_admin('admin/committees/*') .  set_active_admin('admin/committee-members') . set_active_admin('admin/committee-members/*')}}">
                    <a href="#"><i class="fa fa-folder"></i><span>Committees</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/committees') . set_active_admin('admin/committees/*') }}">
                            <a href="{{ url('admin/committees') }}"><i class="fa fa-circle-o"></i> Committees</a></li>
                        <li class="{{ set_active_admin('admin/committee-members') . set_active_admin('admin/committee-members/*') }}">
                            <a href="{{ url('admin/committee-members') }}"><i class="fa fa-circle-o"></i> Committee
                                Members</a></li>
                    </ul>
                </li>

                <li class="treeview {{ set_active_admin('admin/roles') . set_active_admin('admin/membershiptypes') .  set_active_admin('admin/members')  . set_active_admin('admin/servicetypes')  .  set_active_admin('admin/members/*') .    set_active_admin('admin/membershiptypes/*')  . set_active_admin('admin/memberorders')   }}">
                    <a href="#"><i class="fa fa-folder"></i><span>Members</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/members')  }}"><a href="{{ url('admin/members') }}"><i
                                        class="fa fa-circle-o"></i> Members</a></li>
                        <li class="{{ set_active_admin('admin/memberorders') . set_active_admin('admin/memberorders/*') }}">
                            <a href="{{ url('admin/memberorders') }}"><i class="fa fa-circle-o"></i> Membership
                                Orders</a></li>
                        <li class="{{ set_active_admin('admin/membershiptypes')  }}"><a
                                    href="{{ url('admin/membershiptypes') }}"><i class="fa fa-circle-o"></i> Membership
                                Types</a></li> 
                        <li class="{{ set_active_admin('admin/roles') . set_active_admin('admin/roles/*') }}"><a
                                    href="{{ url('admin/roles') }}"><i class="fa fa-circle-o"></i> Roles</a></li>
                    </ul>
                </li>

                <li class="treeview {{  set_active_admin('admin/events')  .  set_active_admin('admin/events/*') .  set_active_admin('admin/event-categories')}}">
                    <a href="#"><i class="fa fa-folder"></i><span>Events</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/events') . set_active_admin('admin/events/*') }}"><a
                                    href="{{ url('admin/events') }}"><i class="fa fa-circle-o"></i> Events</a></li>
                        <li class="{{ set_active_admin('admin/events/create') }}"><a
                                    href="{{ url('admin/events/create') }}"><i class="fa fa-circle-o"></i> Add Event</a>
                        </li>
                        <li class="{{ set_active_admin('admin/event-categories') . set_active_admin('admin/event-categories/*') }}">
                            <a href="{{ url('admin/event-categories') }}"><i class="fa fa-circle-o"></i> Event
                                Categories</a></li>
                    </ul>
                </li>

                  <li class="treeview {{ set_active_admin('admin/donations') . set_active_admin('admin/donations/*') .  set_active_admin('admin/donation-categories')  . set_active_admin('admin/donation-categories/*')  }}">
                    <a href="#"><i class="fa fa-dollar"></i><span>Donations</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/donations') . set_active_admin('admin/donations/*') }}"><a
                                    href="{{ url('admin/donations') }}"><i class="fa fa-circle-o"></i> Donations</a>
                        </li>
                        <li class="{{ set_active_admin('admin/donation-categories')  }}">
                            <a href="{{ url('admin/donation-categories') }}"><i class="fa fa-circle-o"></i> Donation
                                Categories</a></li>
                    </ul>
                </li> 
 
                <li class="treeview {{ set_active_admin('admin/expenses') .  set_active_admin('admin/expenses-categories')  .  set_active_admin('admin/expenses/*') .    set_active_admin('admin/expenses-categories/*') }}">
                    <a href="#"><i class="fa fa-folder"></i><span>Expenses</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/expenses') . set_active_admin('admin/expenses/*') }}"><a
                                    href="{{ url('admin/expenses') }}"><i class="fa fa-circle-o"></i> Expenses</a>
                        </li>
                        <li class="{{ set_active_admin('admin/expenses-categories') . set_active_admin('admin/expenses-categories/*') }}">
                            <a href="{{ url('admin/expenses-categories') }}"><i class="fa fa-circle-o"></i> Expenses
                                Categories</a></li>
                    </ul>
                </li>
                <li class="treeview {{ set_active_admin('admin/photo-categories') .  set_active_admin('admin/photos')  .  set_active_admin('admin/photos/*') .    set_active_admin('admin/photo-categories/*').  set_active_admin('admin/videos') . set_active_admin('admin/videos/*') .   set_active_admin('admin/video-categories') . set_active_admin('admin/video-categories/*') }}">
                    <a href="#"><i class="fa fa-folder"></i><span>Gallery</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/photos') . set_active_admin('admin/photos/*') }}"><a
                                    href="{{ url('admin/photos') }}"><i class="fa fa-circle-o"></i> Photos List</a></li>
                        <li class="{{ set_active_admin('admin/photo-categories') . set_active_admin('admin/photo-categories/*') }}">
                            <a href="{{ url('admin/photo-categories') }}"><i class="fa fa-circle-o"></i> Photo
                                Categories</a></li>
                        <li class="{{ set_active_admin('admin/videos') . set_active_admin('admin/videos/*') }}"><a
                                    href="{{ url('admin/videos') }}"><i class="fa fa-circle-o"></i> Videos List</a></li>
                        <li class="{{ set_active_admin('admin/video-categories')  }}">
                            <a href="{{ url('admin/video-categories') }}"><i class="fa fa-circle-o"></i> Video
                                Categories</a></li>
                    </ul>
                </li>

                <li class="treeview {{ set_active_admin('admin/contacts') . set_active_admin('admin/subscribers') .  set_active_admin('admin/priceonrequest')  .  set_active_admin('admin/docsdownloads')  .  set_active_admin('admin/docsrequest') }}">
                    <a href="#"><i class="fa fa-folder"></i><span>Forms</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/contacts') }}"><a href="{{ url('admin/contacts') }}"><i
                                        class="fa fa-book"></i>Enquiries</a></li>
                        <li class="{{ set_active_admin('admin/subscribers') }}"><a
                                    href="{{ url('admin/subscribers') }}"><i class="fa fa-book"></i>Subscribers</a></li>

                    </ul>
                </li>
                <li class="treeview {{ set_active_admin('admin/application-settings') .  set_active_admin('admin/currency-codes')  .  set_active_admin('admin/payment-methods') .   set_active_admin('admin/payment-settings')      }}">
                    <a href="#"><i class="fa fa-folder"></i><span>Settings</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ set_active_admin('admin/application-settings') }}"><a
                                    href="{{ url('admin/application-settings') }}"><i class="fa fa-book"></i>Application
                                Settings</a></li>
                        <li class="{{ set_active_admin('admin/payment-methods') }}"><a
                                    href="{{ url('admin/payment-methods') }}"><i class="fa fa-book"></i>Payment Methods</a>
                        </li>
                        <li class="{{ set_active_admin('admin/payment-settings') }}"><a
                                    href="{{ url('admin/payment-settings') }}"><i class="fa fa-book"></i>Paypal/Sendgrid Settings</a>
                        </li>


                    </ul>
                </li>
                <li class="{{ set_active_admin('admin/mail-templates') . set_active_admin('admin/mail-templates/*') }}">
                    <a href="{{ url('admin/mail-templates') }}"><i class="fa fa-envelope"></i>Mail Templates</a></li>
                <li class="{{ set_active_admin('admin/sendmails') . set_active_admin('admin/sendmails/*') }}"><a
                            href="{{ url('admin/sendmails') }}"><i class="fa fa-envelope"></i> Send Mails</a></li>
            </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
