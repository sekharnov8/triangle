<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('backend/js/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('backend/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/js/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/alertifyjs/alertify.js') }}"></script>
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('plugins/sweetalert/dist/sweetalert.js')}}"></script>
  <script src="{{ asset('assets/js/jquery.validate.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
 <script src="{{ asset('assets/js/script.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    $(function () {

        $(".phone").mask("(999) 999-9999");

     $('.datepicker').datepicker({format: 'yyyy-mm-dd',autoclose:true});
     $('.datepickerstart').datepicker({format: 'yyyy-mm-dd', autoclose: true});
     $('.datepickerstart2').datepicker({format: 'yyyy-mm-dd', autoclose: true,startDate: 'd'});
     $('.datepickerend').datepicker({format: 'yyyy-mm-dd', autoclose: true,startDate: '.datepickerstart'});


        $('[data-toggle="tooltip"]').tooltip();
        $('.data-table table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "scrollX": true
        });

     $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-orange',
      radioClass   : 'iradio_minimal-orange'
    })
    });



    $(window).load(function() {
    var $modal = $('#ajax-modal');
        $(document).on('click','[data-toggle="ajax-modal"]', function(e) {
         e.preventDefault();
        var url = $(this).attr('href');
        if (url.indexOf('#') === 0) {
            $('#mainModal').modal('open');
        } else {
            $.get(url, function(data) {
                $modal.modal();
                $modal.html(data);
            });
        }
    });});
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->