@if (session()->has('success'))
    <div class="alert alert-success fadein alertgap">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('error'))
    <div class="alert alert-danger fadein alertgap">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
         {{ session()->get('error') }}
    </div>
@endif
@if (session()->has('error_message'))
    <div class="alert alert-danger fadein alertgap">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
         {{ session()->get('error_message') }}
    </div>
@endif
@if (session()->has('flash_message'))
    <p>{{ session()->get('flash_message') }}</p>
@endif
@if(count($errors->all()) > 0 )
    <div class="validation-errors alert alert-danger fadein alertgap">
        @foreach($errors->all() as $error )
            <li>{{ $error }}</li>
        @endforeach
    </div>
@endif