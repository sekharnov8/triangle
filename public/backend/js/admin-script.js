// JavaScript Document 
function readURL(input,src) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            $('#'+src).attr('src', e.target.result).fadeIn('slow');
        };

        reader.readAsDataURL(input.files[0]);

    }

}
 
 