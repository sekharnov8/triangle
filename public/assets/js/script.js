$(document).ready(function () {

var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween:15,
        scrollbar: {
          el: ".swiper-scrollbar",
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },

        breakpoints: {
           1200: {
                slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:30,
           },
           900: {
               slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:15
           },
           520: {
               slidesPerView: 2,
               slidesPerGroup: 2,
               spaceBetween:15,
           }
        }
      });


var swiper = new Swiper(".GallerySwiper", {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween:15,
        scrollbar: {
          el: ".swiper-scrollbar",
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },

        breakpoints: {
           1200: {
                slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:30,
           },
           900: {
               slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:15
           },
           520: {
               slidesPerView: 2,
               slidesPerGroup: 2,
               spaceBetween:15,
           }
        }
      });

var swiper = new Swiper(".VideoSwiper", {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween:0,
        scrollbar: {
          el: ".swiper-scrollbar",
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },

        breakpoints: {
           1200: {
                slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:30,
           },
           900: {
               slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:15
           },
           520: {
               slidesPerView: 2,
               slidesPerGroup: 2,
               spaceBetween:10,
           }
        }
      });


       
       var swiper = new Swiper(".directors_slider", {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween:0,
        scrollbar: {
          el: ".swiper-scrollbar",
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },

        breakpoints: {
           1200: {
                slidesPerView: 5,
               slidesPerGroup: 5,
               spaceBetween:30,
           },
           760: {
               slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:15
           },
           520: {
               slidesPerView: 2,
               slidesPerGroup: 2,
               spaceBetween:10,
           }
        }
      });

       $('#horizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion           
        width: 'auto', //auto or any width like 600px
        fit: true,   // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        activate: function(event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);
        $name.text($tab.text());
        $info.show();
        }
        });
        $('#verticalTab').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true
        });

        var swiper = new Swiper(".SposonrSwiper", {
            direction: "vertical",
            slidesPerView: 3,
               slidesPerGroup: 3,
               spaceBetween:5,
               loop: true,
               autoplay: {
                  delay: 3000,
                  disableOnInteraction: false,
                },
            pagination: {
              el: ".swiper-pagination",
              clickable: true,
            },
          });

        var swiper = new Swiper(".PlatinumSponsors", {
            direction: "vertical",
            slidesPerView: 2,
               slidesPerGroup: 2,
               spaceBetween:5,
               loop: true,
               autoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },
            pagination: {
              el: ".swiper-pagination",
              clickable: true,
            },
          });


});