<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RemindersController;
use App\Http\Controllers\MembershipController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ActivationsController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ArticleController;

use App\Http\Controllers\Member\MemberAccountController;
use App\Http\Controllers\Member\UserEventsController;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminSettingsController;
use App\Http\Controllers\Admin\AdminArticlesController;
use App\Http\Controllers\Admin\AdminBannerController;
use App\Http\Controllers\Admin\AdminCommitteeMembersController;
use App\Http\Controllers\Admin\AdminCommitteesController;
use App\Http\Controllers\Admin\AdminContactProgramsController;
use App\Http\Controllers\Admin\AdminContactsController;
use App\Http\Controllers\Admin\AdminDonationCategoriesController;
use App\Http\Controllers\Admin\AdminEventCategoriesController;
use App\Http\Controllers\Admin\AdminDonationsController;
use App\Http\Controllers\Admin\AdminEventRegistrationsController;
use App\Http\Controllers\Admin\AdminEventsController;
use App\Http\Controllers\Admin\AdminEventSponsorshipsController;
use App\Http\Controllers\Admin\AdminExpensesCategoriesController;
use App\Http\Controllers\Admin\AdminExpensesController;
use App\Http\Controllers\Admin\AdminGreetingController;
use App\Http\Controllers\Admin\AdminMailTemplatesController;
use App\Http\Controllers\Admin\AdminOrdersController;
use App\Http\Controllers\Admin\AdminMembersController;
use App\Http\Controllers\Admin\AdminMembershiptypesController;
use App\Http\Controllers\Admin\AdminSignificantsController;
use App\Http\Controllers\Admin\AdminPageCategoriesController;
use App\Http\Controllers\Admin\AdminPagesController;
use App\Http\Controllers\Admin\AdminPaymentMethodsController;
use App\Http\Controllers\Admin\AdminPhotosController;
use App\Http\Controllers\Admin\AdminPhotoCategoriesController;
use App\Http\Controllers\Admin\AdminNewsController;
use App\Http\Controllers\Admin\AdminSponsorCategoriesController;
use App\Http\Controllers\Admin\AdminSponsorsController;
use App\Http\Controllers\Admin\AdminSubscribersController;
use App\Http\Controllers\Admin\AdminVideoCategoriesController;
use App\Http\Controllers\Admin\AdminVideosController;
use App\Http\Controllers\Admin\AdminUserController;
use App\Http\Controllers\Admin\AdminStatusCategoriesController;
use App\Http\Controllers\Admin\AdminStatusTypesController;

// Router patterns
Route::pattern('id', '\d+');

Route::get('/', [HomeController::class, 'index'])->name('home');

// Authentication
Route::middleware('guest')->group(function () {
    Route::get('login', [AuthController::class, 'login'])->name('login');
    Route::post('login', [AuthController::class, 'auth'])->name('user.auth');
});

Route::get('logout', [AuthController::class, 'logout']);
Route::post('logout', [AuthController::class, 'logout']);

// Password Reset
Route::get('reset', [RemindersController::class, 'reminder'])->name('user.remember');
Route::post('reset', [RemindersController::class, 'remind'])->name('user.remind');
Route::get('reset/{id}/{code}', [RemindersController::class, 'reset'])->name('user.remember.confirm');
Route::post('reset/{id}/{code}', [RemindersController::class, 'processReset']);

Route::get('membership', [MembershipController::class, 'getRegister']);
Route::post('membership', [MembershipController::class, 'register']);
Route::get('payment', [MembershipController::class, 'getPayment']);
Route::post('payment', [MembershipController::class, 'postPayment'])->name('payment');
Route::get('acknowledgement', [MembershipController::class, 'getAcknowledgement'])->name('acknowledgement');
Route::get('membership-status', [MembershipController::class, 'checkMembership']);
Route::post('membership-status', [MembershipController::class, 'postMembershipStatus']);
Route::post('subscribe', [PagesController::class, 'PostSubscribe']);
Route::get('contact-us', [PagesController::class, 'getContactUs']);
Route::post('contact-us', [PagesController::class, 'contactus']);

Route::get('donation/status', [PaypalController::class, 'getDonationPaymentStatus'])->name('donation.status');
Route::get('upgradepayment/status', [PaypalController::class, 'getUpgradePaymentStatus'])->name('upgradepayment.status');
Route::get('donation/details/{id}', [MemberAccountController::class, 'getDonationDetails']);
Route::get('eventreg/details/{id}', [MemberAccountController::class, 'getEventRegDetails']);

//Event
Route::get('event/{slug}', [EventController::class, 'getEvent']);
Route::get('event/{slug}/registration', [EventController::class, 'getEventRegistration']);
Route::get('event/{slug}/{id}/registration', [EventController::class, 'getEventRegistrationPayment']);
Route::post('event_registration', [EventController::class, 'SaveEventRegistration']);
Route::post('eventpayment', [EventController::class, 'postPayment'])->name('eventpayment');
Route::get('eventpayment/status', [PaypalController::class, 'getEventPaymentStatus'])->name('eventpayment.status');
Route::post('check-regcount', [EventController::class, 'CheckRegCount']);

Route::get('news/{slug}', [NewsController::class, 'getNews']);
Route::get('article/{slug}', [ArticleController::class, 'getArticle']);

//leadership
Route::get('leadership/{slug}', [PagesController::class, 'getLeadership']);
Route::get('donors', [PagesController::class, 'getDonors']);
Route::get('sponsors', [PagesController::class, 'getSponsors']);
Route::get('photos', [PagesController::class, 'getPhotos']);
Route::get('photos/{slug}', [PagesController::class, 'getPictures']);
Route::get('videos', [PagesController::class, 'getVideos']);
Route::get('join-us', [PagesController::class, 'getJoin']);
Route::get('news', [PagesController::class, 'getNews']);
Route::get('blog', [PagesController::class, 'getArticles']);
Route::get('events', [PagesController::class, 'getEvents']);
Route::get('past-events', [PagesController::class, 'getPastEvents']);

Route::middleware(['web', 'XSS'])->group(function () {
    Route::get('payment/status', [PaypalController::class, 'getPaymentStatus'])->name('payment.status');
});

Route::post('search', [PagesController::class, 'postSearch'])->name('search');

// Password Reset
Route::get('reset', [RemindersController::class, 'reminder'])->name('user.remember');
Route::post('reset', [RemindersController::class, 'remind'])->name('user.remind');
Route::get('reset/{id}/{code}', [RemindersController::class, 'reset'])->name('user.remember.confirm');
Route::post('reset/{id}/{code}', [RemindersController::class, 'processReset']);

// Activation
Route::get('activate/{id}/{code}', [ActivationsController::class, 'activate'])->name('user.activate');
Route::post('check-email', [MembershipController::class, 'CheckUserEmail']);
Route::post('check-captcha', [PagesController::class, 'CheckCaptcha']);


// //User Frontend
Route::middleware(['auth'])->group(function () {
    Route::get('account', [MemberAccountController::class, 'getProfile'])->name('account');
    Route::get('membership-info', [MemberAccountController::class, 'getMembershipInfo'])->name('membership-info');
    Route::get('membership-upgrade-acknowledgement', [MemberAccountController::class, 'getUpgradeAcknowledgement'])->name('membership-upgrade-acknowledgement');

    Route::get('change-password', [MemberAccountController::class, 'getChangePassword'])->name('change-password');
    Route::get('user-events', [UserEventsController::class, 'getEvents'])->name('user-events');
    Route::post('change-userevent-status', [UserEventsController::class, 'changeStatus']);

    Route::post('renewal-membership', [MemberAccountController::class, 'postRenewalMembership'])->name('renewal-membership');

    Route::post('uploadProfilePicture', [MemberAccountController::class, 'uploadProfilePicture']);

    Route::resource('user-events', UserEventsController::class);
    Route::get('donation-transactions', [MemberAccountController::class, 'getTransactions'])->name('donation-transactions');
    Route::get('event-transactions', [MemberAccountController::class, 'getEventTransactions'])->name('event-transactions');
    Route::get('event-notification', [UserEventsController::class, 'EventNotification'])->name('event-notification');
    Route::post('send-event-notification', [UserEventsController::class, 'PostSendNotification']);
    Route::get('event-notifications', [UserEventsController::class, 'getNotifications']);
    Route::get('event-notification-status/{id}', [UserEventsController::class, 'getNotificationStatusInfo']);

    Route::get('event-registrations/{slug}', [UserEventsController::class, 'getEventRegistrations'])->name('event_registrations');

    Route::post('updateProfile', [MemberAccountController::class, 'updateProfile'])->name('updateProfile');
    Route::post('changePassword', [MemberAccountController::class, 'changePassword'])->name('changePassword');

    Route::get('send-mail', [MemberAccountController::class, 'sendMail']);
    Route::post('send-mail', [MemberAccountController::class, 'PostSendMail']);
    Route::get('notifications', [MemberAccountController::class, 'getNotifications']);
    Route::get('notification-status/{id}', [MemberAccountController::class, 'getNotificationStatusInfo']);
    Route::get('notification/{id}', [MemberAccountController::class, 'getNotification']);
});


Route::middleware(['auth', 'admin'])->prefix('admin')->group(function () {
    Route::get('dashboard', [AdminController::class, 'index']);
    Route::get('/', [AdminController::class, 'index']);
    Route::resource('page-categories', AdminPageCategoriesController::class);
    Route::resource('photo-categories', AdminPhotoCategoriesController::class);
    Route::resource('photos', AdminPhotosController::class);
    Route::resource('video-categories', AdminVideoCategoriesController::class);
    Route::resource('videos', AdminVideosController::class);
    Route::resource('sponsor-categories', AdminSponsorCategoriesController::class);
    Route::resource('sponsors', AdminSponsorsController::class);
    Route::get('pages/category/{category}', [AdminPagesController::class, 'getByCategory']);
    Route::post('add_link_to_pagecategories', [AdminPageCategoriesController::class, 'postLinkAdd'])->name('add_link_to_pagecategories');
    Route::post('searchPages', [AdminPagesController::class, 'postByCategory'])->name('searchPages');


    Route::resource('contactprogram', AdminContactProgramsController::class);

    Route::resource('pages', AdminPagesController::class);

    Route::resource('news', AdminNewsController::class);
    Route::resource('articles', AdminArticlesController::class);

    Route::resource('events.registrations', AdminEventRegistrationsController::class);
    Route::get('events/data', [AdminEventsController::class, 'filterData'])->name('eventsdata');
    Route::resource('events', AdminEventsController::class);
    Route::resource('event-categories', AdminEventCategoriesController::class);
    Route::resource('event-sponsorships', AdminEventSponsorshipsController::class);

    Route::resource('committees', AdminCommitteesController::class);
    Route::get('committee-members/{committee}', [AdminCommitteeMembersController::class, 'getByCommittee']);
    Route::post('searchCommitteemembers', [AdminCommitteeMembersController::class, 'postByCommittee'])->name('searchCommitteemembers');
    Route::resource('committee-members', AdminCommitteeMembersController::class);

    Route::resource('mail-templates', AdminMailTemplatesController::class);
    Route::get('donations/data', [AdminDonationsController::class, 'filterData'])->name('donationsdata');
    Route::resource('donations', AdminDonationsController::class);
    Route::resource('donation-categories', AdminDonationCategoriesController::class);
    Route::post('admin_donations_actions', [AdminDonationsController::class, 'deleteChecked'])->name('admin_donations_actions');

    Route::resource('subscribers', AdminSubscribersController::class);
    Route::get('admin_subscribers_export', [AdminSubscribersController::class, 'getExcelExport'])->name('admin_subscribers_export');

    Route::get('contacts/data', [AdminContactsController::class, 'filterData'])->name('contactsdata');

    Route::resource('contacts', AdminContactsController::class);
    Route::post('admin_enquiries_actions', [AdminContactsController::class, 'deleteChecked'])->name('admin_enquiries_actions');

    Route::get('resetPasswords', [AdminMembersController::class, 'resetPasswords'])->name('resetPasswords');
    Route::get('members/data', [AdminMembersController::class, 'filterData'])->name('membersdata');
    Route::resource('membershiptypes', AdminMembershiptypesController::class);
    Route::resource('expenses', AdminExpensesController::class);
    Route::resource('expenses-categories', AdminExpensesCategoriesController::class);

    Route::get('memberorders/data', [AdminOrdersController::class, 'filterData'])->name('memberordersdata');
    Route::get('members/{id}/orders', [AdminOrdersController::class, 'memberOrderList'])->name('member_orders');
    Route::resource('memberorders', AdminOrdersController::class);
    Route::post('admin_memberorder_actions', [AdminOrdersController::class, 'deleteChecked'])->name('admin_orders_actions');
    Route::resource('members.significants', AdminSignificantsController::class);
    Route::resource('members', AdminMembersController::class);
    Route::resource('banners', AdminBannerController::class);
    Route::resource('greetings', AdminGreetingController::class);

    Route::get('sendmails', [AdminController::class, 'SendMails']);
    Route::post('send_mail', [AdminController::class, 'PostSendMails']);

    Route::get('members-sendmails', [AdminController::class, 'MembersSendMails']);
    Route::post('members_send_mail', [AdminController::class, 'PostMembersSendMails']);

    Route::get('application-settings', [AdminSettingsController::class, 'getApplicationSettings']);
    Route::post('appinfo', [AdminSettingsController::class, 'PostAppInfo']);
    Route::get('payment-settings', [AdminSettingsController::class, 'getPaymentSettings']);
    Route::post('payment-settings', [AdminSettingsController::class, 'postPaymentSettings']);

    Route::resource('payment-methods', AdminPaymentMethodsController::class);
    Route::resource('status-types', AdminStatusTypesController::class);
    Route::resource('status-categories', AdminStatusCategoriesController::class);

    Route::get('admin_members_sample', [AdminMembersController::class, 'getMembersSampleExcel'])->name('admin_members_sample');
    Route::post('admin_members_import', [AdminMembersController::class, 'postMembersExcelImport'])->name('admin_members_import');

    Route::get('members-export', [AdminMembersController::class, 'getMembersExcelExport'])->name('admin_members_export');
    Route::get('orders-export', [AdminOrdersController::class, 'getOrdersExcelExport'])->name('admin_orders_export');
    Route::get('enquiries-export', [AdminContactsController::class, 'getExcelExport'])->name('admin_enquiries_export');
    Route::get('donations-export', [AdminDonationsController::class, 'getExcelExport'])->name('admin_donations_export');
    Route::get('expenses-export', [AdminExpensesController::class, 'getExcelExport'])->name('admin_expenses_export');

    Route::post('admin_users_import', [AdminUserController::class, 'postUsersExcelImport'])->name('admin_users_import');

    Route::post('get-members-by-committee', [AdminCommitteeMembersController::class, 'PostCommitteeMembers'])->name('get-members-by-committee');
    Route::post('get-user-filter-by-committee', [AdminCommitteeMembersController::class, 'PostUsersList'])->name('get-user-filter-by-committee');

    Route::post('change-pagecategory-status', [AdminPageCategoriesController::class, 'changeStatus']);
    Route::post('change-photocategory-status', [AdminPhotoCategoriesController::class, 'changeStatus']);
    Route::post('change-paymentmethod-status', [AdminPaymentMethodsController::class, 'changeStatus']);
    Route::post('change-page-status', [AdminPagesController::class, 'changeStatus']);
    Route::post('change-membershiptype-status', [AdminMembershiptypesController::class, 'changeStatus']);
    Route::post('change-sponsor-status', [AdminSponsorsController::class, 'changeStatus']);
    Route::post('change-news-status', [AdminNewsController::class, 'changeStatus']);
    Route::post('change-donation-status', [AdminDonationsController::class, 'changeStatus']);
    Route::post('change-banner-status', [AdminBannerController::class, 'changeStatus']);
    Route::post('change-greeting-status', [AdminGreetingController::class, 'changeStatus']);
    Route::post('change-memberorder-status', [AdminOrdersController::class, 'changeStatus'])->name('change-memberorder-status');
    Route::post('change-user-status', [AdminMembersController::class, 'changeStatus']);
    Route::post('change-video-status', [AdminVideosController::class, 'changeStatus']);
    Route::post('change-photo-status', [AdminPhotosController::class, 'changeStatus']);
    Route::post('change-event-status', [AdminEventsController::class, 'changeStatus']);
    Route::post('change-committee-status', [AdminCommitteesController::class, 'changeStatus']);
    Route::post('change-committeemember-status', [AdminCommitteeMembersController::class, 'changeStatus']);
    Route::post('change-mailtemplate-status', [AdminMailTemplatesController::class, 'changeStatus']);
    Route::post('change-article-status', [AdminArticlesController::class, 'changeStatus']);
    Route::post('change-contactprogram-status', [AdminContactProgramsController::class, 'changeStatus']);
    Route::post('DeleteNewsImage', [AdminNewsController::class, 'deleteimage']);
    Route::post('DeleteBannerImage', [AdminBannerController::class, 'deleteimage']);
    Route::post('DeleteEventImage', [AdminEventsController::class, 'deleteimage']);
    Route::post('DeleteSponsorImage', [AdminSponsorsController::class, 'deleteimage']);
    Route::post('DeleteArticleImage', [AdminArticlesController::class, 'deleteimage']);
    Route::post('DeletePhotoImage', [AdminPhotosController::class, 'deleteimage']);
    Route::post('DeleteEventRegtype', [AdminEventsController::class, 'deleteEventRegtype']);
});

Route::middleware(['auth', 'admin'])->prefix('admin')->group(function () {
    Route::resource('roles', RolesController::class);
});

Route::get('{slug}', [PagesController::class, 'getPage']);
