<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AuthorizedController;
use App\Repositories\Member\OrderRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Log;
use Validator;
use URL;
use Redirect;
use Input;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Illuminate\Support\Facades\Cache;
use App\Mail\PaymentSuccessful;
use App\Models\MailTemplate;
use App\Models\Member;
use App\Repositories\Event\EventUserRepositoryInterface;
use App\Repositories\Donation\DonationRepositoryInterface;
use Mail;
use App\Models\Appinfo;

class PaypalController extends AuthorizedController
{

    private $_api_context;

    protected $order;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(OrderRepositoryInterface $order, EventUserRepositoryInterface $eventuser,  DonationRepositoryInterface $donation)
    {
        parent::__construct();

        $this->order = $order;
        $this->eventuser = $eventuser;
        $this->donation = $donation;

        /** setup PayPal api context **/
        $this->appinfo = Appinfo::where('id', 1)->first();

        $settings = array(
            'mode' => $this->appinfo->paypal_mode,
            'http.ConnectionTimeOut' => 2000,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path() . '/logs/paypal.log',
            'log.LogLevel' => 'ERROR'
        );

        if ($this->appinfo->paypal_mode == 'live') {
            $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_client_id, $this->appinfo->paypal_secret));
        } else {
            $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_sandbox_client_id, $this->appinfo->paypal_sandbox_secret));
        }
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
    }

    /**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getPaymentStatus(Request $request)
    {
        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');

        $payment_id = Session::get('paypal_payment_id');
        $reference_no = Session::get('reference_no');

        if (empty($payerId) || empty($token)) {
            return redirect('membership')->withError('Payment failed');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        Session::forget('paypal_payment_id');
        // Session::forget('reg_step');
        // Session::forget('user_id');
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $p_order = $this->order->findOrderByRefNo($reference_no);

        //echo('<pre>');print($p_order);exit;

        if ($result->getState() == 'approved') {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                'transaction_id' => $order->getId(),
                'response_message' => $result,
                'order_date' => date("Y-m-d"),
                'status' => 1,
                'expired_at' => subscription_expired_at()
            ];

            if ($p_order) {
                $this->order->updateOrderDetails($p_order->id, $updateData);

                //send Successful email to  user.
                $user_email = getMemberEmail($p_order->user_id);
                $transaction_id = $order->getId();

                // Mail::to($user_email)->send(new PaymentSuccessful($transaction_id));
                User::whereId($p_order->user_id)->update(['membershiptype_id' => $p_order->membershiptype_id]);

                return redirect('acknowledgement');
            }
        } else {
            $updateData = [
                'response_message' => $result,
                'status' => 0
            ];

            if ($p_order) {
                $this->order->updateOrderDetails($p_order->id, $updateData);
            }
            return redirect('membership')->withErrors("Payment failed");
        }
    }

    public function getUpgradePaymentStatus(Request $request)
    {
        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');

        $payment_id = Session::get('paypal_payment_id');
        $reference_no = Session::get('reference_no');

        if (empty($payerId) || empty($token)) {
            return redirect('membership-info')->withError('Payment failed');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        Session::forget('paypal_payment_id');

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        $p_order = $this->order->findOrderByRefNo($reference_no);

        //echo('<pre>');print($p_order);exit;

        if ($result->getState() == 'approved') {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                'transaction_id' => $order->getId(),
                'response_message' => $result,
                'order_date' => date("Y-m-d"),
                'status' => 1,
                'expired_at' => subscription_expired_at()
            ];

            if ($p_order) {
                $this->order->updateOrderDetails($p_order->id, $updateData);
                  User::whereId($p_order->user_id)->update(['membershiptype_id' => $p_order->membershiptype_id]);

                //send Successful email to  user.
                $user_email = getUserEmail($p_order->user_id);
                $transaction_id = $order->getId();

                // Mail::to($user_email)->send(new PaymentSuccessful($transaction_id));

                return redirect('membership-upgrade-acknowledgement');
            }
        } else {
            $updateData = [
                'response_message' => $result,
                'status' => 0
            ];

            if ($p_order) {
                $this->order->updateOrderDetails($p_order->id, $updateData);
            }
            return redirect('membership-info')->withErrors("Payment failed");
        }
    }

    public function getEventPaymentStatus(Request $request)
    {

        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');
        $payment_id = Session::get('paypal_payment_id');
        $eventuser_id = Session::get('eventuser_id');
        // $event= Session::get('eventuser_id');

        if (empty($payerId) || empty($token)) {
            return redirect()->to('events')->withError('Payment failed');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        Session::forget('paypal_payment_id');

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        $eventuser = $this->eventuser->find($eventuser_id);

        //echo('<pre>');print($p_order);exit;

        if ($result->getState() == 'approved') {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                'transaction_id' => $order->getId(),
                'response_message' => $result,
                'payment_date' => date("Y-m-d"),
                'payment_status' => 1,
            ];



            if ($eventuser) {
                $eventuser->fill($updateData)->save();
                $template = MailTemplate::findOrFail(9);
                $content = $template->template;
                $content = str_replace('[USERNAME]', $eventuser->first_name . ' ' . $eventuser->last_name, $content);
                $content = str_replace('[ApplicationName]', 'TUTA', $content);
                $content = str_replace('[EventName]', $eventuser->event->name, $content);
                $content = str_replace('[siteurl]', url('/') . '/', $content);

                $event_details = "Event Date: " . dateformat($eventuser->event->start_date) . "<br/>Location: " . $eventuser->event->location . ' ' . $eventuser->event->city;
                if ($eventuser->event->state) $event_details .= ', ' . $eventuser->event->state;
                if ($eventuser->event->zipcode) $event_details .= '-' . $eventuser->event->zipcode;
                $event_details .= $eventuser->event->mail_description;


                $event_details = str_replace('src="/public/', 'src="' . url('/') . '/public/', $event_details);



                $content = str_replace('[event_details]', $event_details, $content);

                $subject = "Thank you for Registering to our Event in TUTA";

                // $mailcontent = array('mailbody' =>  $content);  

                // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message) use($event, $subject) {
                //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($eventuser->email)->subject($subject);
                // }); 
                $mailresponse = sendmail_api($subject, $content, $eventuser->email);

                $eventuser = $this->eventuser->find($eventuser_id);

                return view('events.event_success', compact('eventuser'));
            }
        } else {
            $updateData = [
                'response_message' => $result,
                'payment_status' => 0
            ];

            if ($eventuser) {
                $eventuser->fill($updateData)->save();
            }
            return redirect('event/' . $eventuser->event->slug . '/registration')->withErrors("Payment failed");
        }
    }

    public function getDonationPaymentStatus(Request $request)
    {
        $input = $request->input();
        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');

        $payment_id = Session::get('paypal_payment_id');
        $donation_id = Session::get('donation_id');
        $donation = $this->donation->find($donation_id);
        if (empty($payerId) || empty($token)) {
            $this->donation->delete($donation_id);
            return redirect('donation')->withError('Payment failed');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $donation = $this->donation->find($donation_id);
        // print_r($donation_id);
        Session::forget('paypal_payment_id');

        //echo('<pre>');print($p_order);exit;

        if ($result->getState() == 'approved') {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                'transaction_id' => $order->getId(),
                'response_message' => $result,
                'order_date' => date("Y-m-d"),
                'status' => 1
            ];
            // print_r($updateData);
            //  dd($donation);
            if ($donation) {
                $donation->fill($updateData)->save();

                $donation = $this->donation->find($donation_id);

                $template = MailTemplate::findOrFail(1);
                $content = $template->template;
                $subject = $template->subject;
                $content = str_replace('[USERNAME]', $donation->first_name . ' ' . $donation->last_name, $content);
                $content = str_replace('[DonationProgram]', 'Donation', $content);
                $content = str_replace('[FirstName]', $donation->first_name, $content);
                $content = str_replace('[LastName]', $donation->last_name, $content);
                $content = str_replace('[Email]', $donation->email, $content);
                $content = str_replace('[Category]', $donation->category->name, $content);
                $content = str_replace('[adminemail]', 'info@tutanc.org', $content);
                $content = str_replace('[PhoneNo]', $donation->mobile, $content);
                $content = str_replace('[Address]', $donation->address, $content);
                $content = str_replace('[DonationCause]', $donation->donation_cause, $content);
                $content = str_replace('[PaymentStatus]', 'Completed', $content);
                $content = str_replace('[DonationAmount]', $donation->amount, $content);
                $content = str_replace('[TransactionId]', $donation->transaction_id, $content);
                $content = str_replace('[PaymentDate]', dateformat($donation->created_at), $content);
                $content = str_replace('[ApplicationName]', 'TUTA', $content);
                $content = str_replace('[siteurl]', env('siteurl'), $content);

                // $mailcontent = array('mailbody' =>  $content);  

                // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message) use($donation, $subject) {
                //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($donation->email)->subject($subject);
                // });
                sendmail_api($subject, $content, $donation->email);
                return view('services.donation_success', compact('donation'));
            }
        } else {
            // dd($servicedonation);
            $updateData = [
                'response_message' => $result,
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect('/donation')->withErrors("Payment failed");
        }
    }
}
