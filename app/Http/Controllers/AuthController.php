<?php

/**
 * Part of the Sentinel Kickstart application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel Kickstart
 * @version    5.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2017, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace App\Http\Controllers;

use App\Http\Requests\Register;
use App\Http\Requests\Authenticate;
use Illuminate\Support\Facades\Mail;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\Http\Requests\LoginFormRequest;
use Session;
use Illuminate\Http\Request;
use Artisan;

class AuthController extends BaseController
{
    /**
     * Show the form for logging the user in.
     *
     * @return \Illuminate\View\View
     */

    public function login()
    {

        return view('sentinel.login.index');
    }

    /**
     * Handle posting of the form for logging the user in.
     *
     * @param  \App\Http\Requests\Authenticate  $request
     * @return \Illuminate\Http\RedirectResponse
     */


    public function ajaxauth(Authenticate $request)
    {
        try {
            // Get the submited request data
            $input = $request->all();
            $input = array_map('strip_tags', $input);

            // Remember me flag for authentication
            $remember = (bool) array_pull($input, 'remember', false);
            // Authenticate the user
            if (Sentinel::authenticate($input, $remember)) {
                return 'success';
            } else
                $errors = 'Invalid login or password.';
        } catch (NotActivatedException $e) {
            $errors = 'Account is not activated!';
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();

            $errors = "Your account is blocked for {$delay} second(s).";
        }

        return $errors;
    }

    public function auth(Request $request)
    {
        try {
            // Get the submited request data
            $input = $request->all();

            // Remember me flag for authentication
            $remember = (bool) array_pull($input, 'remember', false);

            // Authenticate the user
            if (Sentinel::authenticate($input, $remember)) {
                $user = Sentinel::getUser();
                  if(!$user->is_approved)
                {
                   Sentinel::logout();
                   return redirect('login')->withError('Need approval from admin to login');
                }
                $admin = Sentinel::findRoleByName('Admin');
                $users = Sentinel::findRoleByName('User');

                $pwd = $input['password'];
                if ((!preg_match('/[A-Z]/', $pwd) || (!preg_match('/\d/', $pwd) && !preg_match('/[^a-zA-Z\d]/', $pwd))) && !$user->inRole($admin)) {
                    return redirect('change-password')->withError('Present Password is not secured. Please change your password.');
                }

                if (Session::get('redirect_page') && !$user->inRole($admin))
                    return redirect()->intended(Session::get('redirect_page'));
                $request->session()->put('first_login', 1);
                if ($user->inRole($admin)) {
 
                    return redirect()->intended('/admin');
                } else {
                    return redirect()->intended('/account');
                }
            }

            $errors = 'Invalid login or password.';
        } catch (NotActivatedException $e) {
            $errors = 'Account is not activated!';
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $errors = "Your account is blocked for {$delay} second(s).";
        }
        return redirect()->back()->withError($errors);
    }
 

    /**
     * Logs the user out.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Sentinel::logout();

        Session::forget('user_id');
        Session::forget('reg_step');
        Artisan::call('cache:clear');
        return redirect(route('home'));
    }
 
}
