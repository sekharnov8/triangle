<?php

/**
 * Part of the Sentinel Kickstart application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel Kickstart
 * @version    5.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2017, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace App\Http\Controllers;

use App\Http\Requests\Role\Create;
use App\Http\Requests\Role\Update;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Models\Role;

class RolesController extends AuthorizedController
{
    /**
     * The Sentinel Roles repository.
     *
     * @var \Cartalyst\Sentinel\Roles\RoleRepositoryInterface
     */
    protected $roles;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->roles = Sentinel::getRoleRepository();
    }

    /**
     * Displays a listing of roles.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $roles = $this->roles->createModel()->paginate();

        return view('sentinel/roles/index', compact('roles'));
    }

    /**
     * Shows the form for creating a new role.
     *
     * @return \Illuminate\View\View
     */
    // public function create()
    // {
    //     // Create a new role instance
    //     $role = $this->roles->createModel();

    //     // Show the form
    //     return view('sentinel/roles/form', compact('role'));
    // }

    /**
     * Handles posting of the form for creating a new role.
     *
     * @param  \App\Http\Requests\Role\Create  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
         // Get the submited request data
        $input = $request->all();

        $checkrole=Role::where('name',$input['name'])->orWhere('slug',$input['slug'])->first();
        if($checkrole)
        return redirect(route('roles.index'))->withError('Already Role Exists');

        // Create the role
        $role = $this->roles->createModel()->create($input);

        // Redirect to the roles listing
        return redirect(route('roles.index'))->withSuccess('Added Role Successfully');
    }

    /**
     * Shows the form for updating a role.
     *
     * @param  int  $id
     * @return \Illuminate\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit(Request $request, $id)
    {
        // Get the role object
        if ($role = $this->roles->createModel()->find($id)) {
            if($request->ajax())
            {
            return view('sentinel.roles.edit_modal', compact('role'))->render();
            }
            else {
                abort(404);
            }
         }

        // Redirect to the roles listing
        return redirect(route('roles.index'))->withErrors(
            trans('roles/messages.not_found', compact('id'))
        );
    }

    /**
     * Handles posting of the form for updating a role.
     *
     * @param  \App\Http\Requests\Role\Update  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Update $request, $id)
    {
        // Get the role object
        $role = $this->roles->createModel()->find($id);

        // Get the submited request data
        $input = $request->all();

        // Update the role
        $role->fill($input)->save();

        // Redirect to the roles listing
        return redirect(route('roles.index'))->withSuccess(
            trans('roles/messages.success.update')
        );
    }

    /**
     * Removes the specified role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // Get the role object
        $role = $this->roles->createModel()->find($id);

        // Check if the role exists and there are no users attached
        if ($role && $role->users->count() === 0) {
            // Delete the role
            $role->delete();
        }
    }
}
