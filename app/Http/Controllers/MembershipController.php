<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AuthorizedController;
use App\Http\Requests\Login;
use App\Http\Requests\Register;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\Repositories\Member\OrderRepositoryInterface;
use App\Repositories\Member\MembershiptypeRepositoryInterface;
use App\Repositories\Member\SignificantRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface;
use Log;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Servicetype;
use App\Models\Significant;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Committee;
use App\Models\Membershiptype;
use Input;
use Session;
use Validator;
use URL;
use Redirect;
use App\Models\SecurityQuestion;
use App\Mail\ActivateAccount;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Mail;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Page\PageCategoryRepositoryInterface;
use App\Models\Appinfo;

class MembershipController extends AuthorizedController
{
    protected $mtype;
    protected $user;
    protected $significant;
    protected $activation;

    public function __construct(MembershiptypeRepositoryInterface $mtype, SignificantRepositoryInterface $significant, UserRepositoryInterface $user, OrderRepositoryInterface $order, MailTemplateRepositoryInterface $template,  PageCategoryRepositoryInterface $pagecategory, PageRepositoryInterface $page )
    {
        parent::__construct();
        $this->mtype = $mtype;
        $this->user = $user;
        $this->order = $order;
        $this->significant = $significant;
        $this->template= $template;
        $this->activation = Sentinel::getActivationRepository();
         $this->page = $page;
        $this->pagecategory = $pagecategory;

        $this->appinfo=Appinfo::where('id',1)->first(); 

        $settings = array(
        'mode' => $this->appinfo->paypal_mode,
        'http.ConnectionTimeOut' => 2000,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
        );

        if($this->appinfo->paypal_mode=='live')
        {
        $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_client_id, $this->appinfo->paypal_secret));
        }
        else
        {
        $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_sandbox_client_id, $this->appinfo->paypal_sandbox_secret));
        }
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings); 
    }

    public function checkMembership()
    {
         $page = $this->page->findBySlug('membership-status');
         $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
        return view('pages.check-membership-status',compact('category','catitems','page'));
    }
     public function postMembershipStatus(Request $request)
    {
            $page = $this->page->findBySlug('membership-status');
         $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
        $input=$request->input();
        $type='norecords';
        if($input['email'])
        {
            $users=User::where('email',$input['email'])->select()->first();
            if($users)
            $type='single';
        }
        else
        {
            $users=User::where('first_name','like','%'.$input['firstname'].'%')->where('last_name','like','%'.$input['lastname'].'%')->take(10)->get();
            if(count($users)==1)
            {
                 $users=User::where('first_name','like','%'.$input['firstname'].'%')->where('last_name','like','%'.$input['lastname'].'%')->select()->first();
                $type='single';
            }
            else
            $type='list';
        }
         
         return view('pages.check-membership-status',compact('users','type','category','catitems','page'));
    }

    public function getRegister()
    {
        if(Sentinel::getUser())
        {
            Session::forget('user_id');
            Session::forget('reg_step');
            return redirect('account');
        }
        $servicetypes=Servicetype::where('status',1)->pluck('name','id')->toArray();
         $mtypes=Membershiptype::where('status',1)->orderBy('price')->get();
       $payment_methods=PaymentMethod::where('status',1)->pluck('name','id')->toArray();

            return view('pages.membership.membership', compact('servicetypes','mtypes','payment_methods'));
    }


    public function register(Request $request)
    {
        $user_data=$request->only('email','first_name','last_name','mobile','title','middle_name');
        $member_data=$request->only('gender','home_phone','address','address2','age','dob','city','state','country','zipcode','occupation','membershiptype_id','referred_by','packageamount','donation_amount','memberage');
        $input=$request->input();

         if($request->file('profile_image'))
        {
            $image              = $request->file('profile_image');
            $destinationPath    = 'uploads/users';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename2);
            $user_data=array_merge($user_data,['profile_image'=>$filename2]);
        }
        $password=$request->get('password');
        $user_reg=array_merge($user_data,['password'=>$password]);
        if ($user = Sentinel::register($user_reg)) 
        {
            $usersRole = Sentinel::findRoleByName('User');
            $usersRole->users()->attach($user);

            $code = Activation::create($user)->code;
            $this->activation->complete($user, $code);

            $member_data=array_merge($member_data,['user_id'=>$user->id]);
            $member=$this->member->create($member_data);
            $userinfo=$this->user->find($user->id);
            // $mid = str_pad($user->id, 4, '0', STR_PAD_LEFT);
            // $member_id="TUTA"."$mid";
             $member_id=10000+$user->id;
            $user_data=array_merge($user_data,['member_id'=>$member_id]);
            $user_data=array_merge($user_data,['is_approved'=>0]);
            $userinfo->fill($user_data)->save();
            
            $request->session()->put('reg_step', 1);
            $request->session()->put('user_id', $user->id); 

            if($input['spouse_first_name']!='' ||   $input['spouse_last_name']!='' || $input['spouse_email']!='' || $input['spouse_phone']!='' || $input['spouse_occupation']!='' )
            {
                $significant=array();
                $significant['user_id']=Session::get('user_id');
                $significant['first_name']=$input['spouse_first_name'];
                $significant['last_name']=$input['spouse_last_name'];
                $significant['email']=$input['spouse_email'];
                $significant['phone']=$input['spouse_phone'];
                $significant['skills']=$input['spouse_skills'];
                $significant['occupation']=$input['spouse_occupation'];
                $significant['relationship']='Spouse';
                $this->significant->create($significant);
            }

            if($request->has('child_firstname'))
            {
                foreach($input['child_firstname'] as $key=>$child)
                {
                    if($child!='')
                    {
                        $significant=array();
                        $significant['user_id']=Session::get('user_id');
                        $significant['first_name']=$input['child_firstname'][$key];
                        $significant['last_name']=$input['child_lastname'][$key];
                        $significant['dob']=$input['child_dob'][$key];
                        $significant['relationship']=$input['child_relation'][$key];
                        $significant['skills']=$input['child_skills'][$key];
                        $this->significant->create($significant);
                    }
                }
            }


            $userCurrency = 'USD';
            $membershiptype_id=$member_data['membershiptype_id'];
            $pay_amount =$member_data['packageamount']+$member_data['donation_amount'];
            $input['user_id']=$user->id;
     
            $input['reference_no']=gen_random_string();
            $input['amount']=$pay_amount;
            if($pay_amount==0)
            {
                 $input['status']=1;
                 $input['paymentmethod_id']=0;
            }
            else
            {
                 $input['status']=0;
            }
             
            $is_duplicate=Order::where('user_id',$input['user_id'])->where('membershiptype_id',$input['membershiptype_id'])->where('paymentmethod_id',$input['paymentmethod_id'])->where('amount',$input['amount'])->where('created_at','>',date('Y-m-d'))->first();
           
            if(!$is_duplicate)
            { 
                $order = $this->order->create($input);
            }
            else
            {
                $is_duplicate->fill($input)->save();
                $order=$this->order->find($is_duplicate->id);            
            } 

            if($pay_amount>0)
            {
                if($input['paymentmethod_id']==1) 
                {
                    Session::put('reference_no', $order->reference_no);

                    $payer = new Payer();
                    $payer->setPaymentMethod('paypal');

                    $item_1 = new Item();

                    $item_1->setName('Item 1')
                        ->setCurrency($userCurrency)
                        ->setQuantity(1)
                        ->setPrice($pay_amount);

                    $item_list = new ItemList();
                    $item_list->setItems(array($item_1));

                    $amount = new Amount();
                    $amount->setCurrency($userCurrency)
                        ->setTotal($pay_amount);

                    $transaction = new Transaction();
                    $transaction->setAmount($amount)
                        ->setInvoiceNumber($order->reference_no)
                        ->setItemList($item_list)
                        ->setDescription('Transaction Details');

                    $redirect_urls = new RedirectUrls();
                    $redirect_urls->setReturnUrl(URL::route('payment.status'))
                        ->setCancelUrl(URL::route('payment.status'));
                    $payment = new Payment();
                    $payment->setIntent('Sale')
                        ->setPayer($payer)
                        ->setRedirectUrls($redirect_urls)
                        ->setTransactions(array($transaction));

                    try 
                    {
                        $payment->create($this->_api_context);
                    } 
                    catch (\PayPal\Exception\PayPalConnectionException $ex) 
                    {
                        if (\Config::get('app.debug')) {
                            return redirect('membership')->withError('Connection timeout');
                            /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                            /** $err_data = json_decode($ex->getData(), true); **/
                            /** exit; **/
                        } else {
                            return redirect('membership')->withError('Some error occur, sorry for inconvenient');
                            /** die('Some error occur, sorry for inconvenient'); **/
                        }
                    }
                    //dd($payment);
                    foreach ($payment->getLinks() as $link) {
                        if ($link->getRel() == 'approval_url') 
                        {
                            $redirect_url = $link->getHref();
                            break;
                        }
                    }
                    /** add payment ID to session **/
                    Session::put('paypal_payment_id', $payment->getId());
                    if (isset($redirect_url)) 
                    {
                        /** redirect to paypal **/
                        return redirect()->to($redirect_url);
                    }
                }
                else
                {
                    return redirect('acknowledgement');
                }
            }
            else
            {
                return redirect('acknowledgement');
            }
        } 
        else
        {
             return redirect('membership')->withInput()->withErrors('Failed to register.');
        }

    }
     
      
    public function getAcknowledgement()
    {
      if(!Session::has('user_id'))
        {
            return redirect('membership')->withErrors('No information found');
        }
         $user=$this->user->find(Session::get('user_id'));
         $order=Order::where('user_id',$user->id)->orderBy('id','desc')->first();
         $template=$this->template->findByName('Admin Member Registration');
         if($template)
        {
        $content=$template->template;
        $subject=$template->subject;
        $subject=str_replace('[MembershipType]',$order->membershiptype->name,$subject);
        $subject=str_replace('[MemberName]',$user->first_name.' '.$user->last_name,$subject);
        $content=str_replace('[UserId]',$user->id,$content);
        $content=str_replace('[MembershipType]',$order->membershiptype->name,$content);
        $content=str_replace('[FirstName]',$user->first_name,$content);
        $content=str_replace('[LastName]',$user->last_name,$content);
        $content=str_replace('[Email]',$user->email,$content);
        if($order)
        {
            $paymenttype='';
            if($order->paymentmethod)
                $paymenttype=$order->paymentmethod->name;
            $content=str_replace('[PaymentType]',$paymenttype,$content);
            if($paymenttype=='Check')
            {
                $content=str_replace('[TransactionId]','</td></tr><tr><td>Check Details</td><td>: 
                Bank Name: '.$order->bankname.', Check Date: '.$order->cheque_date.', Check No: '.$order->cheque_no.', Check Handed to '.$order->handedto ,$content); 
            }
            else
            {
                $content=str_replace('[TransactionId]',$order->transaction_id,$content); 
            }
            $content=str_replace('[PaymentDate]',dateformat($order->created_at),$content);
        $content=str_replace('[Amount]',$order->membershiptype->price,$content);
        }
        else
        {
             $content=str_replace('[TransactionId]','',$content); 
            $content=str_replace('[PaymentType]','Free',$content);
            $content=str_replace('[PaymentDate]','',$content);
        $content=str_replace('[Amount]','',$content);
        }

        $content=str_replace('[ApplicationName]','TUTA',$content);
        $content=str_replace('[siteurl]',env('siteurl'),$content);

        // $membership_email=Committee::where('name','Membership')->pluck('email')->first();

        $mailcontent = array('mailbody' =>  $content);   

         sendmail_api($subject, $content, getenv('ADMIN_EMAIL_ID')); 

        }

        $template=$this->template->findByName('Member Registration');
        if($template)
        {
        $content=$template->template;
        $subject=$template->subject;
        $content=str_replace('[UserId]',$user->id,$content);
        $content=str_replace('[FirstName]',$user->first_name,$content);
        $content=str_replace('[LastName]',$user->last_name,$content);
        $content=str_replace('[Email]',$user->email,$content);

        if($order)
        {
            $content=str_replace('[TransactionId]',$order->transaction_id,$content); 
            
            if($order->paymentmethod)
            $content=str_replace('[PaymentType]',$order->paymentmethod->name,$content);
            else
            $content=str_replace('[PaymentType]','',$content);

            $content=str_replace('[PaymentDate]',dateformat($order->created_at),$content);

            if($order->membershiptype)
            $content=str_replace('[Amount]',$order->membershiptype->price,$content);
            else
            $content=str_replace('[Amount]','',$content);

        }
        else
        {
             $content=str_replace('[TransactionId]','',$content); 
            $content=str_replace('[PaymentType]','Free',$content);
            $content=str_replace('[PaymentDate]','',$content);
            $content=str_replace('[Amount]','',$content);
        }        

        $content=str_replace('[ApplicationName]','TUTA',$content);
        $content=str_replace('[siteurl]',env('siteurl'),$content);

        $mailcontent = array('mailbody' =>  $content);  

        // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($user, $subject, $content) {
        //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($user->email)->subject($subject)->setBody($content, 'text/html');
        // }); 

        sendmail_api($subject, $content, $user->email); 

        }
            return view('pages.membership.acknowledgement', compact('user', 'order'));
    }

    public function CheckUserEmail(Request $request)
    {
        $user = User::where('email', $request->get('email'))->get();

        if(count($user) >0){
            if(Sentinel::getUser() && Sentinel::getUser()->email==$request->get('email'))
                return 0;
            else
                return 1;
        }else{
            return 0;
        }
    }

}
