<?php

/**
 * Part of the Sentinel Kickstart application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel Kickstart
 * @version    5.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2017, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Reminder\Reset;
use App\Http\Requests\Reminder\Create;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Models\MailTemplate;

class RemindersController extends BaseController
{
    /**
     * The Sentinel Reminders repository.
     *
     * @var \Cartalyst\Sentinel\Reminders\ReminderRepositoryInterface
     */
    protected $reminder;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->reminder = Sentinel::getReminderRepository();
    }

    /**
     * Show the form for the forgot password.
     *
     * @return \Illuminate\View\View
     */
    public function reminder()
    {
        return view('sentinel/reset/begin');
    }

    /**
     * Handle posting of the form for the forgot password.
     *
     * @param  \App\Http\Requests\Reminder\Create  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remind(Create $request)
    {
        $email = $request->input('email');

        if ($user = Sentinel::findByCredentials(compact('email'))) {
            $reminder = $this->reminder->create($user);

            $code = $reminder->code;

            if ($user) {
                $template = MailTemplate::findOrFail(7);
                $content = $template->template;
                $subject = $template->subject;
                $content = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $content);
                $content = str_replace('[FirstName]', $user->first_name, $content);
                $content = str_replace('[LastName]', $user->last_name, $content);
                $content = str_replace('[adminemail]', 'info@tutanc.org', $content);
                $content = str_replace('[ApplicationName]', 'TUTA', $content);
                $content = str_replace('[siteurl]', env('siteurl'), $content);
                $content = str_replace('[UserEmail]', $user->email, $content);
                $content = str_replace('[code]', env('siteurl') . 'reset/' . $user->getUserId() . '/' . $code, $content);

                $mailcontent = array('mailbody' =>  $content);

                // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($subject, $user) {
                //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($user->email)->subject($subject);
                // }); 
                $response = sendmail_api($subject, $content, $user->email);
            }

            // $u=sendmail_api($subject, $content, $user->email);

            return redirect(route('user.remember'))->withSuccess(
                'An email was sent with instructions on how to reset your password.'
            );
        }
        return redirect(route('user.remember'))->withError(
            'No Account with this Email found in records.'
        );
    }

    /**
     * Show the form for the password reminder confirmation.
     *
     * @return \Illuminate\View\View
     */
    public function reset()
    {
        return view('sentinel/reset/complete');
    }

    /**
     * Handle posting of the form for the password reminder confirmation.
     *
     * @param  \App\Http\Requests\Reminder\Reset  $request
     * @param  int  $id
     * @param  string  $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processReset(Reset $request, $id, $code)
    {
        if (!$user = Sentinel::findById($id)) {
            return redirect()->back()->withInput()->withErrors(
                'The user no longer exists.'
            );
        }

        if (!$this->reminder->complete($user, $code, $request->input('password'))) {
            return redirect(route('user.login'))->withErrors(
                'Invalid or expired reset code.'
            );
        }

        return redirect(route('user.login'))->withSuccess(
            'Password was successfully resetted.'
        );
    }
}
