<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Event\EventRepositoryInterface;
use App\Repositories\Expenses\ExpensesRepositoryInterface;
use App\Repositories\Expenses\ExpensesCategoryRepositoryInterface;
// use Bogardo\Mailgun\Service;
use Illuminate\Http\Request;
use Sentinel;
use Input;
use App\Models\Expenses;
use App\Models\Committee;
use Email;

class ExpensesController extends Controller
{
    protected $expenses;

    public function __construct(ExpensesRepositoryInterface $expenses, EventRepositoryInterface $event, ExpensesCategoryRepositoryInterface $category, UserRepositoryInterface  $user)
    {
        $this->user=$user;
        $this->expenses=$expenses;
        $this->category=$category;
        $this->event=$event;
    }

    public function index()
    {
        $categories = $this->category->getList();        
        $expenses=$this->expenses->getByUserId(Sentinel::getUser()->id);
        return view('protected.member.expenses.index', compact('categories','expenses'));
    }
    public function create()
    {
        $categories = $this->category->getList();
        $events_list = $this->event->getList();
        $committees=Committee::where('status',1)->where('type','standing')->pluck('name','id')->toArray();
        return view('protected.member.expenses.create', compact('categories','events_list','committees'));
    }
    public function store(Request $request)
    {

        if($request->file('attachment'))
        {
            $image              = $request->file('attachment');
            $destinationPath    = 'uploads/expenses';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['receipt'=>$filename]);
        }
 
        $request->merge(['user_id'=>Sentinel::getUser()->id, 'status'=>'Pending']);
        $expenses=$this->expenses->create($request->input());
 
        return redirect('expenses')->withSuccess('Expenses Added successfully');
    }
    public function edit($id)
    {
        $expenses = $this->expenses->find($id);
        $categories = $this->category->getList();
        return view('protected.member.expenses.edit', compact('expenses','categories'));
    }

    public function update(Request $request, $id)
    {
        if($request->file('attachment'))
        {
            $image              = $request->file('attachment');
            $destinationPath    = 'uploads/expenses';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['expenses'=>$filename]);
        }
        $expenses = $this->expenses->find($id);
         $expenses->fill($request->input())->save();
 
        return redirect()->route("expenses")->withSuccess('Updated successfully!!!');
    }

  
    public function destroy($id)
    {
        $expenses = $this->expenses->find($id);
        if($expenses->user_id==Sentinel::getUser()->id)
        {
            $this->expenses->delete($id);
            return 'success';
        }
        else
        {
            return 'error';
        }
    } 
}
