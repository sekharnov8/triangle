<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Repositories\Member\SignificantRepositoryInterface;
use App\Repositories\Member\OrderRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Donation\DonationRepositoryInterface;
use App\Repositories\Event\EventUserRepositoryInterface;
use App\Repositories\Member\MembershiptypeRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\Notification\NotificationStatusRepositoryInterface;
use Illuminate\Http\Request;
use Sentinel;
use Input;
use App\Models\User;
use App\Models\Significant;
use App\Models\Order;
use App\Models\Donation;
use App\Models\EventUser;
use App\Models\Committee;
use App\Models\Notification;
use App\Models\NotificationStatus;
use Validator;
use Hash;
use Session;
use App\Models\PaymentMethod;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use URL;
use Redirect;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface;
use Mail;
use Datatables;
use Log;
use DB;
use App\Repositories\Status\StatusTypeRepositoryInterface;
use Excel;

class MemberAccountController extends Controller
{
    public function __construct(EventUserRepositoryInterface $eventuser, DonationRepositoryInterface $donation, OrderRepositoryInterface $order, UserRepositoryInterface $user, SignificantRepositoryInterface $significant, MembershiptypeRepositoryInterface $mtype, MailTemplateRepositoryInterface $template, NotificationRepositoryInterface $notification, NotificationStatusRepositoryInterface $notificationstatus, StatusTypeRepositoryInterface $statustype)
    {
        $this->user = $user;
        $this->significant = $significant;
        $this->order = $order;
        $this->donation = $donation;
        $this->eventuser = $eventuser;
        $this->mtype = $mtype;
        $this->template = $template;
        $this->notification = $notification;
        $this->notificationstatus = $notificationstatus;
        $this->statustype = $statustype;

        $paypal_conf = \Config::get('paypal');

        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function getProfile()
    {

        $user = Sentinel::getUser();

        if ($user->is_approved == 2) {
            Sentinel::logout();
            Session::put('user_id', $user->id);
            Session::put('reg_step', 2);
            return redirect('membership_choices')->withErrors('Please complete remaining steps');
        }
        if ($user->is_approved == 2) {
            Sentinel::logout();
            return redirect('login')->withErrors('Please correspond to Admin to enable your account.');
        }
        if (!$user->member_id) {
            $user_data = array();
            $userinfo = $this->user->find($user->id);
            $member_id = 10000 + $user->id;
            $user_data = ['member_id' => $member_id];
            $user->fill($user_data)->save();
            $user = Sentinel::getUser();
        }


        if (!$user->is_approved) {
            Sentinel::logout();
            return redirect('login')->withError('Need approval from admin to login');
        }


        $mtypes = $this->mtype->getAllActive();
        $payment_methods = PaymentMethod::where('status', 1)->pluck('name', 'id')->toArray();
        if ($user) {
            $order = Order::where('user_id', $user->id)->where('membershiptype_id', $user->membershiptype_id)->orderBy('id', 'desc')->first();
            $spouse = Significant::where('user_id', $user->id)->where('relationship', 'Spouse')->first();
            $childrens = Significant::where('user_id', $user->id)->where('relationship', '<>', 'Spouse')->get();
        }

        $admin = Sentinel::findRoleByName('Admin');

        if ($user->inRole('users') && !$user->membershiptype_id) {
            Sentinel::logout();
            Session::put('user_id', $user->id);
            Session::put('reg_step', 2);
            return redirect('membership_choices')->withErrors('Please complete remaining steps');
        }


        return view('protected.member.profile', compact('user', 'member', 'spouse', 'childrens', 'order', 'mtypes', 'payment_methods'));
    }

    public function getMembershipInfo()
    {

        $user = Sentinel::getUser();
        $mtypes = $this->mtype->getTypes($user->membershiptype_id);

        if ($user->membership && $user->membership->name == 'Patron Member') {
            $is_renewal = false;
        } else {
            $is_renewal = true;
        }

        $payment_methods = PaymentMethod::where('status', 1)->pluck('name', 'id')->toArray();
        $orders = Order::where('user_id', $user->id)->get();
        if ($member) {
            $order = Order::where('user_id', $user->id)->where('membershiptype_id', $user->membershiptype_id)->orderBy('id', 'desc')->first();
        }

        $admin = Sentinel::findRoleByName('Admin');

        return view('protected.member.membership_info', compact('user',  'order', 'mtypes', 'payment_methods', 'is_renewal', 'orders'));
    }
    public function getTransactions()
    {
        $user = Sentinel::getUser();
        $donations = Donation::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        return view('protected.member.donation-transactions', compact('user', 'donations'));
    }
    public function getEventTransactions()
    {
        $user = Sentinel::getUser();
        $eventusers = EventUser::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        return view('protected.member.event-transactions', compact('user',  'eventusers'));
    }

    public function postRenewalMembership(Request $request)
    {
        $input = $request->input();
        $user = Sentinel::getUser();
        $membershiptype = $this->mtype->find($input['membershiptype_id']);
        $userCurrency = 'USD';
        $pay_amount = $membershiptype->price;
        $input['user_id'] = $user->id;
        $input['membershiptype_id'] = $input['membershiptype_id'];
        $input['status'] = 0;
        $input['reference_no'] = gen_random_string();
        $input['amount'] = $pay_amount;

        $input = array_add($input, 'user_id', $user->id);
        $input = array_add($input, 'amount', $pay_amount);

        $is_upgrade = Order::where('user_id', $user->id)->where('membershiptype_id', $input['membershiptype_id'])->where('amount', $pay_amount)->where('status', 1)->first();

        if ($is_upgrade)
            return redirect('membership-info')->withError('Already Upgraded Membership');

        $is_duplicate = Order::where('user_id', $input['user_id'])->where('membershiptype_id', $input['membershiptype_id'])->where('paymentmethod_id', $input['paymentmethod_id'])->where('status', 0)->where('amount', $input['amount'])->where('created_at', '>', date('Y-m-d'))->first();
        if (!$is_duplicate) {
            $order = $this->order->create($input);
        } else {
            $is_duplicate->fill($input)->save();
            $order = $is_duplicate;
        }


        if ($pay_amount > 0) {
            if ($input['paymentmethod_id'] == 1) {
                Session::put('reference_no', $order->reference_no);

                $payer = new Payer();
                $payer->setPaymentMethod('paypal');

                $item_1 = new Item();

                $item_1->setName('Item 1')
                    ->setCurrency($userCurrency)
                    ->setQuantity(1)
                    ->setPrice($pay_amount);

                $item_list = new ItemList();
                $item_list->setItems(array($item_1));

                $amount = new Amount();
                $amount->setCurrency($userCurrency)
                    ->setTotal($pay_amount);

                $transaction = new Transaction();
                $transaction->setAmount($amount)
                    ->setInvoiceNumber($order->reference_no)
                    ->setItemList($item_list)
                    ->setDescription('Transaction Details');

                $redirect_urls = new RedirectUrls();
                $redirect_urls->setReturnUrl(URL::route('upgradepayment.status'))
                    ->setCancelUrl(URL::route('upgradepayment.status'));
                $payment = new Payment();
                $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));

                try {
                    $payment->create($this->_api_context);
                } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                    if (\Config::get('app.debug')) {
                        return redirect('membership-info')->withError('Connection timeout');
                    } else {
                        return redirect('membership-info')->withError('Some error occur, sorry for inconvenient');
                        /** die('Some error occur, sorry for inconvenient'); **/
                    }
                }
                //dd($payment);
                foreach ($payment->getLinks() as $link) {
                    if ($link->getRel() == 'approval_url') {
                        $redirect_url = $link->getHref();
                        break;
                    }
                }
                /** add payment ID to session **/
                \Session::put('paypal_payment_id', $payment->getId());
                if (isset($redirect_url)) {
                    /** redirect to paypal **/
                    return redirect()->to($redirect_url);
                }
            } else {
                return redirect('membership-upgrade-acknowledgement')->withSuccess('Payment received');
            }
        } else {
            return redirect('membership-upgrade-acknowledgement');
        }
    }

    public function getUpgradeAcknowledgement()
    {
        $user_id = Sentinel::getUser()->id;
        $order = Order::where('member_id', $user->id)->orderBy('id', 'desc')->first();
        $user = $this->user->find($user_id);
        $template = $this->template->findByName('Admin Membership Upgrade Registration');
        if ($template) {
            $content = $template->template;
            $subject = $template->subject;
            $content = str_replace('[MemberId]', $user->member_id, $content);
            $content = str_replace('[FirstName]', $user->first_name, $content);
            $content = str_replace('[LastName]', $user->last_name, $content);
            $content = str_replace('[Email]', $user->email, $content);
            $content = str_replace('[Amount]', '$' . $order->amount, $content);

            $content = str_replace('[TransactionId]', $order->transaction_id, $content);
            if ($order->paymentmethod_id == 1) {
                $content = str_replace('[PaymentType]', PaymentMethod($order->paymentmethod_id), $content);
            } else {
                $content = str_replace('[PaymentType]', 'Check. (Bank Name: ' . $order->bankname . ', Check No: ' . $order->cheque_no . ', Check Date:' . $order->cheque_date . ')', $content);
            }
            $content = str_replace('[PaymentDate]', dateformat($order->created_at), $content);
            $content = str_replace('[MembershipType]', $order->membershiptype->name, $content);

            $content = str_replace('[ApplicationName]', 'TUTA', $content);
            $content = str_replace('[siteurl]', env('siteurl'), $content);


            // $membership_email=Committee::where('name','Membership')->pluck('email')->first();
            // cc([$membership_email])
            // $admin = sendmail_api($subject, $content, "info@tutanc.org");

            $mailcontent = array('mailbody' =>  $content);
            // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($subject) {
            //        $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to(getenv('ADMIN_EMAIL_ID'))->subject($subject);
            //    });

            sendmail_api($subject, $content, getenv('ADMIN_EMAIL_ID'));
        }

        $template = $this->template->findByName('Member Upgrade Registration');
        if ($template) {
            $content = $template->template;
            $subject = $template->subject;
            $subject = str_replace('[MembershipType]', $order->membershiptype->name, $subject);

            $content = str_replace('[MemberId]', $user->member_id, $content);
            $content = str_replace('[FirstName]', $user->first_name, $content);
            $content = str_replace('[LastName]', $user->last_name, $content);
            $content = str_replace('[Email]', $user->email, $content);
            $content = str_replace('[Amount]', $order->amount, $content);
            $content = str_replace('[MembershipType]', $order->membershiptype->name, $content);

            $content = str_replace('[TransactionId]', $order->transaction_id, $content);
            if ($order->paymentmethod_id == 1) {
                $content = str_replace('[PaymentType]', PaymentMethod($order->paymentmethod_id), $content);
            } else {
                $content = str_replace('[PaymentType]', 'Payment Method: Check. (Bank Name: ' . $order->bankname . ', Check No: ' . $order->cheque_no . ', Check Date:' . $order->cheque_date . ')', $content);
            }
            $content = str_replace('[PaymentDate]', dateformat($order->created_at), $content);
            $content = str_replace('[ApplicationName]', 'TUTA', $content);
            $content = str_replace('[siteurl]', env('siteurl'), $content);

            $mailcontent = array('mailbody' =>  $content);
            // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($user, $subject) {
            //        $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($user->email)->subject($subject);
            //    }); 
            sendmail_api($subject, $content, $user->email);
        }
        return view('protected.member.acknowledgement', compact('member', 'user', 'order'));
    }


    public function UploadProfilePicture(Request $request)
    {
        if ($request->ajax()) {
            $id       = Sentinel::getUser()->id;
            $file     = $request->file('profile_image');
            $rules        = array('file' => 'required|image|max:10240');
            $validator     = Validator::make(array('file' => $file), $rules);

            if ($validator->passes()) {
                $destinationPath = 'uploads/users/';
                $filename = strtolower($request->file('profile_image')->getClientOriginalName());
                $filename = time() . '_' . $filename;
                $filename = str_replace(' ', '_', $filename);
                $request->file('profile_image')->move($destinationPath, $filename);
                User::where('id', $id)->update(array('profile_image' => $filename));

                return response(['success' => true, 'message' => 'Profile Picture updated Successfully', 'image' => $filename]);
            } else {
                return response(['success' => false, 'message' => 'File must be an image']);
            }
        }
    }


    public function uploadProfile(Request $request)
    {
        if ($request->ajax()) {
            $id       = Sentinel::getUser()->id;
            $file     = $request->file('profile_image');
            $rules        = array('file' => 'required|image|max:10240');
            $validator     = Validator::make(array('file' => $file), $rules);

            if ($validator->passes()) {
                $destinationPath = 'uploads/users/';
                $filename = strtolower($request->file('profile_image')->getClientOriginalName());
                $filename = time() . '_' . $filename;
                $filename = str_replace(' ', '_', $filename);
                $request->file('profile_image')->move($destinationPath, $filename);
                User::where('id', $id)->update(array('profile_image' => $filename));

                return response(['success' => true, 'message' => 'Profile Picture updated Successfully', 'image' => $filename]);
            } else {
                return response(['success' => false, 'message' => 'File must be an image']);
            }
        }
    }
    public  function  updateProfile(Request $request)
    {
        $input = $request->all();

        $user = $this->user->find(Sentinel::getUser()->id);
        $userdata = $request->only('title', 'first_name', 'last_name', 'middle_name', 'mobile','gender', 'age', 'dob', 'address', 'address2', 'referred_by', 'skills', 'city', 'state', 'designation', 'country', 'zipcode', 'memberage');

        if ($request->file('profileimage')) {
            $destinationPath = 'uploads/users/';
            $filename = strtolower($request->file('profileimage')->getClientOriginalName());
            $filename = time() . '_' . $filename;
            $filename = str_replace(' ', '_', $filename);
            $request->file('profileimage')->move($destinationPath, $filename);
            $userdata['profile_image'] = $filename;
        }

        $user->fill($userdata)->save();
        if ($request->has('spouse_id')) {
            $spouse = Significant::findOrFail($request->get('spouse_id'));
            $significant = array();
            $significant['first_name'] = $input['spouse_first_name'];
            $significant['last_name'] = $input['spouse_last_name'];
            $significant['email'] = $input['spouse_email'];
            $significant['phone'] = $input['spouse_phone'];
            $significant['skills'] = $input['spouse_skills'];
            $significant['occupation'] = $input['spouse_occupation'];
            $spouse->fill($significant)->save();
        } else {
            if ($input['spouse_first_name'] != '' ||  $input['spouse_last_name'] != ''  || $input['spouse_email'] != '' || $input['spouse_phone'] != '' || $input['spouse_skills'] != '' || $input['spouse_occupation'] != '') {
                $significant = array();
                $significant['user_id'] = $user->id;
                $significant['first_name'] = $input['spouse_first_name'];
                $significant['last_name'] = $input['spouse_last_name'];
                $significant['email'] = $input['spouse_email'];
                $significant['phone'] = $input['spouse_phone'];
                $significant['skills'] = $input['spouse_skills'];
                $significant['occupation'] = $input['spouse_occupation'];
                $significant['relationship'] = 'Spouse';
                $this->significant->create($significant);
            }
        }

        if ($request->has('child_firstname')) {
            foreach ($input['child_firstname'] as $key => $child) {
                if (isset($input['child_id'][$key])) {
                    $child = Significant::findOrFail($input['child_id'][$key]);
                    $significant = array();
                    $significant['first_name'] = $input['child_firstname'][$key];
                    $significant['dob'] = $input['child_dob'][$key];
                    $significant['skills'] = $input['child_skills'][$key];
                    $significant['relationship'] = $input['child_relation'][$key];
                    $child->fill($significant)->save();
                } else {
                    $significant = array();
                    $significant['user_id'] = $user->id;
                    if ($input['child_firstname'][$key] != '' || $input['child_dob'][$key] != '' || $input['child_skills'][$key] != '' || $input['child_relation'][$key] != '') {
                        $significant['first_name'] = $input['child_firstname'][$key];
                        $significant['dob'] = $input['child_dob'][$key];
                        $significant['skills'] = $input['child_skills'][$key];
                        $significant['relationship'] = $input['child_relation'][$key];
                        $this->significant->create($significant);
                    }
                }
            }
        }


        return redirect('account')->withSuccess('Profile Updated successfully');
    }

    public function getChangePassword()
    {
        $user = Sentinel::getUser();
        return view('protected.member.change-password', compact('user'));
    }
    public  function  changePassword(Request $request)
    {
        //$userdata=$request->only('oldpwd','pwd');

        $user = $this->user->find(Sentinel::getUser()->id);
        $password = $user->password;
        $check = Hash::check($request->input('oldpwd'), $user->password);

        if ($check) {
            $hashpwd = Hash::make($request->input('pwd'));
            $user->password = $hashpwd;
            $user->last_password_change = date("Y-m-d H:i:s");
            $user->save();

            // $subject="Successfully changed your password in TUTA";
            // $body= view('emails.member.change_password', compact('user'));

            // $u=sendmail_api($subject, $body, $user->email , $category);
            return redirect('account')->withSuccess('Password Updated successfully');
        } else {
            return redirect('account')->withErrors('Password Updation Failed');
        }
    }
    public function getDonationDetails($id)
    {
        $donation = $this->donation->find($id);
        $eventuser = array();
        $memberorder = array();
        if ($donation->eventuser_id)
            $eventuser = $this->eventuser->find($donation->eventuser_id);
        if ($donation->memberorder_id)
            $memberorder = $this->order->find($donation->memberorder_id);
        return view('protected.member.donation_details', compact('donation', 'memberorder', 'eventuser'));
    }

    public function getEventRegDetails($id)
    {
        $eventreg = $this->eventuser->find($id);
        return view('protected.member.events.eventreg_details', compact('eventreg'));
    }
    public function getNotifications()
    {
        if (is_membership_chair()) {
            $notifications = Notification::where('related', 'Membership')->orderBy('id', 'desc')->paginate(10);
            if ($notifications->count() == 0) {
                // return redirect()->to('notifications');
            }

            return view('protected.member.notifications.notifications', compact('notifications'));
        } else {
            return redirect('account')->withError('Notification info not acessable');
        }
    }
    public function getNotificationStatusInfo($id)
    {
        if (is_membership_chair()) {
            $notification = $this->notification->find($id);
            return view('protected.member.notifications.notification_status_info', compact('notification'));
        } else {
            return redirect('account')->withError('Notification info not acessable');
        }
    }
    public function getNotification($id)
    {
        $notification = $this->notification->find($id);
        $notificationstatus = NotificationStatus::where('notification_id', $id)->where('user_id', Sentinel::getUser()->id)->where('status', 0)->first();
        if ($notificationstatus) {
            $notificationstatus->fill(['status' => 1])->save();
            $notificationstatus->update();
            return view('protected.member.notifications.notification_details', compact('notification'));
        } else {
            return redirect('account')->withError('Notification info not available');
        }
    }


    public function sendMail()
    {

        if (!is_membership_chair())
            return redirect()->route("account");
        else {
            $members = User::whereIn('membershiptype_id', [1, 2, 6])->selectRaw("CONCAT(first_name, ' ', last_name,' <',email,'>') as fullName, id")->pluck('fullName', 'id');
            return view('protected.member.send_mail', compact('members'));
        }
    }
    public function getMemberDetails(Request $request, $id)
    {
        $user = $this->user->find($id);
        $order = Order::where('user_id', $user->id)->orderBy('id', 'desc')->first();
        $statustypes = $this->statustype->getByStatusCategoryId(1);
        return view('protected.member.member_details', compact('user', 'order', 'statustypes'));
    }
    public function getMembersList()
    {
        $user = Sentinel::getUser();

        if (is_membership_chair() || is_treasurer()) {
            $mtypes = $this->mtype->getList();
            $statustypes = $this->statustype->getByStatusCategoryId(2);
            return view('protected.member.members', compact('mtypes', 'statustypes'));
        } else {
            return redirect()->route("account");
        }
    }
    public function filterMemberData(Request $request)
    {
        $input = $request->input();
        $start = $input['start'];
        $length = $input['length'];
        $datefrom = $input['datefrom'];
        $dateto = $input['dateto'];
        $is_approved = $input['is_approved'];
        $mtype = $input['mtype'];
        $query = User::leftJoin('members', 'members.user_id', '=', 'users.id')->leftJoin('membershiptypes', 'membershiptypes.id', '=', 'members.membershiptype_id')->leftJoin('role_users', 'role_users.user_id', '=', 'users.id')->leftJoin('status_types', 'status_types.order_no', '=', 'users.is_approved');
        $query = $query->where('status_types.status_category_id', 2);

        $keyword = $input['keyword'];
        if ($keyword) {
            $query->where(function ($query) use ($keyword, $input) {

                foreach ($input['columns'] as $key => $column) {
                    if ($column['searchable'] == 'true') {
                        if ($column['name'] == 'membershiptype') {
                            $query->orWhere('membershiptypes.name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->orWhere('users.' . $column['name'], 'like', '%' . $keyword . '%');
                        }
                    }
                }
            });
        }

        if ($datefrom && $dateto) {
            $query = $query->whereBetween('users.created_at', array($datefrom . ' 00:00:01', $dateto . ' 23:59:59'));
        }
        if ($is_approved != '') {
            $query = $query->where('users.is_approved', $is_approved);
        }

        if ($datefrom == ''  && $dateto == '' && $is_approved == '' &&  $mtype == '' && $keyword == '') {
            $query = $query->whereIn('users.is_approved', [0, 2]);
        }

        if ($mtype != '') {
            $query = $query->where('members.membershiptype_id', $mtype);
        }
        $query = $query->where('members.membershiptype_id', '<>', 10);


        $query = $query->distinct();

        $col = $input['order'][0]['column'];
        $colorder = $input['order'][0]['dir'];
        $columnorder = $input['columns'][$col]['name'];
        if ($columnorder == 'city') {
            $query = $query->orderBy('members.' . $columnorder, $colorder);
        } else {
            $query = $query->orderBy('users.' . $columnorder, $colorder);
        }
        $count = $query->count();
        $query = $query->skip($start)->limit($length);
        $query = $query->select('users.member_id', 'users.id', 'members.id as memberid', 'users.email', 'users.mobile', 'users.first_name', 'users.last_name', 'users.created_at', 'users.last_login', 'membershiptypes.name as membershiptype', 'users.is_approved', 'users.created_at', 'status_types.name as statusname');
        // Log::info($query->toSql());

        $users = $query->get();

        $statustypes = $this->statustype->getByStatusCategoryId(2);

        return datatables()->of($users)->addIndexColumn()->editColumn('status', function ($users) use ($statustypes) {
            if ($users->is_approved == 2) {
                $st = $users->statusname;
                $st = $st . '<br/><a href="' . url('member-sendmail/' . $users->id) . '" class="r-m10" data-toggle="ajax-modal" data-tip="tooltip" title="Send mail to Member"><img src="https://tutanc.org/images/message2.png" width="18" height="18" border="0" alt=""></a>';
            } else {
                $st = '<select name="status" class=" form-control status" data-style="btn-outline-primary pt-0 pb-0" data-id=' . $users->id . ' data-status=' . $users->is_approved . '>';
                foreach ($statustypes as $key => $data) {
                    $st = $st . '<option value="' . $key . '"';
                    if ($users->is_approved == $key) {
                        $st = $st . 'selected';
                    }
                    $st = $st . '>' . $data . '</option>';
                }
                $st = $st . '</select>';
            }
            return $st;
        })->editColumn('updated_at', function ($user) {
            return dateformat($user->updated_at, 1);
        })->editColumn('last_login', function ($user) {
            return dateformat($user->last_login, 1);
        })->editColumn('member_id', function ($user) {
            return '<a href="/member/' . $user->id . '" class="r-m10"  data-toggle="ajax-modal" data-tip="tooltip" title="View Member Details">' . $user->member_id . '</a>';
        })->skipPaging()->setTotalRecords($count)->make(true);
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');

        User::whereId($id)->update(['is_approved' => $status]);

        if ($status == 1) {

            $user = User::where('id', $id)->first();
            $template = $this->template->findByName('Member Approval');
            if ($template) {
                $content = $template->template;
                $subject = $template->subject;
                $subject = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $subject);
                $content = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $content);
                if ($user->membership && $user->membership->price > 0) {
                    $order = Order::where('user_id', $user->id)->where('membershiptype_id', $user->membershiptype_id)->orderBy('id', 'desc')->first();
                    if ($order) {
                        $payment = 'Payment Method: ' . $order->paymentmethod->name;
                        if ($order->paymentmethod->name == 'Paypal') {
                            $payment = $payment . '<br/>Transaction Id: ' . $order->transaction_id . '<br/>';
                        } else {
                            $payment = $payment . '<br/>Bank Name: ' . $order->bankname . '<br/>Cheque No.' . $order->cheque_no . '<br/>Cheque Date:' . $order->cheque_date . '<br/>';
                        }
                    } else {
                        $payment = '';
                    }
                } else {
                    $payment = '';
                }
                $content = str_replace('[MembershipType]', $user->membership->name, $content);
                $content = str_replace('[adminemail]', 'info@tutanc.org', $content);
                $content = str_replace('[MemberId]', $user->member_id, $content);
                $content = str_replace('[Payment]', $payment, $content);
                $content = str_replace('[ApplicationName]', 'TUTA', $content);
                $content = str_replace('[siteurl]', env('siteurl'), $content);

                $user_data = User::findOrFail($id);
                $user = Sentinel::findByCredentials(['login' => $user_data['email']]);

                $mailcontent = array('mailbody' =>  $content);
                // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($user, $subject) {
                //        $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($user->email)->subject($subject);
                //    });  
                sendmail_api($subject, $content, $user->email);
            }
        } else if ($status == 4) {
            $user = User::where('id', $id)->first();
            $this->user->delete($user->id);
            Order::where('user_id', $id)->delete();
            Significant::where('user_id', $id)->delete();
        }
    }


    public function PostSendMail(Request $request)
    {
        $loggedin_user = Sentinel::getUser();
        $template = $this->template->findByName('Common Template');
        $input = $request->input();
        if ($template) {
            $subject = $input['subject'];
            $mailcontent = $template->template;
            $content = str_replace('[Content]', $input['description'], $mailcontent);
            $content = str_replace('[siteurl]', 'https://tutanc.org/', $content);
            $notificationinfo = array();
            $notificationinfo['user_id'] = $loggedin_user->id;
            $notificationinfo['subject'] = $input['subject'];
            $notificationinfo['message'] = $input['description'];
            $notificationinfo['related'] = 'Membership';

            if (count($input['members']) > 0) {
                $notification = $this->notification->create($notificationinfo);

                foreach ($input['members'] as $member) {
                    if ($member) {
                        $user = $this->user->find($member);
                        if ($user) {
                            if ($request->has('login_alert') && $input['login_alert'] == 1) {
                                $statusinfo = array();
                                $statusinfo['notification_id'] = $notification->id;
                                $statusinfo['user_id'] = $member;
                                $statusinfo['status'] = 0;
                                $notificationstatus = $this->notificationstatus->create($statusinfo);
                            }
                            if ($request->has('send_mail') && $input['send_mail'] == 1) {
                                $usercontent = $content;
                                $usercontent = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $usercontent);

                                $mailcontent = array('mailbody' =>  $usercontent);
                                // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($user, $subject) {
                                //        $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($user->email)->subject($subject);
                                //    });  
                                sendmail_api($subject, $usercontent, $user->email);
                            }
                        }
                    }
                }
                return redirect('send-mail')->withSuccess(count($input['members']) . ' Notifications has been sent');
            } else
                return redirect('send-mail')->withError('No members selected to send  Notification');
        } else
            return redirect('send-mail')->withError('Error in sedning Notification');
    }

    public function MemberSendMail($id)
    {
        $user = $this->user->find($id);
        return view('protected.member.member_sendmail', compact('user'));
    }
    public function SendMailToMember(Request $request)
    {
        $input = $request->input();
        $user = $this->user->find($input['user_id']);
        $template = $this->template->findByName('Common Template');
        $input = $request->input();
        if ($template) {
            $subject = $input['subject'];
            $mailcontent = $template->template;
            $content = str_replace('[Content]', $input['description'], $mailcontent);
            $content = str_replace('[siteurl]', 'https://tutanc.org/', $content);

            if ($user) {
                $usercontent = $content;
                $usercontent = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $usercontent);

                $mailcontent = array('mailbody' =>  $usercontent);
                // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($user, $subject) {
                //        $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($user->email)->subject($subject);
                //    });  
                sendmail_api($subject, $usercontent, $user->email);
            }
        }
        return redirect('members')->withSuccess('Mail has been sent to member');
    }

    public function changeOrderStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        $order = $this->order->find($id);
        Order::whereId($id)->update(['status' => $status]);
        if ($status == 1) {
            User::whereId($order->user_id)->update(['membershiptype_id' => $order->membershiptype_id]);
        }
    }



    public function getMemberOrders()
    {
        $mtypes = $this->mtype->getList();
        $paymentmethods = PaymentMethod::pluck('name', 'id')->toArray();
        return view('protected.member.orders.membership_order', compact('mtypes', 'paymentmethods'));
    }

    public function filterMemberOrdersData(Request $request)
    {
        $input = $request->input();
        $start = $input['start'];
        $length = $input['length'];
        $datefrom = $input['datefrom'];
        $dateto = $input['dateto'];
        $status = $input['is_approved'];
        $mtype = $input['mtype'];
        $paymentmethod = $input['paymentmethod'];
        $query = Order::leftJoin('membershiptypes', 'membershiptypes.id', '=', 'orders.membershiptype_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->leftJoin('paymentmethods', 'paymentmethods.id', '=', 'orders.paymentmethod_id');
        $keyword = $input['keyword'];

        if ($keyword) {
            $query->where(function ($query) use ($keyword, $input) {

                foreach ($input['columns'] as $key => $column) {
                    if ($column['searchable'] == 'true') {
                        if ($column['name'] == 'membership') {
                            $query->orWhere('membershiptypes.name', 'like', '%' . $keyword . '%');
                        } else if ($column['name'] == 'email') {
                            $query = $query->orWhere('users.email', $keyword);
                        } else if ($column['name'] == 'paymentmethod') {
                            $query->orWhere('paymentmethods.name', 'like', '%' . $keyword . '%');
                        } else if ($column['name'] == 'member_id') {
                            $query->orWhere('users.member_id', $keyword);
                        } else if ($column['name'] == 'user_name') {
                            $query->orWhere('users.first_name', 'like', '%' . $keyword . '%')->orWhere('users.last_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->orWhere('orders.' . $column['name'], 'like', '%' . $keyword . '%');
                        }
                    }
                }
            });
        }
        if ($datefrom && $dateto) {
            $query = $query->whereBetween('orders.created_at', array($datefrom . ' 00:00:01', $dateto . ' 23:59:59'));
        }
        if ($status != '') {
            $query = $query->where('orders.status', $status);
        }

        if ($mtype != '') {
            $query = $query->where('members.membershiptype_id', $mtype);
        }
        if ($paymentmethod != '') {
            $query = $query->where('orders.paymentmethod_id', $paymentmethod);
        }

        $col = $input['order'][0]['column'];
        $colorder = $input['order'][0]['dir'];
        $columnorder = $input['columns'][$col]['name'];

        if ($columnorder == 'user_name') {
            $query = $query->orderBy('users.first_name', $colorder);
        } else if ($columnorder == 'member_id') {
            $query = $query->orderBy('users.member_id', $colorder);
        } else if ($columnorder == 'membership') {
            $query = $query->orderBy('membershiptypes.name', $colorder);
        } else {
            $query = $query->orderBy('orders.' . $columnorder, $colorder);
        }

        $count = $query->count();
        $query = $query->skip($start)->limit($length);
        $query = $query->select(
            'orders.*',
            DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'),
            'users.member_id',
            'membershiptypes.name as membership',
            'paymentmethods.name as paymentmethod'
        );

        $morders = $query->get();
        Log::info($query->toSql());

        return datatables()->of($morders)
            ->addIndexColumn()
            ->addColumn('paymentstatus', function ($morder) {
                $st = '<select name="status" class=" form-control status" data-style="btn-outline-primary pt-0 pb-0" data-id=' . $morder->id . ' data-status=' . $morder->is_approved . '>
                <option value="0" ';
                if ($morder->status == 0) {
                    $st = $st . 'selected';
                }
                $st = $st . '>Pending</option><option value="1" ';
                if ($morder->status == 1) {
                    $st = $st . 'selected';
                }
                $st = $st . '>Completed</option></select>';
                return $st;
            })
            ->addColumn('actions', function ($morder) {
                $actiondata = '';
                $actiondata = $actiondata . '<a class="btn  btn-sm btn-warning" href="' . url('member-orders/' . $morder->id . '/edit') . '" data-toggle="edit-ajax-modal" title="Edit"><i class="ion-edit"></i></a>';
                return $actiondata;
            })
            ->editColumn('updated_at', function ($morder) {
                return dateformat($morder->updated_at, 1);
            })
            ->editColumn('member_id', function ($morder) {
                return '<a href="/member/' . $morder->id . '" class="r-m10"  data-toggle="ajax-modal" data-tip="tooltip" title="View Member Details">' . $morder->user_id . '</a>';
            })
            ->skipPaging()->escapeColumns([''])->setTotalRecords($count)->make(true);
    }

    public function editMemberOrder($id)
    {
        $morder = Order::with('member', 'user', 'membershiptype', 'PaymentMethod')->whereId($id)->first();
        return view('protected.member.orders.edit_membership_order', compact('morder'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMemberOrder(Request $request, $id)
    {
        $morder = $this->order->find($id);
        $request->merge(['updated_by' => Sentinel::getUser()->id]);
        $morder->fill($request->input())->save();
        return redirect(url("member-orders"))->withSuccess('Updated Member Order successfully!!!');
    }

    public  function getMemberOrdersExcelExport()
    {
        $filter = Input::only('datefrom', 'dateto', 'status', 'paymentmethod', 'mtype');
        $query = Order::with('user', 'membershiptype', 'paymentmethod')
            ->leftJoin('membershiptypes', 'membershiptypes.id', '=', 'orders.membershiptype_id')
            ->leftJoin('paymentmethods', 'paymentmethods.id', '=', 'orders.paymentmethod_id');
        if ($filter['datefrom'] != '' && $filter['dateto'] != '') {
            $query->whereBetween('orders.created_at', array($filter['datefrom'] . ' 00:00:01', $filter['dateto'] . ' 23:59:59'));
        }
        $status = $filter['status'];
        $mtype = $filter['mtype'];
        $paymentmethod = $filter['paymentmethod'];
        if ($status != '') {
            $query->where('orders.status', $status);
        }
        if ($mtype != '') {
            $query->where('users.membershiptype_id', $mtype);
        }
        if ($paymentmethod != '') {
            $query->where('orders.paymentmethod_id', $paymentmethod);
        }
        $query = $query->select(
            'orders.*',
            DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'),
            'users.email',
            'membershiptypes.name as membership',
            'paymentmethods.name as payment_method'
        );
        $morder_requests = $query->get();

        Excel::create('MembershipOrders', function ($excel) use ($morder_requests) {
            $excel->sheet('MembershipOrders', function ($sheet) use ($morder_requests) {
                $sheet->loadView('protected.member.orders.export_membership_order', ['membership_orders' => $morder_requests]);
            });
        })->export('xls');
        return back()->withSuccess('Exported as Excel successfully!');
    }
}
