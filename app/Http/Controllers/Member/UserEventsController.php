<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Event\EventRepositoryInterface;
use App\Repositories\Event\EventCategoryRepositoryInterface;
use App\Repositories\Event\EventRegistrationTypeRepositoryInterface;
use App\Repositories\Event\EventSponsorshipRepositoryInterface;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface;
use App\Repositories\Event\EventUserRepositoryInterface;

// use Bogardo\Mailgun\Service;
use Illuminate\Http\Request;
use Sentinel;
use Input;
use App\Models\Event;
use Illuminate\Support\Str;
use Session;
use App\Models\User;
use App\Models\CommitteeMember;
use App\Models\Committee;
use App\Models\Notification;
use Mail;
use DateTime;

class UserEventsController extends Controller
{
    public function __construct(EventUserRepositoryInterface $eventuser, UserRepositoryInterface $user, EventRepositoryInterface $event, EventCategoryRepositoryInterface $category, EventRegistrationTypeRepositoryInterface $eventregistrationtype, MailTemplateRepositoryInterface $template, EventSponsorshipRepositoryInterface $sponsorship)
    {
        $this->user = $user;
        $this->event = $event;
        $this->category = $category;
        $this->eventregistrationtype = $eventregistrationtype;
        $this->template = $template;
        $this->eventuser = $eventuser;
        $this->sponsorship = $sponsorship;
    }

    public function getEvents()
    {
        // $events=$this->event->getByUserId(Sentinel::getUser()->id);
        $user = Sentinel::getUser();
        if (is_committee_member_any($user->id) || $user->inRole('admin')) {
            $events = Event::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10);
            return view('protected.member.events.events', compact('events'));
        } else {
            return redirect('account')->withError('You are no access to manage events');
        }
    }

    public function getMemberEvents()
    {
        $user = Sentinel::getUser();
        $committee_member = CommitteeMember::where('user_id', $user->id)->where('status', 1)->pluck('committee_id');
        $committee = Committee::whereIn('id', $committee_member)->pluck('id');
        if (count($committee_member)) {
            $events = Event::whereIn('committee_id', $committee)->orderBy('id', 'desc')->paginate(10);
            return view('protected.member.events.memberevents', compact('events'));
        } else {
            return redirect('account')->withError('You are no access to manage events');
        }
    }


    public function EventNotification()
    {
        $user = Sentinel::getUser();
        $events = $this->event->getListByUserId($user->id);
        $events = $events->toArray();

        return view('protected.member.send_mail_event', compact('events'));
    }



    public function create()
    {
        $category_list = $this->category->getList();
        return view('protected.member.events.add_event', compact('category_list'));
    }
    public function store(Request $request)
    {
        $loggedin_user = Sentinel::getUser();
        if (Input::file('banner_url')) {
            $image              = Input::file('banner_url');
            $destinationPath    = 'uploads/events';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            // $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['banner_url' => $filename]);
        }
        $request->merge(['slug' => str_slug($request->input('slug'))]);

        $request->merge(['user_id' => $loggedin_user->id]);
        $request->merge(['status' => 0]);

        if (!$request->get('committees_id'))
            $request->merge(['committees_id' => 10]);

        if ($request->input('start_date') && strpos($request->input('start_date'), '/') !== false) {
            $startdate = DateTime::createFromFormat("m/d/Y", $request->input('start_date'));
            $request->merge(['start_date' => $startdate->format('Y-m-d')]);
        }
        if ($request->input('end_time') != '')
            $request->merge(['end_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('end_time')))]);

        if ($request->input('start_time') != '')
            $request->merge(['start_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('start_time')))]);
        if ($request->input('registration_start_date')  && strpos($request->input('registration_start_date'), '/') !== false) {
            $registration_start_date = DateTime::createFromFormat("m/d/Y", $request->input('registration_start_date'));
            $request->merge(['registration_start_date' => $registration_start_date->format('Y-m-d')]);
        }
        if ($request->input('registration_end_date')  && strpos($request->input('registration_end_date'), '/') !== false) {
            $registration_end_date = DateTime::createFromFormat("m/d/Y", $request->input('registration_end_date'));
            $request->merge(['registration_end_date' => $registration_end_date->format('Y-m-d')]);
        }

        $event = $this->event->create($request->input());

        if ($request->get('is_registration') == 1) {
            $registration_types = $request->only('regtype');

            if (isset($registration_types['regtype']) && count($registration_types['regtype'])) {
                foreach ($registration_types['regtype'] as $key => $type) {
                    if ($request->input('regname_' . $type)) {
                        $regtypes = array();
                        $regtypes['name'] = $request->input('regname_' . $type);
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $request->input('amount_' . $type);
                        $regtypes['reg_count'] = $request->input('reg_count_' . $type);
                        $regtypes['order_no'] = $request->input('order_no_' . $type);
                        $this->eventregistrationtype->create($regtypes);
                    }
                }
            }

            if (isset($input['sponsortype']) && count($input['sponsortype'])) {
                foreach ($input['sponsortype'] as $key => $type) {
                    if ($input['sponsortype_' . $type]) {
                        $regtypes = array();
                        $regtypes['name'] = $input['sponsortype_' . $type];
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $input['sponsoramount_' . $type];
                        $regtypes['order_no'] = $input['sponsororder_no_' . $type];
                        $this->sponsorship->create($regtypes);
                    }
                }
            }
        }

        return redirect('user-events')->withSuccess('Event Added successfully');
    }
    public function edit($id)
    {
        $event = $this->event->find($id);
        $category_list = $this->category->getList();
        return view('protected.member.events.edit_event', compact('event', 'category_list'));
    }

    public function update(Request $request, $id)
    {
        if ($request->file('banner_url')) {
            $image              = $request->file('banner_url');
            $destinationPath    = 'uploads/events';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            // $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['banner_url' => $filename]);
        }
        $event = $this->event->find($id);

        if ($request->input('registration_start_date')  && strpos($request->input('registration_start_date'), '/') !== false) {
            $registration_start_date = DateTime::createFromFormat("m/d/Y", $request->input('registration_start_date'));
            $request->merge(['registration_start_date' => $registration_start_date->format('Y-m-d')]);
        }
        if ($request->input('registration_end_date')  && strpos($request->input('registration_end_date'), '/') !== false) {
            $registration_end_date = DateTime::createFromFormat("m/d/Y", $request->input('registration_end_date'));
            $request->merge(['registration_end_date' => $registration_end_date->format('Y-m-d')]);
        }

        if ($request->input('start_date') && strpos($request->input('start_date'), '/') !== false) {
            $startdate = DateTime::createFromFormat("m/d/Y", $request->input('start_date'));
            $request->merge(['start_date' => $startdate->format('Y-m-d')]);
        }
        if ($request->input('end_time') != '')
            $request->merge(['end_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('end_time')))]);

        if ($request->input('start_time') != '')
            $request->merge(['start_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('start_time')))]);

        $request->merge(['slug' => str_slug($request->input('name'), '-')]);
        $request->merge(['user_id' => Sentinel::getUser()->id]);
        $event->fill($request->input())->save();

        if ($request->get('is_registration') == 1) {
            $registration_types = $request->only('regname', 'memberamount', 'non_memberamount', 'reg_count', 'order_no', 'regid');
            if (isset($registration_types['regname'])) {
                foreach ($registration_types['regname'] as $key => $type) {
                    if ($type != '') {
                        $regtypes = array();
                        $regtypes['name'] = $type;
                        $regtypes['event_id'] = $event->id;
                        $regtypes['memberamount'] = $registration_types['memberamount'][$key];
                        $regtypes['non_memberamount'] = $registration_types['non_memberamount'][$key];
                        $regtypes['reg_count'] = $registration_types['reg_count'][$key];
                        $regtypes['order_no'] = $registration_types['order_no'][$key];

                        if (isset($registration_types['regid'][$key])) {
                            $regtype = $this->eventregistrationtype->find($registration_types['regid'][$key]);
                            $regtype->fill($regtypes)->save();
                        } else {

                            $this->eventregistrationtype->create($regtypes);
                        }
                    }
                }
            }
        }
        return redirect()->route("user-events")->withSuccess('Updated successfully!!!');
    }

    public function deleteEventRegtype(Request $request)
    {
        $id = $request->get('id');
        $this->eventregistrationtype->delete($id);
    }
    public function destroy($id)
    {
        $event = $this->event->find($id);
        if ($event->user_id == Sentinel::getUser()->id) {
            $this->event->delete($id);
            $this->eventuser->deleteByEventId($id);
            return 'success';
        } else {
            return 'error';
        }
    }
    public function getEventRegistrations($slug)
    {
        $event = $this->event->findBySlug($slug);
        return view('protected.member.event_registrations', compact('event'));
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        if ($status == 1) {
            $newstatus = 0;
            Event::whereId($id)->update(['status' => $newstatus]);
        } else {
            $newstatus = 1;
            Event::whereId($id)->update(['status' => $newstatus]);
            $event = $this->event->find($id);
            $loggedin_user = Sentinel::getUser();
        }
        return json_encode('Event Status Updated');
    }

    public function PostSendNotification(Request $request)
    {
        $loggedin_user = Sentinel::getUser();
        $template = $this->template->findByName('Common Template');
        $input = $request->input();
        if (isset($input['event']) &&  $input['event'] != '') {
            $event = $this->event->find($input['event']);

            define('ICAL_FORMAT', 'Ymd\THis');

            $event_location = '';
            if ($event->location_type == 'Online')
                $event_location = $event->location_type;
            else
                $event_location = $event->location . ' ' . $event->city . ', ' . $event->state . ' ' . $event->zipcode;


            $filename = $event->slug . ".ics";
            $event_description = str_replace('\n', ' ', $event->mail_description);
            $event_description = strip_tags($event_description);
            $event_description = str_replace(array("\r\n ", "\t", "\n", "\r\n\t", "&nbsp;"), ' ', $event_description);
            $event_description = preg_replace("/<br>|\n/", "", $event_description);
            $event_description = trim($event_description);
            $event_description = $event_description . " Details Link: " . url('event/' . $event->slug);

            $icalObject = "
BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
BEGIN:VEVENT
UID:$event->name
SUMMARY:$event->name
DTSTART;TZID=America/New_York:" . date(ICAL_FORMAT, strtotime($event->start_date)) . "
DTEND;TZID=America/New_York:" . date(ICAL_FORMAT, strtotime($event->end_date)) . "
LOCATION:$event_location
DESCRIPTION: $event_description
ORGANIZER;TUTA
STATUS:CONFIRMED
SEQUENCE:3
BEGIN:VALARM
TRIGGER:-PT10M
ACTION:DISPLAY
END:VALARM
END:VEVENT
END:VCALENDAR";
            file_put_contents($filename, "\xEF\xBB\xBF" .  $icalObject);
        } else
            $event = null;
        if (isset($input['event_mail'])) {
            $event_mail_description = $event->mail_description;
            $event_mail_description = str_replace('src="/public/', 'src="' . url('/') . '/public/', $event_mail_description);
        } else
            $event_mail_description = '';

        if ($template) {
            $description = $input['description'];
            $description = str_replace('src="/public/', 'src="' . url('/') . '/public/', $description);
            $description = $description . $event_mail_description;

            $subject = $input['subject'];
            $mailcontent = $template->template;
            $content = str_replace('[Content]', $description, $mailcontent);
            $content = str_replace('[siteurl]', url('/').'/', $content);
            $notificationinfo = array();
            $notificationinfo['user_id'] = $loggedin_user->id;
            $notificationinfo['subject'] = $input['subject'];
            $notificationinfo['message'] = $description;
            $notificationinfo['message'] = $description;
            $notificationinfo['related'] = 'Event';
            $notificationinfo['sent_by'] = $loggedin_user->id;

            if (count($input['members']) > 0) {
                $notification = $this->notification->create($notificationinfo);

                // Log::info('Mail of events notifications start');
                $memberlist = array();
                foreach ($input['members'] as $member) {
                    if ($member) {
                        if ($event) {

                            $user = $this->eventuser->find($member);
                        } else
                            $user = $this->user->find($member);
                        if ($user) {
                            if ($input['action'] == 'Send Notification' || $input['action'] == 'Send Email and Notification') {
                                $statusinfo = array();
                                $statusinfo['notification_id'] = $notification->id;
                                if ($event)
                                    $statusinfo['user_id'] = $user->user_id;
                                else
                                    $statusinfo['user_id'] = $user->id;
                                $statusinfo['status'] = 0;
                                $notificationstatus = $this->notificationstatus->create($statusinfo);
                            }
                            $memberlist[] = $user->email;
                        }
                    }
                }

                if ($input['action'] == 'Send Email' || $input['action'] == 'Send Email and Notification' && count($memberlist)) {
                    $appinfo = Appinfo::where('id', 1)->first();

                    $usercontent = $content;
                    $usercontent = str_replace('[UserName]', 'Member', $usercontent);

                    $member_list = array_chunk($memberlist, $appinfo->mail_limit);
                    $mailcontent = array('mailbody' =>  $usercontent);
                    foreach ($member_list as $members) {
                        if ($event) {
                            Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use ($user, $subject, $members, $filename) {
                                $message->from(getenv('ADMIN_EMAIL_ID'), 'TUTA')->bcc($members)->subject($subject);
                                $message->attach($filename, array('mime' => "text/calendar"));
                            });
                        } else {

                            Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use ($user, $subject, $members) {
                                $message->from(getenv('ADMIN_EMAIL_ID'), 'TUTA')->bcc($members)->subject($subject);
                            });
                        }
                    }
                }


                if (strpos(url()->previous(), 'event-notification-committee') !== false)
                    return redirect("event-notification-committee")->withSuccess(count($memberlist) . ' Notifications has been sent');
                else
                    return redirect('event-notification')->withSuccess(count($memberlist) . ' Notifications has been sent');
            } else {
                if (strpos(url()->previous(), 'event-notification-committee') !== false)
                    return redirect("event-notification-committee")->withError('No members selected to send  Notification');
                else
                    return redirect('event-notification')->withError('No members selected to send  Notification');
            }
        } else
            return redirect('event-notification')->withError('Error in sedning Notification');
    }

    public function getNotificationsCommittee()
    {
        $user = Sentinel::getUser();
        if (is_committee_member_any($user->id)) {
            $committee_member = CommitteeMember::where('user_id', $user->id)->where('status', 1)->first();
            $member_ids = CommitteeMember::where('committee_id', $committee_member->committee_id)->where('status', 1)->pluck('user_id');

            $notifications = Notification::where('related', 'Event')->whereIn('sent_by', $member_ids)->orderBy('id', 'desc')->paginate(10);
            return view('protected.member.notifications.event-notifications', compact('notifications'));
        } else {
            return redirect('account')->withError('Notification info not acessable');
        }
    }
    public function getNotificationStatusInfo($id)
    {
        $user = Sentinel::getUser();
        if (is_committee_member_any($user->id)) {
            $notification = $this->notification->find($id);
            return view('protected.member.notifications.notification_status_info', compact('notification'));
        } else {
            return redirect('account')->withError('Notification info not acessable');
        }
    }
}
