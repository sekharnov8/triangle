<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Committee\CommitteeRepositoryInterface;
use App\Repositories\Committee\CommitteeMemberRepositoryInterface;
// use Bogardo\Mailgun\Service;
use Illuminate\Http\Request;
use Sentinel;
use Input;
use Email;

class CommitteesController extends Controller
{
    protected $member;

    public function __construct(CommitteeRepositoryInterface $committee,CommitteeMemberRepositoryInterface $committeemember, UserRepositoryInterface $user)
    {
        $this->user=$user;
        $this->committee=$committee;
        $this->committeemember=$committeemember;
    }

    public function getCommittees()
    {
        $user=Sentinel::getUser();
        $committees=$this->committee->getList();

        $committeemembers=$this->committeemember->getMembersByUser_id($user->id);
    //    dd($committeemembers);
        $roles = Sentinel::getRoleRepository()->pluck('name','id')->toArray();
        return view('protected.member.committees', compact('user','committeemembers','roles','committees'));
    }
}
