<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Subscribers\SubscribersRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\Subscribers;
use App\Exports\SubscribersExport;
use Excel;

class AdminSubscribersController extends Controller {

     public function __construct(SubscribersRepositoryInterface $subscribe)
    {
        $this->subscribe = $subscribe;
    }

    public function index()
    {

        $subscribers = $this->subscribe->getAll();
        return view('protected.admin.subscribers.index', compact('subscribers'));
    }
  
    public function destroy($id)
    {
        $this->subscribe->delete($id);
    }

    public function getExcelExport()
    {
        return app()->make(SubscribersExport::class)->download('subscribers.xlsx');
    }
}

?>