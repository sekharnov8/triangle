<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\MailTemplate;

class AdminMailTemplatesController extends Controller {

    public function __construct(MailTemplateRepositoryInterface $template)
    {
        $this->template = $template;
    }

    public function index()
    {
        $templates = $this->template->getAll();
        return view('protected.admin.mailtemplates.index', compact('templates'));
    }

    public function create()
    {
        return view('protected.admin.mailtemplates.create');
    }

    public function store(Request $request)
    {
        $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
        $this->template->create($request->input());
        return redirect()->route("mail-templates.index")->withSuccess('Mail Template has been added successfully!');
    }

    public function edit($id)
    {
        $template = $this->template->find($id);
        return view('protected.admin.mailtemplates.edit', compact('template'));
    }

    public function update(Request $request, $id)
    {
        $template= $this->template->find($id);
        $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
        // dd($request->input());
        $template->fill($request->input())->save();
        return redirect()->route("mail-templates.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->template->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        MailTemplate::whereId($id)->update(['status' => $status]);
    }
}
