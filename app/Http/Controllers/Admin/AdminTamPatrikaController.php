<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SponsorRequest;
use App\Repositories\Patrika\TamPatrikaRepositoryInterface;
use Input;
use Sentinel;
use App\Http\Requests;
use App\Models\TamPatrika;

class AdminTamPatrikaController extends Controller {

    public function __construct(TamPatrikaRepositoryInterface $patrika)
    {
        $this->patrika = $patrika;
    }

    public function index()
    {
        $patrikas = $this->patrika->getAll();
         return view('protected.admin.patrika.index', compact('patrikas'));
    }

    public function create()
    {
        return view('protected.admin.patrika.create');
    }

    public function store(Request $request)
    {
       if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/patrika';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename2);
            $request->merge(['image'=>$filename2]);
        }
        if($request->file('file'))
        {
            $image              = $request->file('file');
            $destinationPath    = 'uploads/patrika_pdf';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename2);
            $request->merge(['file_url'=>$filename2]);
        }

       $this->patrika->create($request->input());

        return redirect()->route("patrika.index")->withSuccess('Patrika has been added successfully!');
    }

    public function edit($id)
    {
        $patrika = $this->patrika->find($id);
         return view('protected.admin.patrika.edit', compact('patrika'));
    }

    public function update(Request $request, $id)
    {
        $patrika= $this->patrika->find($id);
        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/patrika';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename2);
            $request->merge(['image'=>$filename2]);
        }
        if($request->file('file'))
        {
            $image              = $request->file('file');
            $destinationPath    = 'uploads/patrika_pdf';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename2);
            $request->merge(['file_url'=>$filename2]);
        }
        $patrika->fill($request->input())->save();
        return redirect()->route("patrika.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->patrika->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }
 
	 public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        TamPatrika::whereId($id)->update(['status' => $status]);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        TamPatrika::whereId($id)->update(['image' => '']);
    }
}

?>