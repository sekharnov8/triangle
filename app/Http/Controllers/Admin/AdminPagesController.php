<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Page\PageCategoryRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Page;

class AdminPagesController extends Controller {

    public function __construct(PageRepositoryInterface $page, PageCategoryRepositoryInterface $category)
    {
        $this->page = $page;
        $this->category = $category;
    }
     public function show($id)
    {
        $page = $this->page->find($id);
        $category_list = $this->category->getByParentId(0);
        return view('protected.admin.pages.view', compact('page','category_list'));
    }

    public function index()
    {
        $pages = $this->page->getAll();
        $category_list = $this->category->getByParentId(0);
        return view('protected.admin.pages.index', compact('pages','category_list'));
    }
    public function getByCategory($id)
    {
        $pages = $this->page->getAllByCategoryId($id);
        $category_list = $this->category->getByParentId(0);
        $category=$id;
        return view('protected.admin.pages.index', compact('pages','category_list','category'));
    }
    public function postByCategory(Request $request)
    {
        $category=$request->get('category');
        $pages = $this->page->getAllByCategoryId($category);
        $category_list = $this->category->getByParentId(0);
        return view('protected.admin.pages.index', compact('pages','category_list','category'));
    }

    public function create()
    {
        $maxno=$this->page->getMax();
        $category_list = $this->category->getByParentId(0);
        return view('protected.admin.pages.create', compact('category_list','maxno'));
    }


    public function store(Request $request)
    {

       if($request->input('slug')=='')
       $request->merge(['slug'=>str_slug($request->input('name'), '-')]);

       $request->merge(['updated_by'=>Sentinel::getUser()->id]);

       $this->page->create($request->input());

        return redirect()->route("pages.index")->withSuccess('Page has been added successfully!');
    }

    public function edit($id)
    {
        $page = $this->page->find($id);
        $category_list = $this->category->getByParentId(0);
        return view('protected.admin.pages.edit', compact('page','category_list'));
    }

    public function update(Request $request, $id)
    {
        $page= $this->page->find($id);
       if($request->input('slug')=='')
        $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
         $page->fill($request->input())->save();
        return redirect()->route("pages.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->page->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Page::whereId($id)->update(['status' => $status]);
    }
}

?>