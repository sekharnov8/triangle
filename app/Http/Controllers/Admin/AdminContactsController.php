<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactRepositoryInterface;
use App\Repositories\Contact\ContactProgramRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Exports\EnquiriesExport;
use App\Models\Contact;
use Log;
use Datatables;
use Excel;
use DB;

class AdminContactsController extends Controller
{

    public function __construct(ContactRepositoryInterface $contact, ContactProgramRepositoryInterface $contactprogram)
    {
        $this->contact = $contact;
        $this->contactprogram = $contactprogram;
    }

    public function index()
    {
        $programs = $this->contactprogram->getList();
        return view('protected.admin.contacts.index', compact('programs'));
    }
    public function show($id)
    {
        $contact = $this->contact->find($id);
        return view('protected.admin.contacts.view_modal', compact('contact'));
    }
    public function filterData(Request $request)
    {
        $input = $request->input();
        $start = $input['start'];
        $length = $input['length'];
        $datefrom = $input['datefrom'];
        $dateto = $input['dateto'];
        $program_id = $input['program_id'];
        $query = Contact::leftJoin('contactprograms', 'contactprograms.id', '=', 'enquiries.program');
        $keyword = $input['keyword'];
        if ($keyword) {
            $query->where(function ($query) use ($keyword, $input) {

                foreach ($input['columns'] as $key => $column) {
                    if ($column['searchable'] == 'true') {
                        if ($column['name'] == 'program' || $column['name'] == 'program_email') {
                            $query = $query->orWhere('contactprograms.name', 'like', '%' . $keyword . '%');
                            $query = $query->orWhere('contactprograms.email', 'like', '%' . $keyword . '%');
                        } else {
                            $query = $query->orWhere('enquiries.' . $column['name'], 'like', '%' . $keyword . '%');
                        }
                    }
                }
            });
        }

        if ($datefrom && $dateto) {
            $query = $query->whereBetween('enquiries.created_at', array($datefrom . ' 00:00:01', $dateto . ' 23:59:59'));
        }
        if ($program_id != '') {
            $query = $query->where('enquiries.program', $program_id);
        }

        $col = $input['order'][0]['column'];
        $colorder = $input['order'][0]['dir'];
        $columnorder = $input['columns'][$col]['name'];
        if ($columnorder == 'program') {
            $query = $query->orderBy('contactprograms.name', $colorder);
        } else {
            $query = $query->orderBy('enquiries.' . $columnorder, $colorder);
        }
        $count = $query->count();
        $query = $query->skip($start)->limit($length);
        $query = $query->select('enquiries.*', DB::raw('CONCAT(contactprograms.name, "<br/>", contactprograms.email) AS program'));

        // Log::info($query->toSql());
        // <a class="btn  btn-sm btn-info" href="'.url('admin/donations/'.$donation->id).'" data-toggle="tooltip" title="View"><i class="ion-eye"></i></a>&nbsp;
        $contacts = $query->get();

        return datatables()->of($contacts)->addIndexColumn()->addColumn('cb', function ($contacts) {
            $st = '<input name="checkall[]" type="checkbox" class="checkall" value="' . $contacts->id . '">';
            return $st;
        })->addColumn('actions', function ($contacts) {
            return '<a href="javascript: void(0);" data-id="' . $contacts->id . '" class="btn view btn-sm btn-info" data-toggle="tooltip" title="View"><i class="ion-eye"></i></a>&nbsp;<a href="javascript:void(0);"   class="btn btn-sm btn-danger delete  id_' . $contacts->id . '" data-id=' . $contacts->id . ' data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>';
        })->editColumn('created_at', function ($contacts) {
            return dateformat($contacts->created_at, 1);
        })->skipPaging()->escapeColumns([''])->setTotalRecords($count)->make(true);
    }
    public function destroy($id)
    {
        $this->contact->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Contact::whereId($id)->update(['status' => $status]);
    }
    public function getExcelExport()
    {
        // return Excel::download(new EnquiriesExport, 'enquiries.xlsx');

        return app()->make(EnquiriesExport::class)->download('enquiries.xlsx');
    }

    public function deleteChecked(Request $request)
    {
        $ids = $request->get('checkall');
        $count = count($ids);
        if ($count) {
            Contact::whereIn('id', $ids)->update(array('deleted_at' => date('y-m-d H:i:s')));
            return redirect()->route("contacts.index")->withSuccess('Deleted ' . $count . ' selected enquiries successfully!!!');
        } else {
            return redirect()->route("contacts.index")->withError('No checkboxes selected to delete!!!');
        }
    }
}
