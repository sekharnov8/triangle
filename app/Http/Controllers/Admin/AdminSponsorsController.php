<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SponsorRequest;
use App\Repositories\Sponsor\SponsorRepositoryInterface;
use App\Repositories\Sponsor\SponsorCategoryRepositoryInterface;
use Input;
use Sentinel;
use App\Http\Requests;
use App\Models\Sponsor;

class AdminSponsorsController extends Controller {

    public function __construct(SponsorRepositoryInterface $sponsor, SponsorCategoryRepositoryInterface $category)
    {
        $this->sponsor = $sponsor;
        $this->category = $category;
    }

    public function index()
    {
        $sponsors = $this->sponsor->getAll();
        return view('protected.admin.sponsors.index', compact('sponsors'));
    }

    public function create()
    {
        $maxno=$this->sponsor->getMax();
        $categories = $this->category->getList();
        return view('protected.admin.sponsors.create', compact('maxno','categories'));
    }

    public function store(Request $request)
    {

       $request->merge(['updated_by'=>Sentinel::getUser()->id]);

       if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/sponsors';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['logo_url'=>$filename]);
        }

       $this->sponsor->create($request->input());

        return redirect()->route("sponsors.index")->withSuccess('Sponsor has been added successfully!');
    }

    public function edit($id)
    {
        $sponsor = $this->sponsor->find($id);
        $categories = $this->category->getList();
        return view('protected.admin.sponsors.edit', compact('sponsor','categories'));
    }

    public function update(Request $request, $id)
    {

        $sponsor= $this->sponsor->find($id);
        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/sponsors';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['logo_url'=>$filename]);
        }
        $sponsor->fill($request->input())->save();
        return redirect()->route("sponsors.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->sponsor->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }

    public function PostSponsorCategories(Request $request)
    {
         $categories = $this->category->getList();
        return $categories;
    }
	 public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Sponsor::whereId($id)->update(['status' => $status]);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        Sponsor::whereId($id)->update(['logo_url' => '']);
    }
}

?>