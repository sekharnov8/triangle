<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Member\SignificantRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Significant;
use App\Models\Member;
class AdminSignificantsController extends Controller {

    public function __construct(SignificantRepositoryInterface $msignificant, UserRepositoryInterface $user)
    {
        $this->msignificant = $msignificant;
        $this->user = $user;
    }

    public function index($id)
    {
        $user=$this->user->find($id);
        $msignificants = $this->msignificant->getByMemberId($id);
        // dd($msignificants);
        return view('protected.admin.membersignificants.index', compact('msignificants','user'));
    }

    public function create($member)
    {

    }
    public function store($user, Request $request)
    {

        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
        $request->merge(['user_id'=>$user]);
        $this->msignificant->create($request->input());

        return redirect('admin/members/'.$user.'/significants')->withSuccess('Significant  added successfully...');
    }

    public function edit($user, Request $request, $id)
    {
        $msignificant = $this->msignificant->find($id);
        if($request->ajax()){
            return view('protected.admin.membersignificants.edit_modal', compact('msignificant','user'))->render();
        }else {
              abort(404);
        }
    }

    public function update($user, Request $request, $id)
    {
        $msignificant= $this->msignificant->find($id);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
        $msignificant->fill($request->input())->save();
        return redirect('admin/members/'.$user.'/significants')->withSuccess('Updated successfully!!!');
    }

    public function destroy($user, $id)
    {
        $this->msignificant->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Significant::whereId($id)->update(['status' => $status]);
    }
}
?>