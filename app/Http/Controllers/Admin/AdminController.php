<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Donation;
use Mail;
use Log;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    public function __construct(UserRepositoryInterface $user, MailTemplateRepositoryInterface $template)
    {
        $this->user = $user;
        $this->template = $template;
    }

    /**
     * Show the application dashboard.
     */
    public function index()
    {
        if (Sentinel::check()) {
            $statistics = array();
            $statistics['members'] = getTotalCount('\App\Models\User');
            $statistics['banners'] = getTotalCount('\App\Models\Banner');
            $statistics['enquiries'] = getTotalCount('\App\Models\Contact');
            $statistics['subscribers'] = getTotalCount('\App\Models\Subscribers');
            $statistics['events'] = getTotalCount('\App\Models\Event');
            $statistics['sponsors'] = getTotalCount('\App\Models\Sponsor');
            $statistics['pages'] = getTotalCount('\App\Models\Page');
            $statistics['roles'] = getTotalCount('\App\Models\Role');
            $statistics['news'] = getTotalCount('\App\Models\News');
            $statistics['articles'] = getTotalCount('\App\Models\Article');
            $statistics['donations'] = numberformat(Donation::where('status', 1)->sum('amount'));

            $role = Sentinel::findRoleByName('User');
            $user_count = $role->users()->count();
            $statistics['users'] = $user_count;
            return view('protected.admin.dashboard', compact('statistics'));
        } else {
            return redirect('login');
        }
    }

    public function UpdateSoon()
    {
        return view('protected.admin.updatesoon');
    }

    public function SendMails()
    {
        return view('protected.admin.send-mail');
    }

    public function PostSendMails(Request $request)
    {
        $input = $request->input();
        $emails = $input['to_email'];
        $emails = explode(',', $emails);
        $count = 0;
        foreach ($emails as $email) {
            $email = trim($email);
            if ($email) {
                $description = $input['description'];
                $mailcontent = array('mailbody' =>  $description);
                $a = sendmail_api($input['subject'], $description, $email);
            }
        }
        return redirect('admin/sendmails')->withSuccess('Sent Mails');
    }

    public function MembersSendMails()
    {
        return view('protected.admin.members-send-mail');
    }

    public function PostMembersSendMails(Request $request)
    {
        $input = $request->input();
        $template = $this->template->findByName('MemberNotification');
        if ($template) {
            $members = $input['to_email'];
            $members = explode(',', $members);
            $content = $template->template;
            $subject = $template->subject;
            foreach ($members as $member) {
                if ($member) {
                    $user = $this->user->find($member);
                    if ($user) {
                        $usercontent = $content;
                        $usercontent = str_replace('[NAME]', $user->first_name . ' ' . $user->last_name, $usercontent);
                        $usercontent = str_replace('[EMAIL]', $user->email, $usercontent);
                        $a = sendmail_api($subject, $usercontent, $user->email);
                    }
                }
            }
            return redirect('admin/members-sendmails')->withSuccess('Sent Mails');
        } else {
            return redirect('admin/sendmails')->withError('Error in Sending Mails');
        }
    }
}
