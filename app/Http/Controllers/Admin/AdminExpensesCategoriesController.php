<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Expenses\ExpensesCategoryRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;

class AdminExpensesCategoriesController extends Controller
{

    public function __construct(ExpensesCategoryRepositoryInterface $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->getAll();
        return view('protected.admin.expensescategories.index', compact('categories'));
    }
    public function store(Request $request)
    {
        $this->category->create($request->input());
        return redirect()->route("expenses-categories.index")->withSuccess('Category has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $category = $this->category->find($id);
        if ($request->ajax()) {
            return view('protected.admin.expensescategories.edit_modal', compact('category'))->render();
        } else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $category = $this->category->find($id);
        $category->fill($request->input())->save();
        return redirect()->route("expenses-categories.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->category->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }
}
