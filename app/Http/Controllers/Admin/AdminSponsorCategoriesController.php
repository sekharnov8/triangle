<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Sponsor\SponsorCategoryRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\SponsorCategory;

class AdminSponsorCategoriesController extends Controller {

    public function __construct(SponsorCategoryRepositoryInterface $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->getAll();
        return view('protected.admin.sponsorcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('protected.admin.sponsorcategories.create');
    }

    public function store(Request $request)
    {

        $count =SponsorCategory::where('category_name',$request->input('category_name'))->count();
        if($count)
         {
            return redirect()->route("sponsor-categories.index")->withError($request->input('name').' Category Already Exists!');
         }

       $this->category->create($request->input());

        return redirect()->route("sponsor-categories.index")->withSuccess('Category has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $category = $this->category->find($id);
        if($request->ajax()){
            return view('protected.admin.sponsorcategories.edit_modal', compact('category'))->render();
        }else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $category= $this->category->find($id);
        $category->fill($request->input())->save();
        return redirect()->route("sponsor-categories.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->category->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }
}

?>