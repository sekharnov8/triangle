<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\GreetingRequest;
use App\Models\Greeting;
use App\Repositories\Greeting\GreetingRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;

class AdminGreetingController extends Controller
{

    public function __construct(GreetingRepositoryInterface $greeting)
    {
        $this->greeting = $greeting;
    }

    public function index()
    {
        $greetings = $this->greeting->getAll();
        return view('protected.admin.greetings.index', compact('greetings'));
    }

    public function create()
    {
        return view('protected.admin.greetings.create');
    }

    public function store(Request $request)
    {
        if ($request->file('image')) {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/greetings';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            $filename2 = time() . '_' . $filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image' => $filename]);
        }

        $this->greeting->create($request->input());

        return redirect()->route("greetings.index")->withSuccess('Greeting has been added successfully!');
    }

    public function edit($id)
    {
        $greeting = $this->greeting->find($id);
        return view('protected.admin.greetings.edit', compact('greeting'));
    }

    public function update(Request $request, $id)
    {

        $greeting = $this->greeting->find($id);
        if ($request->file('image')) {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/greetings';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            $filename2 = time() . '_' . $filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image' => $filename]);
        }
        $greeting->fill($request->input())->save();
        return redirect()->route("greetings.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->greeting->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Greeting::whereId($id)->update(['status' => $status]);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        Greeting::whereId($id)->update(['image' => '']);
    }
}
