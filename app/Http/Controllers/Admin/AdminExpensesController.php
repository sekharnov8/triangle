<?php

namespace App\Http\Controllers\Admin;

use App\Models\Expenses;
use App\Http\Controllers\Controller;
use App\Repositories\Expenses\ExpensesRepositoryInterface;
use Input;
use Excel;
use App\Exports\ExpensesExport;

class AdminExpensesController extends Controller
{

    public function __construct(ExpensesRepositoryInterface $expenses)
    {
        $this->expenses = $expenses;
    }

    public function index()
    {
        $expenses = $this->expenses->getAll();
        return view('protected.admin.expenses.index', compact('expenses'));
    }

    public function destroy($id)
    {
        $this->expenses->delete($id);
        return redirect()->route("admin.expenses.index")->withFlashErrorsMessage('Deleted successfully!');
    }
    public function getExcelExport()
    {
        return app()->make(ExpensesExport::class)->download('expenses.xlsx');
    }
}
