<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Event\EventRepositoryInterface;
use App\Repositories\Event\EventCategoryRepositoryInterface;
use App\Repositories\Event\EventRegistrationTypeRepositoryInterface;
use App\Repositories\Event\EventUserRepositoryInterface;

use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\EventUser;
use App\Models\PaymentMethod;

class AdminEventRegistrationsController extends Controller {

    public function __construct(EventRepositoryInterface $event, EventCategoryRepositoryInterface $category, EventUserRepositoryInterface $register, EventRegistrationTypeRepositoryInterface $eventregistrationtype)
    {
        $this->register = $register;
        $this->event = $event;
        $this->category = $category;
        $this->eventregistrationtype = $eventregistrationtype;
    }


    public function index($id)
    {
        $event=$this->event->find($id);
        $users = $this->register->getByEventId($id);
          return view('protected.admin.eventusers.index', compact('users','event'));
    }
    public function edit($event_id,$id)
    {
        $event = $this->event->find($event_id);
        $reg=$this->register->find($id);
        $paymentmethods=PaymentMethod::pluck('name','id')->toArray();
        return view('protected.admin.eventusers.edit', compact('event','reg','paymentmethods'));
    }

    public function update($eventid,Request $request, $id)
    {
        $reg = $this->register->find($id);
        $reg->fill($request->input())->save();
        return redirect('admin/events/'.$eventid.'/registrations')->withSuccess('Updated successfully...');
    }

   public function destroy($eventid, $id)
    {
        $this->register->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        User::whereId($id)->update(['status' => $status]);
    }
}
