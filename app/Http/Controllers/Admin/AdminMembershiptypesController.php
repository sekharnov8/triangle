<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Member\MembershiptypeRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Membershiptype;

class AdminMembershiptypesController extends Controller
{

    public function __construct(MembershiptypeRepositoryInterface $mtype)
    {
        $this->mtype = $mtype;
    }

    public function index()
    {
        $mtypes = $this->mtype->getAll();
        $maxno = $this->mtype->getMax();
        return view('protected.admin.membershiptypes.index', compact('mtypes', 'maxno'));
    }

    public function store(Request $request)
    {
        // dd($request->input());
        $mtype = Membershiptype::where('name', $request->input('name'))->count();
        if ($mtype) {
            return redirect()->route("membershiptypes.index")->withError('Already have with this type - ' . $request->input('name'));
        }
        $request->merge(['updated_by' => Sentinel::getUser()->id]);
        $this->mtype->create($request->input());

        return redirect()->route("membershiptypes.index")->withSuccess('Membership type has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $mtype = $this->mtype->find($id);
        if ($request->ajax()) {
            return view('protected.admin.membershiptypes.edit_modal', compact('mtype'))->render();
        } else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $mtype = $this->mtype->find($id);
        $request->merge(['updated_by' => Sentinel::getUser()->id]);
        $mtype->fill($request->input())->save();
        return redirect()->route("membershiptypes.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->mtype->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Membershiptype::whereId($id)->update(['status' => $status]);
    }
}
