<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Donation\DonationRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Donation;
use App\Models\PaymentMethod;
use DB;
use Log;
use Excel;
use Datatables;
use App\Exports\DonationsExport;

class AdminDonationsController extends Controller {

    public function __construct(DonationRepositoryInterface $donation)
    {
        $this->donation = $donation;
    }

    public function index()
    {
         return view('protected.admin.donations.index');
    }
    
    public function filterData(Request $request)
    {
        $input=$request->input();
        $start=$input['start'];
        $length=$input['length'];
        $datefrom=$input['datefrom'];
        $dateto=$input['dateto'];
        $status=$input['status'];
        $donationtype=$input['donationtype'];
        $query=Donation::leftJoin('paymentmethods', 'paymentmethods.id', '=', 'donations.paymentmethod_id');
        $keyword=$input['keyword'];
         if($keyword)
        {
                 $query->where(function ($query) use ($keyword, $input){

            foreach($input['columns'] as $key=>$column) {
                if($column['searchable']=='true')
                {
                   if ($column['name'] == 'paymentmethod')
                    {
                        $query->orWhere('paymentmethods.name', 'like', '%' . $keyword . '%');
                    } 
                    else if ($column['name'] == 'user_name')
                    {
                        $query->orWhere('donations.first_name', 'like', '%' . $keyword . '%');
                        $query->orWhere('donations.last_name', 'like', '%' . $keyword . '%');
                    }
                    else
                    {
                        $query=$query->orWhere('donations.' .$column['name'],'like','%'.$keyword.'%');
                    }
                }
            }
            });
        }

        if($datefrom && $dateto)
        {
            $query=$query->whereBetween('donations.created_at',array($datefrom.' 00:00:01', $dateto.' 23:59:59'));
        }
        if($status!='')
        {
            $query=$query->where('donations.status',$status);
        }
    if($donationtype!='')
        {
            
            if($donationtype=='event')  
            {  
                $query=$query->whereNotNull('donations.eventuser_id');
                if($type_id!='')
                $query=$query->where('donations.eventuser_id',$type_id);
            }
            if($donationtype=='donationpage')  
            {  
                $query=$query->whereNotNull('donations.donationpage_id');
                if($type_id!='')
                $query=$query->where('donations.donationpage_id',$type_id);
         }
            if($donationtype=='membership')  
            { 
                $query=$query->whereNotNull('donations.order_id');
                if($type_id!='')
                $query=$query->where('donations.order_id',$type_id);
            }
        }
        

        $col=$input['order'][0]['column'];
        $colorder=$input['order'][0]['dir'];
        $columnorder = $input['columns'][$col]['name'];
        if ($columnorder == 'paymentmethod')
        {
            $query = $query->orderBy('paymentmethods.name', $colorder);
        }
        else if ($columnorder == 'user_name')
        {
            $query = $query->orderBy('donations.first_name', $colorder);
        }
        else
        {
            $query = $query->orderBy('donations.' . $columnorder, $colorder);
        }
        $count=$query->count();
        $query=$query->skip($start)->limit($length);
        $query=$query->select('donations.id','donations.created_at','donations.transaction_id','donations.amount',DB::raw('CONCAT(donations.first_name, " ", donations.last_name) AS user_name'),'paymentmethods.name as paymentmethod','donations.status',  'donations.donation_cause');

        // Log::info($query->toSql());
        // <a class="btn  btn-sm btn-info" href="'.url('admin/donations/'.$donation->id).'" data-toggle="tooltip" title="View"><i class="ion-eye"></i></a>&nbsp;
        $donations=$query->get();

        return datatables()->of($donations)->addIndexColumn()->addColumn('cb', function ($donation) {$st = '<input name="checkall[]" type="checkbox" class="checkall" value="' . $donation->id . '">'; return $st;})->addColumn('status', function ($donation) {$st = '<select name="status" class=" form-control status" data-style="btn-outline-primary pt-0 pb-0" data-id=' . $donation->id . ' data-status=' . $donation->status . '><option value="0" ';  if ($donation->status == 0)  {  $st = $st . 'selected';  }   $st = $st . '>Pending</option><option value="1" ';  if ($donation->status == 1)    {  $st = $st . 'selected';  }   $st = $st . '>Active</option></select>'; return $st;})->addColumn('actions', function ($donation) { return '<a href="javascript: void(0);" data-id="'.$donation->id.'" class="btn view btn-sm btn-info" data-toggle="tooltip" title="View"><i class="ion-eye"></i></a> <a class="btn  btn-sm btn-success" href="'.url('admin/donations/'.$donation->id.'/edit').'" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>&nbsp;<a href="javascript:void(0);"   class="btn btn-sm btn-danger delete  id_'.$donation->id.'" data-id=' . $donation->id . ' data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>';})->editColumn('created_at', function ($donation) {return dateformat($donation->created_at,1);})->skipPaging()->escapeColumns([''])->setTotalRecords($count)->make(true);

    }
    public function deleteChecked(Request $request)
    { 
        $ids=$request->get('checkall');
        $count=count($ids);
        if($count)
        {
            Donation::whereIn('id', $ids)->update(array('deleted_at' => date('y-m-d H:i:s')));
            return redirect()->route("donations.index")->withSuccess('Deleted '.$count.' selected donations successfully!!!');
        }
        else
        {
            return redirect()->route("donations.index")->withError('No checkboxes selected to delete!!!');
        }
     }

    public function create()
    {

    }

    public function store(NewsRequest $request)
    {

    }
    public function show($id)
    {
        $donation = $this->donation->find($id);
        return view('protected.admin.donations.view', compact('donation'));
    }
    public function edit($id)
    {
        $donation = $this->donation->find($id);
        return view('protected.admin.donations.edit', compact('donation'));
    }

    public function update(Request $request, $id)
    {
         $donation= $this->donation->find($id);

        if($request->input('slug')=='')
            $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);

        $donation->fill($request->input())->save();
        return redirect()->route("donations.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->donation->delete($id);
        return 'success';
        // return back()->withSuccess('Deleted successfully!!!');
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Donation::whereId($id)->update(['status' => $status]);
    }
    public  function getExcelExport(Request $request)
    {
        $filter=$request->input(); 
        return app()->make(DonationsExport::class)->forFilter($filter)->download('donations.xlsx');
    }
}

?>