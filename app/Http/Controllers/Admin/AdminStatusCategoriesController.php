<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Status\StatusCategoryRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\StatusCategory;

class AdminStatusCategoriesController extends Controller {

    public function __construct( StatusCategoryRepositoryInterface $category)
    {

        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->getAll();
        return view('protected.admin.statuscategories.index', compact('categories'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $this->category->create($request->input());
        return redirect()->route("status-categories.index")->withSuccess('Status Category  has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $category = $this->category->find($id);
        if($request->ajax()){
            return view('protected.admin.statuscategories.edit_modal', compact('category'))->render();
        }else {
            abort(404);
        }

    }

    public function update(Request $request, $id)
    {

        $category= $this->category->find($id);
        $category->fill($request->input())->save();
        return redirect()->route("status-categories.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->category->delete($id);
    }

}
?>