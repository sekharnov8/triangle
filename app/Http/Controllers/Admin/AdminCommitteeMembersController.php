<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Committee\CommitteeRepositoryInterface;
use App\Repositories\Committee\CommitteeMemberRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\CommitteeMember;
use App\Models\User;
use App\Models\Significant;

class AdminCommitteeMembersController extends Controller
{

    protected $roles;

    public function __construct(CommitteeRepositoryInterface $committee, CommitteeMemberRepositoryInterface $committeemember, UserRepositoryInterface $user)
    {

        $this->committee = $committee;
        $this->user = $user;
        $this->committeemember = $committeemember;
    }

    public function index()
    {
        $roles = Sentinel::getRoleRepository()->pluck('name', 'id')->toArray();
        $committees = $this->committee->getList();
        $committeemembers = $this->committeemember->getAll();
        $user = [];
        return view('protected.admin.committeemembers.index', compact('user', 'roles', 'committees', 'committeemembers'));
    }
    public function getByCommittee($committee)
    {
        $roles = Sentinel::getRoleRepository()->pluck('name', 'id')->toArray();
        $committees = $this->committee->getList();
        $committeemembers = $this->committeemember->getByCommitteeId($committee);
        $committeeusers = CommitteeMember::where('committee_id', $committee)->orWhere('role_id', 6)->pluck('user_id');
        $user = User::whereNotIn('id', $committeeusers)->selectRaw("CONCAT(first_name, ' ', last_name,' (',email,')') as fullName, id")->pluck('fullName', 'id');
        return view('protected.admin.committeemembers.index', compact('user', 'roles', 'committees', 'committeemembers', 'committee'));
    }

    public function PostCommitteeMembers(Request $request)
    {
        $committee_id = $request->get('committee_id');
        $members = $this->committeemember->getMembersListByCommittee($committee_id);
        return $members;
    }

    public function postByCommittee(Request $request)
    {
        $committee = $request->get('committees');
        if ($committee) {
            return redirect()->intended("/admin/committee-members/" . $committee);
        } else {
            return redirect()->intended("/admin/committee-members/");
        }
    }


    public function store(Request $request)
    {
        $input = $request->input();
        if ($input['role_id'] == 6) {
            $committeeusers = CommitteeMember::where('user_id', $input['user_id'])->where('role_id', $input['role_id'])->pluck('committee_id');
            if ($committeeusers) {
                return redirect()->intended("/admin/committee-members/" . $input['committee_id'])->withError('Already this user in another committee chair.');
            }
        }
        $committeemember = $this->committeemember->create($request->input());
        return redirect()->intended("/admin/committee-members/" . $committeemember->committee_id)->withSuccess('Created successfully!!!');
    }

    public function edit(Request $request, $id)
    {
        $committeemember = $this->committeemember->find($id);
        $roles = Sentinel::getRoleRepository()->pluck('name', 'id')->toArray();
        $committees = $this->committee->getList();

        $committeeusers = CommitteeMember::where('committee_id', $committeemember->committee_id)->orWhere('role_id', 6)->pluck('user_id');
        $user = User::whereNotIn('id', $committeeusers)->selectRaw("CONCAT(first_name, ' ', last_name,' (',email,')') as fullName, id")->pluck('fullName', 'id');

        if ($request->ajax()) {
            return view('protected.admin.committeemembers.edit', compact('committeemember', 'committees', 'user', 'roles'))->render();
        } else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {

        $committeemember = $this->committeemember->find($id);
        $request->merge(['updated_by' => Sentinel::getUser()->id]);
        if (!$request->get('user_id')) {
            $request->merge(['user_id' => $committeemember->user_id]);
        }

        if ($request->get('role_id') == 6) {
            $committeeusers = CommitteeMember::where('committee_id', $committeemember->committee_id)->where('role_id', 6)->pluck('user_id');
            if ($committeeusers && $committeeusers != $request->get('user_id'))
                return redirect()->intended("/admin/committee-members/" . $committeemember->committee_id)->withError('Already another user as committee chair.');
        }
        $committeemember->fill($request->input())->save();
        return redirect()->intended("/admin/committee-members/" . $committeemember->committee_id)->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->committeemember->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        CommitteeMember::whereId($id)->update(['status' => $status]);
    }
    public function PostCommittees(Request $request)
    {
        $committees = $this->committee->getList();
        return $committees;
    }
    public function PostUsersList(Request $request)
    {
        $committee_id = $request->get('committee_id');
        $committeeusers = CommitteeMember::where('committee_id', $committee_id)->orWhere('role_id', 6)->pluck('user_id');
        $user = User::whereNotIn('id', $committeeusers)->selectRaw("CONCAT(first_name, ' ', last_name,' (',email,')') as fullName, id")->pluck('fullName', 'id');
        return $user;
    }
}
