<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\PaymentMethod\PaymentMethodRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\PaymentMethod;

class AdminPaymentMethodsController extends Controller {

    public function __construct( PaymentMethodRepositoryInterface $payment)
    {

        $this->payment = $payment;
    }

    public function index()
    {
        $paymentmethods = $this->payment->getAll();
        return view('protected.admin.paymentmethods.index', compact('paymentmethods'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $this->payment->create($request->input());
        return redirect()->route("payment-methods.index")->withSuccess('Payment Method  has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $paymentmethod = $this->payment->find($id);
        if($request->ajax()){
            return view('protected.admin.paymentmethods.edit_modal', compact('paymentmethod'))->render();
        }else {
            abort(404);
        }

    }

    public function update(Request $request, $id)
    {

        $payment= $this->payment->find($id);
        $payment->fill($request->input())->save();
        return redirect()->route("payment-methods.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->payment->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        PaymentMethod::whereId($id)->update(['status' => $status]);
    }

}
?>