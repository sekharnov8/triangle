<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventCategoryRequest;
use App\Repositories\Event\EventCategoryRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\EventCategory;

class AdminEventCategoriesController extends Controller
{

    public function __construct(EventCategoryRepositoryInterface $category)
    {
        $this->category = $category;
    }
    public function index()
    {
        $categories = $this->category->getAll();
        return view('protected.admin.eventcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('protected.admin.eventcategories.create');
    }

    public function store(Request $request)
    {
        $category = $this->category->findByName($request->input('name'));
        if ($category) {
            return redirect()->route("event-categories.index")->withError('Already have with this category - ' . $request->input('name'));
        }
        $request->merge(['updated_by' => Sentinel::getUser()->id]);

        $this->category->create($request->input());

        return redirect()->route("event-categories.index")->withSuccess('Category has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $category = $this->category->find($id);
        if ($request->ajax()) {
            return view('protected.admin.eventcategories.edit_modal', compact('category'))->render();
        } else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $category = $this->category->find($id);
        $request->merge(['updated_by' => Sentinel::getUser()->id]);
        $category->fill($request->input())->save();
        return redirect()->route("event-categories.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->category->delete($id);
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        EventCategory::whereId($id)->update(['status' => $status]);
    }
}
