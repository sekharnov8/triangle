<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\VideoCategoryRequest;
use App\Repositories\Video\VideoCategoryRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;

class AdminVideoCategoriesController extends Controller {

    public function __construct(VideoCategoryRepositoryInterface $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->getAll();
        return view('protected.admin.videocategories.index', compact('categories'));
    }

    public function create()
    {
        return view('protected.admin.videocategories.create');
    }

    public function store(Request $request)
    {

       $request->merge(['updated_by'=>Sentinel::getUser()->id]);

       $this->category->create($request->input());

        return redirect()->route("video-categories.index")->withSuccess('Category has been added successfully!');
    }


    public function edit(Request $request, $id)
    {
        $category = $this->category->find($id);
        if($request->ajax()){
            return view('protected.admin.videocategories.edit_modal', compact('category'))->render();
        }else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $category= $this->category->find($id);
        $category->fill($request->input())->save();
        return redirect()->route("video-categories.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->category->delete($id);
    }
}

?>