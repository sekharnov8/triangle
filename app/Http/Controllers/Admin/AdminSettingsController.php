<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\Appinfo;

class AdminSettingsController extends Controller {

    public function getApplicationSettings()
    {
        $appinfo=Appinfo::where('id',1)->first();
        return view('protected.admin.application-settings', compact('appinfo'));
    }
    public function PostAppInfo(Request $request)
    {
         $input=$request->input();
        $appinfo=Appinfo::where('id',1)->first();
        $appinfo->fill($input)->save();
        return redirect('admin/application-settings')->withSuccess('Updated Application Settings'); 
    }
       public function getPaymentSettings()
    {
        $appinfo=Appinfo::where('id',1)->first();
        return view('protected.admin.payment-settings', compact('appinfo'));
    }
    public function PostPaymentSettings(Request $request)
    {
         $input=$request->input();
        $appinfo=Appinfo::where('id',1)->first();
        $appinfo->fill($input)->save();
        return redirect('admin/payment-settings')->withSuccess('Updated Payment Settings'); 
    }
}
