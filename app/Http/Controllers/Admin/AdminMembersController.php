<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Order;
use App\Models\Significant;
use App\Models\PaymentMethod;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Excel;
use Datatables;
use Log;
use Hash;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Member\MembershiptypeRepositoryInterface;
use App\Repositories\Member\OrderRepositoryInterface;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface;
use Illuminate\Support\Str;
use App\Repositories\Member\SignificantRepositoryInterface;
use Mail;
use App\Repositories\Status\StatusTypeRepositoryInterface;
use App\Exports\MembersExport;

class AdminMembersController extends Controller
{
    public function __construct(MembershiptypeRepositoryInterface $mtype, UserRepositoryInterface $user, OrderRepositoryInterface $order, SignificantRepositoryInterface $significant,  MailTemplateRepositoryInterface $template, StatusTypeRepositoryInterface $statustype)
    {
        $this->user = $user;
        $this->mtype = $mtype;
        $this->order = $order;
        $this->significant = $significant;
        $this->template = $template;
        $this->activation = Sentinel::getActivationRepository();
        $this->statustype = $statustype;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rolelist = Sentinel::getRoleRepository()->all();
        $roles = [];
        foreach ($rolelist as $role) {
            $roles = array_add($roles, $role->id, $role->name);
        }
        $statustypes = $this->statustype->getByStatusCategoryId(2);
        $mtypes = $this->mtype->getList();
        return view('protected.admin.members.index', compact('roles', 'mtypes', 'statustypes'));
    }
    public function filterData(Request $request)
    {
        $input = $request->input();
        $start = $input['start'];
        $length = $input['length'];
        $datefrom = $input['datefrom'];
        $dateto = $input['dateto'];
        $is_approved = $input['is_approved'];
        $role = $input['role'];
        $mtype = $input['mtype'];
        $query = User::leftJoin('membershiptypes', 'membershiptypes.id', '=', 'users.membershiptype_id')->leftJoin('role_users', 'role_users.user_id', '=', 'users.id');
        $keyword = $input['keyword'];
        if ($keyword) {
            $query->where(function ($query) use ($keyword, $input) {

                foreach ($input['columns'] as $key => $column) {
                    if ($column['searchable'] == 'true') {
                        if ($column['name'] == 'membershiptype') {
                            $query = $query->orWhere('membershiptypes.name', 'like', '%' . $keyword . '%');
                        } else {
                            $query = $query->orWhere('users.' . $column['name'], 'like', '%' . $keyword . '%');
                        }
                    }
                }
            });
        }


        if ($datefrom && $dateto) {
            $query = $query->whereBetween('users.created_at', array($datefrom . ' 00:00:01', $dateto . ' 23:59:59'));
        }

        if ($is_approved != '') {
            $query = $query->where('users.is_approved', $is_approved);
        }
        if ($role != '') {
            $query = $query->where('role_users.role_id', $role);
        }
        if ($mtype != '') {
            $query = $query->where('users.membershiptype_id', $mtype);
        }

        $query = $query->distinct();

        $col = $input['order'][0]['column'];
        $colorder = $input['order'][0]['dir'];
        $columnorder = $input['columns'][$col]['name']; 
        $query = $query->orderBy('users.' . $columnorder, $colorder);
        $count = $query->count();
        $query = $query->skip($start)->limit($length);
        $users = $query->select('users.member_id', 'users.id', 'users.email', 'users.mobile', 'users.first_name', 'users.last_name', 'users.created_at', 'users.last_login', 'membershiptypes.name as membershiptype', 'users.is_approved', 'users.created_at');

        // Log::info($query->toSql());
        $users = $query->get();
        $statustypes = $this->statustype->getByStatusCategoryId(2);

        return datatables()->of($users)->addIndexColumn()->addColumn('role', function ($user) {
            return getUserRoles($user->id);
        })->addColumn('status', function ($user) use ($statustypes) {
            $st = '<select name="status" class=" form-control status" data-style="btn-outline-primary pt-0 pb-0" data-id=' . $user->id . ' data-status=' . $user->is_approved . '>';
            foreach ($statustypes as $key => $data) {
                $st = $st . '<option value="' . $key . '"';
                if ($user->is_approved == $key) {
                    $st = $st . 'selected';
                }
                $st = $st . '>' . $data . '</option>';
            }
            $st = $st . '</select>';
            return $st;
        })->addColumn('actions', function ($user) {
            $actiondata = '';
            if ($user->membershiptype) {
                $actiondata = $actiondata . '<a class="btn  btn-sm btn-success" href="' . url('admin/members/' . $user->memberid . '/significants') . '" data-toggle="tooltip" title="Significants"><i class="ion-person"></i></a>&nbsp;';
                $actiondata = $actiondata . '<a class="btn  btn-sm btn-info" href="' . url('admin/members/' . $user->id . '/orders') . '" data-toggle="tooltip" title="Orders"><i class="ion-person"></i></a>&nbsp;';
            }
            $actiondata = $actiondata . '<a class="btn  btn-sm btn-warning" href="' . url('admin/members/' . $user->id . '/edit') . '" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>&nbsp;<a href="javascript:void(0);" class="btn btn-sm btn-danger delete  id_' . $user->id . '" data-id="' . $user->id . '" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>';
            return $actiondata;
        })->editColumn('created_at', function ($user) {
            return dateformat($user->created_at, 1);
        })
            ->editColumn('member_id', function ($user) {
                return '<a href="javascript:void(0)" class="view"  data-id="' . $user->id . '" data-tip="tooltip" title="View Member Details">' . $user->id . '</a>';
            })
            ->editColumn('last_login', function ($user) {
                return dateformat($user->last_login, 1);
            })->skipPaging()->escapeColumns([''])->setTotalRecords($count)->make(true);
    }

    public function getUsersExcelExport(Request $request)
    {
        $filter = $request->input();
        $role = Sentinel::findRoleByName('user');
        $query = $role->users();
        $mtype=$filter['mtype'];

        if ($filter['datefrom'] != '' && $filter['dateto'] != '') {
            $query->whereBetween('role_users.created_at', array($filter['datefrom'] . ' 00:00:01', $filter['dateto'] . ' 23:59:59'));
        }
        if ($mtype != '') {
            $query = $query->where('members.membershiptype_id', $mtype);
        }
        $users = $query->select('id', 'name', 'mobile', 'email', 'last_login', 'role_users.created_at')->orderBy('id', 'desc')->get();
        Excel::create('UsersList', function ($excel) use ($users) {
            $excel->sheet('Users', function ($sheet) use ($users) {
                $sheet->loadview('protected.admin.members.export_users', ['users' => $users]);
            });
        })->export('xls');
        return back()->withFlashSuccessMessage('Members exported as Excel successfully!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rolelist = Sentinel::getRoleRepository()->all();
        $roles = [];
        foreach ($rolelist as $role) {
            $roles = array_add($roles, $role->id, $role->name);
        }
        $membershiptypes = $this->mtype->getListWithPrice();
        return view('protected.admin.members.create', compact('membershiptypes', 'roles'));
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        User::whereId($id)->update(['is_approved' => $status]);

        if ($status == 1) {
            $user = User::where('id', $id)->first();
            $code = Activation::create($user)->code;
            $this->activation->complete($user, $code);

            $template = $this->template->findByName('Member Approval');
            if ($template) {
                $content = $template->template;
                $subject = $template->subject;
                $subject = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $subject);
                $content = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $content);
                if ($user->membership && $user->membership->price > 0) {
                    $order = Order::where('user_id', $user->id)->where('membershiptype_id', $user->membershiptype_id)->orderBy('id', 'desc')->first();
                    $payment = 'Payment Method: ' . $order->paymentmethod->name;
                    if ($order->paymentmethod->name == 'Paypal') {
                        $payment = $payment . '<br/>Transaction Id: ' . $order->transaction_id . '<br/>';
                    } else {
                        $payment = $payment . '<br/>Bank Name: ' . $order->bankname . '<br/>Cheque No.' . $order->cheque_no . '<br/>Cheque Date:' . $order->cheque_date . '<br/>';
                    }
                } else {
                    $payment = '';
                }
                $content = str_replace('[MembershipType]', $user->membership->name, $content);
                $content = str_replace('[adminemail]', 'info@tutanc.org', $content);
                $content = str_replace('[MemberId]', $user->member_id, $content);
                $content = str_replace('[Payment]', $payment, $content);
                $content = str_replace('[ApplicationName]', 'TUTA', $content);
                $content = str_replace('[siteurl]', env('siteurl'), $content);

                $user_data = User::findOrFail($id);
                $user = Sentinel::findByCredentials(['login' => $user_data['email']]);

                $mailcontent = array('mailbody' =>  $content);

                // Mail::send(['html' => 'emails.mail'], $mailcontent,  function($message) use ($subject, $user)
                // {
                //   $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($user->email)->subject($subject);        
                // });  
                sendmail_api($subject, $content, $user->email);
            }
        }
    }


    public function store(Request $request)
    {
        $user_data = $request->only('email', 'mobile', 'title', 'first_name', 'last_name', 'profile_image', 'designation', 'middle_name');
        $password = 'tuta2021';
        $user_data = array_merge($user_data, ['password' => \Hash::make($password)]);
        if ($user = Sentinel::register($user_data)) {
            $code = Activation::create($user)->code;
            $this->activation->complete($user, $code);

            $usersRole = Sentinel::findRoleById($request->input('role_id'));
            $usersRole->users()->attach($user);
            $userinfo = $this->user->find($user->id);
            $user_data = array_merge($user_data, ['member_id' => 10000 + $user->id]);
            $userinfo->fill($user_data)->save();

            $role = $usersRole->name;

             $user_data = $request->only('home_phone', 'fax', 'occupation', 'age', 'employer', 'skills', 'address', 'city', 'state', 'country', 'zipcode', 'primary_member_id', 'referred_by', 'membershiptype_id', 'terms_conditions');
            $orderinput = array();
            $orderinput['user_id'] = $user->id;
            $orderinput['membershiptype_id'] = 1;
            $orderinput['amount'] = 0;

            $is_duplicate = Order::where('user_id', $orderinput['user_id'])->where('membershiptype_id', $orderinput['membershiptype_id'])->where('amount', $orderinput['amount'])->where('created_at', '>', date('Y-m-d'))->first();
            if (!$is_duplicate) {
                $order = $this->order->create($orderinput);
            } else {
                $is_duplicate->fill($orderinput)->save();
                $order = $is_duplicate;
            }

            $this->order->create($orderinput);

            return redirect(route('members.index'))->withSuccess('Member details registered successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $spouse = Significant::where('user_id', $user->id)->where('relationship', 'Spouse')->first();
        $childrens = Significant::where('user_id', $user->id)->where('relationship', '<>', 'Spouse')->get();
        return view('protected.admin.members.view', compact('user',  'spouse', 'childrens'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::pluck('name', 'id')->toArray();
        $user = User::where('id', $id)->first();
        $role_id = RoleUser::where('user_id', $user->id)->pluck('role_id')->first(); 
        $order = Order::where('user_id', $user->id)->where('membershiptype_id', $user->membershiptype_id)->orderBy('id', 'desc')->first();
        if (!$order) {
            $orderdata = ['user_id' => $user->id, 'membershiptype_id' => 6, 'amount' => 0];
            $this->order->create($orderdata);
            $order = Order::where('user_id', $user->id)->where('membershiptype_id', $user->membershiptype_id)->orderBy('id', 'desc')->first();
        }
        $paymentmethods = PaymentMethod::pluck('name', 'id')->toArray();
        $membershiptypes = $this->mtype->getList();
        $member_roles = getUserRoleIds($user->id);
        $statustypes = $this->statustype->getByStatusCategoryId(2);
        $order_statustypes = $this->statustype->getByStatusCategoryId(1);

        $member_roles = explode(',', $member_roles);
        return view('protected.admin.members.edit', compact('member_roles', 'user', 'membershiptypes', 'roles', 'order', 'paymentmethods', 'role_id', 'statustypes', 'order_statustypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_data = $request->only('email', 'mobile', 'title', 'first_name', 'last_name', 'profile_image', 'is_approved', 'designation', 'middle_name');
        $user = User::findOrFail($id);

        if ($request->file('profile_image')) {
            $image              = $request->file('profile_image');
            $destinationPath    = 'uploads/users';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            $filename2 = time() . '_' . $filename;
            $image->move($destinationPath, $filename2);
            $user_data = array_merge($user_data, ['profile_image' => $filename2]);
        }

        $user->fill($user_data)->save();
        $user_roles = $request->get('role');
        $userinfo = Sentinel::findByCredentials(['login' => $user_data['email']]);
        $role_id = RoleUser::where('user_id', $user->id)->pluck('role_id')->first();

        if ($request->get('password') && $request->get('confirmpassword') == $request->get('password')) {
            $hashpwd = Hash::make($request->input('password'));
            $user->password = $hashpwd;
            $user->save();
        }
        $member_roles = $request->get('roles');

        if (count($request->get('roles'))) {
            $user = User::findOrFail($id);
            $eroles = explode(',', getUserRoleIds($user->id));
            foreach ($eroles as $erole) {
                $usersRole = Sentinel::findRoleById($erole);
                if ($usersRole)
                    $usersRole->users()->detach($user);
            }
            foreach ($member_roles as $mrole) {
                $usersRole = Sentinel::findRoleById($mrole);
                if ($usersRole)
                    $usersRole->users()->attach($user);
            }
        }

        return redirect(route('members.index'))->withSuccess('Member details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();
        Order::where('user_id', $id)->delete();
        Significant::where('user_id', $id)->delete();
    }

    public function resetPasswords()
    {
        $users = User::where('id', '>', 3)->get();
        foreach ($users as $user) {
            $user2 = $this->user->find($user->id);
            $password = Str::substr($user->last_name, 0, 4);
            $user_data = array();
            if ($user->mobile)
                $password = $password . Str::substr($user->mobile, -4);
            $user_data = array_merge($user_data, ['password' => \Hash::make($password)]);
            $user2->fill($user_data)->save();
        }
        return 'done';
    }

    public function getMembersExcelExport(Request $request)
    {
        $filter = $request->input();
        return app()->make(MembersExport::class)->forFilter($filter)->download('members.xlsx');
    }

    public function getMembersSampleExcel()
    {
        Excel::create('MemberSample', function ($excel) {

            $excel->sheet('Members', function ($sheet) {
                $sheet->fromArray(array(
                    array('first_name', 'last_name', 'email', 'address', 'address2', 'mobile', 'city', 'state', 'zipcode', 'country', 'membership_type', 'referred_by', 'order_date', 'designation', 'spouse_first_name', 'spouse_last_name', 'spouse_email', 'status'),
                    array('Guravaiah', 'Majety', 'mrguestus@gmail.com', '13646 Cedar Run Ln', '', '70378764880', 'Oak Hill', 'VA', '20171', 'USA', 'Life Membership', '', '17-03-2019 12:09', 'Software Engineer', 'Deepika', 'Majety', 'deepikalashkar@gmail.com', 1)
                ), null, 'A1', false, false);

                $sheet->setStyle(array('font' => array('name' => 'Calibri', 'size' => 10, 'bold' => false)));

                $sheet->cells('A1:B1', function ($cells) {
                    $cells->setFontSize(12);
                    $cells->setFontWeight('bold');
                });
            });
        })->export('xls');

        return redirect()->route("admin.members.index")->withSuccess('Sample Excel exported successfully!');
    }

    public function postMembersExcelImport()
    {

        $count1 = $this->user->getCount();
        $file       = $request->file('file');

        $filename   = strtolower(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $file->getClientOriginalExtension()
        );

        $file->move('imports', $filename);

        $results = Excel::load('imports/' . $filename, function ($reader) {
        })->toArray();

        $user_ids = array(array('user_id'));
        $user_ids2 = array(array('user_id'));


        $info = array('first_name', 'last_name', 'email', 'address', 'address2', 'mobile', 'city', 'state', 'zipcode', 'country', 'membership_type', 'referred_by', 'order_date', 'designation', 'spouse_first_name', 'spouse_last_name', 'spouse_email', 'status');
        $userinfo = array('first_name', 'last_name', 'email', 'mobile', 'designation','address', 'address2', 'city', 'state', 'zipcode', 'country', 'referred_by');
        $spouseinfo = array('spouse_first_name', 'spouse_last_name', 'spouse_email');
        $orderinfo = array('order_date');


        foreach ($results as $key => $result) {
            $user_data = array();
            $member_data = array();
            $order_data = array();
            $email = $result['email'];
            if ($email != '') {
                $user = User::where('email', $email)->first();

                $user_data = array();
                $member_data = array();
                $order_data = array();

                for ($i = 0; $i < sizeof($info); $i++) {
                    if ($info[$i] == 'status') {
                        $user_data = array_add($user_data, 'is_approved', $result[$info[$i]]);
                    }
                    if ($result[$info[$i]] != '' && in_array($info[$i], $userinfo)) {
                        $user_data = array_add($user_data, $info[$i], $result[$info[$i]]);
                    }
                    if ($result[$info[$i]] != '' && in_array($info[$i], $memberinfo)) {
                        $member_data = array_add($member_data, $info[$i], $result[$info[$i]]);
                    }
                    if ($result[$info[$i]] != '' && in_array($info[$i], $orderinfo)) {
                        $order_data = array_add($order_data, $info[$i], $result[$info[$i]]);
                    }
                }


                if (!$user) {

                    $password = Str::substr($result['last_name'], 0, 4);
                    $password = ucwords($password);

                    if ($result['mobile'])
                        $password = $password . Str::substr($result['mobile'], -4);

                    $user_data = array_merge($user_data, ['password' => \Hash::make($password)]);

                    if ($user = Sentinel::register($user_data)) {
                        $code = Activation::create($user)->code;
                        $this->activation->complete($user, $code);

                        $usersRole = Sentinel::findRoleById(2);
                        $usersRole->users()->attach($user);
                        $user2 = $this->user->find($user->id);
                        $member_id = 10000 + $user->id;
                        $user_data = array_merge($user_data, ['member_id' => $member_id]);

 
                        if ($result['membership_type'] != '') {
                            $membershiptype = $this->mtype->getByName($result['membership_type']);
                            if ($membershiptype) {
                                $user_data = array_merge($user_data, ['membershiptype_id' => $membershiptype->id]);
                            }
                        }
                        $user2->fill($user_data)->save();

 
                        if ($result['spouse_first_name'] != '' || $result['spouse_last_name'] != ''  || $result['spouse_email'] != '') {
                            $significant = array();
                            $significant['user_id'] = $user->id;
                            $significant['first_name'] = $result['spouse_first_name'];
                            $significant['last_name'] = $result['spouse_last_name'];
                            $significant['email'] = $result['spouse_email'];
                            $significant['relationship'] = 'Spouse';
                            $this->significant->create($significant);
                        }

                        if ($result['membership_type'] != '') {
                            $order_data = array_merge($order_data, ['user_id' => $user->id, 'membershiptype_id' => $membershiptype->id, 'amount' => $membershiptype->price, 'paymentmethod_id' => 2, 'status' => 1]);

                            $this->order->create($order_data);
                        }
                        $user = User::where('id', $user->id)->first();

                        if ($user->is_approved == 1) {

                            $code = Activation::create($user)->code;
                            $this->activation->complete($user, $code);

                            $template = $this->template->findByName('Member Approval Admin Import');
                            if ($template) {
                                $content = $template->template;
                                $subject = $template->subject;
                                $subject = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $subject);
                                $content = str_replace('[UserName]', $user->first_name . ' ' . $user->last_name, $content);
                                $payment = '';
                                $content = str_replace('[MembershipType]', $user->membership->name, $content);
                                $content = str_replace('[adminemail]', 'info@tutanc.org', $content);
                                $content = str_replace('[MemberId]', $user->member_id, $content);
                                $content = str_replace('[Payment]', $payment, $content);
                                $content = str_replace('[ApplicationName]', 'TUTA', $content);
                                $content = str_replace('[siteurl]', env('siteurl'), $content);

                                $mailcontent = array('mailbody' =>  $content);  
                                sendmail_api($subject, $content, $user->email);
                            }
                        }

                        $user_ids[$key + 1]['user_id'] = $user->id;
                    }
                } else {
                    $user->fill($user_data)->save();

                    $membershiptype = $this->mtype->getByName($result['membership_type']);

                  
                    if ($result['membership_type'] != '') {
                        $member_data = array_merge($member_data, ['membershiptype_id' => $membershiptype->id]);
                        $order_data = array_merge($order_data, ['user_id' => $user->id, 'membershiptype_id' => $membershiptype->id, 'amount' => $membershiptype->price, 'paymentmethod_id' => 2, 'status' => 1]);
                        $this->order->create($order_data);
                    }
                    $user_ids2[$key + 1]['user_id'] = $user->id;
                }
            }
        }
        $count2 = $this->user->getCount();

        if ($count2 - $count1 == 0 && count($user_ids2) == 0) {
            return back()->withError('No Users uploaded.');
        } else if (count($user_ids2) > 0) {
            $ss = count($user_ids2) - 1 . '  Users updated. ';
            if ($count2 - $count1 > 0) {
                $countadded = $count2 - $count1;
                $ss = $ss . $countadded . ' Users Imported';
            }
            return back()->withSuccess($ss);
        } else {
            return redirect('admin/members')->withSuccess($count2 - $count1 . ' Users Imported Successfully');
        }
    }
}
