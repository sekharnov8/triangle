<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Committee\CommitteeRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\Committee;

class AdminCommitteesController extends Controller {

    public function __construct(CommitteeRepositoryInterface $committee)
    {
        $this->committee = $committee;
    }

    public function index()
    {

        $committees = $this->committee->getAll();
        $maxno=$this->committee->getMax();
        return view('protected.admin.committees.index', compact('committees','maxno'));
    }

    public function create()
    {
        return view('protected.admin.committees.create');
    }

    public function store(Request $request)
    {
        $count =Committee::where('name',$request->input('name'))->count();
        if($count)
         {
            return redirect()->route("committees.index")->withError($request->input('name').' Committee Already Exists!');
         }
        $request->merge(['slug'=>str_slug($request->input('name'), '-')]);

        $request->merge(['updated_by'=>Sentinel::getUser()->id]);

        $this->committee->create($request->input());

        return redirect()->route("committees.index")->withSuccess('Service has been added successfully!');
    }

    public function edit($id)
    {
        $committee = $this->committee->find($id);
        return view('protected.admin.committees.edit', compact('committee'));
    }

    public function update(Request $request, $id)
    {

        $committee= $this->committee->find($id);
        $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
        $committee->fill($request->input())->save();
        return redirect()->route("committees.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->committee->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Committee::whereId($id)->update(['status' => $status]);
    }
}
