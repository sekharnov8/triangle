<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Photo\PhotoCategoryRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use App\Models\PhotoCategory;
use Sentinel;

class AdminPhotoCategoriesController extends Controller {

    public function __construct(PhotoCategoryRepositoryInterface $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->getAll();
        $category_list=PhotoCategory::where('parent_id',0)->pluck('name','id')->toArray();
        return view('protected.admin.photocategories.index', compact('categories','category_list'));
    }

    public function create()
    {
        $categories=PhotoCategory::where('parent_id',0)->pluck('name','id')->toArray();

        return view('protected.admin.photocategories.create', compact('categories'));
    }

    public function store(Request $request)
    {

     if($request->input('slug')=='')
       $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        else
       $request->merge(['slug'=>str_slug($request->input('slug'), '-')]);
        if($request->input('parent_id')=='')
       $request->merge(['parent_id'=>0]);

        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/photos';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image'=>$filename]);
        }

       $this->category->create($request->input());

        return redirect()->route("photo-categories.index")->withSuccess('Category has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $category_list=PhotoCategory::where('parent_id',0)->pluck('name','id')->toArray();

        $category = $this->category->find($id);
        if($request->ajax()){
            return view('protected.admin.photocategories.edit_modal', compact('category','category_list'))->render();
        }else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $category= $this->category->find($id);

        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/photos';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename2);
            $request->merge(['image'=>$filename2]);
        }
        if($request->input('slug')=='')
       $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        else
       $request->merge(['slug'=>str_slug($request->input('slug'), '-')]);
        $category->fill($request->input())->save();
        return redirect()->route("photo-categories.index")->withSuccess('Updated successfully!!!');
    }

     public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        PhotoCategory::whereId($id)->update(['status' => $status]);
    }

    public function destroy($id)
    {
        $this->category->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }
}

?>