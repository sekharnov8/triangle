<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Event\EventSponsorshipRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\EventSponsorship;

class AdminEventSponsorshipsController extends Controller
{

    public function __construct(EventSponsorshipRepositoryInterface $sponsorship)
    {
        $this->sponsorship = $sponsorship;
    }
    public function index()
    {
        $sponsorships = $this->sponsorship->getAll();
        return view('protected.admin.eventsponsorships.index', compact('sponsorships'));
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $sponsorship = $this->sponsorship->findByName($request->input('name'));
        if ($sponsorship) {
            return redirect()->route("event-sponsorships.index")->withError('Already have with this sponsorship - ' . $request->input('name'));
        }
        $this->sponsorship->create($request->input());
        return redirect()->route("event-sponsorships.index")->withSuccess('Sponsorship has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $sponsorship = $this->sponsorship->find($id);
        if ($request->ajax()) {
            return view('protected.admin.eventsponsorships.edit_modal', compact('sponsorship'))->render();
        } else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $sponsorship = $this->sponsorship->find($id);
        $sponsorship->fill($request->input())->save();
        return redirect()->route("event-sponsorships.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->sponsorship->delete($id);
    }
}
