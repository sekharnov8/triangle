<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactProgramRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\ContactProgram;

class AdminContactProgramsController  extends Controller {

    public function __construct(ContactProgramRepositoryInterface $contactprogram)
    {
        $this->contactprogram = $contactprogram;
    }

    public function index()
    {
        $contactprogram = $this->contactprogram->getAll();
        $maxno=$this->contactprogram->getMax();
        return view('protected.admin.contactprogram.index', compact('contactprogram','maxno'));
    }

    public function store(Request $request)
    {
        // dd($request->input());

        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
       // dd($request->input());
        $this->contactprogram->create($request->input());

        return redirect()->route("contactprogram.index")->withSuccess('security Question has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $contactprogram = $this->contactprogram->find($id);
        if($request->ajax()){
            return view('protected.admin.contactprogram.edit_modal', compact('contactprogram'))->render();
        }else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $contactprogram= $this->contactprogram->find($id);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
        $contactprogram->fill($request->input())->save();
        return redirect()->route("contactprogram.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->contactprogram->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        ContactProgram::whereId($id)->update(['status' => $status]);
    }
}
