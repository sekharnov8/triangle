<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Repositories\News\NewsRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\News;

class AdminNewsController extends Controller {

    public function __construct(NewsRepositoryInterface $news)
    {
        $this->news = $news;
    }

    public function index()
    {
        $news_list = $this->news->getAll();
        return view('protected.admin.news.index', compact('news_list'));
    }

    public function create()
    {
        $maxno=$this->news->getMax();
        return view('protected.admin.news.create', compact('maxno'));
    }

    public function store(Request $request)
    {
        if($request->input('slug')=='')
            $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
       $request->merge(['updated_by'=>Sentinel::getUser()->id]);

       if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/news';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image'=>$filename]);
        }

       $this->news->create($request->input());

        return redirect()->route("news.index")->withSuccess('News has been added successfully!');
    }

    public function edit($id)
    {
        $news = $this->news->find($id);
        return view('protected.admin.news.edit', compact('news'));
    }

    public function update(Request $request, $id)
    {
        $news= $this->news->find($id);
        if($request->input('slug')=='')
            $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);

        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/news';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image'=>$filename]);
        }

        $news->fill($request->input())->save();
        return redirect()->route("news.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->news->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        News::whereId($id)->update(['status' => $status]);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        News::whereId($id)->update(['image' => '']);
    }
}

?>