<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Event\EventRepositoryInterface;
use App\Repositories\Event\EventCategoryRepositoryInterface;
use App\Repositories\Event\EventRegistrationTypeRepositoryInterface;
use App\Repositories\Event\EventSponsorshipRepositoryInterface;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\Event;
use Log;
use Datatables;
use Excel;
use Carbon\Carbon;
use App\Models\Committee;

class AdminEventsController extends Controller
{

    public function __construct(EventRepositoryInterface $event, EventCategoryRepositoryInterface $category, EventRegistrationTypeRepositoryInterface $eventregistrationtype, EventSponsorshipRepositoryInterface $sponsorship)
    {
        $this->event = $event;
        $this->category = $category;
        $this->eventregistrationtype = $eventregistrationtype;
        $this->sponsorship = $sponsorship;
    }

    public function index()
    {
        return view('protected.admin.events.index');
    }

    public function filterData(Request $request)
    {
        $input = $request->input();
        $start = $input['start'];
        $length = $input['length'];
        $datefrom = $input['datefrom'];
        $dateto = $input['dateto'];
        $status = $input['status'];
        $eventtype = $input['eventtype'];
        $query = Event::leftJoin('eventcategories', 'eventcategories.id', '=', 'events.category_id');
        $keyword = $input['keyword'];
        if ($keyword) {
            $query->where(function ($query) use ($keyword, $input) {

                foreach ($input['columns'] as $key => $column) {
                    if ($column['searchable'] == 'true') {
                        if ($column['name'] == 'category') {
                            $query->orWhere('eventcategories.name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->orWhere('events.' . $column['name'], 'like', '%' . $keyword . '%');
                        }
                    }
                }
            });
        }
        if ($datefrom && $dateto) {
            $query = $query->whereBetween('events.start_date', array($datefrom . ' 00:00:01', $dateto . ' 23:59:59'));
        }

        if ($status != '') {
            $query = $query->where('events.status', $status);
        }

        if ($eventtype != '') {
            if ($eventtype == 'upcoming') {
                $query = $query->where('start_date', '>', Carbon::now());
            }
            if ($eventtype == 'past') {
                $query = $query->where('start_date', '<', Carbon::now());
            }
            if ($eventtype == 'current') {
                $query = $query->where('start_date', '>=', Carbon::now());
            }
        }

        $col = $input['order'][0]['column'];
        $colorder = $input['order'][0]['dir'];
        $columnorder = $input['columns'][$col]['name'];
        if ($columnorder == 'category') {
            $query = $query->orderBy('eventcategories.name', $colorder);
        } else {
            $query = $query->orderBy('events.' . $columnorder, $colorder);
        }
        $count = $query->count();
        $query = $query->skip($start)->limit($length);
        $query = $query->select('events.id', 'events.name', 'events.slug', 'events.is_registration', 'events.user_id', 'events.banner_url', 'events.created_at', 'events.start_date', 'eventcategories.name as category', 'events.status');

        // Log::info($query->toSql());
        $events = $query->get();
        //        dd($events);
        //        return view('protected.admin.events.index',compact('events'));

        return datatables()->of($events)->addIndexColumn()->editColumn('banner_url', function ($event) {
            if (!empty($event->banner_url)) {
                $image = '/uploads/events/' . $event->banner_url;
                $html = '<img src="' . $image . '" width="50px" height="50px"/>';
            } else {
                $html = '';
            }
            return $html;
        })->addColumn('registrations', function ($event) {
            if ($event->is_registration == 1) {
                return '<a href="' . url('admin/events/' . $event->id . '/registrations') . '">Registrations</a>';
            } else {
                return '';
            }
        })->addColumn('user_name', function ($event) {
            return getUserName($event->user_id);
        })->addColumn('status', function ($event) {
            $st = '<select name="status" class=" form-control status" data-style="btn-outline-primary pt-0 pb-0" data-id=' . $event->id . ' data-status=' . $event->status . '><option value="0" ';
            if ($event->status == 0) {
                $st = $st . 'selected';
            }
            $st = $st . '>Pending</option><option value="1" ';
            if ($event->status == 1) {
                $st = $st . 'selected';
            }
            $st = $st . '>Active</option></select>';
            return $st;
        })->addColumn('actions', function ($event) {
            return ' <a class="btn  btn-sm btn-info" href="' . url('event/' . $event->slug) . '" data-toggle="tooltip" title="View" target="_blank"><i class="ion-eye"></i></a>&nbsp;<a class="btn  btn-sm btn-success" href="' . url('admin/events/' . $event->id . '/edit') . '" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>&nbsp;<a href="javascript:void(0);"   class="btn btn-sm btn-danger delete  id_' . $event->id . '" data-id="' . $event->id . '" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>';
        })->skipPaging()->escapeColumns([''])->setTotalRecords($count)->make(true);
    }

    public function create()
    {
        $category_list = $this->category->getList();
        $committees = Committee::where('type', 'standing')->where('status', 1)->pluck('name', 'id')->toArray();
        return view('protected.admin.events.create', compact('category_list', 'committees'));
    }

    public function store(Request $request)
    {
        if (!$request->has('is_registration')) {
            $request->merge(['is_registration' => 0]);
        }
        if ($request->file('banner_url')) {
            $image              = $request->file('banner_url');
            $destinationPath    = 'uploads/events';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            // $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['banner_url' => $filename]);
        }
        // $request->merge(['slug'=>str_slug($request->input('name'))]);

        $request->merge(['user_id' => Sentinel::getUser()->id]);
        if (!$request->get('committees_id')) {
            $request->merge(['committees_id' => 10]);
        }
        if ($request->input('end_time') != '') {
            $request->merge(['end_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('end_time')))]);
        }
        if ($request->input('start_time') != '') {
            $request->merge(['start_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('start_time')))]);
        }
        $event = $this->event->create($request->input());
        if ($request->get('is_registration') == 1) {
            $registration_types = $request->only('regtype');

            if (isset($registration_types['regtype']) && count($registration_types['regtype'])) {
                foreach ($registration_types['regtype'] as $key => $type) {
                    if ($request->input('regname_' . $type)) {
                        $regtypes = array();
                        $regtypes['name'] = $request->input('regname_' . $type);
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $request->input('amount_' . $type);
                        $regtypes['reg_count'] = $request->input('reg_count_' . $type);
                        $regtypes['order_no'] = $request->input('order_no_' . $type);
                        $this->eventregistrationtype->create($regtypes);
                    }
                }
            }


            if (isset($input['sponsortype']) && count($input['sponsortype'])) {
                foreach ($input['sponsortype'] as $key => $type) {
                    if ($input['sponsortype_' . $type]) {
                        $regtypes = array();
                        $regtypes['name'] = $input['sponsortype_' . $type];
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $input['sponsoramount_' . $type];
                        $regtypes['order_no'] = $input['sponsororder_no_' . $type];
                        $this->sponsorship->create($regtypes);
                    }
                }
            }
        }
        return redirect()->route("events.index")->withSuccess('Event has been added successfully!');
    }

    public function edit($id)
    {
        $event = $this->event->find($id);
        $category_list = $this->category->getList();
        return view('protected.admin.events.edit', compact('event', 'category_list'));
    }

    public function update(Request $request, $id)
    {

        $input = $request->input();

        //dd($request->input());
        $event = $this->event->find($id);
        if (!$request->has('is_registration'))
            $request->merge(['is_registration' => 0]);
        if ($request->file('banner_url')) {
            $image              = $request->file('banner_url');
            $destinationPath    = 'uploads/events';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            // $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['banner_url' => $filename]);
        }
        if ($request->get('is_registration') == 1) {
            if (isset($input['regtype']) && count($input['regtype'])) {
                foreach ($input['regtype'] as $key => $type) {
                    if (isset($input['regid'][$key])) {
                        $regtypes = array();
                        $regtypes['name'] = $input['regname2_' . $type];
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $input['amount2_' . $type];
                        $regtypes['reg_count'] = $input['reg_count2_' . $type];
                        $regtypes['order_no'] = $input['order_no2_' . $type];
                        $regtype = $this->eventregistrationtype->find($type);
                        $regtype->fill($regtypes)->save();
                    } else {
                        $regtypes = array();
                        $regtypes['name'] = $input['regname_' . $type];
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $input['amount_' . $type];
                        $regtypes['reg_count'] = $input['reg_count_' . $type];
                        $regtypes['order_no'] = $input['order_no_' . $type];
                        $this->eventregistrationtype->create($regtypes);
                    }
                }
            }

            if (isset($input['sponsortype']) && count($input['sponsortype'])) {
                foreach ($input['sponsortype'] as $key => $type) {
                    if (isset($input['sponsorid'][$key])) {
                        $regtypes = array();
                        $regtypes['name'] = $input['sponsortype2_' . $type];
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $input['sponsoramount2_' . $type];
                        $regtypes['order_no'] = $input['sponsororder_no2_' . $type];
                        $rtype = $this->sponsorship->find($type);
                        $rtype->fill($regtypes)->save();
                    } else {
                        $regtypes = array();
                        $regtypes['name'] = $input['sponsortype_' . $type];
                        $regtypes['event_id'] = $event->id;
                        $regtypes['amount'] = $input['sponsoramount_' . $type];
                        $regtypes['order_no'] = $input['sponsororder_no_' . $type];
                        $this->sponsorship->create($regtypes);
                    }
                }
            }
        }

        $request->merge(['slug' => str_slug($request->input('slug'), '-')]);


        if ($request->input('end_time') != '') {
            $request->merge(['end_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('end_time')))]);
        }
        if ($request->input('start_time') != '') {
            $request->merge(['start_date' => date('Y-m-d H:i', strtotime($request->input('start_date') . ' ' . $request->input('start_time')))]);
        }
        $event->fill($request->input())->save();
        return redirect()->route("events.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->event->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Event::whereId($id)->update(['status' => $status]);
    }

    public function deleteEventRegtype(Request $request)
    {
        $id = $request->get('id');
        $this->eventregistrationtype->delete($id);
    }
    public function deleteEventSponsortype(Request $request)
    {
        $id = $request->get('id');
        $this->sponsorship->delete($id);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        Event::whereId($id)->update(['banner_url' => '']);
    }
}
