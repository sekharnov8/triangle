<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BannerRequest;
use App\Models\Banner;
use App\Repositories\Banner\BannerRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;

class AdminBannerController extends Controller
{
    public function __construct(BannerRepositoryInterface $banner)
    {
        $this->banner = $banner;
    }

    public function index()
    {
        $banners = $this->banner->getAll();
        return view('protected.admin.banners.index', compact('banners'));
    }

    public function create()
    {
        $maxno = $this->banner->getMax();
        return view('protected.admin.banners.create', compact('maxno'));
    }

    public function store(Request $request)
    {
        $request->merge(['updated_by' => Sentinel::getUser()->id]);

        if ($request->file('image')) {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/banners';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            $filename2 = time() . '_' . $filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image' => $filename]);
        }

        $this->banner->create($request->input());

        return redirect()->route("banners.index")->withSuccess('Banner has been added successfully!');
    }

    public function edit($id)
    {
        $banner = $this->banner->find($id);
        return view('protected.admin.banners.edit', compact('banner'));
    }

    public function update(Request $request, $id)
    {

        $banner = $this->banner->find($id);
        if ($request->file('image')) {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/banners';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ', '_', $filename);
            $filename2 = time() . '_' . $filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image' => $filename]);
        }
        $banner->fill($request->input())->save();
        return redirect()->route("banners.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->banner->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Banner::whereId($id)->update(['status' => $status]);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        Banner::whereId($id)->update(['image' => '']);
    }
}
