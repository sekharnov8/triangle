<?php

namespace App\Http\Controllers\Admin;

use App\Models\Photo;
use App\Models\PhotoCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PhotoRequest;
use App\Repositories\Photo\PhotoRepositoryInterface;
use App\Repositories\Photo\PhotoCategoryRepositoryInterface;
use Input;
use Sentinel;
use App\Http\Requests;

class AdminPhotosController extends Controller {

    public function __construct(PhotoRepositoryInterface $photo, PhotoCategoryRepositoryInterface $category)
    {
        $this->photo = $photo;
        $this->category = $category;
    }

    public function index()
    {
        $photos = $this->photo->getAll();
        return view('protected.admin.photos.index', compact('photos'));
    }

    public function create()
    {
        $categories = $this->category->getByParentId(0);
          return view('protected.admin.photos.create', compact('categories'));
    }

    public function store(Request $request)
    {

        $files = $request->file('images');
        $file_count = count($files);
         $uploadcount = 0;
         if($file_count>0)
         {
          foreach($files as $file) {
             $input2=$request->input();
            $destinationPath = 'uploads/photos/';
            $uploadcount++;
            $filename =time().'_'.$uploadcount . '.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
            $input2['image_url']=$filename;
            $this->photo->create($input2);
            }
        }
        return redirect()->route("photos.index")->withSuccess('Added photos ('.$uploadcount.') successfully!!!');

    }

    public function edit($id)
    {
        $photo = $this->photo->find($id);
        $category = $this->category->find($photo->photocategory_id);
        $categories = $this->category->getList();
        return view('protected.admin.photos.edit', compact('photo','categories'));
    }

    public function update(Request $request, $id)
    {

        $photo= $this->photo->find($id);
        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/photos';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image_url'=>$filename]);
        }
         $photo->fill($request->input())->save();
        return redirect()->route("photos.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->photo->delete($id);
    }

    public function PostPhotoCategories(Request $request)
    {
        $categories = $this->category->getList();
        return $categories;
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');

        Photo::whereId($id)->update(['status' => $status]);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        Photo::whereId($id)->update(['image_url' => '']);
    }
}
?>