<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\VideoRequest;
use App\Repositories\Video\VideoRepositoryInterface;
use App\Repositories\Video\VideoCategoryRepositoryInterface;
use Input;
use Sentinel;
use App\Http\Requests;
use App\Models\Video;
class AdminVideosController extends Controller {

    public function __construct(VideoRepositoryInterface $video, VideoCategoryRepositoryInterface $category)
    {
        $this->video = $video;
        $this->category = $category;
    }

    public function index()
    {
        $videos = $this->video->getAll();
        return view('protected.admin.videos.index', compact('videos'));
    }

    public function create()
    {
        $categories = $this->category->getList();
        return view('protected.admin.videos.create', compact('categories'));
    }

    public function store(Request $request)
    {

       $request->merge(['updated_by'=>Sentinel::getUser()->id]);
       $request->merge(['video_url'=>get_youtube_id_from_url($request->input('video_url'))]);

       $this->video->create($request->input());

        return redirect()->route("videos.index")->withSuccess('Video has been added successfully!');
    }

    public function edit($id)
    {
        $video = $this->video->find($id);
        $category = $this->category->find($video->videocategory_id);
 
        $categories = $this->category->getList();

        return view('protected.admin.videos.edit', compact('video','categories'));
    }

    public function update(Request $request, $id)
    {

        $video= $this->video->find($id);
       $request->merge(['video_url'=>get_youtube_id_from_url($request->input('video_url'))]);
        $video->fill($request->input())->save();
        return redirect()->route("videos.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->video->delete($id);
     }

    public function PostVideoCategories(Request $request)
    {
        $categories = $this->category->getList();
        return $categories;
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');

        Video::whereId($id)->update(['status' => $status]);
    }
}

?>