<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sentinel;
use Input;
use Excel;
use Datatables;
use Log;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('protected.admin.user.index');
    }
    public function filterData(Request $request)
    {
        $input=$request->input();
        $start=$input['start'];
        $length=$input['length'];
        $datefrom=$input['datefrom'];
        $dateto=$input['dateto'];
        $role = Sentinel::findRoleByName('user');
        $query = $role->users();
        $keyword=$input['keyword'];
        if($keyword)
        {
            foreach($input['columns'] as $key=>$column) {
                if($column['searchable']=='true')
                {
                    if($key==0)
                {
                     $query=$query->where($column['name'],'like','%'.$keyword.'%');
                 }
                 else
                 {
                    if($column['name']=='created_at')
                    $query=$query->orWhere('role_users.'.$column['name'],'like','%'.$keyword.'%');
                     else
                    $query=$query->orWhere($column['name'],'like','%'.$keyword.'%');
                 }
                }
            }
        }
        if($datefrom && $dateto)
            $query=$query->whereBetween('role_users.created_at',array($datefrom.' 00:00:01', $dateto.' 23:59:59'));

        $col=$input['order'][0]['column'];
        $colorder=$input['order'][0]['dir'];
        $columnorder=$input['columns'][$col]['name'];
        $count=$query->count();
        $query=$query->skip($start)->limit($length);
        $query=$query->orderBy($columnorder,$colorder);
        $query=$query->select('id', 'name', 'email','mobile','last_login','role_users.created_at');
        // Log::info($query->toSql());
        // dd($query->toSql());
        $users=$query->get();
             return datatables()->of($users)->addColumn('boqs', function ($users) { return '<a href="'.url('admin/userfile/'.$users->id).'">BOQs('.getUserBoqCount($users->id).')';})->addColumn('actions', function ($users) { return '<a href="javascript:void(0);" onclick="deleteRecords('.$users->id.')" class="btn btn-xs btn-danger  id_'.$users->id.'" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>';})->skipPaging()->escapeColumns([''])->setTotalRecords($count)->make(true);
    }


    public function getAdmins()
    {
        $role = Sentinel::findRoleByName('admin');
        $users = $role->users()->with('roles')->get();
        return view('protected.admin.user.admins', compact('users'));
    }

     public function getUsersExcelExport()
    {
        $filter=Input::only('datefrom','dateto');
        $role = Sentinel::findRoleByName('user');
        $query = $role->users();
        if($filter['datefrom']!='' && $filter['dateto']!='')
            $query->whereBetween('role_users.created_at',array($filter['datefrom'].' 00:00:01', $filter['dateto'].' 23:59:59'));
        $users= $query->select('id','name','mobile','email','last_login','role_users.created_at')->orderBy('id','desc')->get();
        Excel::create('UsersList', function($excel) use($users) {
            $excel->sheet('Users', function($sheet) use($users) {
                $sheet->loadview('protected.admin.user.export_users', ['users' => $users]);
            });
        })->export('xls');
        return back()->withFlashSuccessMessage('Users exported as Excel successfully!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::find($id);
        return view('admin/user/view',compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id', $id)->delete();
        return back()->withFlashSuccessMessage('Deleted successfully!');
    }
}
