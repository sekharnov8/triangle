<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Member\OrderRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Member\MembershiptypeRepositoryInterface;
use App\Repositories\Status\StatusTypeRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Order;
use App\Models\PaymentMethod;
use Excel;
use Datatables;
use DB;
use Log;
use App\Exports\OrdersExport;

class AdminOrdersController extends Controller
{

    public function __construct( OrderRepositoryInterface $morder, MembershiptypeRepositoryInterface $mtype, UserRepositoryInterface $user, StatusTypeRepositoryInterface $statustype)
    {
        $this->morder = $morder;
        $this->user = $user;
        $this->mtype = $mtype;
        $this->statustype = $statustype;
    }
    public function index()
    {
        $statustypes = $this->statustype->getByStatusCategoryId(1);
        $paymentmethods = PaymentMethod::pluck('name', 'id')->toArray();
        $mtypes = $this->mtype->getList();
        return view('protected.admin.memberorders.index', compact('mtypes', 'statustypes', 'paymentmethods'));
    }
    public function filterData(Request $request)
    {
        $input = $request->input();
        $start = $input['start'];
        $length = $input['length'];
        $datefrom = $input['datefrom'];
        $dateto = $input['dateto'];
        $status = $input['status'];
        $mtype = $input['mtype'];
        $paymentmethod = $input['paymentmethod'];
        $query = Order::leftJoin('membershiptypes', 'membershiptypes.id', '=', 'orders.membershiptype_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->leftJoin('paymentmethods', 'paymentmethods.id', '=', 'orders.paymentmethod_id');
        $keyword = $input['keyword'];
        if ($keyword) {
            $query->where(function ($query) use ($keyword, $input) {
                foreach ($input['columns'] as $key => $column) {
                    if ($column['searchable'] == 'true') {
                        if ($column['name'] == 'membership') {
                            $query->orWhere('membershiptypes.name', 'like', '%' . $keyword . '%');
                        } else if ($column['name'] == 'paymentmethod') {
                            $query->orWhere('paymentmethods.name', 'like', '%' . $keyword . '%');
                        } else if ($column['name'] == 'user_name') {
                            $query->orWhere('users.first_name', 'like', '%' . $keyword . '%')->orWhere('users.last_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->orWhere('orders.' . $column['name'], 'like', '%' . $keyword . '%');
                        }
                    }
                }
            });
        }
        if ($datefrom && $dateto) {
            $query = $query->whereBetween('orders.created_at', array($datefrom . ' 00:00:01', $dateto . ' 23:59:59'));
        }
        if ($status != '') {
            $query = $query->where('orders.status', $status);
        }
        if ($paymentmethod != '') {
            $query = $query->where('orders.paymentmethod_id', $paymentmethod);
        }
        if ($mtype != '') {
            $query = $query->where('orders.membershiptype_id', $mtype);
        }

        $col = $input['order'][0]['column'];
        $colorder = $input['order'][0]['dir'];
        $columnorder = $input['columns'][$col]['name'];
        if ($columnorder == 'paymentmethod') {
            $query = $query->orderBy('paymentmethods.name', $colorder);
        } else if ($columnorder == 'membership') {
            $query = $query->orderBy('membershiptypes.name', $colorder);
        } else if ($columnorder == 'user_name') {
            $query = $query->orderBy('users.first_name', $colorder);
        } else {
            $query = $query->orderBy('orders.' . $columnorder, $colorder);
        }

        $count = $query->count();
        $query = $query->skip($start)->limit($length);
        $query = $query->select(
            'orders.id',
            'orders.status',
            'orders.transaction_id',
            'orders.amount',
            'orders.updated_at',
            DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'),
            'users.member_id',
            'users.id as user_id',
            'membershiptypes.name as membership',
            'paymentmethods.name as paymentmethod',
            'orders.paymentmethod_id'
        );

        // dd($query->toSql());
        $morders = $query->get();
        $statustypes = $this->statustype->getByStatusCategoryId(1);

        return datatables()->of($morders)
            ->addIndexColumn()
            ->addColumn('cb', function ($contacts) {
                $st = '<input name="checkall[]" type="checkbox" class="checkall" value="' . $contacts->id . '">';
                return $st;
            })->addColumn('paymentstatus', function ($morder) {
                return ($morder->status == 1) ? 'Completed' : 'Pending';
            })->addColumn('actions', function ($morder) {
                $actiondata = '';
                $actiondata = $actiondata . '<a href="javascript: void(0);" class="btn edit btn-sm btn-warning"  data-id="' . $morder->id . '" data-toggle="tooltip" title="Edit Order"><i class="ion-edit"></i></a>&nbsp;<a href="javascript:void(0);" class="btn btn-sm btn-danger delete  id_' . $morder->id . '" data-id="' . $morder->id . '" data-toggle="tooltip" title="Delete"><i class="fa fa-trash fa-lg"></i></a>';
                return $actiondata;
            })
            ->editColumn('user_id', function ($morder) {
                return '<a href="javascript:void(0)" data-id="' . $morder->user_id . '" class="r-m10 view"  data-tip="tooltip" title="View Member Details">' . $morder->user_id . '</a>';
            })
            ->editColumn('updated_at', function ($morder) {
                return dateformat($morder->updated_at, 1);
            })
            ->skipPaging()->escapeColumns([''])->setTotalRecords($count)->make(true);
    }

    public function deleteChecked(Request $request)
    {
        $ids = $request->get('checkall');

        if ($ids && count($ids)) {
            Order::whereIn('id', $ids)->update(array('deleted_at' => date('y-m-d H:i:s')));
            return redirect()->route("memberorders.index")->withSuccess('Deleted ' . $count . ' selected orders successfully!!!');
        } else {
            return redirect()->route("memberorders.index")->withError('No checkboxes selected to delete!!!');
        }
    }

    public function memberOrderList($id)
    {
        $user = $this->user->find($id);
        if ($user) {
            $query = Order::leftJoin('users', 'users.id', '=', 'orders.user_id')
                ->leftJoin('membershiptypes', 'membershiptypes.id', '=', 'orders.membershiptype_id')
                ->leftJoin('paymentmethods', 'paymentmethods.id', '=', 'orders.paymentmethod_id');
            $query = $query->where('orders.user_id', $user->id)->select(
                'orders.id',
                'orders.status',
                'orders.transaction_id',
                'orders.amount',
                'orders.updated_at',
                DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'),
                'membershiptypes.name as membership',
                'paymentmethods.name as paymentmethod'
            );
            $morders = $query->get();
            return view('protected.admin.memberorders.memberorders', compact('morders'));
        }
    }

    public function create()
    {
        $category_list = $this->category->getByParentId(0);
        return view('protected.admin.memberorders.create', compact('category_list'));
    }
    public function store(Request $request)
    {
        $request->merge(['updated_by' => Sentinel::getUser()->id]);
        $this->morder->create($request->input());

        return redirect()->route("memberorders.index")->withSuccess('Member order has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $morder = $this->morder->find($id);
        $statustypes = $this->statustype->getByStatusCategoryId(1);

        if ($request->ajax()) {
            return view('protected.admin.memberorders.edit_modal', compact('morder', 'statustypes'))->render();
        } else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $morder = $this->morder->find($id);
        $request->merge(['updated_by' => Sentinel::getUser()->id]);
        if ($request->get('status') == 1 && $morder->status != 1) {
            $user = User::whereId($morder->user_id)->update(['membershiptype_id' => $morder->membershiptype_id]);
        }
        $morder->fill($request->input())->save();

        return redirect()->route("memberorders.index")->withSuccess('Member Order Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->morder->delete($id);
        // return back()->withSuccess('Deleted successfully!!!');
    }

    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        $order = $this->morder->find($id);
        Order::whereId($id)->update(['status' => $status]);
        if ($status == 1) {
            User::whereId($order->user_id)->update(['membershiptype_id' => $order->membershiptype_id]);
        }
    }

    public function getOrdersExcelExport(Request $request)
    {
        $filter = $request->input();
        return app()->make(OrdersExport::class)->forFilter($filter)->download('orders.xlsx');
    }
}
