<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Status\StatusCategoryRepositoryInterface;
use App\Repositories\Status\StatusTypeRepositoryInterface;
use Input;
use Sentinel;
use Illuminate\Http\Request;
use App\Models\StatusType;

class AdminStatusTypesController  extends Controller {

    public function __construct( StatusCategoryRepositoryInterface $category, StatusTypeRepositoryInterface $statustype)
    {

        $this->category = $category;
        $this->statustype = $statustype;
    }

    public function index()
    {
        $statustypes = $this->statustype->getAll();
        $categories = $this->category->getList();
        return view('protected.admin.statustypes.index', compact('statustypes','categories'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $this->statustype->create($request->input());
        return redirect()->route("status-types.index")->withSuccess('Status Type  has been added successfully!');
    }

    public function edit(Request $request, $id)
    {
        $statustype = $this->statustype->find($id);
        $categories = $this->category->getList();
        if($request->ajax()){
            return view('protected.admin.statustypes.edit_modal', compact('statustype','categories'))->render();
        }else {
            abort(404);
        }

    }

    public function update(Request $request, $id)
    {

        $statustype= $this->statustype->find($id);
        $statustype->fill($request->input())->save();
        return redirect()->route("status-types.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->statustype->delete($id);
    }

}
?>