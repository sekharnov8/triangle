<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PageCategoryRequest;
use App\Repositories\Page\PageCategoryRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\PageCategory;
use App\Models\Page;

class AdminPageCategoriesController extends Controller {

    public function __invoke()
    {
    }

    public function __construct(PageCategoryRepositoryInterface $category)
    {
        $this->category = $category;
    }

    public function index()
    {
         $categories = $this->category->getAll();
        $category_list = $this->category->getByParentId(0);
        $maxno=$this->category->getMax();
        $pages_list=Page::where('status',1)->pluck('name','id');
         return view('protected.admin.pagecategories.index', compact('categories','category_list','maxno','pages_list'));
    }

    public function store(Request $request)
    {
        $count =PageCategory::where('name',$request->input('name'))->where('page_parentid',$request->input('page_parentid'))->count();
        if($count)
         {
            return redirect()->route("page-categories.index")->withError($request->input('name').' as Menu Already Exists!');
         }
       $request->merge(['updated_by'=>Sentinel::getUser()->id]);
       $this->category->create($request->input());
        return redirect()->route("page-categories.index")->withSuccess('Category has been added successfully!');
    }

    public function edit(Request $request, $id)
    {

        $category = $this->category->find($id);

        $category_list = $this->category->getByParentId(0);
        if($request->ajax()){
            return view('protected.admin.pagecategories.edit_modal', compact('category','category_list'))->render();
        }else {
            abort(404);
        }

    }

    public function update(Request $request, $id)
    {
        if(!$request->has('is_menubar'))
            $request->merge(['is_menubar'=>0]);
        if(!$request->has('is_topbar'))
            $request->merge(['is_topbar'=>0]);

        $category= $this->category->find($id);
      //  dd($category);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);
        $category->fill($request->input())->save();
        return redirect()->route("page-categories.index")->withSuccess('Updated successfully!!!');
    }

    public function destroy($id)
    {
        $this->category->delete($id);
        return back()->withSuccess('Deleted successfully!!!');
    }

     public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        PageCategory::whereId($id)->update(['status' => $status]);
    }
    public function postLinkAdd(Request $request)
    {
         $id_path = $request->get('id_path');
        $id = $request->get('category_id');
        PageCategory::whereId($id)->update(['id_path' => $id_path]);
         return redirect()->route("page-categories.index")->withSuccess('Updated successfully!!!');

    }
}

?>