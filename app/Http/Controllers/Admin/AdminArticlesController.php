<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ArticleRequest;
use App\Repositories\Article\ArticleRepositoryInterface;
use Input;
use Illuminate\Http\Request;
use Sentinel;
use App\Models\Article;

class AdminArticlesController extends Controller {

    public function __construct(ArticleRepositoryInterface $article)
    {
        $this->article = $article;
    }

    public function index()
    {
        $articles = $this->article->getAll();
        return view('protected.admin.articles.index', compact('articles'));
    }

    public function create()
    {
        $maxno=$this->article->getMax();
        return view('protected.admin.articles.create', compact('maxno'));
    }

    public function store(Request $request)
    {
        if($request->input('slug')=='')
            $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);

        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/articles';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image'=>$filename]);
        }
        if($request->input('post_date')=='')
        $request->merge(['post_date'=>date('Y-m-d')]);         

        $this->article->create($request->input());

        return redirect()->route("articles.index")->withSuccess('Article has been added successfully!');
    }

    public function edit($id)
    {
        $articles = $this->article->find($id);
        return view('protected.admin.articles.edit', compact('articles'));
    }

    public function update(Request $request, $id)
    {
        $articles= $this->article->find($id);
        if($request->input('slug')=='')
            $request->merge(['slug'=>str_slug($request->input('name'), '-')]);
        $request->merge(['updated_by'=>Sentinel::getUser()->id]);

        if($request->file('image'))
        {
            $image              = $request->file('image');
            $destinationPath    = 'uploads/articles';
            $filename           = $image->getClientOriginalName();
            $filename           = str_replace(' ','_',$filename);
            $filename2=time().'_'.$filename;
            $image->move($destinationPath, $filename);
            $request->merge(['image'=>$filename]);
        }

        $articles->fill($request->input())->save();
        return redirect()->route("articles.index")->withSuccess('Updated successfully!!!');
    }


    public function destroy($id)
    {
        $this->article->delete($id);
    }
    public function changeStatus(Request $request)
    {
        $status = $request->get('status');
        $id = $request->get('id');
        Article::whereId($id)->update(['status' => $status]);
    }
    public function deleteimage(Request $request)
    {
        $id = $request->get('id');
        Article::whereId($id)->update(['image' => '']);
    }
}
