<?php

/**
 * Part of the Sentinel Kickstart application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel Kickstart
 * @version    5.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2017, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\URL;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Addons\Social\Laravel\Facades\Social;

class OAuthController extends Controller
{
    /**
     * Shows a link to authenticate a service.
     *
     * @param  string  $slug
     * @return string
     */
    public function getAuthorize($slug)
    {
        $url = Social::getAuthorizationUrl($slug, URL::to("oauth/callback/{$slug}"));

        return redirect($url);
    }

    /**
     * Handles authentication
     *
     * @param  string  $slug
     * @return mixed
     */
    public function getCallback($slug)
    {
        try {
            $user = Social::authenticate($slug, URL::current(), function ($link, $provider, $token, $slug) {
                // Callback after user is linked
            });

            return redirect('oauth/authenticated');
        } catch (Exception $e) {
            return redirect(route('user.login'))->withErrors($e->getMessage());
        }
    }

    /**
     * Returns the "authenticated" view which simply shows the
     * authenticated user.
     *
     * @return mixed
     */
    public function getAuthenticated()
    {
        if (! Sentinel::check()) {
            return redirect(route('user.login'))->withErrors('Not authenticated yet.');
        }

        return redirect(route('account.index'))->withSuccess('Successfully logged in.');
    }
}
