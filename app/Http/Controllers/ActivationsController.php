<?php

/**
 * Part of the Sentinel Kickstart application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Sentinel Kickstart
 * @version    5.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2017, Cartalyst LLC
 * @link       http://cartalyst.com
 */

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

class ActivationsController extends BaseController
{
    /**
     * The Sentinel Activations repository.
     *
     * @var \Cartalyst\Sentinel\Activations\ActivationRepositoryInterface
     */
    protected $activation;

    /**
     * The Sentinel Users repository.
     *
     * @var \Cartalyst\Sentinel\Users\UserRepositoryInterface
     */
    protected $users;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->users = Sentinel::getUserRepository();

        $this->activation = Sentinel::getActivationRepository();
    }

    /**
     * Activates the given user using the given code.
     *
     * @param  string  $id
     * @param  string  $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($id, $code)
    {
        $user = $this->users->find($id);

        if (! $this->activation->complete($user, $code)) {
            return redirect(route('user.login'))->withErrors('Invalid or expired activation code.');
        }

        return redirect(route('user.login'))->withSuccess('Account activated.');
    }

    /**
     * Reactivate the given user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reactivate($id)
    {
        $user = $this->users->find($id);

        $activation = $this->activation->exists($user) ?: $this->activation->create($user);

        if ($this->activation->complete($user, $activation->code)) {
            return redirect(route('user.edit', $id))->withSuccess(
                trans('users/messages.success.activate')
            );
        }

        return redirect(route('user.edit', $id))->withErrors(
            trans('users/messages.error.activate')
        );
    }

    /**
     * Deactivates the given user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deactivate($id)
    {
        $this->activation->remove(
            $this->users->find($id)
        );

        return redirect(route('user.edit', $id))->withSuccess(
            trans('users/messages.success.deactivate')
        );
    }

    public function bulkActions(Request $request)
    {

        $users = $request->get('users');

        if($users){

            if($request->has('activate')){

                foreach($users as $u){

                    $user = $this->users->find($u);

                    $activation = $this->activation->exists($user) ?: $this->activation->create($user);

                    $this->activation->complete($user, $activation->code);
                }

            }

            if($request->has('deactivate')){

                foreach($users as $u){

                    $this->activation->remove(
                        $this->users->find($u)
                    );

                }

            }

        }

        return redirect(route('users.index'));

    }
}
