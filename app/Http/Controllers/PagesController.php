<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Log;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Page\PageCategoryRepositoryInterface;
use App\Repositories\Sponsor\SponsorCategoryRepositoryInterface;
use App\Repositories\Photo\PhotoCategoryRepositoryInterface;
use App\Repositories\Video\VideoCategoryRepositoryInterface;
use App\Repositories\News\NewsRepositoryInterface;
use App\Repositories\Event\EventRepositoryInterface;
use App\Repositories\Committee\CommitteeRepositoryInterface;
use App\Repositories\Committee\CommitteeMemberRepositoryInterface;
use App\Repositories\Subscribers\SubscribersRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Contact\ContactRepositoryInterface;
use App\Repositories\Contact\ContactProgramRepositoryInterface;
use App\Repositories\Article\ArticleRepositoryInterface;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface;
use Session;
use Illuminate\Validation\ValidationException;
use App\Models\Page;
use App\Models\Contact;
use App\Models\PhotoCategory;
use DB;
use Mail;

class PagesController extends BaseController
{
    protected $page;

    public function __construct(ContactRepositoryInterface $contact, PageRepositoryInterface $page,CommitteeRepositoryInterface $committee,CommitteeMemberRepositoryInterface $committeemember, SponsorCategoryRepositoryInterface $sponsorcategory, PhotoCategoryRepositoryInterface $photocategory, VideoCategoryRepositoryInterface $videocategory, NewsRepositoryInterface $news,SubscribersRepositoryInterface $subscribers, EventRepositoryInterface $events,UserRepositoryInterface $user, PageCategoryRepositoryInterface $pagecategory, ContactProgramRepositoryInterface $program, ArticleRepositoryInterface $article,  MailTemplateRepositoryInterface $template)
    {
        $this->page = $page;
        $this->sponsorcategory = $sponsorcategory;
        $this->photocategory = $photocategory;
        $this->videocategory = $videocategory;
        $this->pagecategory = $pagecategory;
        $this->news = $news;
        $this->contact = $contact;
        $this->events = $events;
        $this->committee = $committee;
        $this->committeemember = $committeemember;
        $this->subscribers = $subscribers;
        $this->user = $user;
        $this->template= $template;
        $this->program = $program;
        $this->article = $article;
    }

    public function getPage($slug)
    {
        $page = $this->page->findBySlug($slug);
        if ($page && $page->status==1) {
            if($page->is_login && !Sentinel::getUser())
            {
                 Session::put('redirect_page', $slug);
                 return redirect('login');
            }          
            return view('pages.page', compact('page'));
        }
        else {
            abort(404);
        }
    }
    public function getResources()
    {
        $slug='resources';
        $page = $this->page->findBySlug($slug);
        if ($page) {
            $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
            return view('pages.page', compact('page','catitems','category'));
        } else {
            abort(404);
        }
    } 
    public function getLeadership($slug)
    {
        $committee = $this->committee->findBySlug($slug);
        if($committee)
        {
            $category=$this->pagecategory->findByName('Leadership');
            if($slug=='standing-committees')
            {
                $committees=$this->committee->getStanding();
                 return view('pages.standing_committees', compact('committee','committees','category','roles'));
            } 
            else
            {
                $chair = $this->committeemember->getChair($committee->id);
                $members = $this->committeemember->getMembers($committee->id);
                 return view('pages.leadership', compact('committee','members','chair','category'));
            }
        }
        else {
            abort(404);
        }
    }
    public function getSponsors()
    {

            $category=$this->pagecategory->findByName('Leadership');
             $sponsors=$this->sponsorcategory->getAllActive()->where('category_name', '<>','Media Partners');
            return view('pages.sponsors', compact('category','sponsors'));
        
    }
    public function getDonors()
    {         
            $category=$this->pagecategory->findByName('Leadership');
            $sponsors=$this->sponsorcategory->getByName('Donors');
            return view('pages.donors', compact('category','sponsors')); 
    }
    
     public function getPhotos()
    {
        $page = $this->page->findBySlug('photos');
         if ($page) {
            $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
            $categories=PhotoCategory::with('subitems','photos')->where('parent_id',0)->where('status',1)->get();
            return view('pages.photos', compact('page','category','catitems','categories'));
        } else {
            abort(404);
        }
    }
     public function getPictures($slug)
    {
        $page = $this->page->findBySlug('photos');
        $photocategory = $this->photocategory->findBySlug($slug);
          if ($photocategory) {
            $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
            return view('pages.pictures', compact('page','category','catitems','photocategory'));
        } else {
            abort(404);
        }
    }
     public function getVideos()
    {
        $page = $this->page->findBySlug('videos');
         if ($page) {
            $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
            $videos=$this->videocategory->getAllActive();
            return view('pages.videos', compact('page','category','catitems','videos'));
        } else {
            abort(404);
        }
    }
    public function getJoin()
    {
         $page = $this->page->findBySlug('join-us');
         if ($page) {
            $pages=$this->page->getByCategoryId($page->pagecategory_id);
            return view('pages.join-us', compact('page','pages'));
        } else {
            abort(404);
        }
    }
     public function getNews()
    {
         $page = $this->page->findBySlug('news');
         if ($page) {
            $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
            $news=$this->news->getActive();

            return view('pages.news', compact('page','category','news','catitems'));
        } else {
            abort(404);
        }
    }
    public function getArticles()
    {
         $page = $this->page->findBySlug('blog');
         if ($page) {
            $articles=$this->article->getActive();
            return view('pages.articles', compact('page','articles'));
        } else {
            abort(404);
        }
    }
     
    public function getEvents()
    {
        $page = $this->page->findBySlug('events');
        if ($page) {
            $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
            $events=$this->events->getUpcomingEvents();
            return view('events.events', compact('page','category','catitems','events'));
        } else {
            abort(404);
        }
    }
    public function getPastEvents()
    {
        $page = $this->page->findBySlug('events');
        if ($page) {
            $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
            $events=$this->events->getPastEvents();
            return view('events.past-events', compact('page','category','catitems','events'));
        } else {
            abort(404);
        }
    }

    public function getContactUs()
    {
        $page = $this->page->findBySlug('contact-us');
        $programs = $this->program->getList();
        $category=$this->pagecategory->find($page->pagecategory_id);
            $catitems=$category->subitems()->where('status',1)->orderBy('position')->get();
        if ($page) {
            return view('pages.contact_us',compact('programs','page','catitems','category'));
        } else {
            abort(404);
        }
    }
    public function contactus(Request $request)
    {
        $input=$request->input();
        $is_duplicate=Contact::where('program',$input['program'])->where('name',$input['name'])->where('email',$input['email'])->where('mobile',$input['mobile'])->where('city',$input['city'])->where('state',$input['state'])->where('address',$input['address'])->where('message',$input['message'])->first();
        if(!$is_duplicate)
        {
            $contact=$this->contact->create($request->input());
            $program=$this->program->find($request->get('program'));
            $subject="TUTA Get in Touch - ".$request->get('name');
            $content= view('emails.contact', compact('contact','program'));

            $mailcontent = array('mailbody' =>  $content);  

            // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message) use($program, $subject) {
            //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($program->email)->subject($subject);
            // });  
              sendmail_api($subject, $content, $program->email); 

 
            return 'success';
        }
        else
            return 'error';
    }

    public function PostSubscribe(Request $request)
    {
        $input=array();
        $email=$request->get('subscribemail');
        $input['email']=$email;
        $check=$this->subscribers->findbyEmail($email);
        if(!$check)
        {
            $this->subscribers->create($input);

            $template=$this->template->findByName('Subscribe');
             $content=$template->template;
             $subject=$template->subject;

        $content=str_replace('[ApplicationName]','Triangle United Telugu Association',$content);
        $content=str_replace('[siteurl]',url('/').'/',$content); 

            // $mailcontent = array('mailbody' =>  $content);  

            // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message) use($email, $subject) {
            //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($email)->subject($subject);
            // });  
             sendmail_api($subject, $content, $email); 
 
            return 'success';
        }
        else
            return 'Exists';
    }
    public function postSearch(Request $request)
    {
        $search_txt=$request->input('search');
        $keyword=$search_txt;

         $pages = DB::table('pages')
        ->where('name', 'LIKE', "%$search_txt%")
        ->whereNull('deleted_at')->whereStatus(1)
        ->select('name','slug','description')
        ->get();

    $articles = DB::table('articles')
                  ->where('name', 'LIKE', "%$search_txt%")
                  ->whereNull('deleted_at')
                  ->whereStatus(1)
                  ->select('name','slug','description')
                  ->get();

 $events = DB::table('events')
                  ->where('name', 'LIKE', "%$search_txt%")
                  ->whereNull('deleted_at')->whereStatus(1)
                  ->select('name','slug','event_details')
                  ->get();                  
        $page = $this->page->findBySlug('contact-us');

     return view('pages.search',compact('pages','page','articles','events','keyword'));
    }
}