<?php

namespace App\Http\Controllers;
use App\Models\Donation;
use App\Models\Service;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Page\PageCategoryRepositoryInterface;
use App\Repositories\Donation\DonationRepositoryInterface;
use App\Repositories\Donation\DonationCategoryRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Sentinel;
use Input;
use Session;
use URL;
use Redirect;
use App\Models\PaymentMethod;
use App\Models\User;
use Log;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Models\MailTemplate;
use Mail;
use DateTime;
use App\Models\Appinfo;

class ServiceController extends BaseController
{
    /**
     * Show the homepage.
     *
     * @return \Illuminate\View\View
     */
    protected $event;

    public function __construct(UserRepositoryInterface $user,DonationRepositoryInterface $donation,PageRepositoryInterface $page, DonationCategoryRepositoryInterface $category, PageCategoryRepositoryInterface $pagecategory )
    {
        $this->page = $page;
        $this->donation= $donation;
        $this->user = $user;
        $this->category = $category;
        $this->pagecategory = $pagecategory;

         $this->appinfo=Appinfo::where('id',1)->first(); 

        $settings = array(
        'mode' => $this->appinfo->paypal_mode,
        'http.ConnectionTimeOut' => 2000,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
        );

        if($this->appinfo->paypal_mode=='live')
        {
        $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_client_id, $this->appinfo->paypal_secret));
        }
        else
        {
        $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_sandbox_client_id, $this->appinfo->paypal_sandbox_secret));
        }
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);  

    } 
   
    public  function getDonation()
    {
        $slug='donation';
        $page = $this->page->findBySlug($slug);
        $payment_methods=PaymentMethod::where('status',1)->pluck('name','id')->toArray();
        $categories=$this->category->getList()->toArray();
         return view('services.service_donation', compact('payment_methods','categories'));
    } 

     public function CheckMemberEmail(Request $request)
    {
        $user = User::where('users.email', $request->get('email'))->get();

        if(count($user) == 1){
        $user = User::where('email', $request->get('email'))->select('id','zipcode','email','mobile','address','address2','city','state','users.first_name','users.last_name')->first();
                return $user;
        }else{
            return 0;
        }
    }

    public function postPayment(Request $request)
    {
        $input=$request->input();
        $user= Sentinel::getUser();
        $userCurrency = 'USD';
        $pay_amount =$input['amount'];
        if(!$input['user_id'] && $user)
        {
            $input['user_id']=$user->id;
        }
        $input['status']=0;
        $input['paymentmethod_id']=$input['paymentmethod_id'];
        $input['amount']=$pay_amount;

        if($request->input('cheque_date')  && strpos($request->input('cheque_date'), '/') !== false)
        {
        $cheque_date= DateTime::createFromFormat("m/d/Y" , $request->input('cheque_date'));
        $input['cheque_date']= $cheque_date->format('Y-m-d');
        }

        $donation=$this->donation->create($input);


        Session::put('donation_id',$donation->id);

        if($input['amount']>0)
        {


            if($input['paymentmethod_id']==1)
            { 
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');

                $item_1 = new Item();

                $item_1->setName('Item 1')
                    ->setCurrency($userCurrency)
                    ->setQuantity(1)
                    ->setPrice($pay_amount);

                $item_list = new ItemList();
                $item_list->setItems(array($item_1));

                $amount = new Amount();
                $amount->setCurrency($userCurrency)
                    ->setTotal($pay_amount);
               $timestamp=Carbon::now()->toDateTimeString();

                $transaction = new Transaction();
                $transaction->setAmount($amount)
                    ->setInvoiceNumber($timestamp.'_'.$donation->id)
                    ->setItemList($item_list)
                    ->setDescription('Transaction Details');


                $redirect_urls = new RedirectUrls();
                $redirect_urls->setReturnUrl(URL::route('donation.status'))
                    ->setCancelUrl(URL::route('donation.status'));
                $payment = new Payment();
                $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));


                try {
                    $payment->create($this->_api_context);
                } catch (\PayPal\Exception\PayPalConnectionException $ex) {

                   //  $paypal_conf = \Config::get('paypal');
                   // dd('client_id:'.$paypal_conf['client_id'].'<br/>secret: '.$paypal_conf['secret'].'<br/>settings:'.json_encode($paypal_conf['settings']));
                    return redirect()->back()->withError('Connection timeout')->withInput();
                   

                // dd($input);

                }
                foreach($payment->getLinks() as $link) {
                    if($link->getRel() == 'approval_url') {
                        $redirect_url = $link->getHref();
                        break;
                    }
                }
                /** add payment ID to session **/
                Session::put('paypal_payment_id', $payment->getId());
                if(isset($redirect_url)) {
                    return redirect()->to($redirect_url);
                }
            }
            else
            {
                 $template=MailTemplate::findOrFail(1);
                    $content=$template->template;
                    $subject=$template->subject;
                    $content=str_replace('[USERNAME]',$donation->first_name.' '.$donation->last_name,$content);
                    $content=str_replace('[DonationProgram]','Donation',$content);
                    $content=str_replace('[FirstName]',$donation->first_name,$content);
                    $content=str_replace('[LastName]',$donation->last_name,$content);
                    $content=str_replace('[Email]',$donation->email,$content);
                    $content=str_replace('[adminemail]','info@tutanc.org',$content);
                    $content=str_replace('[PhoneNo]',$donation->mobile,$content);
                    $content=str_replace('[Address]',$donation->address,$content);
                    $content=str_replace('[DonationCause]',$donation->donation_cause,$content);
                    $content=str_replace('[PaymentStatus]','Pending',$content);
                    $content=str_replace('[Category]',$donation->category->name,$content);
                    $content=str_replace('[DonationAmount]',$donation->amount,$content);
                    $content=str_replace('[TransactionId]','Check Payment. Check No: '.$donation->cheque_number.', Bankname: '.$donation->bankname,$content);
                    $content=str_replace('[PaymentDate]',dateformat($donation->created_at),$content);
                    $content=str_replace('[ApplicationName]','TUTA',$content);
                    $content=str_replace('[siteurl]',env('siteurl'),$content);

                    $mailcontent = array('mailbody' =>  $content);  

                    // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($donation, $subject) {
                    //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($donation->email)->subject($subject);
                    // }); 

                    sendmail_api($subject, $content, $donation->email); 


                    $template=MailTemplate::findOrFail(2);
                    $content=$template->template;
                    $subject=$template->subject;
                    $subject=str_replace('[ServiceName]','Donation',$subject);
                    $content=str_replace('[ServiceName]','Donation',$content);
                    $content=str_replace('[FirstName]',$donation->first_name,$content);
                    $content=str_replace('[LastName]',$donation->last_name,$content);
                    $content=str_replace('[Email]',$donation->email,$content);
                    $content=str_replace('[adminemail]','info@tutanc.org',$content);
                    $content=str_replace('[PhoneNo]',$donation->mobile,$content);
                    $content=str_replace('[Address]',$donation->address,$content);
                    $content=str_replace('[Category]',$donation->category->name,$content);
                    $content=str_replace('[DonationCause]',$donation->donation_cause,$content);
                    $content=str_replace('[PaymentStatus]','Pending',$content);
                    $content=str_replace('[DonationAmount]',$donation->amount,$content);
                    $content=str_replace('[TransactionId]','Check Payment. Check No: '.$donation->cheque_number.', Bankname: '.$donation->bankname,$content);
                    $content=str_replace('[PaymentDate]',dateformat($donation->created_at),$content);
                    $content=str_replace('[ApplicationName]','TUTA',$content);
                    $content=str_replace('[siteurl]',env('siteurl'),$content);

                     $mailcontent = array('mailbody' =>  $content);  

                    // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($subject) {
                    //     $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to(getenv('ADMIN_EMAIL_ID'))->subject($subject);
                    // }); 

                    sendmail_api($subject, $content, getenv('ADMIN_EMAIL_ID')); 

                    return view('services.donation_success', compact('donation'));
            }
        }
        else
        {
            return redirect()->back()->withErrors('Error in donation payment')->withInput();
        }
    }
}
