<?php

namespace App\Http\Controllers;
use App\Http\Controllers\AuthorizedController;
use App\Repositories\Event\EventUserRepositoryInterface;
use App\Repositories\Event\EventRepositoryInterface;
use App\Repositories\Event\EventRegistrantRepositoryInterface;
use App\Repositories\Event\EventSponsorshipRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Sentinel;
use Input;
use App\Models\EventUser;
use App\Models\Event;
use App\Models\EventRegistrationCount;
use App\Models\EventRegistrationType;
use App\Models\User;
use App\Models\Donation;
use Session;
use URL;
use Redirect;
use Log;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Models\MailTemplate;
use Mail;
use App\Repositories\Donation\DonationRepositoryInterface;
use App\Models\Appinfo;

class EventController extends AuthorizedController
{
    /**
     * Show the homepage.
     *
     * @return \Illuminate\View\View
     */
    protected $event;

    public function __construct(EventRepositoryInterface $event,EventUserRepositoryInterface $eventuser, EventRegistrantRepositoryInterface $eventregistrant, EventSponsorshipRepositoryInterface $sponsorship, UserRepositoryInterface $user, DonationRepositoryInterface $donation)
    {
        $this->event= $event;
        $this->eventuser= $eventuser;
        $this->eventregistrant= $eventregistrant;
        $this->user = $user;
        $this->sponsorship = $sponsorship; 

         $this->donation= $donation;

       $this->appinfo=Appinfo::where('id',1)->first(); 

        $settings = array(
        'mode' => $this->appinfo->paypal_mode,
        'http.ConnectionTimeOut' => 2000,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
        );

        if($this->appinfo->paypal_mode=='live')
        {
        $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_client_id, $this->appinfo->paypal_secret));
        }
        else
        {
        $this->_api_context = new ApiContext(new OAuthTokenCredential($this->appinfo->paypal_sandbox_client_id, $this->appinfo->paypal_sandbox_secret));
        }
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings); 

    }

    public function getEvent($slug)
    {
         $event = $this->event->findBySlug($slug);
        if ($event) {
            return view('events.event_details', compact('event'));
        } else {
            abort(404);
        }
    }
    public  function getEventRegistration($slug)
    {

        $event = $this->event->findBySlug($slug); 
        $user= $this->currentUser = Sentinel::getUser();
        $sponsorships= $this->sponsorship->getAllActive();
        $userevent=[];
        if($user)
        {
            $userevent=EventUser::where('event_id',$event->id)->where('user_id',$user->id)->orderBy('id','desc')->first();
         }
        if ($event) {
           if($event->start_date>date("Y-m-d") && $event->registration_start_date<=date("Y-m-d") && $event->registration_end_date>=date("Y-m-d")) 
            {
                return view('events.event_registration', compact('event','userevent','sponsorships'));
            }
           else 
                return redirect()->to('/event/'.$event->slug)->withError('Event Registrations are not available now');
        } else {
            abort(404);
        }
    }

    public function CheckEventRegistered(Request $request)
    {
        $user = User::where('users.email', $request->get('email'))->get();
        $userevent=EventUser::where('event_id',$request->get('event_id'))->where('email',$request->get('email'))->first();
        if($userevent){
       return 1;
        }
        else{
            return 0;
        }
    }

     public  function getEventRegistrationPayment($slug,$id)
    {
        $event = $this->event->findBySlug($slug);
        $user= $this->currentUser = Sentinel::getUser();
        $sponsorships= $this->sponsorship->getAllActive();
        $eventuser= $this->eventuser->find($id);
        $userevent=[];
        if($user)
        {
            $userevent=EventUser::where('event_id',$event->id)->where('user_id',$user->id)->orderBy('id','desc')->first();
         }
        if ($event) {
            return view('events.event_registration_payment', compact('event','userevent','sponsorships','eventuser'));
        } else {
            abort(404);
        }
    }


    public function postPayment(Request $request)
    {
        $input=$request->input(); 
        $security_code_number=$input['firstNumber']+$input['secondNumber'];
        if($security_code_number!=$input['security_code'])
        return back()->withInput()->withError('Security Code error occur');
        $user= $this->currentUser = Sentinel::getUser();
        $event = $this->event->find($input['event_id']);
        if($user)
        {
        $input['user_id']=$user->id;
        }
        $userCurrency = 'USD';
        $pay_amount =$input['payamount'];
        $input['paymentmethod_id']=1;
        $input['amount_paid']=$pay_amount;
        if($input['donation_amount']=='')
            $input['donation_amount']=0;

        if(isset($input['eventuser_id']))
            $eventuser=$this->eventuser->find($input['eventuser_id']);
        if(isset($eventuser))
          {
            $eventuser->fill($input)->save();
            EventRegistrationCount::where('eventuser_id',$eventuser->id)->delete();  
            $eventuser=$this->eventuser->find($input['eventuser_id']);          
          }  
          else
         {   
            $eventuser = $this->eventuser->create($input);
        }
        if($eventuser->donation_amount>0)
        {
            $isdonation=Donation::where('eventuser_id',$eventuser->id)->first();
            if(!$isdonation)
            {
              $data=array();
               if($user)
              $data['user_id']=$user->id; 
              $data['status']=0;
              $data['amount']=$eventuser->donation_amount;
              $data['eventuser_id']=$eventuser->id;
              $data['paymentmethod_id']=1;
              $data['donation_cause']=$event->name." Event Donation";
              $data['ip_address']=$request->ip();  
              $this->donation->create($data);
            }
        }
        if($request->has('regtype_id') && count($input['regtype_id']))
        {
            foreach($input['regtype_id'] as $key=>$rtype)
            {
                if($input['regtype_id'][$key]>0)
                {
                    $regdata=array();
                    $regdata['event_id']=$input['event_id'];
                    $regdata['eventuser_id']=$eventuser->id;
                    $regdata['eventregistrationtype_id']=$rtype;
                    if(isset($input['regtype'][$key]))
                    {
                        $regdata_name=$input['regtype'][$key];
                        if($regdata_name=='Family')
                        {
                        $regdata['family_adults']=$input['family_adults'];
                        $regdata['family_kids']=$input['family_kids']; 
                        }
                        $regdata['count']=$input['count'][$key];                        
                        $regdata['amount']=$input['regtypeamount'][$key]*$input['count'][$key];
                        if($input['count'][$key])
                        $this->eventregistrant->create($regdata);
                    }
                }
            }
        }
 
        Session::put('eventuser_id',$eventuser->id);

        // print_r($input);
        // exit();
         if($pay_amount>0)
        {

             $updateData = [
                'paymentmethod_id' => 1,
                'status' =>0
            ];
             $eventuser->fill($updateData)->save();

            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $item_1 = new Item();

            $item_1->setName($event->name. ' Event Registration in TUTA')
                ->setCurrency($userCurrency)
                ->setQuantity(1)
                ->setPrice($pay_amount);

            $item_list = new ItemList();
            $item_list->setItems(array($item_1));

            $amount = new Amount();
            $amount->setCurrency($userCurrency)
                ->setTotal($pay_amount);
                $timestamp=Carbon::now()->toDateTimeString();

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setInvoiceNumber(Carbon::now()->timestamp.'_'.$eventuser->id)
                ->setItemList($item_list)
                ->setDescription('Transaction Details');


            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::route('eventpayment.status'))
                ->setCancelUrl(URL::route('eventpayment.status'));
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

            try {
                $payment->create($this->_api_context);
                }
                catch (\PayPal\Exception\PayPalConnectionException $ex)
                {
                      if (\Config::get('app.debug')) {
                        return back()->withError('Connection timeout');
                        /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                        /** $err_data = json_decode($ex->getData(), true); **/
                        /** exit; **/
                    } else {
                        return back()->withError('Some error occur, sorry for inconvenient');
                        /** die('Some error occur, sorry for inconvenient'); **/
                    }
                }
            //dd($payment);
            foreach($payment->getLinks() as $link) {
                if($link->getRel() == 'approval_url')
                {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            Session::put('paypal_payment_id', $payment->getId());
            if(isset($redirect_url)) {
                /** redirect to paypal **/
                return redirect()->to($redirect_url);
            }
        }
        else
        {

             $eventuser = $this->eventuser->find($eventuser->id);

             $event_details="Event Date: ".dateformat($event->start_date)."<br/>Location: ".$event->location.' '.$event->city;
                if($event->state)$event_details.=', '.$event->state;
                if($event->zipcode)$event_details.='-'.$event->zipcode;
                $event_details.=$event->mail_description;
                $event_details.=". <p>For more event details please <a href='".url('/')."/event/".$event->slug."'>click here: </a></p><p>Please check your attachment (ics file) to add the event to your calendar.</p>";  

                $event_details=str_replace('src="/public/','src="'.url('/').'/public/',$event_details);

                $template=MailTemplate::findOrFail(9);
                $content=$template->template;
                $content=str_replace('[USERNAME]',$eventuser->first_name.' '.$eventuser->last_name,$content);
                $content=str_replace('[ApplicationName]','TUTA',$content);
                $content=str_replace('[EventName]',$eventuser->event->name,$content);
				$content=str_replace('[EventContactMail]',$event->contact_mail,$content);
                $content=str_replace('[event_details]',$event_details,$content);
                $content=str_replace('[siteurl]',url('/'),$content);
                $subject = "Thank you for Registering to our Event in TUTA";

                $mailcontent = array('mailbody' =>  $content);  

                 // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($eventuser, $subject) {
                 //        $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to($eventuser->email)->subject($subject);
                 //    });
                sendmail_api($subject, $content, $eventuser->email);

                $template=MailTemplate::findOrFail(8);
                $content=$template->template;
                $subject =$template->subject;
                $subject=str_replace('[Name]',$eventuser->first_name.' '.$eventuser->last_name,$subject);
                $subject=str_replace('[EventName]',$eventuser->event->name,$subject);
                $content=str_replace('[ApplicationName]','TUTA',$content);
                $content=str_replace('[EventName]',$eventuser->event->name,$content);
                $content=str_replace('[siteurl] ', url('/').'/',$content);


                 $regdetails='<table border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size: 15px; color: #666666; padding-top: 10px;" width="100%">
                        <tbody>
                        <tr>
                            <th style="padding-left: 20px; vertical-align:middle!important;" width="30%">Registration Category</th>
                            <th style="vertical-align:middle!important;" width="20%">Count </th>
                            <th style="vertical-align:middle!important;" width="13%">Donation Amount</th>
                        </tr>';
                        $total=0;
                          foreach($eventuser->registrants as $regdata)
                         { 
                        $regtotal= $regdata->amount;
                        $total=$total+$regtotal;
                        
                            $regdetails=$regdetails.'<tr>
                                <td style="text-align:center;" >'.getEventRegtype($regdata->eventregistrationtype_id).'</td>
                                <td  style="text-align:center;"> $'.getEventRegtypeAmount($regdata->eventregistrationtype_id).'</td>

                                <td>'. (getEventRegtype($regdata->eventregistrationtype_id)=='Family')?'Adults: '.$regdata->family_adults.', Kids: '.$regdata->family_kids:$regdata->count.'
                                 </td>
                                <td style="text-align:center;" > $'.$regtotal.'</td>
                            </tr>';
                        }
                       $regdetails=$regdetails.' <tr>
                        <td colspan="3">Total: </td>
                        <td style="text-align:center;" >$'.$total.'</td>
                                                    </tr>
                                                </tbody></table>';

                $content=str_replace('[RegDetails]',$regdetails,$content);

                $mailcontent = array('mailbody' =>  $content);  

                 // Mail::send(['html' => 'emails.mail'], $mailcontent, function ($message)  use($eventuser, $subject) {
                 //        $message->from(getenv('ADMIN_EMAIL_ID'),'TUTA')->to(getenv('ADMIN_EMAIL_ID'))->subject($subject);
                 //    });
                sendmail_api($subject, $content, getenv('ADMIN_EMAIL_ID'));
                
            return view('events.event_success', compact('eventuser'));
        }
    }

    public function checkRegCount(Request $request)
    {
        $regtype_id=$request->get('id');
        $rtype=EventRegistrationType::where('id',$regtype_id)->first();
        $max=$rtype->reg_count;
        $event_id=$rtype->event_id;
         $usedcount=EventRegistrationCount::join('eventuser_info', 'eventuser_info.id', '=', 'eventregistration_count.eventuser_id')->whereNull('eventuser_info.deleted_at')->where('eventregistration_count.event_id',$event_id)->sum('count','family_adults','family_kids');
        $response=array();
        $total_limit=Event::where('id',$event_id)->pluck('total_registration_count')->first();

        $response['count']=(int)$usedcount;
        $response['total_count']=$total_limit;
        return $response;
    }
}