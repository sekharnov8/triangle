<?php

namespace App\Http\Controllers;
use App\Repositories\News\NewsRepositoryInterface;
use Illuminate\Http\Request;
use Sentinel;
use Input;

class NewsController extends BaseController
{
    /**
     * Show the homepage.
     *
     * @return \Illuminate\View\View
     */
    protected $event;

    public function __construct(NewsRepositoryInterface $news )
    {
        $this->news= $news;

    }

    public function index()
    {
        $page=$this->page->findBySlug('home');
        $news=$this->news->getActive();
        return view('pages.news', compact('page','news'));
    }
    public function getnews($slug)
    {
        $news = $this->news->findBySlug($slug);
        if ($news && $news->status==1) {
            return view('pages.news_details', compact('news'));
        } else {
            abort(404);
        }
    }
}
