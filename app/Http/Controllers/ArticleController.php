<?php

namespace App\Http\Controllers;
use App\Repositories\Article\ArticleRepositoryInterface;
use Illuminate\Http\Request;
use Sentinel;
use Input;

class ArticleController extends BaseController
{

    protected $article;

    public function __construct(ArticleRepositoryInterface $article )
    {
        $this->article= $article;

    }

    public function index()
    {
        $page=$this->page->findBySlug('home');
        $articles=$this->Article->getUpcomingEvents();
        return view('pages.article', compact('page','articles'));
    }
    public function getArticle($slug)
    {
        $article = $this->article->findBySlug($slug);
        if ($article) {
            return view('pages.article_details', compact('article'));
        } else {
            abort(404);
        }
    }
}
