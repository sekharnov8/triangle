<?php

namespace App\Http\Controllers;
use App\Repositories\Page\PageRepositoryInterface;
use App\Repositories\Sponsor\SponsorRepositoryInterface;
use App\Repositories\Banner\BannerRepositoryInterface;
use App\Repositories\Event\EventRepositoryInterface;
use App\Repositories\Video\VideoRepositoryInterface;
use App\Repositories\Photo\PhotoRepositoryInterface;
use App\Repositories\Article\ArticleRepositoryInterface;
use App\Repositories\Committee\CommitteeRepositoryInterface;
use App\Repositories\Committee\CommitteeMemberRepositoryInterface;
use App\Repositories\Greeting\GreetingRepositoryInterface;
use App\Repositories\Sponsor\SponsorCategoryRepositoryInterface;
use Session;
use Carbon\Carbon;
use Mail;

class HomeController extends BaseController
{
    /**
     * Show the homepage.
     *
     * @return \Illuminate\View\View
     */
protected $page;

    public function __construct(PageRepositoryInterface $page, SponsorRepositoryInterface $sponsor, BannerRepositoryInterface $banner, EventRepositoryInterface $event, VideoRepositoryInterface $video,PhotoRepositoryInterface $photo, ArticleRepositoryInterface $article, CommitteeRepositoryInterface $committee,CommitteeMemberRepositoryInterface $committeemember, GreetingRepositoryInterface $greeting, SponsorCategoryRepositoryInterface $sponsorcategory)
    {
        $this->page = $page;
        $this->article = $article;
        $this->sponsorcategory = $sponsorcategory;
        $this->sponsor = $sponsor;
        $this->banner = $banner;
        $this->event= $event;
        $this->video= $video;
        $this->photo= $photo;
        $this->committee = $committee;
        $this->committeemember = $committeemember;
        $this->greeting= $greeting;
    }

    public function index()
    {
        $articles=$this->article->getActive();
        $banners=$this->banner->getActiveBanners();
        $page=$this->page->findBySlug('home');
        $president=$this->page->findBySlug('message');
        $events=$this->event->getUpcomingEvents(4);
        $videos=$this->video->getHomeActive(5);
        $photos=$this->photo->getActive(5);
        $greeting=$this->greeting->getActiveGreeting();
        $committee = $this->committee->findBySlug('executive-committee');
        if($committee)
        $members = $this->committeemember->getMembers($committee->id);
        else
        $members=[];
        $sponsorcategories=$this->sponsorcategory->getAllActive();

          return view('pages.home', compact('sponsorcategories','banners','page','events','videos','photos','president','articles','greeting','members'));
    }

    public function goHome()
    {
        return redirect()->to('/');
    }
}
