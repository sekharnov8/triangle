<?php

use App\Models\PageCategory;
use App\Models\PhotoCategory;
use App\Models\CommitteeMember;
use App\Models\Member;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\Order;
use App\Models\Role;
use App\Models\PaymentMethod;
use App\Models\EventRegistrationType;
use App\Models\EventRegistrationCount;
use App\Models\EventUser;
use App\Models\Significant;
use App\Models\Donation;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\ContactProgram;
use App\Models\StatusType;
use App\Models\Membershiptype;
use App\Models\EventSponsorship;
use Illuminate\Support\Collection;
use App\Models\Appinfo;

 function errors_for($attribute, $errors)
 {
    return $errors->first($attribute, '<p class="text-danger">:message</p>');
 }

 function set_active($path, $active='active')
 {
    // return Request::is($path) || Request::is($path . '/*') ? $active: '';
    return Request::is($path) || Request::is($path . '/*') ? $active: '';
 }

 function set_active_admin($path, $active='active')
 {
    return Request::is($path) ? $active: '';
 }

function getPasswordResetEmail($token)
{
    return DB::table('password_resets')->whereToken($token)->pluck('email');
}
function get_event_regtype($id)
{
    return EventRegistrationType::findOrFail($id);
}
function getTotalCount($model)
{

    return $records = $model::count();
}

function get_youtube_id_from_url($url)
{
    if(strlen($url)>11)
    {
        if (stristr($url,'youtu.be/'))
        {preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID); return $final_ID[4]; }
        else
        {@preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD); return $IDD[5]; }
    }
    else
        return $url;
}

function url_segment($len)
{

    $word = ucfirst(Request::segment($len));
    return preg_replace('/[^a-zA-Z0-9]+/', ' ', $word);
}
function getfullurl()
{
    return Request::fullUrl();
}
function slug($string)
{
    $string=str_slug($string, '-');
    return preg_replace('/-+/', '-', $text);
}
function dateformat($date,$format=0)
{
    if($date!='')
    {
         return date("m-d-Y", strtotime($date));

    }
    else
        return '';
}
 function pagecategories_sublist($id)
{
        return PageCategory::where('page_parentid',$id)->get();
}
 function photocategories_sublist($id)
{
        return PhotoCategory::where('parent_id',$id)->get();
}

function get_sponsorship_name($id)
{
    return EventSponsorship::where('id',$id)->pluck('name')->first();
}
function ShortenText($text, $len)
{
    $chars = $len;
    $text  = $text . " ";
    if (strlen($text) > $len) {
        $text = Str::substr($text, 0, $chars);
        $text = $text . "...";
    }
    $text=strip_tags($text);
    return $text;
}
function subscription_expired_at()
{
    $today = Carbon::now();
    $year = $today->year;
    if( (0 == $year % 4) and (0 != $year % 100) or (0 == $year % 400) )
    {
        $expire_date = Carbon::now()->addDays(366)->subDay();
        //echo "$year is a Leap Year";
    }
    else
    {
        $expire_date = Carbon::now()->addDays(365)->subDay();
        //echo "$year is not a Leap Year";
    }
    return $expire_date;
}

function getMembershipName($id)
{
    return MembershipType::where('id',$id)->pluck('name')->first();
}
function getRoleName($role_id)
{
    return Role::where('id',$role_id)->pluck('name')->first();
}
function getUserRoles($user_id)
{
    $roleids=RoleUser::where('user_id',$user_id)->pluck('role_id');
    $roles=Role::whereIn('id',$roleids)->pluck('name')->toArray();
    $roles=implode(',',$roles);
    return $roles;
}
function getUserRoleIds($user_id)
{
    $roleids=RoleUser::where('user_id',$user_id)->pluck('role_id');
    $roles=Role::whereIn('id',$roleids)->pluck('id')->toArray();
    $roles=implode(',',$roles);
    return $roles;
}
 

function is_committee_member()
{
     $user_id=Sentinel::getUser()->id;
    $committee_member=CommitteeMember::where('user_id',$user_id)->where('status',1)->get();
    $s='';
if(count($committee_member))
{
 foreach($committee_member as $data)
{
         $s=$s. $data->committee->name .' ('.getRoleName($data->role_id).')'.'<br>';
 }
 return $s;
}
else
return '';
}


function is_committee_member_any()
{
         $user_id=Sentinel::getUser()->id;
    $committee_member=CommitteeMember::where('user_id',$user_id)->where('status',1)->first();
 if($committee_member)
{
   return true;
}
else
return '';
}
 




function sendmail_api($subject, $body, $to, $adminemail='Links2MyDemo@gmail.com')
{
$appinfo=Appinfo::where('id',1)->first(); 

$email = new \SendGrid\Mail\Mail();
$email->setFrom($appinfo->sendgrid_email, "TUTA");
$email->setSubject($subject);
$email->addTo($to);
$email->addContent("text/html", $body);
$sendgrid = new \SendGrid($appinfo->sendgrid_api_key);

try {
    $response = $sendgrid->send($email);
} catch (Exception $e) {
    echo 'Caught exception: '. $e->getMessage() ."\n";
}

// print everything out
return $response;

} 

function getUserName($id)
{
    if(strpos($id, '|') !== false)
    {
            $ss=explode('|',$id); 
            $user=Significant::where('id', $ss[1])->first();
    }
    else
    $user=User::where('id',$id)->first();
     if($user)
    return $user->first_name.' '.$user->last_name;
    else
    return '';
}
function getUser($id)
{
      return User::where('id', $id)->first();
}
 

function getSignificant($id)
{
      return Significant::where('id', $id)->first();
}
function getUserEmail($id)
{
    if(strpos($id, '|') !== false)
        {
            $ss=explode('|',$id);
            return Significant::where('id', $ss[1])->pluck('email')->first();
        }
      return User::where('id', $id)->pluck('email')->first();
}
function getProgramContactName($id)
{
      return ContactProgram::where('id', $id)->pluck('name')->first();
}

function getProgramContactNameByEmail($email)
{
      return ContactProgram::where('email', $email)->pluck('name')->first();
}
function numberformat($number)
{
    if($number)
    return number_format((float)$number,2, '.', '');
    else
        return 0;
}

function getEventRegtypeInfo($id)
{
    
    $regtype=EventRegistrationType::whereId($id)->first();
    if($regtype)
    return $regtype;
    else
        return '';
}
function getEventRegtype($id)
{

    $regtype=EventRegistrationType::whereId($id)->first();
    if($regtype)
    return $regtype->name;
    else
        return '';
}
function getEventRegtypeAmount($id)
{
    $regtype=EventRegistrationType::whereId($id)->first();
    if($regtype)
        return $regtype->amount;
    else
        return '';
}
function getDaystoExpiry($date)
{
    $newdate = Carbon::parse($date);
    $now = Carbon::now();
    $diff = $newdate->diffInDays($now);
    return $diff;
}
function chairmember($committee_id)
{
    $roleid = Sentinel::getRoleRepository()->where('name','Chair')->pluck('id')->first();
    return CommitteeMember::join('users', 'users.id', '=', 'committeemembers.user_id')->whereNull('users.deleted_at')->where('committeemembers.committee_id', $committee_id)->where('committeemembers.status',1)->where('committeemembers.role_id', $roleid)->first();
}
function is_treasurer()
{
    $user=Sentinel::getUser();
    if($user->inRole('treasurer') || $user->inRole('joint-treasurer'))
        return true;
    else
        return false;
}
function gen_random_string($ch=5,$num=8)
{
    $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $numbers = "1234567890";

    $rand1 = '';
    $rand2 = '';
    for($i=0;$i<$ch; $i++)
    {
        $rand1 .= $chars[ rand(0,strlen($chars)-1)];

    }
    for($j=0;$j<$num; $j++)
    {
        $rand2 .= $numbers[ rand(0,strlen($numbers)-1)];

    }
    return $rand1.$rand2;
}

function PaymentMethod($id)
{
    if($id)
    {
    $paymentmethod=PaymentMethod::findOrFail($id);
    return $paymentmethod->name;
    }
    else
        return '';
}

function getCurrencytype($type)
{
    if($type=='Rs.')
    {
        return 'Rs.';
    }
    else
    {
        return '$';
    }
}
function getSumByEventRegType($regtype_id, $status)
{
     $rtype=EventRegistrationType::where('id',$regtype_id)->first();
     if($status!=1)
        $status='';
     if($rtype->name=='Family')
     {
         $adults=EventRegistrationCount::join('eventuser_info', 'eventuser_info.id', '=', 'eventregistration_count.eventuser_id')->whereNull('eventuser_info.deleted_at')->where('eventregistration_count.eventregistrationtype_id',$regtype_id)->where('eventuser_info.status',$status)->sum('eventregistration_count.family_adults');
         $adults=EventRegistrationCount::join('eventuser_info', 'eventuser_info.id', '=', 'eventregistration_count.eventuser_id')->whereNull('eventuser_info.deleted_at')->where('eventregistration_count.eventregistrationtype_id',$regtype_id)->where('eventuser_info.status',$status)->sum('eventregistration_count.family_adults');
         $kids=EventRegistrationCount::join('eventuser_info', 'eventuser_info.id', '=', 'eventregistration_count.eventuser_id')->whereNull('eventuser_info.deleted_at')->where('eventregistration_count.eventregistrationtype_id',$regtype_id)->where('eventuser_info.status',$status)->sum('eventregistration_count.family_kids');
         return 'Adults: '.$adults.', Kids:'.$kids;
     }
     else
     {
        $count=EventRegistrationCount::join('eventuser_info', 'eventuser_info.id', '=', 'eventregistration_count.eventuser_id')->whereNull('eventuser_info.deleted_at')->where('eventregistration_count.eventregistrationtype_id',$regtype_id)->where('eventuser_info.status',$status)->sum('count');
        return $count;
     }
}
function getSumOfEventDonationById($event_id,$status)
{

    $donation= EventUser::where('event_id',$event_id)->where('status',$status)->sum('amount_paid');
    return number_format($donation,2);
}
function getMemberCountByEventId($event_id, $status)
{
      if($status!=1)
        $status='';
     $total1=EventRegistrationCount::join('eventuser_info', 'eventuser_info.id', '=', 'eventregistration_count.eventuser_id')->where('eventregistration_count.event_id',$event_id)->whereNull('eventuser_info.deleted_at')->where('eventuser_info.status',$status)->sum(DB::raw('eventregistration_count.family_adults + eventregistration_count.family_kids'));
      $total2=EventRegistrationCount::join('eventuser_info', 'eventuser_info.id', '=', 'eventregistration_count.eventuser_id')->whereNull('eventuser_info.deleted_at')->where('eventregistration_count.event_id',$event_id)->where('eventuser_info.status',$status)->where('family_adults',0)->where('family_kids',0)->sum('count');
      return $total1+$total2;

}

function convertCurrency($amount, $from = 'EUR', $to = 'USD'){
$API = '7f96f64f5aa6582471208f9fb8556f98';
$curl = file_get_contents("http://data.fixer.io/api/latest?access_key=$API&symbols=$from,$to");
$arr = json_decode($curl,true);

$from = $arr['rates'][$from];
$to = $arr['rates'][$to];

$rate = $to / $from;
$result = round($amount * $rate, 1);
return $result;
// echo convertCurrency(1000, 'INR', 'USD');
}
function getSpouseInfo($user_id)
{
    return Significant::where('user_id',$user_id)->where('relationship','Spouse')->first();    
}

function getChildrenInfo($user_id)
{ 
    return Significant::where('user_id',$user_id)->where('relationship','<>','Spouse')->get();
}
function getLatestOrder($user_id, $membershiptype_id)
{
    return Order::where('user_id',$user_id)->where('membershiptype_id',$membershiptype_id)->orderBy('id','desc')->first();
}


function is_membership_member()
{
    $user_id=Sentinel::getUser()->id;
    $committee=CommitteeMember::where('committee_id', 12)->where('user_id', $user_id)->where('status',1)->first();
    if($committee)
        return true;
    else
        return false;
}
function is_technology_member()
{
    $user_id=Sentinel::getUser()->id;
    $committee=CommitteeMember::where('committee_id', 10)->where('user_id', $user_id)->where('status',1)->first();
    if($committee)
        return true;
    else
        return false;
}
function is_membership_chair()
{
    $user_id=Sentinel::getUser()->id;
    $committee=CommitteeMember::where('committee_id', 12)->where('user_id', $user_id)->where('status',1)->whereIn('role_id', [6,7,13])->first();
    if($committee)
        return true;
    else
        return false;
}

function is_technology_chair()
{
    $user_id=Sentinel::getUser()->id;
    $committee=CommitteeMember::where('committee_id', 10)->where('user_id', $user_id)->where('status',1)->whereIn('role_id', [6,7,13])->first();
    if($committee)
        return true;
    else
        return false;
}
 

 function getstatusByTypeAndNo($cat,$no)
 {
        if(StatusType::where('status_category_id',$cat)->where('order_no',$no)->first())
        return StatusType::where('status_category_id',$cat)->where('order_no',$no)->pluck('name')->first();
        else
            return '';
 }

 function user_spouse()
 {
    $user = Cache::remember('user_spouse', 30, function() {

   return DB::select( DB::raw("SELECT DISTINCT
        CONCAT(u.first_name, ' ', u.last_name,' (',u.email,')') as fullName,
        `u`.`id` AS `u_id`
    FROM
        ((((`users` `u`
        LEFT JOIN `membershiptypes` `mt` ON ((`mt`.`id` = `u`.`membershiptype_id`)))
        LEFT JOIN `orders` `mo` ON ((`mo`.`user_id` = `u`.`id`))))
    WHERE
        ((IFNULL(`u`.`is_approved`, 0) NOT IN (4 , 2))
            AND (`mt`.`id` IN (1 , 2, 6))
            AND ISNULL(`u`.`deleted_at`)  
            AND u.is_approved=1
            AND ISNULL(`mo`.`deleted_at`)) 
    UNION SELECT DISTINCT
       CONCAT(ms.first_name, ' ', ms.last_name,' (', (ms.email COLLATE utf8mb4_unicode_ci),')') AS fullName,
        CONCAT(u.id, '|', ms.id) as u_id 
    FROM
        (((((`users` `u`
        LEFT JOIN `membershiptypes` `mt` ON ((`mt`.`id` = `u`.`membershiptype_id`)))
        LEFT JOIN `significants` `ms` ON (((`ms`.`user_id` = `u`.`id`)
            AND (`ms`.`relationship` = 'Spouse'))))
        LEFT JOIN `orders` `mo` ON ((`mo`.`user_id` = `u`.`id`))))
    WHERE
        ((IFNULL(`u`.`is_approved`, 0) NOT IN (4 , 2))
            AND (`mt`.`id` IN (1 , 2, 6))
            AND (`ms`.`id` IS NOT NULL)
            AND (`ms`.`email` IS NOT NULL)
            AND u.is_approved=1
            AND ISNULL(`u`.`deleted_at`)
            AND ISNULL(`mo`.`deleted_at`))")); 
    });
    return $user;
 }
