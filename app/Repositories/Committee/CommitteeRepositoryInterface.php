<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 11/26/2018
 * Time: 12:13 PM
 */
namespace App\Repositories\Committee;

interface CommitteeRepositoryInterface
{
    public function getAll();

    public function getActive();

    public function find($id);

    public function create($fields);

    public function findBySlug($value);

    public function delete($id);
}