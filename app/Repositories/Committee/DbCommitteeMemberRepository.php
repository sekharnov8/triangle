<?php

namespace App\Repositories\Committee;

use App\Models\CommitteeMember;
use Carbon\Carbon;
use Sentinel;

class DbCommitteeMemberRepository implements CommitteeMemberRepositoryInterface
{
    public function getAll()
    {
        return CommitteeMember::all();
    }
    public function getByCommitteeId($committee_id)
    {
        return CommitteeMember::where('committee_id', $committee_id)->get();
    }

    public function getActive()
    {
        return CommitteeMember::where('status',1)->get();
    }
    public function find($id)
    {
        return CommitteeMember::findOrFail($id);
    }
    public function findById($id)
    {
        return CommitteeMember::where('committee_id', $id)->get();
    }
    public function getChair($id)
    {
        $roleid = Sentinel::getRoleRepository()->where('name','Chair')->pluck('id')->first();
         return CommitteeMember::where('committee_id', $id)->where('role_id', $roleid)->first();
    }
    public function getMembers($id)
    {
        $roleid = Sentinel::getRoleRepository()->where('name','Chair')->pluck('id')->first();
        return CommitteeMember::where('committee_id', $id)->where('role_id','<>', $roleid)->orderBy('display_order','asc')->get();
    }
    public function getMembersByUser_id($id)
    {
        return CommitteeMember::where('user_id', $id)->get();
    }

    public function getMembersListByCommittee($committee_id)
    {
        $roles = Sentinel::getRoleRepository()->pluck('name','id')->toArray();
        $members=array();
        $cm=CommitteeMember::where('committee_id', $committee_id)->orderBy('display_order','asc')->get();
        foreach($cm as $key=>$data)
        {
            if($data->user)
            {
            $members[$data->user_id]=$data->user->first_name.' '.$data->user->last_name.' ('.$roles[$data->role_id].')';
            }
         }
         return $members;

    }

    public function create($fields)
    {
        return CommitteeMember::create($fields);
    }
    public function findBySlug($value)
    {
        return CommitteeMember::where('slug', $value)->first();
    }
    public function delete($id)
    {
        return CommitteeMember::where('id', $id)->delete();
    }
    public function getMax()
    {
        $maxno= CommitteeMember::max('display_order');
        return $maxno+1;
    }

}