<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 11/26/2018
 * Time: 3:10 PM
 */
namespace App\Repositories\Committee;

interface CommitteeMemberRepositoryInterface
{
    public function getAll();

    public function getActive();

    public function find($id);

    public function create($fields);

    public function findBySlug($value);

    public function delete($id);

    public function getMax();
}