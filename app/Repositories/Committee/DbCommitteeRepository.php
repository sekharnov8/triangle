<?php

namespace App\Repositories\Committee;

use App\Models\Committee;
use Carbon\Carbon;

class DbCommitteeRepository implements CommitteeRepositoryInterface
{
    public function getAll()
    {
        return Committee::all();
    }

    public function getList()
    {
        return Committee::pluck('name','id')->toArray();
    }
    public function getActive()
    {
        return Committee::where('status',1)->get();
    }
    public function find($id)
    {
        return Committee::findOrFail($id);
    }
    public function getStanding()
    {
        return Committee::where('type','standing')->where('status',1)->orderBy('order_no','asc')->get();
    }
    public function create($fields)
    {
        return Committee::create($fields);
    }
    public function findBySlug($value)
    {
        return Committee::where('slug', $value)->first();
    }
    public function delete($id)
    {
        return Committee::where('id', $id)->delete();
    }
    public function getMax()
    {
        $maxno= Committee::max('order_no');
        return $maxno+1;
    }

}