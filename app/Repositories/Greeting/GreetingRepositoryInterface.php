<?php

namespace App\Repositories\Greeting;

interface GreetingRepositoryInterface
{
    public function getAll();

    public function getActiveGreeting();

    public function find($id);

    public function create($fields);

    public function delete($id);
}