<?php
namespace App\Repositories\Greeting;
use App\Models\Greeting;
use Carbon\Carbon;

class DbGreetingRepository implements GreetingRepositoryInterface
{

    public function getAll()
    {
        return Greeting::all();
    }
    public function getActiveGreeting()
    {
        $query=Greeting::where('status',1);
        $query->where(function ($query){
        $query->whereNull('expiredate')->orWhere('expiredate','>=',Carbon::today()->toDateString());
         }); 
        return $query->first();
    }
    public function find($id)
    {
        return Greeting::findOrFail($id);
    }
    public function create($fields)
    {
        return Greeting::create($fields);
    }
    public function delete($id)
    {
        return Greeting::where('id', $id)->delete();
    }

}