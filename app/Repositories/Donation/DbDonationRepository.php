<?php

namespace App\Repositories\Donation;

use App\Models\Donation;


class DbDonationRepository implements DonationRepositoryInterface
{
    public function getAll()
    {
        return Donation::all();
    }
    public function find($id)
    {
        return Donation::findOrFail($id);
    }
    public function create($fields)
    {
        return Donation::create($fields);
    }
    public function findByName($category)
    {
        return Donation::whereName($category)->first();
    } 
    public function delete($id)
    {
        return Donation::where('id', $id)->delete();
    } 
    public function getMembersDonations($id)
    {
        return Donation::where('user_id', $id)->get();
    }
}