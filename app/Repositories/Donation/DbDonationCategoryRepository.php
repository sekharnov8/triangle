<?php

namespace App\Repositories\Donation;

use App\Models\DonationCategory;


class DbDonationCategoryRepository implements DonationCategoryRepositoryInterface
{
    public function getAll()
    {
        return DonationCategory::all();
    }

    public function find($id)
    {
        return DonationCategory::findOrFail($id);
    }

    public function create($fields)
    {
        return DonationCategory::create($fields);
    }

    public function getList()
    {
        return DonationCategory::where('status',1)->pluck('name','id');
    }
    public function delete($id)
    {
        return DonationCategory::where('id', $id)->delete();
    }
 
}