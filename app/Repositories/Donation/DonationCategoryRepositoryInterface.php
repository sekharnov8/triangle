<?php

namespace App\Repositories\Donation;


interface DonationCategoryRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

    public function getList();

}