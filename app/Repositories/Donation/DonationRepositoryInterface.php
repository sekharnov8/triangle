<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 12/4/2018
 * Time: 1:21 PM
 */
namespace App\Repositories\Donation;

interface DonationRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function findByName($category);

    public function delete($id);
}