<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 11/17/2018
 * Time: 1:18 PM
 */
namespace App\Repositories\Article;

interface ArticleRepositoryInterface
{
    public function getAll();

    public function getActive();

    public function find($id);

    public function getMax();

    public function getList();

    public function create($fields);

    public function delete($id);
}