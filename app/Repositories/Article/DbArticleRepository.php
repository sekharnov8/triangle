<?php

namespace App\Repositories\Article;

use App\Models\Article;

class DbArticleRepository implements ArticleRepositoryInterface
{

    public function getAll()
    {
        return Article::all();
    }
    public function getActive()
    {
        return Article::where('status',1)->orderBy('post_date','desc')->get();
    }
    public function find($id)
    {
        return Article::findOrFail($id);
    }
    public function getMax()
    {
        $maxno= Article::max('display_order');
        return $maxno+1;
    }
    public function getList()
    {
        return Article::pluck('name','id')->toArray();
    }
    public function create($fields)
    {
        return Article::create($fields);
    }
    public function delete($id)
    {
        return Article::where('id', $id)->delete();
    }
    public function findBySlug($value)
    {
        return Article::where('slug', $value)->first();
    }
    

}