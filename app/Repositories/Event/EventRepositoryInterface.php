<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/17/2018
 * Time: 10:16 AM
 */
namespace App\Repositories\Event;

interface EventRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function findBySlug($value);

    public function delete($id);
}