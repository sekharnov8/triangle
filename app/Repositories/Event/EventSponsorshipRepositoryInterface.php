<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/17/2018
 * Time: 10:15 AM
 */
namespace App\Repositories\Event;

interface EventSponsorshipRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function getList();

    public function create($fields);

    public function findByName($category);

    public function delete($id);
}