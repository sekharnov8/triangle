<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 10/30/2018
 * Time: 1:27 PM
 */
namespace App\Repositories\Event;

interface EventRegistrantRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function getByEventUser($id);

    public function create($fields);

    public function delete($id);
}