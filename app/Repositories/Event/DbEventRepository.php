<?php

namespace App\Repositories\Event;

use App\Models\Event;
use Carbon\Carbon;

class DbEventRepository implements EventRepositoryInterface
{
    public function getAll()
    {
        return Event::with('category')->get();
    }

    public function getUpcomingEvents($count=10)
    {
        return Event::with('category')->where('status',1)->where('end_date','>',Carbon::now())->orderBy('end_date','asc')->take($count)->get();

    }
    
      public function getList()
    {
        return Event::whereNotNull('name')->where('name','<>','')->pluck('name','id');
    }

    


    public function getPastEvents($count=10)
    {
        return Event::with('category')->where('status',1)->where('end_date','<',Carbon::now())->orderBy('end_date','desc')->take($count)->get();

    }
    public function getByUserId($user_id)
    {
        return Event::where('user_id',$user_id)->get();
    }
    public function getActive()
    {
        return Event::where('status',1)->get();
    }

    public function find($id)
    {
        return Event::findOrFail($id);
    }
    public function create($fields)
    {
        return Event::create($fields);
    }
    public function findBySlug($value)
    {
        return Event::where('slug', $value)->where('status',1)->first();
    }
    public function delete($id)
    {
        return Event::where('id', $id)->delete();
    }

}