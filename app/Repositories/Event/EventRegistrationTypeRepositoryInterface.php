<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 10/26/2018
 * Time: 5:25 PM
 */
namespace App\Repositories\Event;

interface EventRegistrationTypeRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function findBySlug($value);

    public function delete($id);
}