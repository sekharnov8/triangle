<?php

namespace App\Repositories\Event;

use App\Models\EventRegistrationType;


class DbEventRegistrationTypeRepository implements EventRegistrationTypeRepositoryInterface
{
    public function getAll()
    {
        return EventRegistrationType::with('category')->get();
    }

    public function find($id)
    {
        return EventRegistrationType::findOrFail($id);
    }
    public function create($fields)
    {
        return EventRegistrationType::create($fields);
    }
    public function findBySlug($value)
    {
        return EventRegistrationType::where('slug', $value)->first();
    }
    public function delete($id)
    {
        return EventRegistrationType::where('id', $id)->delete();
    }
}