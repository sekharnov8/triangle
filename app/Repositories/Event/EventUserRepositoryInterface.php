<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 10/30/2018
 * Time: 12:15 PM
 */
namespace App\Repositories\Event;

interface EventUserRepositoryInterface
{
    public function getAll($id);

    public function find($id);

    public function getList();

    public function create($fields);

    public function findByName($category);

    public function delete($id);
}