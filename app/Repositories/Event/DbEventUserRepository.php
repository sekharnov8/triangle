<?php

namespace App\Repositories\Event;

use App\Models\EventUser;
use Carbon\Carbon;



class DbEventUserRepository implements EventUserRepositoryInterface
{
    public function getAll($id)
    {
        return EventUser::with('event_id',$id)->get();
    }
    public function getByEventId($id)
    {
        return EventUser::where('event_id',$id)->orderBy('created_at','desc')->get();
    }
    public function getByUserId($id)
    {
        return EventUser::where('user_id',$id)->get();
    }
    public function find($id)
    {
        return EventUser::findOrFail($id);
    }
    public function getList()
    {
        return EventUser::pluck('name','id')->toArray();
    }
    public function create($fields)
    {
        return EventUser::create($fields);
    }
    public function findByName($name)
    {
        return EventUser::whereName($name)->first();
    }
    public function delete($id)
    {
        return EventUser::where('id', $id)->delete();
    }
    public function deleteByEventId($id)
    {
        return EventUser::where('event_id', $id)->delete();
    }
     public function totalAdultsByEventId($id)
    {
        return EventUser::where('event_id', $id)->sum();
    }
}

