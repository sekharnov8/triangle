<?php

namespace App\Repositories\Event;

use App\Models\EventSponsorship;


class DbEventSponsorshipRepository implements EventSponsorshipRepositoryInterface
{
    public function getAll()
    {
        return EventSponsorship::get();
    }
     public function getAllActive()
    {
        return EventSponsorship::where('status',1)->get();
    }
    public function find($id)
    {
        return EventSponsorship::findOrFail($id);
    }
    public function getList()
    {
        return EventSponsorship::pluck('name','id')->toArray();
    }
    public function create($fields)
    {
        return EventSponsorship::create($fields);
    }
    public function findByName($category)
    {
        return EventSponsorship::whereName($category)->first();
    }
    public function delete($id)
    {
        return EventSponsorship::where('id', $id)->delete();
    }

}