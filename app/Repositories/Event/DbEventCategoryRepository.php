<?php

namespace App\Repositories\Event;

use App\Models\EventCategory;


class DbEventCategoryRepository implements EventCategoryRepositoryInterface
{
    public function getAll()
    {
        return EventCategory::with('events')->get();
    }

    public function find($id)
    {
        return EventCategory::findOrFail($id);
    }
    public function getList()
    {
        return EventCategory::pluck('name','id')->toArray();
    }
    public function create($fields)
    {
        return EventCategory::create($fields);
    }
    public function findByName($category)
    {
        return EventCategory::whereName($category)->first();
    }
    public function delete($id)
    {
        return EventCategory::where('id', $id)->delete();
    }

}