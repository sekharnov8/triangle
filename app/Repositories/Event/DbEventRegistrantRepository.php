<?php

namespace App\Repositories\Event;

use App\Models\EventRegistrationCount;
use Carbon\Carbon;



class DbEventRegistrantRepository implements EventRegistrantRepositoryInterface
{
    public function getAll()
    {
        return EventRegistrationCount::with('regtype')->get();
    }
    public function find($id)
    {
        return EventRegistrationCount::findOrFail($id);
    }
    public function getByEventUser($id)
    {
        return EventRegistrationCount::where('eventuser_id',$id)->get();
    }
    public function create($fields)
    {
        return EventRegistrationCount::create($fields);
    }
    public function delete($id)
    {
        return EventRegistrationCount::where('id', $id)->delete();
    }
}

