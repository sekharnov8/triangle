<?php

namespace App\Repositories\Video;


interface VideoRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

}