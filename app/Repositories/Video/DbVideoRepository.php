<?php

namespace App\Repositories\Video;

use App\Models\Video;
use App\Models\VideoCategory;


class DbVideoRepository implements VideoRepositoryInterface
{
    public function getAll()
    {
        return Video::with('category')->get();
    }

    public function find($id)
    {
        return Video::findOrFail($id);
    }

    public function create($fields)
    {
        return Video::create($fields);
    }

    public function delete($id)
    {
        return Video::where('id', $id)->delete();
    }
    public function getActive($limit=0)
    {
        if($limit)
            return Video::where('status',1)->take($limit)->get();
        else
            return Video::where('status',1)->get();
    }
    public function getHomeActive($limit=0)
    {
        if($limit)
            return Video::where('status',1)->where('is_home',1)->take($limit)->get();
        else
            return Video::where('status',1)->where('is_home',1)->get();
    }
  
}