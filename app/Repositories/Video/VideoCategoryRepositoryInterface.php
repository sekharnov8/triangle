<?php

namespace App\Repositories\Video;


interface VideoCategoryRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

    public function getList();

}