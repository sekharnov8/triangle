<?php

namespace App\Repositories\Video;

use App\Models\VideoCategory;


class DbVideoCategoryRepository implements VideoCategoryRepositoryInterface
{
    public function getAll()
    {
        return VideoCategory::all();
    }

    public function find($id)
    {
        return VideoCategory::findOrFail($id);
    }

    public function create($fields)
    {
        return VideoCategory::create($fields);
    }

    public function getList()
    {
        return VideoCategory::pluck('name','id');
    }
    public function delete($id)
    {
        return VideoCategory::where('id', $id)->delete();
    }
    public function getAllActive()
    {
        return VideoCategory::with('videos')->get();
    }
}