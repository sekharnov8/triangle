<?php

namespace App\Repositories\PaymentMethod;

use App\Models\PaymentMethod;


class DbPaymentMethodRepository implements PaymentMethodRepositoryInterface
{
    public function getAll()
    {
        return PaymentMethod::all();
    }
    public function find($id)
    {
        return PaymentMethod::findOrFail($id);
    }

    public function create($fields)
    {
        return PaymentMethod::create($fields);
    }
    public function delete($id)
    {
        return PaymentMethod::where('id', $id)->delete();
    }
    public function getAllActive()
    {
        return PaymentMethod::where('status',1)->get();
    }
}