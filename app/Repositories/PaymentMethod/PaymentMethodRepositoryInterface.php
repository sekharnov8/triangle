<?php

namespace App\Repositories\PaymentMethod;


interface PaymentMethodRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

    public function delete($id);

    public function getAllActive();

}