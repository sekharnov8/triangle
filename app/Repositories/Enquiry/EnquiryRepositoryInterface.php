<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 27-02-2018
 * Time: 15:35
 */
namespace App\Repositories\Enquiry;

interface EnquiryRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function delete($id);
}