<?php

namespace App\Repositories\Enquiry;

use App\Models\Enquiry;

class DbEnquiryRepository implements EnquiryRepositoryInterface
{

    public function getAll()
    {
        return Enquiry::all();
    }

    public function find($id)
    {
        return Enquiry::findOrFail($id);
    }

    public function create($fields)
    {
        return Enquiry::create($fields);
    }
    public function delete($id)
    {
        return Enquiry::where('id', $id)->delete();
    }
}