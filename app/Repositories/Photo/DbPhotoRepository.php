<?php

namespace App\Repositories\Photo;

use App\Models\Photo;
use App\Models\PhotoCategory;


class DbPhotoRepository implements PhotoRepositoryInterface
{
    public function getAll()
    {
        return Photo::all();
    }

    public function find($id)
    {
        return Photo::findOrFail($id);
    }

    public function create($fields)
    {
        return Photo::create($fields);
    }
    public function delete($id)
    {
        return Photo::where('id', $id)->delete();
    }
    public function getActive($limit=0)
    {
        if($limit)
            return Photo::where('status',1)->where('is_home',1)->take($limit)->get();
        else
            return Photo::where('status',1)->where('is_home',1)->get();
    }
    
}