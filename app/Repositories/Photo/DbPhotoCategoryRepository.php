<?php

namespace App\Repositories\Photo;

use App\Models\PhotoCategory;


class DbPhotoCategoryRepository implements PhotoCategoryRepositoryInterface
{
    public function getAll()
    {
        return PhotoCategory::all();
    }

    public function find($id)
    {
        return PhotoCategory::findOrFail($id);
    }

    public function create($fields)
    {
        return PhotoCategory::create($fields);
    }
    public function getList()
    {
        return PhotoCategory::pluck('name','id');
    } 
    public function findBySlug($value)
    {
        return PhotoCategory::with('photos')->where('slug', $value)->first();
    }
     public function getByParentId($id)
    {
        return PhotoCategory::where('parent_id',$id)->get();
    }
    public function delete($id)
    {
        return PhotoCategory::where('id', $id)->delete();
    }
    public function getAllActive()
    {
        return PhotoCategory::with('photos')->get();
    }
}