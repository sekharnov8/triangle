<?php

namespace App\Repositories\Photo;


interface PhotoRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

}