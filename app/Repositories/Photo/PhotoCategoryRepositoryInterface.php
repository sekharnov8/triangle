<?php

namespace App\Repositories\Photo;


interface PhotoCategoryRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

    public function getList();

}