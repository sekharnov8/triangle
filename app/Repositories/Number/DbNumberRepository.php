<?php

namespace App\Repositories\Number;

use App\Models\Number;

class DbNumberRepository implements NumberRepositoryInterface
{
    public function getAll()
    {
        return Number::all();
    }

    public function find($id)
    {
        return Number::findOrFail($id);
    }

    public function create($fields)
    {
        $yesterday = date("Y-m-d", strtotime( '-1 hour') );
        Number::whereDate('created_at', '<=', $yesterday)->delete();
        Number::wherePhone($fields['email'])->where('status',1)->delete();
        $number=Number::whereEmail($fields['email'])->first();
        // print_r($number);
        if($number)
        {
            if($number->verifycount <3)
            {
            $fields['verifycount']=$number->verifycount+1;
            $number->fill($fields)->save();
            return Number::whereEmail($fields['email'])->first();
            }
            else
            return $number;
        }
        else
        {
            return Number::create($fields);
        }
    }
    public function getByEmail($phone)
    {
        return Number::whereEmail($email)->first();
    }
    public function updateStatus($email,$data)
    {
        return Number::whereEmail($email)->update($data);
    }
    public function updateVerifycount($email)
    {
        return Number::whereEmail($email)->increment('verifycount');
    }

}
