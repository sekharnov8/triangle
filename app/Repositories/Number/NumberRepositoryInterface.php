<?php
namespace App\Repositories\Number;

interface NumberRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

}