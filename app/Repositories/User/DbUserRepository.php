<?php

namespace App\Repositories\User;

// use Cartalyst\Sentinel\Users\EloquentUser as User;
use App\Models\User;
use \DB;
use \Sentinel;

class DbUserRepository implements UserRepositoryInterface {

	public function getAll()
	{
		return User::all();
	}

    public function getList()
    {
        // return Member::pluck('id')->toArray();
        return User::selectRaw("CONCAT(first_name, ' ', last_name) as fullName, id")->pluck('fullName','id')->toArray();
    }
    public function getCount()
    {
        return User::count();
    }
	public function find($id)
	{
        $user = User::where('id', $id)->first();
        if($user)
            return $user;
        else
            return false;
	}

	public function updateRole($user_id, $role_id)
    {
    	DB::table('role_users')
            ->where('user_id', $user_id)
            ->update(['role_id' => $role_id]);
    }

    public function create($fields)
    {
    	return Sentinel::create($fields);
    }

    public function delete($id)
    {
        return User::where('id', $id)->delete();
    }

}