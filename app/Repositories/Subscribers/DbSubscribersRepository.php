<?php

namespace App\Repositories\Subscribers;

use App\Models\Subscribers;


class DbSubscribersRepository implements SubscribersRepositoryInterface
{
    public function getAll()
    {
        return Subscribers::get();
    }

    public function find($id)
    {
        return Subscribers::findOrFail($id);
    }

    public function create($fields)
    {
        return Subscribers::create($fields);
    }
    public function findByEmail($email)
    {
        return Subscribers::whereEmail($email)->first();
    }
    public function delete($id)
    {
        return Subscribers::where('id', $id)->delete();
    }

}