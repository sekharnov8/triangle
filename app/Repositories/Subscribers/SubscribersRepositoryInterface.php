<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 11/24/2018
 * Time: 1:10 PM
 */
namespace App\Repositories\Subscribers;

interface SubscribersRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function findByEmail($email);

    public function delete($id);
}