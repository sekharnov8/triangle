<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 11/27/2018
 * Time: 10:13 AM
 */
namespace App\Repositories\MailTemplate;

interface MailTemplateRepositoryInterface
{
    public function getAll();

    public function getActive();

    public function find($id);

    public function create($fields);

    public function findByName($value);

    public function delete($id);
}