<?php

namespace App\Repositories\MailTemplate;

use App\Models\MailTemplate;
use Carbon\Carbon;

class DbMailTemplateRepository implements MailTemplateRepositoryInterface
{
    public function getAll()
    {
        return MailTemplate::get();
    }
    public function getActive()
    {
        return MailTemplate::where('status',1)->get();
    }
    public function find($id)
    {
        return MailTemplate::findOrFail($id);
    }
    public function create($fields)
    {
        return MailTemplate::create($fields);
    }
    public function findByName($value)
    {
        return MailTemplate::where('name', $value)->first();
    }
    public function delete($id)
    {
        return MailTemplate::where('id', $id)->delete();
    }

}