<?php

namespace App\Repositories\Banner;

interface BannerRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function getList();

    public function create($fields);

    public function delete($id);
}