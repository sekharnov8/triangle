<?php
namespace App\Repositories\Banner;
use App\Models\Banner;
use Carbon\Carbon;

class DbBannerRepository implements BannerRepositoryInterface
{

    public function getAll()
    {
        return Banner::all();
    }
    public function getActiveBanners()
    {
        $query=Banner::where('status',1);
        $query->where(function($query)
        {
        $query->whereNull('expire_date')->orWhere('expire_date','>=',Carbon::today()->toDateString());
        })->orderBy('order_no','asc');
        return $query->get();
    }
    
    public function find($id)
    {
        return Banner::findOrFail($id);
    }
    public function getList()
    {
        return Banner::pluck('name','id')->toArray();
    }
   
    public function getWhere($column, $value, $trash = null)
    {
        if($trash != null)
        {

            return Banner::where($column, $value)->whereNotNull('deleted_at')->get();

        } else {

            return Banner::where($column, $value)->whereNull('deleted_at')->get();
        }
    }
    public function create($fields)
    {
        return Banner::create($fields);
    }
    public function getMax()
    {
        $maxno= Banner::max('order_no');
       //  dd($maxno);
        return $maxno+1;
    }
    public function delete($id)
    {
        return Banner::where('id', $id)->delete();
    }

}