<?php

namespace App\Repositories\Sponsor;


interface SponsorCategoryRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

 
}