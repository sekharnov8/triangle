<?php

namespace App\Repositories\Sponsor;

use App\Models\SponsorCategory;


class DbSponsorCategoryRepository implements SponsorCategoryRepositoryInterface
{
    public function getAll()
    {
        return SponsorCategory::all();
    }

    public function find($id)
    {
        return SponsorCategory::findOrFail($id);
    }

    public function create($fields)
    {
        return SponsorCategory::create($fields);
    }
    public function getAllActive()
    {
        return SponsorCategory::with('sponsors')->get();
    }
     public function getByName($name)
    {
        return SponsorCategory::with('sponsors')->where('category_name',$name)->first();
    }
    public function getList()
    {
        return SponsorCategory::pluck('category_name','id');
    }
 
    public function delete($id)
    {
        return SponsorCategory::where('id', $id)->delete();
    }
}