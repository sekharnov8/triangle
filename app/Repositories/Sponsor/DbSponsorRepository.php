<?php

namespace App\Repositories\Sponsor;

use App\Models\Sponsor;
use App\Models\SponsorCategory;


class DbSponsorRepository implements SponsorRepositoryInterface
{
    public function getAll()
    {
        return Sponsor::all();
    }
    public function getforHomepage()
    {
        return Sponsor::where('status', 1)->where('category_id','<>',1)->take(10)->get();
    }
    
    public function find($id)
    {
        return Sponsor::findOrFail($id);
    }
    public function getMax()
    {
        $maxno= Sponsor::max('order_no');
        return $maxno+1;
    }
    public function create($fields)
    {
        return Sponsor::create($fields);
    }
    public function delete($id)
    {
        return Sponsor::where('id', $id)->delete();
    }
}