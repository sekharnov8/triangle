<?php

namespace App\Repositories\Sponsor;


interface SponsorRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

}