<?php

namespace App\Repositories\Page;


interface PageRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

    public function findBySlug($value);

}