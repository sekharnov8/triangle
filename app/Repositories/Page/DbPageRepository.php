<?php

namespace App\Repositories\Page;

use App\Models\Page;


class DbPageRepository implements PageRepositoryInterface
{
    public function getAll()
    {
        return Page::all();
    }

    public function find($id)
    {
        return Page::findOrFail($id);
    }

    public function create($fields)
    {
        return Page::create($fields);
    }
    public function getMax()
    {
        $maxno= Page::max('display_order');
        return $maxno+1;
    }
    public function getByCategoryId($id)
    {
        return Page::where('pagecategory_id', $id)->where('status',1)->get();
    }
    public function getAllByCategoryId($id)
    {
        return Page::where('pagecategory_id', $id)->get();
    }
    public function findBySlug($value)
    {
        return Page::where('slug', $value)->first();
    }

    public function delete($id)
    {
        return Page::where('id', $id)->delete();
    }
}