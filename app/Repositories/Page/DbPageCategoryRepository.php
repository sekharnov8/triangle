<?php


namespace App\Repositories\Page;

use App\Models\PageCategory;


class DbPageCategoryRepository implements PageCategoryRepositoryInterface
{
    public function getAll()
    {
        return PageCategory::all();
    }

    public function find($id)
    {
        return PageCategory::findOrFail($id);
    }
    public function getList()
    {
        return PageCategory::where('pagelevel','>',0)->pluck('name','id')->toArray();
    }
    public function getListByLevel($level)
    {
        return PageCategory::where('pagelevel',$level)->pluck('name','id')->toArray();
    }
    public function getListByParentId($id)
    {
        return PageCategory::where('page_parentid',$id)->pluck('name','id')->toArray();
    }
    public function getByParentId($id)
    {
        return PageCategory::where('page_parentid',$id)->get();
    }
    public function create($fields)
    {
        return PageCategory::create($fields);
    }
    public function getMax()
    {
        $maxno= PageCategory::max('position');
      //  dd($maxno);
        return $maxno+1;
    }
    public function findByName($category)
    {
        return PageCategory::whereName($category)->first();
    }
    public function delete($id)
    {
        return PageCategory::where('id', $id)->delete();
    }
}