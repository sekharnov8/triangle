<?php

namespace App\Repositories\Page;


interface PageCategoryRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

}