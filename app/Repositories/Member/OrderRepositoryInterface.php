<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/18/2018
 * Time: 12:44 PM
 */
namespace App\Repositories\Member;

interface OrderRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function findOrderByRefNo($ref_no);

    public function updateOrderDetails($ordId,$updateData);

    public static function getOrderDetailsByUserId($userId);

    public static function getAllSuccessOrderDetails();

    public function create($fields);

    public function delete($id);
}