<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 10/18/2018
 * Time: 3:48 PM
 */
namespace App\Repositories\Member;

interface SignificantRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function getByUserId($id);

    public function create($fields);

    public function delete($id);
}