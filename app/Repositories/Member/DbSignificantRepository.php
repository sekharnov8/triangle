<?php

namespace App\Repositories\Member;

use App\Models\Significant;


class DbSignificantRepository implements SignificantRepositoryInterface
{
    public function getAll()
    {
        return Significant::all();
    }
    public function getByUserId($id)
    {
        return Significant::where('user_id',$id)->get();

    }
    public function find($id)
    {
        return Significant::findOrFail($id);
    }
    public function create($fields)
    {
        return Significant::create($fields);
    }
    public function delete($id)
    {
        return Significant::where('id', $id)->delete();
    }
}