<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 15-10-2018
 * Time: 02:36
 */
namespace App\Repositories\Member;

interface MembershiptypeRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function getList();

    public function create($fields);

    public function delete($id);
}