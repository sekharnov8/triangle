<?php


namespace App\Repositories\Member;

use App\Models\Order;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Carbon\Carbon;
use Log;
use App\Mail\SubscriptionExpiredSoon;
use Mail;

class DbOrderRepository implements OrderRepositoryInterface
{
    public function getAll()
    {
        return Order::all();
    }

    public function find($id)
    {
        return Order::findOrFail($id);
    }
    public function findOrderByRefNo($ref_no)
    {
        return Order::with('user')->whereReferenceNo($ref_no)->first();
    }
    public function updateOrderDetails($ordId,$updateData)
    {
        Order::whereId($ordId)->update($updateData);
    }
    public static function getOrderDetailsByUserId($userId)
    {
        return Order::with('user')->where('user_id', $user_id)->paymentSuccess()->notExpired()->get();
    }
    public static function geByUserId($userId)
    {
        return Order::with('user')->where('user_id',$userId)->get();
    }
    public static function getAllSuccessOrderDetails()
    {
        return Order::with('user')->paymentSuccess()->notExpired()->get();
    }
    public function create($fields)
    {
        return Order::create($fields);
    }
    public function delete($id)
    {
        return Order::where('id', $id)->delete();
    }
}