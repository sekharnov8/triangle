<?php

namespace App\Repositories\Member;

use App\Models\Membershiptype;
use DB;

class DbMembershiptypeRepository implements MembershiptypeRepositoryInterface
{
    public function getAll()
    {
        return Membershiptype::all();
    }
    public function getListWithPrice()
    {
        return Membershiptype::select('id', DB::raw("CONCAT(name, ' ($', price, ')') as name"))->pluck('name','id')->toArray();
    }
    public function getTypes($typeid)
    {
        return Membershiptype::where('status',1)->where('price','>',1)->where('id','<>',$typeid)->get();
    }
    public function getAllActive()
    {
        return Membershiptype::where('status',1)->get();
    }
    public function getByName($name)
    {
        return Membershiptype::where('name',$name)->first();
    }
    public function getMax()
    {
        $maxno= Membershiptype::max('order_no');
        return $maxno+1;
    }
    public function find($id)
    {
        return Membershiptype::findOrFail($id);
    }
    public function getList()
    {
        return Membershiptype::pluck('name','id')->toArray();
    }
    public function create($fields)
    {
        return Membershiptype::create($fields);
    }
    public function delete($id)
    {
        return Membershiptype::where('id', $id)->delete();
    }
}