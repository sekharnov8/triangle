<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 12/20/2018
 * Time: 10:21 AM
 */
namespace App\Repositories\Expenses;

interface ExpensesRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function delete($id);
}