<?php

namespace App\Repositories\Expenses;

use App\Models\Expenses;


class DbExpensesRepository implements ExpensesRepositoryInterface
{
    public function getAll()
    {
        return Expenses::get();
    } 
    public function find($id)
    {
        return Expenses::findOrFail($id);
    }

    public function create($fields)
    {
        return Expenses::create($fields);
    } 
    public function delete($id)
    {
        return Expenses::where('id', $id)->delete();
    }
    public function getByUserId($user_id)
    {
      return Expenses::where('user_id',$user_id)->get();
    }

}