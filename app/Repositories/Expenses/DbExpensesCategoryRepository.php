<?php

namespace App\Repositories\Expenses;

use App\Models\ExpensesCategory;


class DbExpensesCategoryRepository implements ExpensesCategoryRepositoryInterface
{
    public function getAll()
    {
        return ExpensesCategory::all();
    }

    public function find($id)
    {
        return ExpensesCategory::findOrFail($id);
    }

    public function create($fields)
    {
        return ExpensesCategory::create($fields);
    }
    
    public function delete($id)
    {
        return ExpensesCategory::where('id', $id)->delete();
    }
     public function getList()
    {
        return ExpensesCategory::pluck('name','id');
    }
}