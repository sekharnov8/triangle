<?php

namespace App\Repositories\Expenses;


interface ExpensesCategoryRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

}