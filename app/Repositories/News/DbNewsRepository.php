<?php
namespace App\Repositories\News;
use App\Models\News;

class DbNewsRepository implements NewsRepositoryInterface
{

    public function getAll()
    {
        return News::all();
    }
    public function getActive()
    {
        return News::where('status',1)->orderBy('created_at','desc')->get();
    }
    public function find($id)
    {
        return News::findOrFail($id);
    }
    public function getMax()
    {
        $maxno= News::max('display_order');
        return $maxno+1;
    }
    public function getList()
    {
        return News::pluck('name','id')->toArray();
    }
    public function create($fields)
    {
        return News::create($fields);
    }
    public function delete($id)
    {
        return News::where('id', $id)->delete();
    }
    public function findBySlug($value)
    {
        return News::where('slug', $value)->first();
    }

}