<?php

namespace App\Repositories\News;

interface NewsRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function getList();

    public function create($fields);

    public function delete($id);
}