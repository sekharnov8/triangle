<?php

namespace App\Repositories\Notification;

interface NotificationStatusRepositoryInterface
{
    public function getAll();

    public function getByNotificationId($notification_id);

    public function getActive();

    public function find($id);

    public function create($fields);

    public function delete($id);
}