<?php

namespace App\Repositories\Notification;

use App\Models\NotificationStatus;
use Carbon\Carbon;

class DbNotificationStatusRepository implements NotificationStatusRepositoryInterface
{
    public function getAll()
    {
        return NotificationStatus::all();
    }
    public function getByNotificationId($notification_id)
    {
        return NotificationStatus::where('notification_id', $notification_id)->get();
    }
    public function getActive()
    {
        return NotificationStatus::where('status',1)->get();
    }
    public function find($id)
    {
        return NotificationStatus::findOrFail($id);
    } 
    public function create($fields)
    {
        return NotificationStatus::create($fields);
    } 
    public function delete($id)
    {
        return NotificationStatus::where('id', $id)->delete();
    } 
}