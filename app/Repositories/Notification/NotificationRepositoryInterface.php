<?php

namespace App\Repositories\Notification;

interface NotificationRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function delete($id);
}