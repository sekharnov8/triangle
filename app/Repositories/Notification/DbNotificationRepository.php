<?php

namespace App\Repositories\Notification;

use App\Models\Notification;
use Carbon\Carbon;

class DbNotificationRepository implements NotificationRepositoryInterface
{
    public function getAll()
    {
        return Notification::all();
    }

    public function find($id)
    {
        return Notification::findOrFail($id);
    }
   
    public function create($fields)
    {
        return Notification::create($fields);
    }
   
    public function delete($id)
    {
        return Notification::where('id', $id)->delete();
    }

}