<?php

namespace App\Repositories\Status;

use App\Models\StatusType;
use App\Models\StatusCategory;


class DbStatusTypeRepository implements StatusTypeRepositoryInterface
{
    public function getAll()
    {
        return StatusType::with('category')->get();
    }

    public function find($id)
    {
        return StatusType::findOrFail($id);
    }

    public function create($fields)
    {
        return StatusType::create($fields);
    }
    public function delete($id)
    {
        return StatusType::where('id', $id)->delete();
    }
    public function getByStatusCategoryId($id)
    {
        return StatusType::where('status_category_id',$id)->orderBy('order_no')->pluck('name','order_no')->toArray();
    }
    public function getByTypeAndCategory($id, $status)
    {
        return StatusType::where('status_category_id',$id)->where('order_no',$status)->pluck('name')->first();
    }
}