<?php

namespace App\Repositories\Status;


interface StatusTypeRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

}