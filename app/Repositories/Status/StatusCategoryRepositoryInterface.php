<?php

namespace App\Repositories\Status;


interface StatusCategoryRepositoryInterface
{

    public function getAll();

    public function find($id);

    public function create($fields);

    public function getList();

}