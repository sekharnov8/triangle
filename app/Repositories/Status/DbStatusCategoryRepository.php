<?php

namespace App\Repositories\Status;

use App\Models\StatusCategory;


class DbStatusCategoryRepository implements StatusCategoryRepositoryInterface
{
    public function getAll()
    {
        return StatusCategory::all();
    }

    public function find($id)
    {
        return StatusCategory::findOrFail($id);
    }

    public function create($fields)
    {
        return StatusCategory::create($fields);
    }

    public function getList()
    {
        return StatusCategory::pluck('category','id')->toArray();
    }
    public function delete($id)
    {
        return StatusCategory::where('id', $id)->delete();
    }
}