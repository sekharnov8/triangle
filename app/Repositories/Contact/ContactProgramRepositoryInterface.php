<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 1/4/2019
 * Time: 5:35 PM
 */
namespace App\Repositories\Contact;

interface ContactProgramRepositoryInterface
{
    public function getAll();

    public function getAllActive();

    public function getMax();

    public function find($id);

    public function getList();

    public function create($fields);

    public function delete($id);
}