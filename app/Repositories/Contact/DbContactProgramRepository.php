<?php

namespace App\Repositories\Contact;

use App\Models\ContactProgram;


class DbContactProgramRepository implements ContactProgramRepositoryInterface
{
    public function getAll()
    {
        return ContactProgram::all();
    }
    public function getAllActive()
    {
        return ContactProgram::where('status',1)->get();
    }
    public function getMax()
    {
        $maxno= ContactProgram::max('display_order');
        return $maxno+1;
    }
    public function find($id)
    {
        return ContactProgram::findOrFail($id);
    }
    public function getList()
    {
        return ContactProgram::where('status',1)->pluck('name','id')->toArray();
    }
    public function create($fields)
    {
        return ContactProgram::create($fields);
    }
    public function delete($id)
    {
        return ContactProgram::where('id', $id)->delete();
    }
}