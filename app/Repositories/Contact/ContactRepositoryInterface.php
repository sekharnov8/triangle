<?php
/**
 * Created by PhpStorm.
 * User: arjun web
 * Date: 12/20/2018
 * Time: 10:21 AM
 */
namespace App\Repositories\Contact;

interface ContactRepositoryInterface
{
    public function getAll();

    public function find($id);

    public function create($fields);

    public function findByEmail($email);

    public function delete($id);
}