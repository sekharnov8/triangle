<?php

namespace App\Repositories\Contact;

use App\Models\Contact;


class DbContactRepository implements ContactRepositoryInterface
{
    public function getAll()
    {
        return Contact::with('programname')->get();
    }

    public function find($id)
    {
        return Contact::findOrFail($id);
    }

    public function create($fields)
    {
        return Contact::create($fields);
    }
    public function findByEmail($email)
    {
        return Contact::whereEmail($email)->first();
    }
    public function delete($id)
    {
        return Contact::where('id', $id)->delete();
    }

}