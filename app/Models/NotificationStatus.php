<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationStatus extends Model {

    protected $table = 'notification_status';
    public $timestamps = true;

    protected $fillable = ['notification_id','user_id','status','related_id'];

}