<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageCategory extends Model {

	protected $table = 'pagecategories';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['name','pagelevel','page_parentid','id_path','position','is_topbar','is_menubar','is_quicklinks','is_footerlinks','status','updated_by'];

    
    public function page()
    {
        return $this->belongsTo('App\Models\Page','id_path');
    }
    public function pages()
    {
        return $this->hasMany('App\Models\Page','pagecategory_id','id')->orderBy('display_order');
    }
    public function subitems()
    {
        return $this->hasMany('App\Models\PageCategory','page_parentid','id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\PageCategory','page_parentid');
    }

}