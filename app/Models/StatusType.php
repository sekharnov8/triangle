<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusType extends Model {

	protected $table = 'status_types';
	public $timestamps = true;

    protected $fillable = ['name','description','status_category_id','order_no'];

    public function category()
    {
        return $this->belongsTo('App\Models\StatusCategory','status_category_id');
    }
}