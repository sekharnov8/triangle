<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model {

	protected $table = 'videos';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['videocategory_id','video_url','name','slug','description', 'display_order','status','is_home','updated_by'];

    public function category()
    {
        return $this->belongsTo('App\Models\VideoCategory', 'videocategory_id');
    }
}