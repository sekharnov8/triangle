<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Laravel\Sanctum\HasApiTokens;

class User extends CartalystUser
{
    use Notifiable;
    use HasApiTokens;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','first_name','last_name','middle_name', 'email', 'mobile','password','last_login','profile_image','designation','is_approved','is_lockedout','is_activated','date_activated','last_password_change','failed_pwd_count','home_phone','fax','occupation','age','dob','gender','employer','skills','address','address2','city','state','country','zipcode','primary_member_id','referred_by','membershiptype_id','terms_conditions','member_since','order_id','memberage'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
 
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function member()
    {
        return $this->belongsTo('App\Models\Member','id','user_id');
    }
    public function status()
    {
       return $this->belongsTo('App\Models\StatusType','id','status_id');
    }
    public function membership()
    {
        return $this->belongsTo('App\Models\Membershiptype','membershiptype_id');
    }
     public function orders()
    {
        return $this->belongsTo('App\Models\Order','user_id');
    }
    public function significants()
    {
        return $this->hasMany('App\Models\Significant','user_id');
    }
}
