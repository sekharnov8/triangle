<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enquiry extends Model {

    protected $table = 'enquiries';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable = ['name','email','city','state','country','zipcode','address','message','mobile','remarks','program'];

    protected $dates = ['deleted_at'];
}