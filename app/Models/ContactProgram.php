<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactProgram extends Model {

    protected $table = 'contactprograms';
    public $timestamps = true;

    protected $fillable = ['name','status','email','display_order'];

}