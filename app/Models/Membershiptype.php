<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membershiptype extends Model {

    protected $table = 'membershiptypes';
    public $timestamps = true;

    protected $fillable = ['name','price', 'validity','status','order_no','updated_by','expiredate'];

 
}