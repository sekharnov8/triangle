<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TamPatrika extends Model {

	protected $table = 'tam_patrika';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['patrika_type','patrika_date','image','file_url','web_url','status','slug'];

}