<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicetype extends Model {

    protected $table = 'servicetypes';
    public $timestamps = true;

    protected $fillable = ['name','description','status','order_no','updated_by'];

}