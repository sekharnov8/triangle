<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model {

    protected $table = 'events';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['slug','category_id','user_id','name','start_date','end_date','registration_start_date','registration_end_date','banner_url','location','city','state','zipcode','event_details','contact_mail','status','page_title','mata_description','mata_keyword','updated_by','is_paid','is_registration','is_homedisplay','topline','start_time','end_time','event_for','mail_description','committee_id','total_registration_count','amount_desc','location_type'];

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\EventCategory', 'category_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function regtypes()
    {
        return $this->hasMany('App\Models\EventRegistrationType','event_id');
    }
     public function sponsortypes()
    {
        return $this->hasMany('App\Models\EventSponsorship','event_id');
    }
     public function registrations()
    {
        return $this->hasMany('App\Models\EventUser','event_id');
    }

}