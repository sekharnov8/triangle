<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhotoCategory extends Model {

	protected $table = 'photocategories';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['name','updated_by','image','slug','parent_id','order_no','status','description'];

     public function photos()
    {
        return $this->hasMany('App\Models\Photo','photocategory_id','id')->orderBy('display_order');
    }
     public function subitems()
    {
        return $this->hasMany('App\Models\PhotoCategory','parent_id','id')->where('status',1);
    }
    public function parent()
    {
        return $this->belongsTo('App\Models\PhotoCategory','parent_id');
    }
}