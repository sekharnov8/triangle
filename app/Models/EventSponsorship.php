<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSponsorship extends Model {

    protected $table = 'eventsponsorships';
    public $timestamps = true;

    protected $fillable = ['name','status','amount','event_id','order_no'];

    public function events()
    {
        return $this->hasMany('App\Models\Event','sponsorship_id','id');
    }
}