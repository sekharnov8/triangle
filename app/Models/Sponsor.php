<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model {

	protected $table = 'sponsors';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['category_id','name','logo_url','redirect_url','site_url','target','status','order_no','updated_by'];

    public function category()
    {
        return $this->belongsTo('App\Models\SponsorCategory', 'category_id');
    }
}