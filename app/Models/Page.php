<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model {

	protected $table = 'pages';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['pagecategory_id','name','slug','description', 'display_order','status','is_login','meta_title','meta_description','url','meta_keywords','topline','updated_by','target'];

    public function category()
    {
        return $this->belongsTo('App\Models\PageCategory', 'pagecategory_id');
    }
}