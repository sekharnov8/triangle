<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model {

    protected $table = 'paymentmethods';
    public $timestamps = true;
    protected $fillable = ['name', 'status'];
}