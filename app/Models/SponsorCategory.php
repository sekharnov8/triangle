<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SponsorCategory extends Model {

	protected $table = 'sponsorcategories';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['category_name'];

   
    public function sponsors()
    {
        return $this->hasMany('App\Models\Sponsor','category_id','id');
    }
}