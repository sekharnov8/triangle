<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model {

    protected $table = 'roles';
    public $timestamps = true;

    protected $fillable = ['slug','name','permissions'];

}