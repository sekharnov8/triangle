<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appinfo extends Model {

    protected $table = 'appinfo';
    public $timestamps = true;

    protected $fillable = ['sitename','address','website','email','phone', 'customercare_number','facebook_url','twitter_url','youtube_url','support_email','enquiry_email','admin_email','top_bg_color','menu_bg_color','menu_bg_color2','menu_text_color','sendgrid_email','sendgrid_api_key', 'paypal_mode','paypal_sandbox_client_id', 'paypal_sandbox_secret','paypal_client_id', 'paypal_secret'];
}