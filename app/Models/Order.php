<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;


class Order extends Model {

    protected $table = 'orders';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id','membershiptype_id','paymentstatus_id', 'paymentmethod_id','amount','transaction_id','payment_by','admin_comments','bankname','cheque_no','cheque_date','user_comments','order_date','expired_at','updated_by','reference_no','response_message','status','donation_amount','handedto'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function membershiptype()
    {
        return $this->belongsTo('App\Models\Membershiptype');
    }
    public function paymentmethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod', 'paymentmethod_id');
    }
    public function scopeNotExpired($query)
    {
        return $query->where('expired_at', '>=', Carbon::now());
    }
}