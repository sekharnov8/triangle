<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = 'notifications';
    public $timestamps = true;

    protected $fillable = ['message','user_id','related','sendmail','subject','status'];

    public function sent_members()
    {
        return $this->hasMany('App\Models\NotificationStatus','notification_id','id');
    }
    public function sent_members_nostatus()
    {
        return $this->hasMany('App\Models\NotificationStatus','notification_id','id')->where('status',1);
    }
}