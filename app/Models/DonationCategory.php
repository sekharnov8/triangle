<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DonationCategory extends Model {

	protected $table = 'donationcategories';
	public $timestamps = true;

    protected $fillable = ['name','display_order','status'];

}