<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sentinel;
use App\Models\CommitteeMember;

class Committee extends Model {

    protected $table = 'committees';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','slug','type','status','order_no','updated_by','email'];

    
    public function members()
    {
        return $this->hasMany('App\Models\CommitteeMember','committee_id','id')->orderBy('display_order','asc');
    }
}