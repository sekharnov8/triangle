<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommitteeMember extends Model {

    protected $table = 'committeemembers';
    public $timestamps = true;



    protected $fillable = ['committee_id','user_id','role_id','status','display_order','designation'];


    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function committee()
    {
        return $this->belongsTo('App\Models\Committee', 'committee_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

}