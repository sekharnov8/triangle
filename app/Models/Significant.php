<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Significant extends Model {

    protected $table = 'significants';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id','first_name','last_name','phone','email','dob','age','relationship','occupation','skills'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}