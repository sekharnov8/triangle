<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventRegistrant extends Model {

    protected $table = 'eventregistrants';
    public $timestamps = true;

   // use SoftDeletes;

    protected $fillable = ['eventuser_id','user_id','first_name','last_name','email','phone','gender','age'];

}