<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donation extends Model {

    protected $table = 'donations';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id','first_name','last_name','email','mobile','city','state','zipcode','address','address2','country','status','paymentmethod_id','donation_cause','transaction_id','amount','orderdate','card_number','cheque_number','payment_by','handedto','cheque_date','bankname','bank_ifsc_code','updated_by','response_message','is_anonymous','category_id','eventuser_id','memberorder_id'];

      
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function paymentmethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod','paymentmethod_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\DonationCategory','category_id');
    }
     public function memberorder()
    {
        return $this->belongsTo('App\Models\Order','memberorder_id','id');
    } 
    public function eventuser()
    {
        return $this->belongsTo('App\Models\EventUser','eventuser_id','id');
    }
}