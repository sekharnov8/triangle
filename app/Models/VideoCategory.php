<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VideoCategory extends Model {

	protected $table = 'videocategories';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['name','year','updated_by'];

    
     public function videos()
    {
        return $this->hasMany('App\Models\Video','videocategory_id','id');
    }
}