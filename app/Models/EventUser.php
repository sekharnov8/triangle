<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventUser extends Model {

    protected $table = 'eventuser_info';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['event_id','user_id','first_name','last_name','email','gender','age','address1','address2','city','state','zipcode','mobile','payment_status','payment_date','paymentmethod_id','transaction_id','amount_paid','status','comments','inserted_by','inserted_time','updated_by','response_message','sponsorship_id','sponsorship_amount','donation_amount'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function registrants()
    {
        return $this->hasMany('App\Models\EventRegistrationCount','eventuser_id','id');
    }
    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'event_id');
    }
     public function paymentmethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod','paymentmethod_id');
    }

}