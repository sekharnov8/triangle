<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventCategory extends Model {

    protected $table = 'eventcategories';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','status','updated_by'];

    public function events()
    {
        return $this->hasMany('App\Models\Event','category_id','id');
    }
}