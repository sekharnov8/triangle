<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model {

    protected $table = 'expenses';
    public $timestamps = true;

    protected $fillable = ['amount','event_id','user_id','receipt','payment_status','category_id','spent_date','comments','paid_date','treasurer_id','paid_amount','treasurer_comments'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
     public function treasurer()
    {
        return $this->belongsTo('App\Models\User','treasurer_id');
    } 
    public function category()
    {
        return $this->belongsTo('App\Models\ExpensesCategory','category_id');
    }
    public function event()
    {
        return $this->belongsTo('App\Models\Event','event_id');
    }
     public function committee()
    {
        return $this->belongsTo('App\Models\Committee','committee_id');
    }
}