<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ContactProgram;
class Contact extends Model {

    protected $table = 'enquiries';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','email','city','state','country','zipcode','address','address2','message','mobile','remarks','program_id','status'];

    public function programname()
    {
        return ContactProgram::where('id', '=', $this->program_id)->pluck('name')->first();
     }
     public function programdata()
    {
        return $this->belongsTo('App\Models\ContactProgram','program_id','id');
    }

}