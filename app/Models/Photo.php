<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model {

	protected $table = 'photos';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = ['photocategory_id','image_url','name','description','default_image', 'display_order','status','is_home','album_link','updated_by'];

    public function category()
    {
        return $this->belongsTo('App\Models\PhotoCategory', 'photocategory_id');
    }
}