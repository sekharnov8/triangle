<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventRegistrationType extends Model {

    protected $table = 'eventregistration_types';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['event_id','name','amount','reg_count','order_no','status','updated_by'];

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\EventCategory', 'category_id');
    }
}