<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventRegistrationCount extends Model {

    protected $table = 'eventregistration_count';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['event_id','eventuser_id','eventregistrationtype_id','count','amount','family_adults','family_kids'];

     public function eventuser()
    {
        return $this->belongsTo('App\Models\EventUser');
    }
    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
    public function regtype()
    {
        return $this->belongsTo('App\Models\EventRegistrationType');
    }

}