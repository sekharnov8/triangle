<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusCategory extends Model {

	protected $table = 'status_categories';
	public $timestamps = true;

    protected $fillable = ['category'];

    public function statustypes()
    {
        return $this->hasMany('App\Models\StatusType','status_category_id','id');
    }
}