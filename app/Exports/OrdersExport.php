<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\User;
use App\Models\Order;
use DB;

class OrdersExport implements FromView
{
    use Exportable;
 
    public function __construct()
    {
    }
    
    public function forFilter(array $filter)
    {
        $this->filter = $filter;
        
        return $this;
    }

    public function view(): View
    { 

         $query=Order::leftJoin('membershiptypes', 'membershiptypes.id', '=', 'orders.membershiptype_id')
            ->leftJoin('users', 'users.id', '=', 'members.user_id')
            ->leftJoin('paymentmethods', 'paymentmethods.id', '=', 'orders.paymentmethod_id');
         $query=$query->whereNull('users.deleted_at');

        if(isset($this->filter['datefrom']) && $this->filter['datefrom']!='' && isset($this->filter['dateto']) && $this->filter['dateto']!='')
        {
            $query=$query->whereBetween('orders.created_at',array($this->filter['datefrom'].' 00:00:01', $this->filter['dateto'].' 23:59:59'));
        }
        if(isset($this->filter['status']) && $this->filter['status']!='')
        {
            $query=$query->where('orders.status',$this->filter['status']);
        }
        if(isset($this->filter['paymentmethod']) && $this->filter['paymentmethod']!='')
        {
            $query=$query->where('orders.paymentmethod_id',$this->filter['paymentmethod']);
        }

        if(isset($this->filter['mtype']) && $this->filter['mtype']!='')
        {
            $query=$query->where('orders.membershiptype_id',$this->filter['mtype']);
        }
        if(isset($this->filter['keyword']) && $this->filter['keyword']!='')
        {
            $keyword=$this->filter['keyword'];
            $cols=['user_id','first_name','last_name'];
            $cols2=['transaction_id','amount'];
              $query->where(function ($query) use ($keyword, $cols, $cols2){
            foreach($cols as $column) {
                        $query=$query->orWhere('users.'.$column,'like','%'.$this->filter['keyword'].'%');
            }
            foreach($cols2 as $column) {
                        $query=$query->orWhere('orders.'.$column,'like','%'.$this->filter['keyword'].'%');
            }
            });
        }
        $query=$query->select('orders.*', 
             DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'users.email',
            'membershiptypes.name as membership','paymentmethods.name as paymentmethodname'
        );
         $membership_orders =$query->get();

     ob_end_clean(); // this
        ob_start(); 
         return view('protected.admin.memberorders.export', [
            'membership_orders' => $membership_orders
        ]);
    }
}

