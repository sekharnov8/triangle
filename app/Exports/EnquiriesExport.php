<?php

namespace App\Exports;

use App\Repositories\Contact\ContactRepositoryInterface;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Contact;

class EnquiriesExport implements FromView
{
    use Exportable;
    protected $contact;

    public function __construct(ContactRepositoryInterface $contact)
    {
        $this->contact = $contact;
    }
    public function forFilter(array $filter)
    {
        $this->filter = $filter;
        
        return $this;
    }


    public function view(): View
    {

        $query=Contact::leftJoin('contactprograms', 'contactprograms.id', '=', 'enquiries.program_id');
        if(isset($this->filter['datefrom']) && $this->filter['datefrom']!='' && isset($this->filter['dateto']) && $this->filter['dateto']!='')
        {
            $query->whereBetween('enquiries.created_at',array($this->filter['datefrom'].' 00:00:01', $this->filter['dateto'].' 23:59:59'));
        } 
         if(isset($this->filter['program_id']) && $this->filter['program_id']!='')        
        {
            $query=$query->where('enquiries.program_id',$this->filter['program_id']);
        }
         if(isset($this->filter['keyword']) && $this->filter['keyword']!='')
        {
            $keyword=$this->filter['keyword'];
            $cols=['name','city','state','message','email','mobile'];
                 $query->where(function ($query) use ($keyword, $cols){
            foreach($cols as $column) {
                        $query=$query->orWhere('enquiries.'.$column,'like','%'.$this->filter['keyword'].'%');
            }
            });
        }
        $contacts=$query->get();
        ob_end_clean(); // this
        ob_start(); 
         return view('protected.admin.contacts.export', [
            'contacts' => $contacts
        ]);
    }
}