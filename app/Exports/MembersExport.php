<?php

namespace App\Exports;

use App\Repositories\User\UserRepositoryInterface;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\User;

class MembersExport implements FromView
{
    use Exportable;
    protected $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }
    
    public function forFilter(array $filter)
    {
        $this->filter = $filter;
        
        return $this;
    }

    public function view(): View
    {

         $query=User::leftJoin('members', 'members.user_id', '=', 'users.id')->leftJoin('membershiptypes', 'membershiptypes.id', '=', 'members.membershiptype_id')->leftJoin('role_users','role_users.user_id','=','users.id');

        if(isset($this->filter['datefrom']) && $this->filter['datefrom']!='' && isset($this->filter['dateto']) && $this->filter['dateto']!='')
        {
            $query->whereBetween('users.created_at',array($filter['datefrom'].' 00:00:01', $filter['dateto'].' 23:59:59'));
        }
        if(isset($this->filter['mtype']) && $this->filter['mtype']!='')
        {
        $query->where('users.membershiptype_id',$this->filter['mtype']);
        }
         if(isset($this->filter['status']) && $this->filter['status']!='')
        {
            $query=$query->where('users.is_approved',$this->filter['status']);
        } 
         if(isset($this->filter['role']) && $this->filter['role']!='')
        {
        $query=$query->where('role_users.role_id',$this->filter['role']);
        }
          if(isset($this->filter['keyword']) && $this->filter['keyword']!='')
        {
            $keyword=$this->filter['keyword'];
            $cols=['member_id','first_name','last_name','email','mobile'];
                 $query->where(function ($query) use ($keyword, $cols){
            foreach($cols as $column) {
                        $query=$query->orWhere('users.'.$column,'like','%'.$this->filter['keyword'].'%');
            }
 
            });
        }

        $query=$query->whereNull('members.deleted_at');  

      ob_end_clean(); // this
      ob_start(); 

       if(isset($this->filter['info']) && $this->filter['info']=='full')
       {
        $users=$query->select('users.*', 'membershiptypes.name as membershiptype')->orderBy('users.id','desc')->get();
   
         return view('protected.admin.members.export_users', [
            'users' => $users
        ]);
        }
        else
        {
            $users=$query->select('users.member_id','users.id', 'users.email','users.mobile','users.first_name','users.last_name', 'users.created_at','users.last_login','membershiptypes.name as membershiptype','users.member_since','users.is_approved','users.created_at')->orderBy('users.id','desc')->get();
         return view('protected.admin.members.export', [
            'users' => $users
        ]);
        }
    }
}

