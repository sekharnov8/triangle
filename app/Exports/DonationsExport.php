<?php

namespace App\Exports;

use App\Repositories\Donation\DonationRepositoryInterface;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Donation;

class DonationsExport implements FromView
{
    use Exportable;
    protected $donation;

    public function __construct(DonationRepositoryInterface $donation)
    {
        $this->donation = $donation;
    }

    public function forFilter(array $filter)
    {
        $this->filter = $filter;
        
        return $this;
    }

    public function view(): View
    {
          $query = Donation::whereNull('deleted_at')->leftJoin('paymentmethods', 'paymentmethods.id', '=', 'donations.paymentmethod_id')->leftJoin('donation_categories', 'donation_categories.id', '=', 'donations.category_id');

        if(isset($this->filter['datefrom']) && $this->filter['datefrom']!='' && isset($this->filter['dateto']) && $this->filter['dateto']!='')
        {
            $query->whereBetween('created_at',array($this->filter['datefrom'].' 00:00:01', $this->filter['dateto'].' 23:59:59'));
        }
         if(isset($this->filter['status']) && $this->filter['status']!='')
        {
            $query=$query->where('donations.status',$this->filter['status']);
        } 
        $donationtype=$this->filter['donationtype'];

         if($donationtype=='event')  
            {  
                $query=$query->whereNotNull('donations.eventuser_id');
                if($type_id!='')
                $query=$query->where('donations.eventuser_id',$type_id);
            }
            if($donationtype=='donationpage')  
            {  
                $query=$query->whereNotNull('donations.donationpage_id');
                if($type_id!='')
                $query=$query->where('donations.donationpage_id',$type_id);
         }
            if($donationtype=='membership')  
            { 
                $query=$query->whereNotNull('donations.memberorder_id');
                if($type_id!='')
                $query=$query->where('donations.memberorder_id',$type_id);
            }

            
        $keyword=$this->filter['keyword'];
         if($keyword)
        {
                 $query->where(function ($query) use ($keyword){ 
                      $query= $query->where('paymentmethods.name', 'like', '%' . $keyword . '%')->orWhere('donations.first_name', 'like', '%' . $keyword . '%')->orWhere('donations.donation_cause', 'like', '%' . $keyword . '%')->orWhere('donations.last_name', 'like', '%' . $keyword . '%')->orWhere('donations.transaction_id',$keyword);
                    });
             }
             $query->select('donations.*',  'paymentmethods.name as paymentmethod_name', 'donation_categories.name as categoryname');

        $service_donations= $query->get();

         return view('protected.admin.donations.export_donation', [
          'service_donations' => $service_donations
        ]);
    }
}