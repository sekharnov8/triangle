<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Subscribers;

class SubscribersExport implements FromView
{
    use Exportable;

    public function __construct()
    {
    }


    public function view(): View
    {

        $subscribers=Subscribers::orderBy('created_at','desc')->get();
      
        ob_end_clean(); // this
        ob_start(); 
         return view('protected.admin.subscribers.export', [
            'subscribers' => $subscribers
        ]);
    }
}