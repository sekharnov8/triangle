<?php

namespace App\Exports;

use App\Repositories\Expenses\ExpensesRepositoryInterface;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExpensesExport implements FromView
{
    use Exportable;
    protected $expenses;

    public function __construct(ExpensesRepositoryInterface $expenses)
    {
        $this->expenses = $expenses;
    }

    public function view(): View
    {
    	$expenses=$this->expenses->getAll();
         return view('protected.admin.expenses.export', [
            'expenses' => $expenses
        ]);
    }
}