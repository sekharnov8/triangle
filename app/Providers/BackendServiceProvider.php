<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       
        $this->app->bind(
            'App\Repositories\Page\PageRepositoryInterface',
            'App\Repositories\Page\DbPageRepository'
        );

        $this->app->bind(
            'App\Repositories\Page\PageCategoryRepositoryInterface',
            'App\Repositories\Page\DbPageCategoryRepository'
        );

        $this->app->bind(
            'App\Repositories\Photo\PhotoRepositoryInterface',
            'App\Repositories\Photo\DbPhotoRepository'
        );

        $this->app->bind(
            'App\Repositories\Photo\PhotoCategoryRepositoryInterface',
            'App\Repositories\Photo\DbPhotoCategoryRepository'
        );

        $this->app->bind(
            'App\Repositories\Sponsor\SponsorRepositoryInterface',
            'App\Repositories\Sponsor\DbSponsorRepository'
        );
         $this->app->bind(
            'App\Repositories\Event\EventSponsorshipRepositoryInterface',
            'App\Repositories\Event\DbEventSponsorshipRepository'
        );   

        $this->app->bind(
            'App\Repositories\News\NewsRepositoryInterface',
            'App\Repositories\News\DbNewsRepository'
        );

        $this->app->bind(
            'App\Repositories\Sponsor\SponsorCategoryRepositoryInterface',
            'App\Repositories\Sponsor\DbSponsorCategoryRepository'
        );

        $this->app->bind(
            'App\Repositories\Video\VideoRepositoryInterface',
            'App\Repositories\Video\DbVideoRepository'
        );

        $this->app->bind(
            'App\Repositories\Video\VideoCategoryRepositoryInterface',
            'App\Repositories\Video\DbVideoCategoryRepository'
        );

        $this->app->bind(
            'App\Repositories\Enquiry\EnquiryRepositoryInterface',
            'App\Repositories\Enquiry\DbEnquiryRepository'
        );

        $this->app->bind(
            'App\Repositories\Banner\BannerRepositoryInterface',
            'App\Repositories\Banner\DbBannerRepository'
        );
        $this->app->bind(
            'App\Repositories\Member\MembershiptypeRepositoryInterface',
            'App\Repositories\Member\DbMembershiptypeRepository'
        ); 
        $this->app->bind(
            'App\Repositories\Member\SignificantRepositoryInterface',
            'App\Repositories\Member\DbSignificantRepository'
        );
        $this->app->bind(
            'App\Repositories\User\UserRepositoryInterface',
            'App\Repositories\User\DbUserRepository'
        ); 
        $this->app->bind(
            'App\Repositories\Member\OrderRepositoryInterface',
            'App\Repositories\Member\DbOrderRepository'
        );
        $this->app->bind(
            'App\Repositories\Event\EventRepositoryInterface',
            'App\Repositories\Event\DbEventRepository'
        );

        $this->app->bind(
            'App\Repositories\Event\EventCategoryRepositoryInterface',
            'App\Repositories\Event\DbEventCategoryRepository'
        );
       
        $this->app->bind(
            'App\Repositories\Event\EventRegistrationTypeRepositoryInterface',
            'App\Repositories\Event\DbEventRegistrationTypeRepository'
        );
        $this->app->bind(
            'App\Repositories\Event\EventUserRepositoryInterface',
            'App\Repositories\Event\DbEventUserRepository'
        );
        $this->app->bind(
            'App\Repositories\Event\EventRegistrantRepositoryInterface',
            'App\Repositories\Event\DbEventRegistrantRepository'
        );
  
        $this->app->bind(
            'App\Repositories\Article\ArticleRepositoryInterface',
            'App\Repositories\Article\DbArticleRepository'
        );
        $this->app->bind(
            'App\Repositories\Subscribers\SubscribersRepositoryInterface',
            'App\Repositories\Subscribers\DbSubscribersRepository'
        );
        $this->app->bind(
            'App\Repositories\Committee\CommitteeRepositoryInterface',
            'App\Repositories\Committee\DbCommitteeRepository'
        );
        $this->app->bind(
            'App\Repositories\Committee\CommitteeMemberRepositoryInterface',
            'App\Repositories\Committee\DbCommitteeMemberRepository'
        );
        $this->app->bind(
            'App\Repositories\MailTemplate\MailTemplateRepositoryInterface',
            'App\Repositories\MailTemplate\DbMailTemplateRepository'
        );
       
        $this->app->bind(
            'App\Repositories\Contact\ContactRepositoryInterface',
            'App\Repositories\Contact\DbContactRepository'
        ); 
         $this->app->bind(
            'App\Repositories\PaymentMethod\PaymentMethodRepositoryInterface',
            'App\Repositories\PaymentMethod\DbPaymentMethodRepository'
        );
         
        $this->app->bind(
            'App\Repositories\Contact\ContactProgramRepositoryInterface',
            'App\Repositories\Contact\DbContactProgramRepository'
        );
        $this->app->bind(
            'App\Repositories\Greeting\GreetingRepositoryInterface',
            'App\Repositories\Greeting\DbGreetingRepository'
        ); 
        $this->app->bind(
            'App\Repositories\Expenses\ExpensesRepositoryInterface',
            'App\Repositories\Expenses\DbExpensesRepository'
        );
         $this->app->bind(
            'App\Repositories\Expenses\ExpensesCategoryRepositoryInterface',
            'App\Repositories\Expenses\DbExpensesCategoryRepository'
        );
        $this->app->bind(
            'App\Repositories\Notification\NotificationRepositoryInterface',
            'App\Repositories\Notification\DbNotificationRepository'
        );
        $this->app->bind(
            'App\Repositories\Notification\NotificationStatusRepositoryInterface',
            'App\Repositories\Notification\DbNotificationStatusRepository'
        );
        $this->app->bind(
            'App\Repositories\Status\StatusTypeRepositoryInterface',
            'App\Repositories\Status\DbStatusTypeRepository'
        ); 
        $this->app->bind(
            'App\Repositories\Status\StatusCategoryRepositoryInterface',
            'App\Repositories\Status\DbStatusCategoryRepository'
        );   
         $this->app->bind(
            'App\Repositories\Donation\DonationRepositoryInterface',
            'App\Repositories\Donation\DbDonationRepository'
        );  
         $this->app->bind(
            'App\Repositories\Donation\DonationCategoryRepositoryInterface',
            'App\Repositories\Donation\DbDonationCategoryRepository'
        );    
    }
}
