<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Sentinel;
use App\Models\News;
use App\Models\PageCategory;
use View;
use App\Models\Menu;
use App\Models\Page;
use App\Models\Country;
use App\Models\State;
use App\Models\Committee;
use Session;
use App\Models\CommitteeMember;
use App\Models\NotificationStatus;
use App\Models\Notification;
use App\Models\Appinfo;
use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('*', function($view)
        {
             $loggedin_user = Sentinel::getUser();
             if($loggedin_user)
             {                 
                 $committeemember=CommitteeMember::where('user_id',$loggedin_user->id)->get();
                 if($committeemember)
                 {
                    if(isset($committeemember->committee))
                    {
                    $committee_member=$committeemember->committee->name;
                    $view->with('committee_member', $committee_member);
                    }
                 }

                //   $notification_ids=NotificationStatus::whereStatus(0)->where('user_id',$loggedin_user->id)->pluck('notification_id');
                //   $user_notifications=Notification::whereIn('id',$notification_ids)->take(10)->get();
                //   $view->with('notification_count', count($notification_ids));
                //   $view->with('user_notifications', $user_notifications);
             }

             // $requeststatus=['1'=>'Ready for Funding','0'=>'New Request','2'=>'Review Pending','3'=>'Closed'];
             $expensesstatus=['Pending'=>'Pending','Paid'=>'Paid'];
             
             $servicestatus=['1'=>'Active','0'=>'Pending','2'=>'Review Pending','3'=>'Closed'];

             $roles = Sentinel::getRoleRepository()->pluck('name','id')->toArray();
             $committee_list=Committee::where('status',1)->where('type','general')->orderBy('order_no')->get();
             $countries=Country::where('status',1)->pluck('country','country')->toArray();
             $states=State::pluck('state','state2')->toArray();
             $news_data=News::where('status',1)->select('name','slug','url')->get();

             $view->with('loggedin_user', $loggedin_user);
             $view->with('roles_info', $roles);
             $view->with('committee_list', $committee_list);
             $view->with('news_data', $news_data);
             $view->with('countries_data', $countries);
             $view->with('states_data', $states);
             $view->with('expensesstatus', $expensesstatus);
             $menulinks=PageCategory::where('status',1)->where('is_menubar',1)->where('page_parentid',0)->orderBy('position')->get();
             $topmenulinks=PageCategory::where('status',1)->where('is_topbar',1)->orderBy('position')->get();
             // dd($topmenulinks);
             $view->with('menulinks', $menulinks);
             $view->with('topmenulinks', $topmenulinks);

             $appinfo = Cache::remember('appinfo', 30, function()  {
                 return Appinfo::where('id',1)->first(); 
             }); 
             $view->with('app_info', $appinfo);
        });
    }
}
