<?php
/**
 * Created by PhpStorm.
 * User: debendrasamal
 * Date: 14/05/2018
 * Time: 5:03 PM
 */

return array(
    /** set your paypal credential **/

    'client_id' =>'AXjfbCWt8ArDoqpDUVU1zCZ90PnbuBOZqoPRxac3Niau0B-eNjy83f1agRQK_3YEkSfoDgUOJ8Cfu0oX',
    'secret' => 'EMhmnXei8d8v5_3PFn8dBa1YyZq6jDfKg0dTxO0X0jstcbcsFA9lucsNa34gzWzAVs_rHFhTcfiRsKRJ',

    /**
     * SDK configuration
     */

    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 2000,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'ERROR'
    ),
);